# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError

from exam.models import UserAssessment, UserUnit


class Command(BaseCommand):
    help = "'UserUnit' - un-submit #6706"

    def add_arguments(self, parser):
        parser.add_argument("user_unit_pk", type=int)

    def handle(self, *args, **options):
        self.stdout.write(f"{self.help}...")
        user_unit_pk = options["user_unit_pk"]
        user_unit = UserUnit.objects.get(pk=user_unit_pk)
        self.stdout.write(f"\nFound     'UserUnit' '{user_unit.pk}'")
        self.stdout.write(f"User      {user_unit.user_course.user.username}")
        self.stdout.write(f"Course    {user_unit.user_course.course.name}")
        self.stdout.write(f"Unit      {user_unit.unit.name}")
        self.stdout.write(
            f"Created   {user_unit.created.strftime('%d/%m/%Y %H:%M')}"
        )
        if user_unit.submitted:
            self.stdout.write(
                f"Submitted {user_unit.submitted.strftime('%d/%m/%Y %H:%M')}"
            )
        user_assessments = UserAssessment.objects.filter(user_unit=user_unit)
        for user_assessment in user_assessments:
            self.stdout.write(
                f"\nFound     'UserAssessment' '{user_assessment.pk}'"
            )
            self.stdout.write(f"Name      '{user_assessment.assessment.name}'")
            if user_assessment.answer_submitted:
                self.stdout.write(
                    f"Submitted {user_assessment.answer_submitted.strftime('%d/%m/%Y %H:%M')}"
                )
                self.stdout.write(
                    f"File      '{user_assessment.answer_original_file_name}'"
                )
            if user_assessment.retake_submitted:
                self.stdout.write(
                    f"Retake submit {user_assessment.retake_submitted.strftime('%d/%m/%Y %H:%M')}"
                )
                self.stdout.write(
                    f"File      '{user_assessment.retake_original_file_name}'"
                )

        if user_unit.submitted:
            user_unit.assessor = None
            user_unit.submitted = None
            user_unit.save()
            self.stdout.write(
                f"\n{self.help} - 'UserUnit' has been un-submitted... "
            )
        else:
            raise CommandError(
                "User unit has not been submitted.  Cannot un-submit!"
            )
