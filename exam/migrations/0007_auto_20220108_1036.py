# Generated by Django 3.2.8 on 2022-01-08 10:36

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("exam", "0006_auto_20210518_1336"),
    ]

    operations = [
        migrations.AddField(
            model_name="course",
            name="is_tailored",
            field=models.BooleanField(default=False),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name="unit",
            name="course_material",
            field=models.FileField(
                blank=True, upload_to="exam/unit/course/material"
            ),
        ),
        migrations.AddField(
            model_name="unit",
            name="description",
            field=models.TextField(blank=True),
        ),
    ]
