# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from exam.models import UserExamVersion
from exam.tests.scenario import set_correct_answers_all


class Command(BaseCommand):
    """Set the correct answer for each question.

    Example::

      django-admin.py demo_data_exam_answer 1

    """

    help = "Update an exam with demo questions and answers"

    def add_arguments(self, parser):
        parser.add_argument(
            "pk", nargs="+", type=int, help="pk of UserExamVersion"
        )

    def handle(self, *args, **options):
        pks = options["pk"]
        for pk in pks:
            user_exam_version = UserExamVersion.objects.get(pk=pk)
            set_correct_answers_all(user_exam_version)
            self.stdout.write("Complete")
