# -*- encoding: utf-8 -*-
import pytest

from exam.models import ProductExam
from exam.tests.factories import ExamFactory
from exam.tests.factories import ProductExamFactory
from stock.tests.factories import ProductFactory


@pytest.mark.django_db
def test_factory():
    ProductExamFactory()


@pytest.mark.django_db
def test_init_product_exam():
    exam = ExamFactory()
    product = ProductFactory()
    obj = ProductExam.objects.init_product_exam(product, exam, order=3)
    obj.refresh_from_db()
    assert 3 == obj.order
    assert product == obj.product
    assert exam == obj.exam
    assert obj.legacy is False


@pytest.mark.django_db
def test_init_product_exam_legacy():
    exam = ExamFactory()
    product = ProductFactory()
    obj = ProductExam.objects.init_product_exam(
        product, exam, order=3, legacy=True
    )
    obj.refresh_from_db()
    assert obj.legacy is True


@pytest.mark.django_db
def test_init_product_exam_legacy_init():
    """``init_product_exam`` for an existing row should not change 'legacy'."""
    obj = ProductExamFactory()
    assert obj.legacy is False
    obj = ProductExam.objects.init_product_exam(
        obj.product, obj.exam, order=3, legacy=True
    )
    obj.refresh_from_db()
    assert obj.legacy is False


@pytest.mark.django_db
def test_init_product_exam_order():
    exam = ExamFactory()
    product = ProductFactory()
    ProductExam.objects.init_product_exam(product, exam, order=3, legacy=True)
    obj = ProductExam.objects.init_product_exam(
        product, exam, order=5, legacy=True
    )
    obj.refresh_from_db()
    assert 5 == obj.order


@pytest.mark.django_db
def test_str():
    exam = ExamFactory(name="Apple")
    obj = ProductExamFactory(exam=exam)
    assert "Apple" == str(obj)


@pytest.mark.django_db
def test_is_valid():
    course = ProductFactory()
    exam = ExamFactory(name="Apple")
    ProductExamFactory(exam=exam, product=course)
    assert ProductExam.objects.is_valid(exam) is True


@pytest.mark.django_db
def test_is_valid_not():
    exam = ExamFactory(name="Apple")
    assert ProductExam.objects.is_valid(exam) is False


@pytest.mark.django_db
def test_products():
    """List of products for an exam - exclude legacy."""
    exam = ExamFactory()
    p1 = ProductFactory(slug="p1")
    p2 = ProductFactory(slug="p2")
    p3 = ProductFactory(slug="p3")
    ProductExam.objects.init_product_exam(p1, exam, order=1, legacy=False)
    ProductExam.objects.init_product_exam(p2, exam, order=2, legacy=True)
    ProductExam.objects.init_product_exam(p3, exam, order=3, legacy=False)
    ProductExam.objects.init_product_exam(
        p3, ExamFactory(), order=4, legacy=False
    )
    # test
    products = ProductExam.objects.products(exam)
    assert ["p1", "p3"] == [product.slug for product in products]
