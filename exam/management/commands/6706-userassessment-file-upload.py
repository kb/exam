# -*- encoding: utf-8 -*-
from django.core.files import File
from django.core.management.base import BaseCommand
from pathlib import Path

from exam.models import UserAssessment


class Command(BaseCommand):
    help = "'UserAssessment' - file upload #6706"

    def add_arguments(self, parser):
        parser.add_argument("user_assessment_pk", type=int)
        parser.add_argument("file_name", type=str)

    def handle(self, *args, **options):
        self.stdout.write(f"{self.help}...")
        file_name = options["file_name"]
        user_assessment_pk = options["user_assessment_pk"]
        user_assessment = UserAssessment.objects.get(pk=user_assessment_pk)
        self.stdout.write(f"\nFound     'UserAssessment': {user_assessment.pk}")
        self.stdout.write(f"Name      '{user_assessment.assessment.name}'")
        if user_assessment.answer_submitted:
            self.stdout.write(
                f"Submitted {user_assessment.answer_submitted.strftime('%d/%m/%Y %H:%M')}"
            )
            self.stdout.write(
                f"File      '{user_assessment.answer_original_file_name}'"
            )
        if user_assessment.retake_submitted:
            self.stdout.write(
                f"Retake submit {user_assessment.retake_submitted.strftime('%d/%m/%Y %H:%M')}"
            )
            self.stdout.write(
                f"File      '{user_assessment.retake_original_file_name}'"
            )
        self.stdout.write(f"Upload    '{Path(file_name).name}'")
        user_assessment.answer_original_file_name = Path(file_name).name
        user_assessment.answer_file.save(
            Path(file_name).name, File(open(file_name, "rb")), save=True
        )
        self.stdout.write(f"\n{self.help}, Complete...")
