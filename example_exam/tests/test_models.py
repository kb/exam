# -*- encoding: utf-8 -*-
import pytest

from exam.models import is_team_member, is_team_member_not_staff
from login.tests.factories import UserFactory
from .factories import ContactFactory


@pytest.mark.django_db
def test_is_team_member():
    user = UserFactory(is_staff=False)
    ContactFactory(user=user, team_member=True)
    assert is_team_member(user) is True


@pytest.mark.django_db
def test_is_team_member_not():
    user = UserFactory(is_staff=False)
    ContactFactory(user=user, team_member=False)
    assert is_team_member(user) is False


@pytest.mark.django_db
def test_is_team_member_not_contact():
    user = UserFactory(is_staff=False)
    assert is_team_member(user) is False


@pytest.mark.django_db
def test_is_team_member_staff():
    user = UserFactory(is_staff=True)
    assert is_team_member(user) is False


@pytest.mark.django_db
def test_is_team_member_not_staff():
    user = UserFactory(is_staff=False)
    ContactFactory(user=user, team_member=True)
    assert is_team_member_not_staff(user) is True


@pytest.mark.django_db
def test_is_team_member_not_staff_is_staaff():
    user = UserFactory(is_staff=True)
    assert is_team_member_not_staff(user) is False
