# -*- encoding: utf-8 -*-
import pytest

from exam.models import _tidy_name_form_file_name, check_perm_user_unit
from exam.tests.factories import (
    CourseFactory,
    CourseUnitFactory,
    UnitFactory,
    UserCourseFactory,
)
from login.tests.factories import UserFactory


@pytest.mark.django_db
def test_check_perm_user_unit():
    user = UserFactory()
    course = CourseFactory()
    unit = UnitFactory()
    CourseUnitFactory(course=course, unit=unit)
    UserCourseFactory(user=user, course=course)
    assert check_perm_user_unit(user, unit) is True


@pytest.mark.django_db
def test_check_perm_user_unit_is_staff():
    user = UserFactory(is_staff=True)
    unit = UnitFactory()
    assert check_perm_user_unit(user, unit) is True


@pytest.mark.django_db
def test_check_perm_user_unit_not():
    user = UserFactory()
    unit = UnitFactory()
    assert check_perm_user_unit(user, unit) is False


def test_tidy_name_form_file_name():
    assert "1a-Clients-Statement" == _tidy_name_form_file_name(
        "1a Client's Statement"
    )


def test_tidy_name_form_file_name_multi_space():
    assert "1a-Clients-Statement" == _tidy_name_form_file_name(
        "1a   Client's  Statement"
    )
