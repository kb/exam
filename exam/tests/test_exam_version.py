# -*- encoding: utf-8 -*-
import pytest

from django.utils import timezone

from exam.models import Exam, ExamVersion
from exam.tests.factories import (
    ExamVersionFactory,
    ExamFactory,
    UserExamVersionFactory,
)
from exam.tests.scenario import setup_exam
from login.tests.factories import UserFactory


@pytest.mark.django_db
def test_can_publish_already_published():
    """Test has a question with no answer."""
    exam_version = setup_exam()
    result, message = exam_version.can_publish()
    assert result is False
    assert "has already been published" in message


@pytest.mark.django_db
def test_factory():
    ExamVersionFactory()


@pytest.mark.django_db
def test_published():
    exam_version_1 = setup_exam()
    exam_version_2 = exam_version_1.exam.edit_version()
    exam_version_2.publish()
    assert exam_version_2.pk > exam_version_1.pk
    obj = ExamVersion.objects.published(exam_version_2.exam)
    assert exam_version_2 == obj


@pytest.mark.django_db
def test_question_count():
    exam = ExamFactory()
    version = exam.create_questions()
    assert 20 == version.question_count()


@pytest.mark.django_db
def test_str():
    exam = ExamFactory(name="Exam One")
    exam_version = ExamVersionFactory(pk=56, exam=exam)
    assert "56. Exam One".format(exam_version.pk) == str(exam_version)


@pytest.mark.django_db
def test_str_django():
    """This test is the same as the one above but without using the factory."""
    exam = Exam(name="Exam1")
    exam.save()
    exam_version = ExamVersion(exam=exam, date_published=timezone.now())
    exam_version.save()
    assert "{}. Exam1".format(exam_version.pk) == str(exam_version)


@pytest.mark.django_db
def test_user_exam_versions():
    exam_version = ExamVersionFactory()
    u = UserFactory()
    UserExamVersionFactory(user=u, exam_version=exam_version, result=1)
    UserExamVersionFactory(user=u)
    UserExamVersionFactory(
        user=u, exam_version=exam_version, legacy_result=True, result=2
    )
    UserExamVersionFactory(user=u, exam_version=exam_version, result=3)
    assert [3, 1] == [obj.result for obj in exam_version.user_exam_versions()]
