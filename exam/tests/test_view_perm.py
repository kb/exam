# -*- encoding: utf-8 -*-
import factory
import pytest

from django.core.files.uploadedfile import SimpleUploadedFile
from django.urls import reverse
from http import HTTPStatus

from exam.tests.factories import (
    AssessmentFactory,
    CourseFactory,
    CourseUnitFactory,
    ExamFactory,
    ExamSettingsFactory,
    ExamVersionFactory,
    QuestionFactory,
    UnitExamFactory,
    UnitFactory,
    UserAssessmentFactory,
    UserCourseFactory,
    UserExamVersionFactory,
    UserUnitFactory,
)
from exam.tests.scenario import setup_exam, set_correct_answers_all
from login.tests.factories import TEST_PASSWORD, UserFactory
from login.tests.fixture import perm_check
from stock.tests.factories import ProductFactory


@pytest.mark.django_db
def test_assessment_create(perm_check):
    unit = UnitFactory()
    url = reverse("exam.assessment.create", args=[unit.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_assessment_delete(perm_check):
    assessment = AssessmentFactory()
    url = reverse("exam.assessment.delete", args=[assessment.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_assessment_download_logged_in_enrolled(client):
    """Testing 'download_assessment'."""
    user = UserFactory()
    user_course = UserCourseFactory(user=user)
    unit = UnitFactory()
    UserUnitFactory(user_course=user_course, unit=unit)
    assessment = AssessmentFactory(unit=unit, name="Apple")
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(
        reverse(
            "exam.assessment.download.pdf",
            args=[assessment.pk],
        )
    )
    assert HTTPStatus.OK == response.status_code
    assert "Content-Disposition" in response.headers
    assert (
        'attachment; filename="Apple-Assessment.pdf"'
        == response.headers["Content-Disposition"]
    )


@pytest.mark.django_db
def test_assessment_download_logged_in_not_enrolled(client):
    """Testing 'download_assessment'."""
    assessment = AssessmentFactory(
        example_form=factory.django.FileField(filename="the_example_form.doc")
    )
    user = UserFactory()
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(
        reverse(
            "exam.assessment.download.pdf",
            args=[assessment.pk],
        )
    )
    assert HTTPStatus.FORBIDDEN == response.status_code


@pytest.mark.django_db
def test_assessment_download_logged_in_staff(client):
    """Testing 'download_assessment'."""
    user = UserFactory(is_staff=True)
    assessment = AssessmentFactory(
        example_form=factory.django.FileField(filename="the_example_form.doc"),
    )
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(
        reverse(
            "exam.assessment.download.pdf",
            args=[assessment.pk],
        )
    )
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_assessment_download_not_logged_in(client):
    """Testing 'download_assessment'."""
    assessment = AssessmentFactory()
    response = client.get(
        reverse(
            "exam.assessment.download.pdf",
            args=[assessment.pk],
        )
    )
    assert HTTPStatus.FORBIDDEN == response.status_code


@pytest.mark.django_db
def test_assessment_download(perm_check):
    """Testing 'download_assessment'."""
    assessment = AssessmentFactory()
    perm_check.staff(
        reverse(
            "exam.assessment.download.pdf",
            args=[assessment.pk],
        )
    )


@pytest.mark.django_db
def test_assessment_download_blank_form_logged_in_enrolled(client):
    """Testing 'download_assessment_blank_form'."""
    user = UserFactory()
    course = CourseFactory()
    UserCourseFactory(user=user, course=course)
    unit = UnitFactory()
    CourseUnitFactory(course=course, unit=unit)
    assessment = AssessmentFactory(
        unit=unit,
        blank_form=factory.django.FileField(filename="the_blank_form.doc"),
        name="Apple and Orange",
    )
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(
        reverse(
            "exam.assessment.download.blank.form",
            args=[assessment.pk],
        )
    )
    assert HTTPStatus.OK == response.status_code
    assert "Content-Disposition" in response.headers
    assert (
        'attachment; filename="Apple-and-Orange-Blank-Form.doc"'
        == response.headers["Content-Disposition"]
    )


@pytest.mark.django_db
def test_assessment_download_blank_form_logged_in_not_enrolled(client):
    """Testing 'download_assessment_blank_form'."""
    assessment = AssessmentFactory(
        blank_form=factory.django.FileField(filename="the_blank_form.doc")
    )
    user = UserFactory()
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(
        reverse(
            "exam.assessment.download.blank.form",
            args=[assessment.pk],
        )
    )
    assert HTTPStatus.FORBIDDEN == response.status_code


@pytest.mark.django_db
def test_assessment_download_blank_form_logged_in_staff(client):
    """Testing 'download_assessment_blank_form'."""
    user = UserFactory(is_staff=True)
    assessment = AssessmentFactory(
        blank_form=factory.django.FileField(filename="the_blank_form.doc"),
    )
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(
        reverse(
            "exam.assessment.download.blank.form",
            args=[assessment.pk],
        )
    )
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_assessment_download_blank_form_not_logged_in(client):
    """Testing 'download_assessment_blank_form'."""
    assessment = AssessmentFactory()
    response = client.get(
        reverse(
            "exam.assessment.download.blank.form",
            args=[assessment.pk],
        )
    )
    assert HTTPStatus.FORBIDDEN == response.status_code


@pytest.mark.django_db
def test_assessment_download_blank_form(perm_check):
    """Testing 'download_assessment_blank_form'."""
    assessment = AssessmentFactory()
    perm_check.staff(
        reverse(
            "exam.assessment.download.blank.form",
            args=[assessment.pk],
        )
    )


@pytest.mark.django_db
def test_assessment_download_example_form_logged_in_enrolled(client):
    """Testing 'download_assessment_example_form'."""
    user = UserFactory()
    course = CourseFactory()
    UserCourseFactory(user=user, course=course)
    unit = UnitFactory()
    CourseUnitFactory(course=course, unit=unit)
    assessment = AssessmentFactory(
        unit=unit,
        example_form=factory.django.FileField(filename="the_example_form.doc"),
        name="2 Hatherleigh",
    )
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(
        reverse(
            "exam.assessment.download.example.form",
            args=[assessment.pk],
        )
    )
    assert HTTPStatus.OK == response.status_code
    assert "Content-Disposition" in response.headers
    assert (
        'attachment; filename="2-Hatherleigh-Example-Form.doc"'
        == response.headers["Content-Disposition"]
    )


@pytest.mark.django_db
def test_assessment_download_example_form_logged_in_not_enrolled(client):
    """Testing 'download_assessment_example_form'."""
    assessment = AssessmentFactory(
        example_form=factory.django.FileField(filename="the_example_form.doc")
    )
    user = UserFactory()
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(
        reverse(
            "exam.assessment.download.example.form",
            args=[assessment.pk],
        )
    )
    assert HTTPStatus.FORBIDDEN == response.status_code


@pytest.mark.django_db
def test_assessment_download_example_form_logged_in_staff(client):
    """Testing 'download_assessment_example_form'."""
    user = UserFactory(is_staff=True)
    assessment = AssessmentFactory(
        example_form=factory.django.FileField(filename="the_example_form.doc"),
    )
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(
        reverse(
            "exam.assessment.download.example.form",
            args=[assessment.pk],
        )
    )
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_assessment_download_example_form_not_logged_in(client):
    """Testing 'download_assessment_example_form'."""
    assessment = AssessmentFactory()
    response = client.get(
        reverse(
            "exam.assessment.download.example.form",
            args=[assessment.pk],
        )
    )
    assert HTTPStatus.FORBIDDEN == response.status_code


@pytest.mark.django_db
def test_assessment_download_example_form(perm_check):
    """Testing 'download_assessment_example_form'."""
    assessment = AssessmentFactory()
    perm_check.staff(
        reverse(
            "exam.assessment.download.example.form",
            args=[assessment.pk],
        )
    )


@pytest.mark.django_db
def test_assessment_list(perm_check):
    course = CourseFactory()
    unit = UnitFactory()
    CourseUnitFactory(course=course, unit=unit, order=1)
    AssessmentFactory(unit=unit)
    url = reverse("exam.unit.detail", args=[unit.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_assessment_update(perm_check):
    assessment = AssessmentFactory()
    url = reverse("exam.assessment.update", args=[assessment.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_class_information_create(perm_check):
    perm_check.staff(reverse("exam.class.information.create"))


@pytest.mark.django_db
def test_class_information_list(perm_check):
    perm_check.staff(reverse("exam.class.information.list"))


@pytest.mark.django_db
def test_course_create(perm_check):
    product = ProductFactory()
    ExamSettingsFactory(course=product.category.product_type)
    url = reverse("exam.course.create")
    perm_check.staff(url)


@pytest.mark.django_db
def test_course_delete(perm_check):
    course = CourseFactory()
    url = reverse("exam.course.delete", args=[course.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_course_detail(perm_check):
    course = CourseFactory()
    unit_1 = UnitFactory()
    CourseUnitFactory(course=course, unit=unit_1, order=1)
    unit_2 = UnitFactory()
    CourseUnitFactory(course=course, unit=unit_2, order=2)
    url = reverse("exam.course.detail", args=[course.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_course_list(perm_check):
    CourseFactory()
    url = reverse("exam.course.list")
    perm_check.staff(url)


@pytest.mark.django_db
def test_course_unit_delete(perm_check):
    course = CourseFactory()
    unit = UnitFactory()
    course_unit = CourseUnitFactory(course=course, unit=unit, order=1)
    url = reverse("exam.course.unit.delete", args=[course_unit.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_course_update(perm_check):
    product = ProductFactory()
    ExamSettingsFactory(course=product.category.product_type)
    course = CourseFactory()
    url = reverse("exam.course.update", args=[course.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_exam_list(perm_check):
    url = reverse("exam.list")
    perm_check.staff(url)


@pytest.mark.django_db
def test_exam_create(perm_check):
    ExamSettingsFactory()
    url = reverse("exam.create")
    perm_check.staff(url)


@pytest.mark.django_db
def test_exam_detail(perm_check):
    exam = ExamFactory()
    exam_version = ExamVersionFactory(exam=exam, date_published=None)
    QuestionFactory(exam_version=exam_version)
    url = reverse("exam.detail", args=[exam.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_exam_settings(perm_check):
    url = reverse("exam.settings")
    perm_check.staff(url)


@pytest.mark.django_db
def test_unit_create(perm_check):
    url = reverse("exam.unit.create")
    perm_check.staff(url)


@pytest.mark.django_db
def test_exam_update(perm_check):
    ExamSettingsFactory()
    exam = ExamFactory()
    exam_version = ExamVersionFactory(exam=exam, date_published=None)
    QuestionFactory(exam_version=exam_version)
    url = reverse("exam.update", args=[exam.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_question_update(perm_check):
    exam = ExamFactory()
    exam_version = ExamVersionFactory(exam=exam, date_published=None)
    question = QuestionFactory(exam_version=exam_version)
    url = reverse("exam.question.update", args=[question.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_exam_user_list(perm_check):
    user = UserFactory()
    user_exam_version = UserExamVersionFactory(user=user)
    url = reverse(
        "exam.version.user.list", args=[user_exam_version.exam_version.pk]
    )
    perm_check.staff(url)


@pytest.mark.django_db
def test_exam_user_version_result(perm_check):
    user = UserFactory()
    exam_version = setup_exam()
    obj = UserExamVersionFactory(user=user, exam_version=exam_version)
    set_correct_answers_all(obj)
    url = reverse("exam.user.version.result", args=[obj.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_exam_user_version_result_create(perm_check):
    user = UserFactory()
    exam_version = setup_exam()
    product = ProductFactory()
    url = reverse(
        "exam.user.version.result.create",
        args=[exam_version.exam.pk, user.pk, product.pk],
    )
    perm_check.staff(url)


@pytest.mark.django_db
def test_exam_user_version_result_update(perm_check):
    user = UserFactory()
    obj = UserExamVersionFactory(user=user)
    url = reverse("exam.user.version.result.update", args=[obj.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_exam_version_detail(perm_check):
    exam = ExamFactory()
    exam_version = ExamVersionFactory(exam=exam, date_published=None)
    QuestionFactory(exam_version=exam_version)
    url = reverse("exam.version.detail", args=[exam_version.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_exam_version_publish(perm_check):
    exam = ExamFactory()
    version = exam.create_questions()
    url = reverse("exam.version.publish", args=[version.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_unit_course_materials_download_logged_in_enrolled(client):
    """Testing 'download_unit_course_materials'."""
    user = UserFactory()
    course = CourseFactory()
    unit = UnitFactory(
        course_material=factory.django.FileField(
            filename="example_course_material.doc"
        )
    )
    CourseUnitFactory(course=course, unit=unit)
    UserCourseFactory(user=user, course=course)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(
        reverse(
            "exam.unit.download.course.materials",
            args=[unit.pk],
        )
    )
    assert HTTPStatus.OK == response.status_code
    assert "Content-Disposition" in response.headers
    assert "example course material" in response.headers["Content-Disposition"]


@pytest.mark.django_db
def test_unit_course_materials_download_logged_in_not_enrolled(client):
    """Testing 'download_unit_course_materials'."""
    unit = UnitFactory()
    user = UserFactory()
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(
        reverse(
            "exam.unit.download.course.materials",
            args=[unit.pk],
        )
    )
    assert HTTPStatus.FORBIDDEN == response.status_code


@pytest.mark.django_db
def test_unit_course_materials_download_logged_in_staff(client):
    """Testing 'download_unit_course_materials'."""
    user = UserFactory(is_staff=True)
    unit = UnitFactory(
        course_material=factory.django.FileField(
            filename="example_course_material.doc"
        )
    )
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(
        reverse(
            "exam.unit.download.course.materials",
            args=[unit.pk],
        )
    )
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_unit_course_materials_download_not_logged_in(client):
    """Testing 'download_unit_course_materials'."""
    unit = UnitFactory()
    response = client.get(
        reverse(
            "exam.unit.download.course.materials",
            args=[unit.pk],
        )
    )
    assert HTTPStatus.FORBIDDEN == response.status_code


@pytest.mark.django_db
def test_unit_course_materials_download(perm_check):
    """Testing 'download_unit_course_materials'."""
    unit = UnitFactory(
        course_material=factory.django.FileField(
            filename="example_course_material.doc"
        )
    )
    perm_check.staff(
        reverse(
            "exam.unit.download.course.materials",
            args=[unit.pk],
        )
    )


@pytest.mark.django_db
def test_unit_create(perm_check):
    perm_check.staff(reverse("exam.unit.create"))


@pytest.mark.django_db
def test_unit_delete(perm_check):
    course = CourseFactory()
    unit = UnitFactory()
    CourseUnitFactory(course=course, unit=unit, order=1)
    url = reverse("exam.unit.delete", args=[unit.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_unit_exam_delete(perm_check):
    unit_exam = UnitExamFactory()
    url = reverse("exam.unit.exam.delete", args=[unit_exam.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_unit_exam_update(perm_check):
    course = CourseFactory()
    unit = UnitFactory()
    CourseUnitFactory(course=course, unit=unit, order=1)
    url = reverse("exam.unit.exam.update", args=[unit.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_unit_list(perm_check):
    course = CourseFactory()
    unit_1 = UnitFactory()
    CourseUnitFactory(course=course, unit=unit_1, order=1)
    unit_2 = UnitFactory()
    CourseUnitFactory(course=course, unit=unit_2, order=2)
    perm_check.staff(reverse("exam.unit.list"))


@pytest.mark.django_db
def test_unit_update(perm_check):
    course = CourseFactory()
    unit = UnitFactory()
    CourseUnitFactory(course=course, unit=unit, order=1)
    url = reverse("exam.unit.update", args=[unit.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_user_assessment_update(perm_check):
    usercourse1 = UserCourseFactory()
    unit = UnitFactory()
    CourseUnitFactory(course=usercourse1.course, unit=unit, order=1)
    userunit1 = UserUnitFactory(unit=unit, user_course=usercourse1)
    assessment1 = AssessmentFactory(unit=unit)
    userassessment1 = UserAssessmentFactory(
        user_unit=userunit1, assessment=assessment1
    )
    url = reverse("exam.user.assessment.update", args=[userassessment1.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_user_assessment_download_answer_file_logged_in_enrolled(client):
    """Testing 'download_user_assessment_answer_file'."""
    user = UserFactory()
    user_course = UserCourseFactory(user=user)
    user_unit = UserUnitFactory(user_course=user_course)
    user_assessment = UserAssessmentFactory(
        user_unit=user_unit,
        answer_file=factory.django.FileField(filename="the_answer_file.doc"),
    )
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(
        reverse(
            "exam.user.assessment.download.answer.file",
            args=[user_assessment.pk],
        )
    )
    assert HTTPStatus.OK == response.status_code
    assert "Content-Disposition" in response.headers
    assert "the answer file" in response.headers["Content-Disposition"]


@pytest.mark.django_db
def test_user_assessment_download_answer_file_logged_in_not_enrolled(client):
    """Testing 'download_user_assessment_answer_file'."""
    user_assessment = UserAssessmentFactory(
        answer_file=factory.django.FileField(filename="the_answer_file.doc"),
    )
    user = UserFactory()
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(
        reverse(
            "exam.user.assessment.download.answer.file",
            args=[user_assessment.pk],
        )
    )
    assert HTTPStatus.FORBIDDEN == response.status_code


@pytest.mark.django_db
def test_user_assessment_download_answer_file_logged_in_staff(client):
    """Testing 'download_user_assessment_answer_file'."""
    user_assessment = UserAssessmentFactory()
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(
        reverse(
            "exam.user.assessment.download.answer.file",
            args=[user_assessment.pk],
        )
    )
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_user_assessment_download_answer_file_not_logged_in(client):
    """Testing 'download_user_assessment_answer_file'."""
    user_assessment = UserAssessmentFactory()
    response = client.get(
        reverse(
            "exam.user.assessment.download.answer.file",
            args=[user_assessment.pk],
        )
    )
    assert HTTPStatus.FORBIDDEN == response.status_code


@pytest.mark.django_db
def test_user_assessment_download_answer_file(perm_check):
    """Testing 'download_user_assessment_answer_file'."""
    user_assessment = UserAssessmentFactory()
    perm_check.staff(
        reverse(
            "exam.user.assessment.download.answer.file",
            args=[user_assessment.pk],
        )
    )


@pytest.mark.django_db
def test_user_assessment_download_retake_answer_file_logged_in_enrolled(client):
    """Testing 'download_user_assessment_retake_answer_file'."""
    user = UserFactory()
    user_course = UserCourseFactory(user=user)
    user_unit = UserUnitFactory(user_course=user_course)
    user_assessment = UserAssessmentFactory(
        user_unit=user_unit,
        retake_answer_file=factory.django.FileField(
            filename="the_answer_file.doc"
        ),
    )
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(
        reverse(
            "exam.user.assessment.download.retake.answer.file",
            args=[user_assessment.pk],
        )
    )
    assert HTTPStatus.OK == response.status_code
    assert "Content-Disposition" in response.headers
    assert "the answer file" in response.headers["Content-Disposition"]


@pytest.mark.django_db
def test_user_assessment_download_retake_answer_file_logged_in_not_enrolled(
    client,
):
    """Testing 'download_user_assessment_retake_answer_file'."""
    user_assessment = UserAssessmentFactory(
        retake_answer_file=factory.django.FileField(
            filename="the_answer_file.doc"
        ),
    )
    user = UserFactory()
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(
        reverse(
            "exam.user.assessment.download.retake.answer.file",
            args=[user_assessment.pk],
        )
    )
    assert HTTPStatus.FORBIDDEN == response.status_code


@pytest.mark.django_db
def test_user_assessment_download_retake_answer_file_logged_in_staff(client):
    """Testing 'download_user_assessment_retake_answer_file'."""
    user_assessment = UserAssessmentFactory(
        retake_answer_file=factory.django.FileField(
            filename="the_answer_file.doc"
        ),
    )
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(
        reverse(
            "exam.user.assessment.download.retake.answer.file",
            args=[user_assessment.pk],
        )
    )
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_user_assessment_download_retake_answer_file_not_logged_in(client):
    """Testing 'download_user_assessment_retake_answer_file'."""
    user_assessment = UserAssessmentFactory()
    response = client.get(
        reverse(
            "exam.user.assessment.download.retake.answer.file",
            args=[user_assessment.pk],
        )
    )
    assert HTTPStatus.FORBIDDEN == response.status_code


@pytest.mark.django_db
def test_user_assessment_download_retake_answer_file(perm_check):
    """Testing 'download_user_assessment_retake_answer_file'."""
    user_assessment = UserAssessmentFactory()
    perm_check.staff(
        reverse(
            "exam.user.assessment.download.retake.answer.file",
            args=[user_assessment.pk],
        )
    )


@pytest.mark.django_db
def test_user_assessment_download_blank_form(perm_check):
    assessment = AssessmentFactory(blank_form__data=b"abc")
    user_assessment = UserAssessmentFactory(assessment=assessment)
    perm_check.staff(
        reverse(
            "exam.assessment.download.blank.form",
            args=[assessment.pk],
        )
    )


@pytest.mark.django_db
def test_user_assessment_download_example_form(perm_check):
    assessment = AssessmentFactory(example_form__data=b"abc")
    user_assessment = UserAssessmentFactory(assessment=assessment)
    perm_check.staff(
        reverse(
            "exam.assessment.download.example.form",
            args=[assessment.pk],
        )
    )


@pytest.mark.django_db
def test_user_assessment_download_retake_answer_file(perm_check):
    user_assessment = UserAssessmentFactory(
        retake_answer_file=SimpleUploadedFile(
            "my_retake_answer_file.doc", b"file contents"
        ),
    )
    perm_check.staff(
        reverse(
            "exam.user.assessment.download.retake.answer.file",
            args=[user_assessment.pk],
        )
    )


@pytest.mark.django_db
def test_user_assessment_update(perm_check):
    usercourse1 = UserCourseFactory()
    unit = UnitFactory()
    CourseUnitFactory(course=usercourse1.course, unit=unit, order=1)
    userunit1 = UserUnitFactory(unit=unit, user_course=usercourse1)
    assessment1 = AssessmentFactory(unit=unit)
    userassessment1 = UserAssessmentFactory(
        user_unit=userunit1, assessment=assessment1
    )
    url = reverse("exam.user.assessment.update", args=[userassessment1.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_user_course_delete(perm_check):
    user_course = UserCourseFactory()
    url = reverse("exam.user.course.delete", args=[user_course.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_user_course_detail(perm_check):
    user_course = UserCourseFactory()
    url = reverse("exam.user.course.detail", args=[user_course.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_user_course_list(perm_check):
    UserCourseFactory()
    perm_check.staff(reverse("exam.user.course.list"))


@pytest.mark.django_db
def test_user_course_marking_list(perm_check):
    """Testing the ``UserCourseMarkingListView``.

    .. tip:: Also see ``test_user_course_marking_list_team_member``
             in ``example_exam/tests/test_view_perm.py``

    """
    UserCourseFactory()
    perm_check.staff(reverse("exam.user.course.marking.list"))


@pytest.mark.django_db
def test_user_course_list_to_mark(perm_check):
    UserCourseFactory()
    perm_check.staff(reverse("exam.user.course.list.to.mark"))


@pytest.mark.django_db
def test_user_course_profile_update(perm_check):
    user_course = UserCourseFactory()
    url = reverse("exam.user.course.profile.update", args=[user_course.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_user_course_update(perm_check):
    user_course = UserCourseFactory()
    url = reverse("exam.user.course.update.result", args=[user_course.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_user_exam_version_download_logged_in_enrolled(client):
    """Testing 'download_user_exam_version'."""
    user = UserFactory()
    user_exam_version = UserExamVersionFactory(user=user)
    QuestionFactory(exam_version=user_exam_version.exam_version)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(
        reverse("exam.user.download", args=[user_exam_version.pk])
    )
    assert HTTPStatus.OK == response.status_code
    assert "Content-Disposition" in response.headers
    assert (
        f"test-{user_exam_version.pk}"
        in response.headers["Content-Disposition"]
    )


@pytest.mark.django_db
def test_user_exam_version_download_logged_in_not_enrolled(client):
    """Testing 'download_user_exam_version'."""
    user_exam_version = UserExamVersionFactory(user=UserFactory())
    QuestionFactory(exam_version=user_exam_version.exam_version)
    user = UserFactory()
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(
        reverse(
            "exam.user.download",
            args=[user_exam_version.pk],
        )
    )
    assert HTTPStatus.FORBIDDEN == response.status_code


@pytest.mark.django_db
def test_user_exam_version_download_logged_in_staff(client):
    """Testing 'download_user_exam_version'."""
    user_exam_version = UserExamVersionFactory(user=UserFactory())
    QuestionFactory(exam_version=user_exam_version.exam_version)
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(
        reverse("exam.user.download", args=[user_exam_version.pk])
    )
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_user_exam_version_download_not_logged_in(client):
    """Testing 'download_user_exam_version'."""
    user_exam_version = UserExamVersionFactory(user=UserFactory())
    response = client.get(
        reverse("exam.user.download", args=[user_exam_version.pk])
    )
    assert HTTPStatus.FORBIDDEN == response.status_code


@pytest.mark.django_db
def test_user_exam_version_download(perm_check):
    """Testing 'download_user_exam_version'."""
    user_exam_version = UserExamVersionFactory(user=UserFactory())
    QuestionFactory(exam_version=user_exam_version.exam_version)
    perm_check.staff(reverse("exam.user.download", args=[user_exam_version.pk]))


@pytest.mark.django_db
def test_user_unit_detail(perm_check):
    user_unit = UserUnitFactory()
    url = reverse("exam.user.unit.detail", args=[user_unit.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_user_unit_update_comment(perm_check):
    user_course = UserCourseFactory()
    unit = UnitFactory()
    CourseUnitFactory(course=user_course.course, unit=unit, order=1)
    user_unit = UserUnitFactory(unit=unit, user_course=user_course)
    url = reverse("exam.user.unit.update.comment", args=[user_unit.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_user_unit_update_result(perm_check):
    """

    .. tip:: Testing ``UserUnitResultUpdateView``.

    """
    user_course = UserCourseFactory()
    unit = UnitFactory()
    CourseUnitFactory(course=user_course.course, unit=unit, order=1)
    user_unit = UserUnitFactory(unit=unit, user_course=user_course)
    url = reverse("exam.user.unit.update.result", args=[user_unit.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_user_unit_update_retake_comment(perm_check):
    user_course = UserCourseFactory()
    unit = UnitFactory()
    CourseUnitFactory(course=user_course.course, unit=unit, order=1)
    user_unit = UserUnitFactory(unit=unit, user_course=user_course)
    url = reverse("exam.user.unit.update.comment.retake", args=[user_unit.pk])
    perm_check.staff(url)
