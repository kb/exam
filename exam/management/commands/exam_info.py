# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from exam.models import get_contact_model, UserExamVersion


class Command(BaseCommand):
    help = "Information for an 'exam'"

    def add_arguments(self, parser):
        parser.add_argument("contact_pk", type=int)

    def handle(self, *args, **options):
        contact_pk = options["contact_pk"]
        contact_model = get_contact_model()
        contact = contact_model.objects.get(pk=contact_pk)
        self.stdout.write(
            "{} for {} ({})".format(self.help, contact, contact.email())
        )
        qs = UserExamVersion.objects.filter(user=contact.user)
        self.stdout.write(
            "{:<20}  {:<20} {:<40}".format("Created", "Marked", "Course")
        )
        self.stdout.write(
            "{:<20}  {:<20} {:<40}".format("-------", "------", "------")
        )
        for user_exam_version in qs:
            info = []
            if user_exam_version.is_fail():
                info.append("fail")
            if user_exam_version.is_marked():
                info.append("marked")
            if user_exam_version.is_pass():
                info.append("pass")
            if user_exam_version.previous_test():
                info.append("retake")
            if user_exam_version.legacy_result:
                info.append("legacy_result")
            if user_exam_version.legacy_retake:
                info.append("legacy_retake")
            date_marked = user_exam_version.date_marked
            result_as_percent = user_exam_version.result_as_percent()
            self.stdout.write(
                "{:<20}  {:<20} {:<40} result={:<5} ({:<5}%) [{}]".format(
                    user_exam_version.created.strftime("%d/%m/%Y %H:%M:%S"),
                    (
                        date_marked.strftime("%d/%m/%Y %H:%M:%S")
                        if date_marked
                        else ""
                    ),
                    user_exam_version.exam_version.exam.name,
                    # user_exam_version.user.username,
                    user_exam_version.result or "",
                    (
                        "{}%".format(result_as_percent)
                        if result_as_percent
                        else ""
                    ),
                    "|".join(info),
                )
            )
        self.stdout.write("{}".format(self.help))
