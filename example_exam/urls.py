# -*- encoding: utf-8 -*-
from django.conf import settings
from django.conf.urls.static import static
from django.urls import include, path, re_path, reverse_lazy
from django.views.generic import RedirectView

from exam.views import download_assessment, download_user_exam_version
from .views import (
    ContactDetailView,
    HomeView,
    LmsDashView,
    SettingsView,
    UserContactRedirectView,
    UserExamQuestionView,
    UserExamVersionResultView,
    UserExamVersionUpdateView,
)


urlpatterns = [
    re_path(r"^$", view=HomeView.as_view(), name="project.home"),
    re_path(
        r"^settings/$",
        view=SettingsView.as_view(),
        name="project.settings",
    ),
    re_path(r"^", view=include("login.urls")),
    re_path(
        r"^contact/(?P<pk>\d+)/$",
        view=ContactDetailView.as_view(),
        name="contact.detail",
    ),
    re_path(r"^exam/", view=include("exam.urls.dash")),
    re_path(r"^exam/redirect/", view=include("exam.urls.redirect")),
    re_path(r"^gallery/", view=include("gallery.urls")),
    re_path(r"^gdpr/", view=include("gdpr.urls")),
    re_path(
        r"^home/user/$",
        view=RedirectView.as_view(url=reverse_lazy("project.home")),
        name="project.dash",
    ),
    re_path(r"^lms/dash/$", view=LmsDashView.as_view(), name="lms.dash"),
    re_path(
        r"^user/(?P<pk>\d+)/redirect/$",
        view=UserContactRedirectView.as_view(),
        name="user.redirect.contact",
    ),
    # pages for the user (student) to take an exam
    re_path(
        r"^user/exam/version/(?P<pk>\d+)/$",
        view=UserExamVersionUpdateView.as_view(),
        name="web.exam.user.version.update",
    ),
    re_path(
        r"^user/(?P<version_pk>\d+)/question/(?P<pk>\d+)/$",
        view=UserExamQuestionView.as_view(),
        name="web.exam.user.question",
    ),
    re_path(
        r"^user/exam/version/(?P<pk>\d+)/result/$",
        view=UserExamVersionResultView.as_view(),
        name="web.exam.user.version.result",
    ),
    # 01/08/2022, Copy of 'UserExamVersionResultView' for testing purposes
    re_path(
        r"^user/exam/version/(?P<pk>\d+)/submitted/$",
        view=UserExamVersionResultView.as_view(),
        name="web.exam.user.version.submitted",
    ),
    re_path(
        r"^assessment/(?P<assessment_pk>\d+)/download/$",
        view=download_assessment,
        name="exam.assessment.download.pdf",
    ),
    re_path(
        r"^user/exam/(?P<pk>\d+)/download/$",
        view=download_user_exam_version,
        name="exam.user.download",
    ),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        re_path(r"^__debug__/", include(debug_toolbar.urls))
    ] + urlpatterns
