# -*- encoding: utf-8 -*-
import datetime
import pytest

from datetime import date
from django.core.files.uploadedfile import SimpleUploadedFile
from django.utils import timezone
from freezegun import freeze_time

from exam.models import ExamError, UserAssessment, UserCourse, UserUnit
from exam.tests.factories import (
    AssessmentFactory,
    CourseFactory,
    CourseUnitFactory,
    ExamFactory,
    ExamVersionFactory,
    UnitExamFactory,
    UnitFactory,
    UserAssessmentFactory,
    UserCourseFactory,
    UserExamVersionFactory,
    UserUnitFactory,
)
from login.tests.factories import UserFactory


@pytest.mark.django_db
def test_assessment_completed_count():
    unit = UnitFactory()
    assessment_1 = AssessmentFactory(unit=unit)
    assessment_2 = AssessmentFactory(unit=unit)
    user_unit = UserUnitFactory(unit=unit)
    UserAssessmentFactory(
        user_unit=user_unit,
        assessment=assessment_1,
        can_retake=False,
        answer_submitted=timezone.now(),
    )
    UserAssessmentFactory(
        user_unit=user_unit,
        assessment=assessment_2,
        can_retake=True,
        retake_submitted=timezone.now(),
    )
    assert 2 == user_unit.assessment_completed_count()


@pytest.mark.django_db
def test_assessment_count():
    unit = UnitFactory()
    AssessmentFactory(unit=unit)
    assessment_2 = AssessmentFactory(unit=unit)
    assessment_2.set_deleted(UserFactory())
    AssessmentFactory(unit=UnitFactory())
    AssessmentFactory(unit=unit)
    user_unit = UserUnitFactory(unit=unit)
    assert 2 == user_unit.assessment_count()


@pytest.mark.django_db
def test_assessment_count_none():
    unit = UnitFactory()
    assessment = AssessmentFactory(unit=unit)
    assessment.set_deleted(UserFactory())
    user_unit = UserUnitFactory(unit=unit)
    assert 0 == user_unit.assessment_count()


@pytest.mark.django_db
def test_assessments():
    unit = UnitFactory(name="u1")
    CourseUnitFactory(course=CourseFactory(name="c1"), unit=unit, order=1)
    assessment_1 = AssessmentFactory(unit=unit, name="a1")
    AssessmentFactory(unit=unit, name="a2")
    assessment_3 = AssessmentFactory(unit=unit, name="a3")
    # user
    user = UserFactory(username="pat")
    user_unit = UserUnitFactory(
        user_course=UserCourseFactory(user=user), unit=unit
    )
    UserAssessmentFactory(user_unit=user_unit, assessment=assessment_1)
    # retake of assessment 1
    UserAssessmentFactory(user_unit=user_unit, assessment=assessment_1)
    # assessment 3
    UserAssessmentFactory(user_unit=user_unit, assessment=assessment_3)
    # assessment 3 (duplicate)
    user_assessment = UserAssessmentFactory(
        user_unit=user_unit, assessment=assessment_3
    )
    user_assessment.set_deleted(user)
    # test
    assessments = user_unit.assessments()
    assert [
        set(["assessment", "user_assessments"]),
        set(["assessment", "user_assessments"]),
        set(["assessment", "user_assessments"]),
    ] == [x.keys() for x in assessments]
    result = []
    for row in assessments:
        assessment = str(row["assessment"])
        user_assessments = row["user_assessments"]
        data = []
        for x in user_assessments:
            data.append(str(x))
        result.append({assessment: data})
    assert [
        {"a1 for u1": ["a1: u1 (for 'pat')", "a1: u1 (for 'pat')"]},
        {"a2 for u1": []},
        {"a3 for u1": ["a3: u1 (for 'pat')"]},
    ] == result


@pytest.mark.django_db
def test_assessments_none():
    user_unit = UserUnitFactory()
    assert [] == user_unit.assessments()


@pytest.mark.django_db
def test_assessments_exist():
    unit = UnitFactory()
    AssessmentFactory(unit=unit)
    user_unit = UserUnitFactory(unit=unit)
    assert user_unit.assessments_exist() is True


@pytest.mark.django_db
def test_assessments_exist_none():
    unit = UnitFactory()
    assessment = AssessmentFactory(unit=unit)
    assessment.set_deleted(UserFactory())
    user_unit = UserUnitFactory(unit=unit)
    assert user_unit.assessments_exist() is False


@pytest.mark.django_db
def test_can_mark():
    user = UserFactory()
    course = CourseFactory()
    unit = UnitFactory()
    CourseUnitFactory(course=course, unit=unit, order=1)
    user_course = UserCourseFactory(user=user, course=course)
    user_unit = UserUnitFactory(
        user_course=user_course,
        unit=unit,
        submitted=timezone.now(),
        assessed=None,
    )
    assert user_unit.can_mark() is True


@pytest.mark.django_db
def test_can_mark_assessed():
    user = UserFactory()
    course = CourseFactory()
    unit = UnitFactory()
    CourseUnitFactory(course=course, unit=unit, order=1)
    user_course = UserCourseFactory(user=user, course=course)
    user_unit = UserUnitFactory(
        user_course=user_course,
        unit=unit,
        submitted=timezone.now(),
        assessed=timezone.now(),
    )
    assert user_unit.can_mark() is False


@pytest.mark.django_db
def test_can_mark_retake_assessed():
    user = UserFactory()
    course = CourseFactory()
    unit = UnitFactory()
    assessment = AssessmentFactory(unit=unit)
    CourseUnitFactory(course=course, unit=unit, order=1)
    user_course = UserCourseFactory(user=user, course=course)
    user_unit = UserUnitFactory(
        user_course=user_course,
        unit=unit,
        submitted=timezone.now(),
        assessed=timezone.now(),
        retake_submitted=timezone.now(),
        # retake_assessed=timezone.now(),
    )
    user_assessment = UserAssessmentFactory(
        user_unit=user_unit, assessment=assessment, can_retake=True
    )
    assert user_unit.can_mark() is False


@pytest.mark.django_db
def test_can_user_start():
    user = UserFactory()
    course = CourseFactory()
    user_course = UserCourseFactory(user=user, course=course)
    unit_1 = UnitFactory()
    CourseUnitFactory(course=course, unit=unit_1, order=1)
    user_unit_1 = UserUnitFactory(user_course=user_course, unit=unit_1)
    user_unit_1.submit()
    unit_2 = UnitFactory()
    CourseUnitFactory(course=course, unit=unit_2, order=2)
    assert UserUnit.objects.can_user_start(user_course, unit_2) is True


# @pytest.mark.django_db
# def test_can_user_start_no_prev_unit():
#    user = UserFactory()
#    course = CourseFactory()
#    user_course = UserCourseFactory(user=user, course=course)
#    unit = UnitFactory()
#    CourseUnitFactory(course=course, unit=unit, order=2)
#    UserUnitFactory(user_course=user_course, unit=unit)
#    assert UserUnit.objects.can_user_start(user_course, unit) is False


@pytest.mark.django_db
def test_can_user_start_not_enrolled():
    """User not enrolled, so cannot start.

    PJK 10/01/2022, I am not sure why this throws an exception now, but didn't
    before.  Feel free to revert if this causes an issue.

    """
    user_course = UserCourseFactory(
        user=UserFactory(), course=CourseFactory(name="Java")
    )
    unit = UnitFactory(name="Unit 3")
    with pytest.raises(ExamError) as e:
        UserUnit.objects.can_user_start(user_course, unit)
    assert "'Unit 3' is not linked to 'Java'" in str(e.value)


# @pytest.mark.django_db
# def test_can_user_start_order_less_than_2():
#    user = UserFactory()
#    course = CourseFactory()
#    user_course = UserCourseFactory(user=user, course=course)
#    unit = UnitFactory()
#    CourseUnitFactory(course=course, unit=unit, order=1)
#    UserUnitFactory(user_course=user_course, unit=unit)
#    assert UserUnit.objects.can_user_start(user_course, unit) is True


@pytest.mark.django_db
def test_create_user_unit():
    user = UserFactory()
    course = CourseFactory()
    user_course = UserCourseFactory(user=user, course=course)
    unit = UnitFactory()
    CourseUnitFactory(course=course, unit=unit, order=1)
    x = UserUnit.objects._create_user_unit(user_course, unit)
    assert int(x.pk) > 0
    assert user == x.user_course.user
    assert unit == x.unit
    assert x.assessor is None
    assert "" == x.assessor_comments
    assert x.result is None


@pytest.mark.django_db
def test_create_user_unit_diff_course():
    course = CourseFactory(name="Fruit")
    user_course = UserCourseFactory(course=course)
    unit = UnitFactory(name="Apple")
    # Link the unit to a different course!
    CourseUnitFactory(course=CourseFactory(), unit=unit, order=1)
    with pytest.raises(ExamError) as e:
        UserUnit.objects._create_user_unit(user_course, unit)
    assert "The 'Apple' unit does not belong to the 'Fruit' course" in str(
        e.value
    )


@pytest.mark.django_db
def test_current():
    user = UserFactory()
    # course 1
    course_1 = CourseFactory()
    user_course_1 = UserCourseFactory(user=user, course=course_1)
    unit_1 = UnitFactory()
    user_unit_1 = UserUnitFactory(
        user_course=user_course_1, unit=unit_1, assessor_comments="a1"
    )
    # course 2
    course_2 = CourseFactory()
    user_course_2 = UserCourseFactory(user=user, course=course_2)
    unit_2 = UnitFactory()
    user_unit_2 = UserUnitFactory(
        user_course=user_course_2, unit=unit_2, assessor_comments="a2"
    )
    user_unit_2.set_deleted(user)
    # course 3
    course_3 = CourseFactory()
    user_course_3 = UserCourseFactory(user=user, course=course_3)
    unit_3 = UnitFactory()
    user_unit_3 = UserUnitFactory(
        user_course=user_course_3, unit=unit_1, assessor_comments="a3"
    )
    assert set(["a1", "a3"]) == set(
        [x.assessor_comments for x in UserUnit.objects.current()]
    )


@pytest.mark.django_db
def test_current_to_mark(django_assert_num_queries):
    user = UserFactory(first_name="P", last_name="Kimber", username="pkimber")
    # course 1 - course comments, but no submitted units - ignore
    course_1 = CourseFactory(name="Farming")
    unit_1 = UnitFactory(name="Dairy")
    CourseUnitFactory(course=course_1, unit=unit_1)
    with freeze_time(
        datetime.datetime(2023, 5, 27, 7, 30, 1, tzinfo=datetime.timezone.utc)
    ):
        user_course_1 = UserCourseFactory(
            course=course_1, user=user, assessor_comments="uc1"
        )
        UserUnitFactory(
            unit=unit_1,
            user_course=user_course_1,
            submitted=None,
            retake_assessed=None,
            retake_submitted=None,
            assessed=None,
        )
        # course 2 - course comments, unassessed submitted unit -include
        course_2 = CourseFactory(name="Developer")
        unit_2 = UnitFactory(name="python")
        CourseUnitFactory(
            course=course_2,
            unit=unit_2,
            name="C++ Developer",
        )
        user_course_2 = UserCourseFactory(
            course=course_2, user=user, assessor_comments="uc2"
        )
        user_unit_2 = UserUnitFactory(
            unit=unit_2,
            user_course=user_course_2,
            submitted=timezone.now(),
            retake_assessed=None,
            retake_submitted=None,
            assessed=None,
        )
        # course 3 - course assessor_comments can retake but not retake_submitted
        # -ignore
        course_3 = CourseFactory(name="Reception")
        unit_3 = UnitFactory(name="Medical")
        CourseUnitFactory(course=course_3, unit=unit_3)
        user_course_3 = UserCourseFactory(
            course=course_3, user=user, assessor_comments="uc3"
        )
        UserUnitFactory(
            unit=unit_3,
            user_course=user_course_3,
            submitted=None,
            retake_assessed=None,
            retake_submitted=None,
            assessed=None,
        )
        # course 4 - course assessor_comments can retake, retake is submitted
        # - include even though there's no answer
        # can this happen / should it be allowed?
        # include - it should be in the list so it get's noticed
        # if it happens should UI allow resetting submitted?
        course_4 = CourseFactory(name="Councillor")
        unit_4 = UnitFactory(name="District")
        CourseUnitFactory(
            course=course_4, unit=unit_4, name="Borough Councillor"
        )
        user_course_4 = UserCourseFactory(
            course=course_4, user=user, assessor_comments="uc4"
        )
        user_unit_4 = UserUnitFactory(
            user_course=user_course_4,
            unit=unit_4,
            submitted=None,
            retake_assessed=timezone.now(),
            retake_submitted=timezone.now(),
            assessed=None,
        )
        # course 5 - course comments, retake submitted
        course_5 = CourseFactory(name="Student")
        unit_5 = UnitFactory()
        CourseUnitFactory(course=course_5, unit=unit_5, name="College Student")
        user_course_5 = UserCourseFactory(
            course=course_5, user=user, assessor_comments="uc5"
        )
        user_unit_5 = UserUnitFactory(
            user_course=user_course_5,
            unit=unit_5,
            submitted=None,
            retake_assessed=timezone.now(),
            retake_submitted=timezone.now(),
            assessed=None,
        )
        # course 6 - can retake, retake_assessed but not retake_submitted
        course_6 = CourseFactory(name="Builder")
        unit_6 = UnitFactory()
        CourseUnitFactory(course=course_6, unit=unit_6)
        user_course_6 = UserCourseFactory(
            course=course_6, user=user, assessor_comments="uc6"
        )
        UserUnitFactory(
            user_course=user_course_6,
            unit=unit_6,
            submitted=None,
            retake_assessed=timezone.now(),
            retake_submitted=None,
            assessed=None,
        )
        # course 7 - can retake, retake_assessed and then retake_submitted
        course_7 = CourseFactory(name="Welder")
        unit_7 = UnitFactory()
        CourseUnitFactory(course=course_7, unit=unit_7, name="Arc Welding")
        user_course_7 = UserCourseFactory(
            course=course_7, user=user, assessor_comments="uc7"
        )
        user_unit_7 = UserUnitFactory(
            user_course=user_course_7,
            unit=unit_7,
            submitted=None,
            retake_assessed=timezone.now(),
            retake_submitted=timezone.now(),
            assessed=None,
        )
        # course 8 - 18/03/2023 - https://www.kbsoftware.co.uk/crm/ticket/6657/
        course_8 = CourseFactory()
        unit_8 = UnitFactory()
        CourseUnitFactory(course=course_8, unit=unit_8)
        user_course_8 = UserCourseFactory(
            course=course_8, user=user, assessor_comments="uc8"
        )
        UserUnitFactory(
            user_course=user_course_8,
            unit=unit_8,
            submitted=timezone.now(),
            retake_assessed=timezone.now(),
            retake_submitted=None,
            assessed=None,
        )
    # test
    with django_assert_num_queries(12):
        user_unit_data = UserUnit.objects.current_to_mark()
    assert [
        {
            "course_name": "Councillor",
            "full_name": "P Kimber",
            "is_retake": True,
            "retake_submitted": datetime.datetime(
                2023, 5, 27, 7, 30, 1, tzinfo=datetime.timezone.utc
            ),
            "submitted": None,
            "unit_name": "Borough Councillor",
            "user_course_pk": user_course_4.pk,
            "user_name": "pkimber",
            "user_pk": user.pk,
            "user_unit_pk": user_unit_4.pk,
        },
        {
            "course_name": "Student",
            "full_name": "P Kimber",
            "is_retake": True,
            "retake_submitted": datetime.datetime(
                2023, 5, 27, 7, 30, 1, tzinfo=datetime.timezone.utc
            ),
            "submitted": None,
            "unit_name": "College Student",
            "user_course_pk": user_course_5.pk,
            "user_name": "pkimber",
            "user_pk": user.pk,
            "user_unit_pk": user_unit_5.pk,
        },
        {
            "course_name": "Welder",
            "full_name": "P Kimber",
            "is_retake": True,
            "retake_submitted": datetime.datetime(
                2023, 5, 27, 7, 30, 1, tzinfo=datetime.timezone.utc
            ),
            "submitted": None,
            "unit_name": "Arc Welding",
            "user_course_pk": user_course_7.pk,
            "user_name": "pkimber",
            "user_pk": user.pk,
            "user_unit_pk": user_unit_7.pk,
        },
        {
            "course_name": "Developer",
            "full_name": "P Kimber",
            "is_retake": False,
            "retake_submitted": None,
            "submitted": datetime.datetime(
                2023, 5, 27, 7, 30, 1, tzinfo=datetime.timezone.utc
            ),
            "unit_name": "C++ Developer",
            "user_course_pk": user_course_2.pk,
            "user_name": "pkimber",
            "user_pk": user.pk,
            "user_unit_pk": user_unit_2.pk,
        },
    ] == user_unit_data


@pytest.mark.django_db
def test_current_to_mark_ignore_already_marked(django_assert_num_queries):
    """LMS assessor area - not in date order.

    https://www.kbsoftware.co.uk/crm/ticket/6805/

    """
    user = UserFactory(first_name="P", last_name="Kimber", username="pkimber")
    course = CourseFactory(name="Councillor")
    unit_1 = UnitFactory(name="Borough")
    unit_2 = UnitFactory(name="County")
    CourseUnitFactory(course=course, unit=unit_1, name="Borough Councillor")
    CourseUnitFactory(course=course, unit=unit_2, name="County Councillor")
    user_course = UserCourseFactory(course=course, user=user)
    # ignore this unit because it has already been assessed
    UserUnitFactory(
        user_course=user_course,
        unit=unit_1,
        submitted=datetime.datetime(
            2023, 7, 16, 0, 0, 0, tzinfo=datetime.timezone.utc
        ),
        retake_assessed=datetime.datetime(
            2023, 7, 24, 0, 0, 0, tzinfo=datetime.timezone.utc
        ),
        retake_submitted=datetime.datetime(
            2023, 7, 24, 0, 0, 0, tzinfo=datetime.timezone.utc
        ),
        assessed=datetime.datetime(
            2023, 7, 31, 0, 0, 0, tzinfo=datetime.timezone.utc
        ),
    )
    user_unit = UserUnitFactory(
        user_course=user_course,
        unit=unit_2,
        submitted=datetime.datetime(
            2023, 8, 18, 0, 0, 0, tzinfo=datetime.timezone.utc
        ),
        retake_assessed=None,
        retake_submitted=None,
        assessed=None,
    )
    with django_assert_num_queries(6):
        user_unit_data = UserUnit.objects.current_to_mark()
    assert [
        {
            "course_name": "Councillor",
            "full_name": "P Kimber",
            "is_retake": False,
            "retake_submitted": None,
            "submitted": datetime.datetime(
                2023, 8, 18, 0, 0, tzinfo=datetime.timezone.utc
            ),
            "unit_name": "County Councillor",
            "user_course_pk": user_course.pk,
            "user_name": "pkimber",
            "user_pk": user.pk,
            "user_unit_pk": user_unit.pk,
        }
    ] == user_unit_data


@pytest.mark.django_db
def test_current_user_course():
    user = UserFactory()
    # course 1
    course_1 = CourseFactory()
    user_course_1 = UserCourseFactory(user=user, course=course_1)
    unit_1 = UnitFactory()
    UserUnitFactory(
        user_course=user_course_1, unit=unit_1, assessor_comments="a1"
    )
    # course 2
    course_2 = CourseFactory()
    user_course_2 = UserCourseFactory(user=user, course=course_2)
    unit_2 = UnitFactory()
    user_unit_2 = UserUnitFactory(
        user_course=user_course_2, unit=unit_2, assessor_comments="a2"
    )
    user_unit_2.set_deleted(user)
    # course 3
    course_3 = CourseFactory()
    user_course_3 = UserCourseFactory(user=user, course=course_3)
    unit_3 = UnitFactory()
    UserUnitFactory(
        user_course=user_course_3, unit=unit_3, assessor_comments="a3"
    )
    assert ["a3"] == [
        x.assessor_comments for x in UserUnit.objects.current(user_course_3)
    ]


@pytest.mark.django_db
def test_for_course(django_assert_num_queries):
    course = CourseFactory()
    unit_1 = UnitFactory(name="unit-1")
    unit_2 = UnitFactory(name="unit-2")
    unit_3 = UnitFactory(name="unit-3")
    unit_4 = UnitFactory(name="unit-4")
    unit_5 = UnitFactory(name="unit-5")
    CourseUnitFactory(course=course, unit=unit_1, order=1)
    CourseUnitFactory(course=course, unit=unit_2, order=3)
    CourseUnitFactory(course=course, unit=unit_3, order=2)
    CourseUnitFactory(course=course, unit=unit_4, order=4)
    CourseUnitFactory(course=course, unit=unit_5, order=5)
    AssessmentFactory(unit=unit_1)
    AssessmentFactory(unit=unit_2)
    AssessmentFactory(unit=unit_3)
    # No assessment, so should not be included
    # AssessmentFactory(unit=unit_4)
    AssessmentFactory(unit=unit_5)
    user = UserFactory()
    user_course = UserCourseFactory(user=user, course=course)
    UserUnitFactory(user_course=user_course, unit=unit_1)
    UserUnitFactory(user_course=user_course, unit=unit_2)
    UserUnitFactory(user_course=user_course, unit=unit_3)
    UserUnitFactory(user_course=user_course, unit=unit_4)
    # The student hasn't started ``unit_5``
    with django_assert_num_queries(6):
        result = list(UserUnit.objects.for_user_course(user_course))
    assert ["unit-1", "unit-3", "unit-2", "unit-4"] == [
        x.unit.name for x in result
    ]


@pytest.mark.django_db
def test_init_user_unit():
    course = CourseFactory()
    unit = UnitFactory()
    CourseUnitFactory(course=course, unit=unit, order=1)
    user = UserFactory()
    user_course = UserCourseFactory(course=course, user=user)
    x = UserUnit.objects._init_user_unit(user_course, unit)
    assert unit == x.unit
    assert course == x.user_course.course
    assert user == x.user_course.user


@pytest.mark.django_db
def test_init_user_unit_exists():
    course = CourseFactory()
    unit = UnitFactory()
    CourseUnitFactory(course=course, unit=unit, order=1)
    user = UserFactory()
    user_course = UserCourseFactory(course=course, user=user)
    UserUnitFactory(user_course=user_course, unit=unit)
    assert 1 == UserUnit.objects.count()
    x = UserUnit.objects._init_user_unit(user_course, unit)
    assert 1 == UserUnit.objects.count()
    assert unit == x.unit
    assert course == x.user_course.course
    assert user == x.user_course.user


@pytest.mark.django_db
def test_init_user_unit_undelete():
    course = CourseFactory()
    unit = UnitFactory()
    CourseUnitFactory(course=course, unit=unit, order=1)
    user = UserFactory()
    user_course = UserCourseFactory(course=course, user=user)
    user_unit = UserUnitFactory(user_course=user_course, unit=unit)
    user_unit.set_deleted(UserFactory())
    assert user_unit.is_deleted is True
    x = UserUnit.objects._init_user_unit(user_course, unit)
    x.refresh_from_db()
    assert 1 == UserUnit.objects.count()
    assert unit == x.unit
    assert course == x.user_course.course
    assert user == x.user_course.user
    assert x.is_deleted is False


@pytest.mark.django_db
def test_is_complete():
    user_unit = UserUnitFactory()
    assert user_unit.is_complete() is True


@pytest.mark.django_db
def test_is_complete_assessment_not_submitted():
    course = CourseFactory(name="Course")
    user = UserFactory(username="pat")
    user_course = UserCourse.objects.enrol(user, course)
    unit = UnitFactory(name="Unit 1")
    CourseUnitFactory(course=course, unit=unit, order=1)
    assessment = AssessmentFactory(unit=unit)
    user_unit = UserUnitFactory(user_course=user_course, unit=unit)
    UserAssessmentFactory(user_unit=user_unit, assessment=assessment)
    assert user_unit.is_complete() is False


@pytest.mark.django_db
def test_is_complete_assessment_submitted():
    course = CourseFactory(name="Course")
    user = UserFactory(username="pat")
    user_course = UserCourse.objects.enrol(user, course)
    unit = UnitFactory(name="Unit 1")
    CourseUnitFactory(course=course, unit=unit, order=1)
    assessment = AssessmentFactory(unit=unit)
    user_unit = UserUnitFactory(user_course=user_course, unit=unit)
    UserAssessmentFactory(user_unit=user_unit, assessment=assessment)
    UserAssessment.objects.sit_assessment(
        user_unit,
        assessment,
        SimpleUploadedFile("my-answers.doc", b"file contents"),
    )
    assert user_unit.is_complete() is True


@pytest.mark.django_db
def test_is_current_user_assessment():
    with freeze_time(date(2024, 1, 1)):
        user_course = UserCourseFactory(
            user=UserFactory(),
            course=CourseFactory(),
            # assessed=timezone.now(),
            # result=12,
        )
    user_unit = UserUnitFactory(
        user_course=user_course, assessed=timezone.now(), result=12
    )
    assert user_unit.is_current_user_assessment() is False


@pytest.mark.django_db
@pytest.mark.parametrize(
    "is_assessed,result",
    [
        (True, None),
        (False, None),
        (False, 23),
    ],
)
def test_is_current_user_assessment_not(is_assessed, result):
    with freeze_time(date(2024, 2, 1)):
        user_course = UserCourseFactory(
            user=UserFactory(),
            course=CourseFactory(),
            # assessed=assessed,
            # result=result,
        )
    assessed = None
    if is_assessed:
        assessed = timezone.now()
    user_unit = UserUnitFactory(
        user_course=user_course, assessed=assessed, result=result
    )
    assert user_unit.is_current_user_assessment() is True


@pytest.mark.django_db
def test_is_marking_finished():
    unit = UnitFactory()
    user_unit = UserUnitFactory(
        unit=unit, assessed=timezone.now(), retake_assessed=None, result=1
    )
    UserAssessmentFactory(
        user_unit=user_unit,
        assessment=AssessmentFactory(unit=unit),
        can_retake=False,
    )
    assert user_unit.is_marking_finished() is True


@pytest.mark.django_db
@pytest.mark.parametrize(
    "assessed,retake_assessed,result",
    [
        (None, None, 1),
        (None, timezone.now(), 1),
        (timezone.now(), None, 0),
        (timezone.now(), timezone.now(), 0),
        (timezone.now(), timezone.now(), None),
    ],
)
def test_is_marking_finished_not(assessed, retake_assessed, result):
    unit = UnitFactory()
    user_unit = UserUnitFactory(
        unit=unit,
        assessed=assessed,
        retake_assessed=retake_assessed,
        result=result,
    )
    UserAssessmentFactory(
        user_unit=user_unit,
        assessment=AssessmentFactory(unit=unit),
        can_retake=False,
    )
    assert user_unit.is_marking_finished() is False


@pytest.mark.django_db
def test_is_marking_finished_retake():
    unit = UnitFactory()
    user_unit = UserUnitFactory(
        unit=unit, assessed=None, retake_assessed=timezone.now()
    )
    UserAssessmentFactory(
        user_unit=user_unit,
        assessment=AssessmentFactory(unit=unit),
        can_retake=True,
    )
    UserAssessmentFactory(
        user_unit=user_unit,
        assessment=AssessmentFactory(unit=unit),
        can_retake=False,
    )
    assert user_unit.is_marking_finished_retake() is True


@pytest.mark.django_db
def test_is_marking_finished_retake_not():
    unit = UnitFactory()
    user_unit = UserUnitFactory(unit=unit, assessed=None, retake_assessed=None)
    UserAssessmentFactory(
        user_unit=user_unit,
        assessment=AssessmentFactory(unit=unit),
        can_retake=True,
    )
    UserAssessmentFactory(
        user_unit=user_unit,
        assessment=AssessmentFactory(unit=unit),
        can_retake=False,
    )
    assert user_unit.is_marking_finished_retake() is False


@pytest.mark.django_db
def test_is_retake():
    unit = UnitFactory()
    user_unit = UserUnitFactory(unit=unit)
    UserAssessmentFactory(
        user_unit=user_unit,
        assessment=AssessmentFactory(unit=unit),
        can_retake=True,
    )
    UserAssessmentFactory(
        user_unit=user_unit,
        assessment=AssessmentFactory(unit=unit),
        can_retake=False,
    )
    assert user_unit.is_retake() is True


@pytest.mark.django_db
def test_is_retake_not():
    user_unit = UserUnitFactory()
    assert user_unit.is_retake() is False


@pytest.mark.django_db
@pytest.mark.parametrize(
    "submitted,can_retake,retake_submitted,expected",
    [
        (False, False, True, False),
        (False, True, False, False),
        (True, False, True, True),
        (True, True, False, False),
        (True, True, True, True),
    ],
)
def test_is_submitted(submitted, can_retake, retake_submitted, expected):
    retake_submitted_date = submitted_date = None
    if submitted:
        submitted_date = timezone.now()
    if retake_submitted:
        retake_submitted_date = timezone.now()
    unit = UnitFactory()
    assessment = AssessmentFactory(unit=unit)
    user_unit = UserUnitFactory(
        unit=unit,
        submitted=submitted_date,
        retake_submitted=retake_submitted_date,
    )
    user_assessment = UserAssessmentFactory(
        user_unit=user_unit, assessment=assessment, can_retake=can_retake
    )
    assert user_unit.is_submitted() is expected


@pytest.mark.django_db
def test_is_complete_assessments():
    user_unit = UserUnitFactory()
    assert user_unit.is_complete_assessments() is True


@pytest.mark.django_db
def test_is_complete_assessments_check_deleted():
    unit = UnitFactory()
    assessment_1 = AssessmentFactory(unit=unit)
    assessment_1.set_deleted(UserFactory())
    assessment_2 = AssessmentFactory(unit=unit)
    assessment_2.set_deleted(UserFactory())
    user_unit = UserUnitFactory(unit=unit)
    user_assessment_1 = UserAssessmentFactory(
        user_unit=user_unit,
        assessment=assessment_1,
        can_retake=False,
        answer_submitted=timezone.now(),
    )
    user_assessment_1.set_deleted(UserFactory())
    user_assessment_2 = UserAssessmentFactory(
        user_unit=user_unit,
        assessment=assessment_2,
        can_retake=True,
        retake_submitted=timezone.now(),
    )
    user_assessment_2.set_deleted(UserFactory())
    assert user_unit.is_complete_assessments() is True


@pytest.mark.django_db
def test_is_complete_assessments_not_submitted():
    course = CourseFactory(name="Course")
    user = UserFactory(username="pat")
    user_course = UserCourse.objects.enrol(user, course)
    unit = UnitFactory(name="Unit 1")
    CourseUnitFactory(course=course, unit=unit, order=1)
    assessment = AssessmentFactory(unit=unit)
    user_unit = UserUnitFactory(user_course=user_course, unit=unit)
    UserAssessmentFactory(user_unit=user_unit, assessment=assessment)
    assert user_unit.is_complete_assessments() is False


@pytest.mark.django_db
def test_is_complete_assessments_submitted():
    course = CourseFactory(name="Course")
    user = UserFactory(username="pat")
    user_course = UserCourse.objects.enrol(user, course)
    unit = UnitFactory(name="Unit 1")
    CourseUnitFactory(course=course, unit=unit, order=1)
    assessment = AssessmentFactory(unit=unit)
    user_unit = UserUnitFactory(user_course=user_course, unit=unit)
    UserAssessmentFactory(user_unit=user_unit, assessment=assessment)
    UserAssessment.objects.sit_assessment(
        user_unit,
        assessment,
        SimpleUploadedFile("my-answers.doc", b"file contents"),
    )
    assert user_unit.is_complete_assessments() is True


@pytest.mark.django_db
def test_is_complete_exam_passed():
    exam = ExamFactory()
    exam_version = ExamVersionFactory(exam=exam)
    # course and unit
    course = CourseFactory(name="Course")
    unit = UnitFactory(name="Unit 1")
    CourseUnitFactory(course=course, unit=unit, order=1)
    # link the exam to the unit
    UnitExamFactory(unit=unit, exam=exam)
    # user
    user = UserFactory(username="pat")
    # enrolled on the course
    user_course = UserCourse.objects.enrol(user, course)
    # working on the unit
    user_unit = UserUnitFactory(user_course=user_course, unit=unit)
    # work on the exam
    user_exam_version = UserExamVersionFactory(
        user=user, exam_version=exam_version
    )
    user_exam_version.set_result(16)
    assert user_unit.is_complete() is True


@pytest.mark.django_db
def test_is_complete_exam_not_submitted():
    exam = ExamFactory()
    exam_version = ExamVersionFactory(exam=exam)
    # course and unit
    course = CourseFactory(name="Course")
    unit = UnitFactory(name="Unit 1")
    CourseUnitFactory(course=course, unit=unit, order=1)
    # link the exam to the unit
    UnitExamFactory(unit=unit, exam=exam)
    # user
    user = UserFactory(username="pat")
    # enrolled on the course
    user_course = UserCourse.objects.enrol(user, course)
    # working on the unit
    user_unit = UserUnitFactory(user_course=user_course, unit=unit)
    # work on the exam - but don't submit it!
    UserExamVersionFactory(user=user, exam_version=exam_version)
    assert user_unit.is_complete() is False


@pytest.mark.django_db
def test_result_status():
    user_unit = UserUnitFactory(result=78, assessed=timezone.now())
    assert "78%" == user_unit.result_status()


@pytest.mark.django_db
def test_result_status_no_result():
    user_unit = UserUnitFactory(result=None, assessed=timezone.now())
    assert "Awaiting Completion" == user_unit.result_status()


@pytest.mark.django_db
def test_result_status_not_assessed():
    user_unit = UserUnitFactory(result=91, assessed=None)
    assert "Awaiting Completion" == user_unit.result_status()


@pytest.mark.django_db
def test_result_status_retake():
    unit = UnitFactory()
    assessment = AssessmentFactory(unit=unit)
    user_unit = UserUnitFactory(unit=unit, result=None, assessed=None)
    user_assessment = UserAssessmentFactory(
        user_unit=user_unit, assessment=assessment, can_retake=True
    )
    assert user_unit.is_retake() is True
    assert "Awaiting Retake" == user_unit.result_status()


@pytest.mark.django_db
def test_result_status_retake_submitted():
    unit = UnitFactory()
    assessment = AssessmentFactory(unit=unit)
    user_unit = UserUnitFactory(
        unit=unit, retake_submitted=timezone.now(), result=None, assessed=None
    )
    UserAssessmentFactory(
        user_unit=user_unit, assessment=assessment, can_retake=True
    )
    assert user_unit.is_retake() is True
    assert "Retake Submitted" == user_unit.result_status()


@pytest.mark.django_db
def test_result_status_submitted():
    unit = UnitFactory()
    user_unit = UserUnitFactory(
        unit=unit, submitted=timezone.now(), result=None, assessed=None
    )
    assert user_unit.is_retake() is False
    assert "Submitted" == user_unit.result_status()


@pytest.mark.django_db
def test_str():
    course = CourseFactory(name="Tree")
    user_course = UserCourseFactory(
        course=course, user=UserFactory(username="patrick")
    )
    unit = UnitFactory(name="Fruit")
    CourseUnitFactory(course=course, unit=unit, order=1)
    user_unit = UserUnitFactory(
        unit=UnitFactory(name="Fruit"), user_course=user_course
    )
    assert "patrick: Fruit (for Tree)" == str(user_unit)


@pytest.mark.django_db
def test_submit():
    course = CourseFactory()
    user = UserFactory()
    user_course = UserCourse.objects.enrol(user, course)
    unit = UnitFactory()
    course_unit = CourseUnitFactory(course=course, unit=unit, order=1)
    user_unit = UserUnitFactory(user_course=user_course, unit=unit)
    # test
    with freeze_time(timezone.now()):
        # test
        user_unit.submit()
        # check
        user_course = UserCourse.objects.get(user=user, course=course)
        assert 1 == UserUnit.objects.count()
        user_unit = UserUnit.objects.first()
        assert course == user_unit.user_course.course
        assert user == user_unit.user_course.user
        assert unit == user_unit.unit
        assert timezone.now() == user_unit.submitted
        assert user_unit.retake_submitted is None


@pytest.mark.django_db
def test_submit_already_submitted():
    course = CourseFactory(name="Farming")
    user = UserFactory(username="patrick")
    unit = UnitFactory(name="Fencing")
    user_course = UserCourse.objects.enrol(user, course)
    course_unit = CourseUnitFactory(course=course, unit=unit, order=1)
    user_unit = UserUnitFactory(
        user_course=user_course, unit=unit, submitted=timezone.now()
    )
    assert user_unit.is_retake() is False
    # test
    with pytest.raises(ExamError) as e:
        # UserUnit.objects.submit(user, course_unit)
        user_unit.submit()
    assert (
        "'Fencing' has already been submitted for 'patrick' ({})".format(
            user_unit.pk
        )
    ) in str(e.value)


@pytest.mark.django_db
def test_submit_already_submitted_but_retake():
    """The unit was submitted, then marked, then asking for a retake."""
    course = CourseFactory(name="Farming")
    user = UserFactory(username="patrick")
    unit = UnitFactory(name="Fencing")
    assessment = AssessmentFactory(unit=unit)
    user_course = UserCourse.objects.enrol(user, course)
    course_unit = CourseUnitFactory(course=course, unit=unit, order=1)
    user_unit = UserUnitFactory(
        user_course=user_course, unit=unit, submitted=timezone.now()
    )
    user_assessment = UserAssessmentFactory(
        user_unit=user_unit,
        assessment=assessment,
        can_retake=True,
        retake_submitted=timezone.now(),
    )
    assert user_unit.retake_submitted is None
    assert user_unit.submitted.date() == timezone.now().date()
    assert user_unit.is_retake() is True
    # test
    with freeze_time(timezone.now()):
        user_unit.submit()
        user_unit.refresh_from_db()
        assert user_unit.retake_submitted == timezone.now()


@pytest.mark.django_db
def test_submit_already_submitted_retake():
    """The unit and retake have already been submitted."""
    course = CourseFactory(name="Farming")
    user = UserFactory(username="patrick")
    unit = UnitFactory(name="Fencing")
    assessment = AssessmentFactory(unit=unit)
    user_course = UserCourse.objects.enrol(user, course)
    course_unit = CourseUnitFactory(course=course, unit=unit, order=1)
    user_unit = UserUnitFactory(
        user_course=user_course,
        unit=unit,
        submitted=timezone.now(),
        retake_submitted=timezone.now(),
    )
    user_assessment = UserAssessmentFactory(
        user_unit=user_unit,
        assessment=assessment,
        can_retake=True,
        retake_submitted=timezone.now(),
    )
    assert user_unit.is_retake() is True
    # test
    with pytest.raises(ExamError) as e:
        user_unit.submit()
    assert (
        "'Fencing' has already been submitted for 'patrick' ({})".format(
            user_unit.pk
        )
    ) in str(e.value)


@pytest.mark.django_db
def test_submit_not_complete():
    course = CourseFactory(name="Farming")
    user = UserFactory(username="patrick")
    unit = UnitFactory(name="Fencing")
    user_course = UserCourse.objects.enrol(user, course)
    course_unit = CourseUnitFactory(course=course, unit=unit, order=1)
    assessment = AssessmentFactory(unit=unit)
    user_unit = UserUnitFactory(user_course=user_course, unit=unit)
    UserAssessmentFactory(user_unit=user_unit, assessment=assessment)
    # test
    with pytest.raises(ExamError) as e:
        # UserUnit.objects.submit(user, course_unit)
        user_unit.submit()
    assert (
        "Cannot submit 'Fencing' until all the assessments for "
        "'patrick' have been submitted ({})".format(user_unit.pk)
    ) in str(e.value)


@pytest.mark.django_db
def test_user_unit_from_course_unit():
    course = CourseFactory(name="Farming")
    user = UserFactory(username="patrick")
    # the user is not enrolled on the course!
    unit = UnitFactory(name="Fencing")
    course_unit = CourseUnitFactory(course=course, unit=unit, order=1)
    # test
    with pytest.raises(ExamError) as e:
        UserUnit.objects.user_unit_from_course_unit(user, course_unit)
    assert (
        f"'patrick' (ID {user.pk}) is not enrolled on the 'Farming' "
        f"course (ID {course.pk}) (unit: 'Fencing')" in str(e.value)
    )


@pytest.mark.django_db
def test_user_unit_from_course_unit_not_enrolled_unit():
    course = CourseFactory(name="Farming")
    user = UserFactory(username="patrick")
    # enrol the user on the course
    user_course = UserCourseFactory(user=user, course=course)
    unit = UnitFactory(name="Fencing")
    course_unit = CourseUnitFactory(course=course, unit=unit, order=1)
    # test
    with pytest.raises(ExamError) as e:
        UserUnit.objects.user_unit_from_course_unit(user, course_unit)
    assert (
        f"'patrick' (ID {user.pk}) is not enrolled on the 'Fencing' "
        f"unit (ID {unit.pk}) (course: 'Farming')" in str(e.value)
    )


@pytest.mark.django_db
def test_user_unit_if_exists():
    user_course = UserCourseFactory(course=CourseFactory(), user=UserFactory())
    unit = UnitFactory()
    user_unit = UserUnitFactory(unit=unit, user_course=user_course)
    assert user_unit == UserUnit.objects.user_unit_if_exists(user_course, unit)


@pytest.mark.django_db
def test_user_unit_if_exists_not():
    user_course = UserCourseFactory(course=CourseFactory(), user=UserFactory())
    unit = UnitFactory()
    assert UserUnit.objects.user_unit_if_exists(user_course, unit) is None


@pytest.mark.django_db
def test_with_assessment(django_assert_num_queries):
    course = CourseFactory()
    unit_1 = UnitFactory(name="unit-1")
    unit_2 = UnitFactory(name="unit-2")
    unit_3 = UnitFactory(name="unit-3")
    unit_4 = UnitFactory(name="unit-4")
    unit_5 = UnitFactory(name="unit-5")
    CourseUnitFactory(course=course, unit=unit_1, order=1)
    CourseUnitFactory(course=course, unit=unit_2, order=3)
    CourseUnitFactory(course=course, unit=unit_3, order=2)
    CourseUnitFactory(course=course, unit=unit_4, order=4)
    CourseUnitFactory(course=course, unit=unit_5, order=5)
    AssessmentFactory(unit=unit_1)
    AssessmentFactory(unit=unit_2)
    AssessmentFactory(unit=unit_3)
    # No assessment, so should not be included
    # AssessmentFactory(unit=unit_4)
    AssessmentFactory(unit=unit_5)
    user = UserFactory()
    user_course = UserCourseFactory(user=user, course=course)
    UserUnitFactory(user_course=user_course, unit=unit_1)
    UserUnitFactory(user_course=user_course, unit=unit_2)
    UserUnitFactory(user_course=user_course, unit=unit_3)
    UserUnitFactory(user_course=user_course, unit=unit_4)
    # The student hasn't started ``unit_5``
    with django_assert_num_queries(6):
        result = list(UserUnit.objects.with_assessment(user_course))
    assert ["unit-1", "unit-3", "unit-2"] == [x.unit.name for x in result]
