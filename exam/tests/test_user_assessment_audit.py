# -*- encoding: utf-8 -*-
import pytest

from django.conf import settings
from django.utils import timezone

from exam.models import UserAssessmentAudit, UserAssessmentAuditManager
from exam.tests.factories import (
    UserAssessmentFactory,
    UserUnitFactory,
)

from login.tests.factories import UserFactory


@pytest.mark.django_db
def test_create_user_assessment_audit():
    user_assessment = UserAssessmentFactory()
    x = UserAssessmentAudit.objects.create_user_assessment_audit(
        user=UserFactory(username="John"),
        user_assessment=user_assessment,
        can_retake=True,
        error_count=3,
        assessor_comments="Some comments...",
    )
    assert "John" == x.user.username
    assert user_assessment == x.user_assessment
    assert x.can_retake is True
    assert 3 == x.error_count
    assert "Some comments..." == x.assessor_comments
    assert timezone.now().date() == x.created.date()


@pytest.mark.django_db
def test_ordering():
    UserAssessmentAudit.objects.create_user_assessment_audit(
        user=UserFactory(username="John"),
        user_assessment=UserAssessmentFactory(),
        can_retake=True,
        error_count=3,
        assessor_comments="Some comments...",
    )
    UserAssessmentAudit.objects.create_user_assessment_audit(
        user=UserFactory(username="Patrick"),
        user_assessment=UserAssessmentFactory(),
        can_retake=True,
        error_count=2,
        assessor_comments="Some comments...",
    )

    assert ["Patrick", "John"] == [
        x.user.username for x in UserAssessmentAudit.objects.all()
    ]


@pytest.mark.django_db
def test_str():
    user_assessment = UserAssessmentFactory()
    x = UserAssessmentAudit.objects.create_user_assessment_audit(
        user=UserFactory(username="John"),
        user_assessment=user_assessment,
        can_retake=True,
        error_count=3,
        assessor_comments="Some comments...",
    )
    assert "John" == str(x)
