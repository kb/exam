# -*- encoding: utf-8 -*-
from exam.models import Option, Question
from exam.tests.factories import ExamFactory


def create_option_content(exam_version):
    """Fill in the text of the option (so it isn't empty)."""
    pks = []
    for question in exam_version.questions():
        for option in question.options():
            pks.append(option.pk)
    for pk in pks:
        option = Option.objects.get(pk=pk)
        option.option = "option {}.{}".format(
            option.question.number, option.number
        )
        option.save()


def create_question_content(exam_version):
    """Fill in the text of the question (so it isn't empty)."""
    pks = [obj.pk for obj in exam_version.questions()]
    for pk in pks:
        question = Question.objects.get(pk=pk)
        question.question = "question {}".format(question.number)
        question.save()


def create_answer_for_each_question(exam_version):
    """Set an answer for each question."""
    pks = [obj.pk for obj in exam_version.questions()]
    for pk in pks:
        question = Question.objects.get(pk=pk)
        answer = Option.objects.get(question=question, number=1)
        question.answer = answer
        question.save()


def set_correct_answers(user_exam_version, correct):
    """Set some correct answers.

    Arguments:
    correct -- list of question numbers to set correct e.g. [1, 3, 5]

    """
    qs = Question.objects.filter(exam_version=user_exam_version.exam_version)
    for question in qs:
        if question.number in correct:
            user_exam_version.set_answer(question, question.answer)


def set_correct_answers_all(user_exam_version):
    qs = Question.objects.filter(exam_version=user_exam_version.exam_version)
    for question in qs:
        user_exam_version.set_answer(question, question.answer)


def set_incorrect_answers(user_exam_version, incorrect):
    """Set some incorrect answers.

    Arguments:
    incorrect -- list of question numbers to get wrong e.g. [1, 3, 5]

    """
    qs = Question.objects.filter(exam_version=user_exam_version.exam_version)
    for question in qs:
        options = question.options()
        if question.number in incorrect:
            if question.answer == options.first():
                answer = options.last()
            else:
                answer = options.first()
            user_exam_version.set_answer(question, answer)


def setup_exam(name=None, question_count=None):
    """Create an exam and return the published version."""
    if name:
        exam = ExamFactory(name=name)
    else:
        exam = ExamFactory()
    check = exam.create_questions(question_count)
    version = exam.edit_version()
    assert check == version
    create_question_content(version)
    create_option_content(version)
    create_answer_for_each_question(version)
    version.publish()
    return version
