Management Commands
*******************

``exam_info``
=============

List the exam information::

  django-admin exam_info 15712

.. tip:: The parameter is the contact ID e.g.
         https://www.institutelegalsecretaries.com/member/contact/15712/

.. tip:: Source code in ``exam/management/commands``.

e.g::

  Information for an 'exam' for Miss A Apple (test@yahoo.co.uk)

  Created               Marked               Course
  -------               ------               ------
  07/09/2021 16:49:43   12/09/2021 15:25:52  General Procedures Achievement Test      result=20    (100% %) [marked|pass]
  15/11/2021 15:49:39   15/11/2021 16:46:47  English Legal System Achievement Test    result=14    (70%  %) [fail|marked]
  15/11/2021 16:46:47                        English Legal System Achievement Test    result=      (     %) [retake]


``student-course``
==================

Course details for a student.

Find the course by logging in as the student e.g:
https://ilspa.lms.kbsoftware.co.uk/lms/student/course/1/

::

  django-admin student-course 1

.. tip:: The parameter is the ID of the ``UserCourse``.

.. tip:: Source code in ``exam/management/commands``.

``UserAssessment``
==================

Upload
------

To replace the existing ``answer_file`` for a ``UserAssessment``:

1. Find the ``pk`` of the ``UserAssessment`` using the ``6706-userunit``
   management command (see below).
2. Find the file to upload (``my-new-doc.pdf`` in the example below).

::

  django-admin 6706-userassessment-file-upload 275 ~/Downloads/my-new-doc.pdf

.. tip:: Source code in
         ``exam/management/commands/6706-userassessment-file-upload.py``

``UserUnit``
============

Summary
-------

The only parameter is the ``pk`` of the ``UserUnit``. This can be found by

1. Browsing to the course on the dashboard e.g. ``/exam/user/course/119/``
2. Clicking on the unit e.g. ``/exam/user/unit/342/``

.. image:: ./misc/2023-04-26-user-unit-dashboard.png

The ``pk`` is the ID in the URL e.g::

  django-admin 6706-userunit 342

.. tip:: Source code in ``exam/management/commands/6706-userunit.py``

un-submit
---------

.. tip:: To view the details of the ``UserUnit`` without changing any data,
         use the ``6706-userunit`` management command (see above).

The only parameter is the ``pk`` of the ``UserUnit``. This can be found using
the two steps described in the *UserUnit*, *Summary* (see above).

The ``pk`` is the ID in the URL e.g::

  django-admin 6706-userunit-unsubmit 342

.. tip:: Source code in
         ``exam/management/commands/6706-userunit-unsubmit.py``
