# -*- encoding: utf-8 -*-
import factory

from example_exam.models import Contact


class ContactFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Contact

    team_member = False
