# -*- encoding: utf-8 -*-
from django.conf import settings
from django.db import models
from django.urls import reverse

from exam.models import Course


class Contact(models.Model):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE
    )
    team_member = models.BooleanField(default=False)

    class Meta:
        ordering = ("user__username",)
        verbose_name = "Contact"

    def email(self):
        return self.user.email

    def full_name(self):
        return f"{self.user.first_name} {self.user.last_name}"

    def is_team_member(self):
        """Is this contact a team member?"""
        return self.team_member


class CourseGuidanceManager(models.Manager):
    def _create_course_guidance(self, course, user):
        x = self.model(course=course, created_by=user)
        x.save()
        return x

    def init_course_guidance(self, course, user):
        try:
            x = self.model.objects.get(course=course)
        except self.model.DoesNotExist:
            x = self._create_course_guidance(course, user)
        return x


class CourseGuidance(models.Model):
    """Course guidance.

    Adapted from ``CourseGuidance`` model on the customer system.

    """

    course = models.OneToOneField(Course, on_delete=models.CASCADE)
    page = models.TextField(blank=True)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE
    )
    objects = CourseGuidanceManager()

    class Meta:
        ordering = ("course__order",)
        verbose_name = "Course Guidance"

    def __str__(self):
        if self.page:
            result = "{}: {}".format(self.course.name, self.page)
        else:
            result = "{} (no guidance)".format(self.course.name)
        return result

    def get_caption(self):
        return self.page if self.page else None

    def get_update_url(self):
        return reverse("project.settings")

    def has_guidance(self):
        return bool(self.page)
