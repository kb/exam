# -*- encoding: utf-8 -*-
import pytest

from exam.tests.factories import QuestionFactory


@pytest.mark.django_db
def test_can_edit_answer():
    question = QuestionFactory()
    question.exam_version.date_published = None
    question.exam_version.save()
    assert question.can_edit_answer() is True


@pytest.mark.django_db
def test_can_edit_answer_already_published():
    question = QuestionFactory()
    # the factory will create an exam version which has been published
    assert question.exam_version.date_published is not None
    assert question.can_edit_answer() is False


@pytest.mark.django_db
def test_factory():
    QuestionFactory()


@pytest.mark.django_db
def test_str():
    question = QuestionFactory(number=5, question="Question Five")
    assert "5. Question Five" == str(question)


@pytest.mark.django_db
def test_str_long():
    question = QuestionFactory(
        number=6, question="Twitter did not give a reason for the closure"
    )
    assert "6. Twitter did not give a reason " == str(question)
