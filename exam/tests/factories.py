# -*- encoding: utf-8 -*-
import factory

from django.utils import timezone

from exam.models import (
    Assessment,
    ClassInformation,
    Course,
    CourseProduct,
    CourseUnit,
    Exam,
    ExamSettings,
    ExamVersion,
    Option,
    ProductExam,
    Question,
    Selection,
    Unit,
    UnitExam,
    UserAssessment,
    UserCourse,
    UserExamVersion,
    UserUnit,
)
from login.tests.factories import UserFactory
from stock.tests.factories import ProductFactory, ProductTypeFactory


class ClassInformationFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ClassInformation


class CourseFactory(factory.django.DjangoModelFactory):
    is_tailored = False

    class Meta:
        model = Course

    # @factory.sequence
    # def description(n):
    #     return "description_{:02d}".format(n)

    @factory.sequence
    def name(n):
        return "name_{:02d}".format(n)

    @factory.sequence
    def order(n):
        return n


class CourseProductFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = CourseProduct

    course = factory.SubFactory(CourseFactory)
    product = factory.SubFactory(ProductFactory)


class ExamFactory(factory.django.DjangoModelFactory):
    @factory.sequence
    def name(n):
        return "name_{:02d}".format(n)

    class Meta:
        model = Exam


class ExamSettingsFactory(factory.django.DjangoModelFactory):
    course = factory.SubFactory(ProductTypeFactory)

    class Meta:
        model = ExamSettings


class ExamVersionFactory(factory.django.DjangoModelFactory):
    exam = factory.SubFactory(ExamFactory)
    date_published = factory.LazyFunction(timezone.now)

    class Meta:
        model = ExamVersion


class QuestionFactory(factory.django.DjangoModelFactory):
    @factory.sequence
    def number(n):
        return n + 1

    @factory.sequence
    def question(n):
        return "question_{:02d}".format(n)

    exam_version = factory.SubFactory(ExamVersionFactory)

    class Meta:
        model = Question


class OptionFactory(factory.django.DjangoModelFactory):
    @factory.sequence
    def number(n):
        return n + 1

    @factory.sequence
    def option(n):
        return "option_{:02d}".format(n)

    question = factory.SubFactory(QuestionFactory)

    class Meta:
        model = Option


class ProductExamFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ProductExam

    product = factory.SubFactory(ProductFactory)
    exam = factory.SubFactory(ExamFactory)

    @factory.sequence
    def order(n):
        return n + 1


class SelectionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Selection


class UnitFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Unit

    @factory.sequence
    def name(n):
        return "name_{:02d}".format(n)


class CourseUnitFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = CourseUnit

    order = 1
    course = factory.SubFactory(CourseFactory)
    unit = factory.SubFactory(UnitFactory)


class AssessmentFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Assessment

    unit = factory.SubFactory(UnitFactory)
    blank_form = factory.django.FileField()
    example_form = factory.django.FileField()

    @factory.sequence
    def order(n):
        return n

    @factory.sequence
    def name(n):
        return "name_{:02d}".format(n)

    @factory.sequence
    def instructions(n):
        return "instructions_{:02d}".format(n)


class UnitExamFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = UnitExam

    unit = factory.SubFactory(UnitFactory)
    exam = factory.SubFactory(ExamFactory)


class UserCourseFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = UserCourse

    user = factory.SubFactory(UserFactory)
    course = factory.SubFactory(CourseFactory)
    assessor = factory.SubFactory(UserFactory)

    @factory.sequence
    def assessor_comments(n):
        return "assessor_comments_{:02d}".format(n)


class UserExamVersionFactory(factory.django.DjangoModelFactory):
    exam_version = factory.SubFactory(ExamVersionFactory)

    class Meta:
        model = UserExamVersion


class UserUnitFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = UserUnit

    user_course = factory.SubFactory(UserCourseFactory)
    unit = factory.SubFactory(UnitFactory)
    assessor = factory.SubFactory(UserFactory)

    # @factory.sequence
    # def result(n):
    #     return "result_{:02d}".format(n)

    @factory.sequence
    def assessor_comments(n):
        return "assessor_comments_{:02d}".format(n)


class UserAssessmentFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = UserAssessment

    user_unit = factory.SubFactory(UserUnitFactory)
    assessment = factory.SubFactory(AssessmentFactory)
    answer_file = factory.django.FileField(filename="the_answer_file.doc")

    @factory.sequence
    def answer_original_file_name(n):
        return "the_answer_file_{:02d}".format(n)
