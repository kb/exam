# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from exam.models import (
    Assessment,
    CourseUnit,
    UserAssessment,
    UserCourse,
    UserUnit,
)


class Command(BaseCommand):
    help = "Student course details #3119"

    def add_arguments(self, parser):
        parser.add_argument("user_course_pk", type=int)

    def handle(self, *args, **options):
        user_course_pk = options["user_course_pk"]
        user_course = UserCourse.objects.get(pk=user_course_pk)
        self.stdout.write(
            "{}: ['{}'] {}".format(
                self.help, user_course.user.username, user_course.course.name
            )
        )
        fmt = "{:<6}  {:<4}  {:<50}  {:<20}  {}"
        self.stdout.write(fmt.format("ID", "Type", "Name", "Created", "Status"))
        self.stdout.write(
            fmt.format(
                "------", "----", "-------", "---------", "-------------------"
            )
        )
        user_unit_list = UserUnit.objects.for_user_course(user_course)
        for user_unit in user_unit_list:
            self.stdout.write(
                fmt.format(
                    user_unit.unit.pk,
                    "Unit",
                    user_unit.unit.name[:35],
                    user_unit.created.strftime("%d/%m/%Y %H:%M:%S"),
                    user_unit.result_status(),
                )
            )
            for data in user_unit.assessments():
                assessment = data["assessment"]
                user_assessments = data["user_assessments"]

                if user_assessments:
                    for user_assessment in user_assessments:
                        self.stdout.write(
                            fmt.format(
                                user_assessment.assessment.pk,
                                user_assessment.assessment.unit.assignment_or_assessment[
                                    :4
                                ].title(),
                                user_assessment.assessment.name[:35],
                                user_assessment.created.strftime(
                                    "%d/%m/%Y %H:%M:%S"
                                ),
                                user_assessment.answer_button_caption(),
                            )
                        )
                else:
                    self.stdout.write(
                        fmt.format(
                            assessment.unit.assignment_or_assessment,
                            assessment.unit.assignment_or_assessment[:4],
                            assessment.pk,
                            assessment.name[:35],
                            "",
                            "Not Started",
                        )
                    )
            print()
        self.stdout.write("{} - Complete".format(self.help))
