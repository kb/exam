#!/bin/bash
# exit immediately if a command exits with a nonzero exit status.
set -e
# treat unset variables as an error when substituting.
set -u

echo Drop database: $DATABASE_NAME
if [[ -z "${DATABASE_HOST}" ]]; then
  psql -U $DATABASE_USER -c "DROP DATABASE IF EXISTS $DATABASE_NAME;"
  psql -U $DATABASE_USER -c "CREATE DATABASE $DATABASE_NAME TEMPLATE=template0 ENCODING='utf-8';"
else
  PGPASSWORD=$DATABASE_PASS psql --host $DATABASE_HOST --port $DATABASE_PORT -U $DATABASE_USER -c "DROP DATABASE IF EXISTS $DATABASE_NAME;"
  PGPASSWORD=$DATABASE_PASS psql --host $DATABASE_HOST --port $DATABASE_PORT -U $DATABASE_USER -c "CREATE DATABASE $DATABASE_NAME TEMPLATE=template0 ENCODING='utf-8';"
fi

django-admin migrate --noinput
django-admin demo_data_login
django-admin init_app_exam
# django-admin init_project
django-admin runserver 0.0.0.0:8000
