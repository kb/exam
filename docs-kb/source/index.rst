.. Exam - KB Documentation documentation master file, created by
   sphinx-quickstart on Tue Jan  8 15:52:46 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Exam - Technical Documentation
==============================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   exam
   lms
   management-commands
   testing
   user-feedback

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
