# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand
from django.db import transaction

from exam.models import Exam, Option


class Command(BaseCommand):
    help = "Update an exam with demo questions and answers"

    data = {
        "Samite is a type of what": [
            ("Fabric", True),
            ("Stone", False),
            ("Dog", False),
            ("Cake", False),
        ],
        "Vermillion is a shade of which colour": [
            ("Green", False),
            ("Blue", False),
            ("Red", True),
            ("Yellow", False),
        ],
        "A caparison is an ornamental cloth used to cover a what": [
            ("Alter", False),
            ("Horse", True),
            ("Bed", False),
            ("Window", False),
        ],
        "In which famous building are rooms named Vermeil, China, Red, Blue, Green, and Yellow Oval": [
            ("Buckingham Palace", False),
            ("The Whitehouse", True),
            ("Palace of Versailles", False),
            ("Westminster Abbey", False),
        ],
        "Which 1920s film star's third husband was Henri Le Bailly de la Falaise, Marquis de la Coudraye": [
            ("Clara Bow", False),
            ("Lillian Gish", False),
            ("Mae West", False),
            ("Gloria Swanson", True),
        ],
        "In anatomy, 'plantar' relates to which part of the human body": [
            ("Foot", True),
            ("Stomach", False),
            ("Head", False),
            ("Hand", False),
        ],
        "A 2010 publicity-driven competition called the Carbuncle Cup focused on unpopular British what": [
            ("Architecture", True),
            ("Politicians", False),
            ("Corporations", False),
            ("Law", False),
        ],
        "Which English royal house held the throne between 1154 and 1485": [
            ("Stewart", False),
            ("Tudor", False),
            ("Plantagenet", True),
            ("Lancaster", False),
        ],
        "Sfumato is a technique in what": [
            ("Painting", True),
            ("Cooking", False),
            ("Martial Arts", False),
            ("Meditation", False),
        ],
        "If something coruscates, what does it do": [
            ("Expands", False),
            ("Fades", False),
            ("Sparkles", True),
            ("Shrinks", False),
        ],
        "Evo Morales became president of which country in 2006": [
            ("Bolivia", True),
            ("Argentina", False),
            ("Ecuador", False),
            ("Peru", False),
        ],
        "The port of Mocha is in which country": [
            ("Somalia", False),
            ("Oman", False),
            ("Iran", False),
            ("Yemen", True),
        ],
        "A nide is a brood or nest of which type of birds": [
            ("Emus", False),
            ("Sparrows", False),
            ("Swans", False),
            ("Pheasants", True),
        ],
        "A Munro, of which there are 283, is a Scottish mountain which is over how many feet high": [
            ("1,000", False),
            ("2,000", False),
            ("3,000", True),
            ("4,000", False),
        ],
        "Literally meaning repentance in Italian, what is the visible trace of an earlier painting beneath newer artwork on canvas": [
            ("Pimento", False),
            ("Paliamento", False),
            ("Pinto", False),
            ("Pentimento", True),
        ],
        "An anemometer is a guage used for recording the speed of what": [
            ("Light", False),
            ("Spacecraft", False),
            ("Wind", True),
            ("Athletes", False),
        ],
        "In the UK in the 1930s, what was named after the Minister of Transport": [
            ("Pelican Crossing", False),
            ("Shadwell Station", False),
            ("Belisha Beacon", True),
            ("Ashgrove College", False),
        ],
        "Which of the Rolling Stones has a cameo role as Captain Jack Teague, father of Jack Sparrow in 'Pirates of the Caribbean, At World's End": [
            ("Mick Jagger", False),
            ("Charlie Watts", False),
            ("Keith Richards", True),
            ("Ronnie Wood", False),
        ],
        "Why did John B. Watson reject the structuralist study of mental events?": [
            (
                "He believed that structuralism relied too heavily on scientific methods",
                False,
            ),
            (
                "He rejected the concept that psychologists should study observable behavior",
                False,
            ),
            (
                "He believed that scientists should focus on what is objectively observable",
                True,
            ),
            (
                "He actually embraced both structuralism and functionalism",
                False,
            ),
        ],
        "Explaining a student's poor performance on an exam to the unfair difficulty level of the questions refers to what kind of cause?": [
            ("immediate, external cause", True),
            ("immediate, internal cause", False),
            ("developmental cause", False),
            ("necessary and sufficient cause", False),
        ],
    }

    def sample(self):
        for k, v in self.data.items():
            yield k, v

    def add_arguments(self, parser):
        parser.add_argument("exam_pk", nargs="+", type=int)

    def handle(self, *args, **options):
        pks = options["exam_pk"]
        for pk in pks:
            try:
                exam = Exam.objects.get(pk=pk)
                questions = exam.edit_questions()
                self.stdout.write(
                    "Exam '{}' has {} questions".format(
                        exam.pk, questions.count()
                    )
                )
                with transaction.atomic():
                    sample = self.sample()
                    for question in questions:
                        try:
                            q, a = next(sample)
                        except StopIteration:
                            pass
                        self.stdout.write("{}. {}".format(question.pk, q))
                        correct_answer = None
                        for count, answer in enumerate(a):
                            option, correct = answer
                            self.stdout.write(
                                "{}. {} = {}".format(count, option, correct)
                            )
                            number = count + 1
                            obj = Option.objects.get(
                                question=question, number=number
                            )
                            obj.option = option
                            obj.save()
                            if correct:
                                correct_answer = obj
                        if not correct_answer:
                            raise Exception("No correct answer")
                        question.question = q
                        question.answer = correct_answer
                        question.save()
            except Exam.DoesNotExist:
                self.stderr.write("Exam '{}' does not exist".format(pk))
        self.stdout.write("Complete")
