# -*- encoding: utf-8 -*-
import pathlib
import pytest

from django.contrib.auth.models import AnonymousUser
from django.core.files.uploadedfile import SimpleUploadedFile

from exam.models import Assessment
from exam.tests.factories import (
    AssessmentFactory,
    CourseFactory,
    CourseUnitFactory,
    UnitFactory,
    UserCourseFactory,
)
from login.tests.factories import UserFactory


@pytest.mark.django_db
def test_assessment_type_default():
    assessment = AssessmentFactory()
    assert assessment.is_uploaded() is True


@pytest.mark.django_db
def test_assessment_type_unknown():
    assessment = AssessmentFactory(assessment_type="X")
    assert assessment.is_uploaded() is False
    assert assessment.is_written() is False


@pytest.mark.django_db
def test_blank_form_download_file_name():
    assessment = AssessmentFactory(
        name="2a | Letter of Claim",
        blank_form=SimpleUploadedFile("my_blank_form.pdf", b"file contents"),
    )
    assert (
        "2a-Letter-of-Claim-Blank-Form.pdf"
        == assessment.blank_form_download_file_name()
    )


@pytest.mark.django_db
def test_create_assessment():
    unit = UnitFactory()
    x = Assessment.objects.create_assessment(
        unit, 4, "My name", "My instructions", "What do we do?"
    )
    assert int(x.pk) > 0
    assert unit == x.unit
    assert 4 == x.order == 4
    assert "My name" == x.name
    assert "My instructions" == x.instructions
    assert "What do we do?" == x.what_to_do


@pytest.mark.django_db
def test_example_form_download_file_name():
    sample_pdf_file = pathlib.Path(
        pathlib.Path.cwd(), "exam", "tests", "data", "sample.pdf"
    )
    with open(sample_pdf_file, "rb") as f:
        example_form = SimpleUploadedFile("my_blank_form.pdf", f.read())
    assessment = AssessmentFactory(
        name="2a | Letter of Claim", example_form=example_form
    )
    assert (
        "2a-Letter-of-Claim-Example-Form.pdf"
        == assessment.example_form_download_file_name()
    )


@pytest.mark.django_db
def test_init_assessment():
    unit = UnitFactory()
    x1 = Assessment.objects.init_assessment(
        unit, 1, "My name", "My instructions", "Orange"
    )
    x2 = Assessment.objects.init_assessment(
        unit, 1, "My name", "My instructions", "Apple"
    )
    assert int(x1.pk) > 0
    assert x1.pk == x2.pk
    x2.refresh_from_db()
    assert "Apple" == x2.what_to_do


@pytest.mark.django_db
def test_is_uploaded():
    assessment = AssessmentFactory(
        assessment_type=Assessment.ASSESSMENT_UPLOADED
    )
    assert assessment.is_uploaded() is True


@pytest.mark.django_db
def test_is_user_enrolled_or_staff():
    user = UserFactory()
    course = CourseFactory()
    user_course = UserCourseFactory(user=user, course=course)
    unit = UnitFactory()
    CourseUnitFactory(course=course, unit=unit)
    assessment = AssessmentFactory(unit=unit)
    assert assessment.is_user_enrolled_or_staff(user) is True


@pytest.mark.django_db
def test_is_user_enrolled_or_staff_anonymous():
    assessment = AssessmentFactory()
    assert assessment.is_user_enrolled_or_staff(AnonymousUser) is False


@pytest.mark.django_db
def test_is_user_enrolled_or_staff_not_enrolled():
    user = UserFactory()
    course = CourseFactory()
    unit = UnitFactory()
    CourseUnitFactory(course=course, unit=unit)
    assessment = AssessmentFactory(unit=unit)
    assert assessment.is_user_enrolled_or_staff(user) is False


@pytest.mark.django_db
def test_is_user_enrolled_or_staff_not_enrolled_but_staff():
    user = UserFactory(is_staff=True)
    assessment = AssessmentFactory()
    assert assessment.is_user_enrolled_or_staff(user) is True


@pytest.mark.django_db
def test_is_written():
    assessment = AssessmentFactory(
        assessment_type=Assessment.ASSESSMENT_WRITTEN
    )
    assert assessment.is_written() is True


@pytest.mark.django_db
def test_str():
    unit = UnitFactory(name="Fruit")
    assessment = AssessmentFactory(unit=unit, name="Orange")
    assert "Orange for Fruit" == str(assessment)
