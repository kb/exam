# -*- encoding: utf-8 -*-
import pytest

from exam.tests.factories import (
    OptionFactory,
    QuestionFactory,
    SelectionFactory,
    UserExamVersionFactory,
)
from login.tests.factories import UserFactory


@pytest.mark.django_db
def test_factory():
    user = UserFactory()
    user_exam_version = UserExamVersionFactory(user=user)
    question = QuestionFactory(exam_version=user_exam_version.exam_version)
    option = OptionFactory(question=question)
    SelectionFactory(user_exam_version=user_exam_version, answer=option)


@pytest.mark.django_db
def test_factory_answer():
    user = UserFactory()
    user_exam_version = UserExamVersionFactory(user=user)
    question = QuestionFactory(exam_version=user_exam_version.exam_version)
    option = OptionFactory(question=question)
    SelectionFactory(user_exam_version=user_exam_version, answer=option)


@pytest.mark.django_db
def test_correct_answer():
    user = UserFactory()
    user_exam_version = UserExamVersionFactory(user=user)
    question = QuestionFactory(exam_version=user_exam_version.exam_version)
    option = OptionFactory(question=question)
    question.answer = option
    question.save()
    selection = SelectionFactory(
        user_exam_version=user_exam_version, answer=option
    )
    assert selection.correct_answer() is True


@pytest.mark.django_db
def test_correct_answer_not():
    user = UserFactory()
    user_exam_version = UserExamVersionFactory(user=user)
    question = QuestionFactory(exam_version=user_exam_version.exam_version)
    option = OptionFactory(question=question)
    question.answer = option
    question.save()
    selection = SelectionFactory(
        user_exam_version=user_exam_version,
        answer=OptionFactory(question=question),
    )
    assert selection.correct_answer() is False
