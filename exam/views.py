# -*- encoding: utf-8 -*-
import humanize
import mimetypes

from braces.views import (
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    UserPassesTestMixin,
)
from django.contrib import messages
from django.contrib.auth import get_user_model, REDIRECT_FIELD_NAME
from django.core.exceptions import ImproperlyConfigured, PermissionDenied
from django.db import transaction
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.utils import timezone
from django.utils.html import escape
from django.views.decorators.cache import never_cache
from django_sendfile import sendfile
from django.views.generic import (
    CreateView,
    DetailView,
    ListView,
    RedirectView,
    UpdateView,
)
from operator import itemgetter
from pathlib import Path

from base.url_utils import url_with_querystring
from base.view_utils import BaseMixin, RedirectNextMixin
from exam.forms import (
    AssessmentEmptyForm,
    AssessmentForm,
    ClassInformationForm,
    CourseEmptyForm,
    CourseForm,
    CourseUnitCreateForm,
    CourseUnitEmptyForm,
    CourseUnitUpdateForm,
    ExamForm,
    ExamSettingsForm,
    ExamVersionEmptyForm,
    OptionFormSet,
    QuestionForm,
    UnitEmptyForm,
    UnitExamEmptyForm,
    UnitExamForm,
    UnitForm,
    UserAssessmentForm,
    UserAssessmentRetakeForm,
    UserCourseCoreCreateForm,
    UserCourseCreateForm,
    UserCourseEmptyForm,
    UserCourseForm,
    UserCourseProfileForm,
    UserExamQuestionForm,
    UserExamVersionEmptyForm,
    UserExamVersionPercentForm,
    UserUnitCommentForm,
    UserUnitResultForm,
    UserUnitRetakeCommentForm,
)
from exam.models import (
    Assessment,
    check_perm_user_unit,
    ClassInformation,
    Course,
    CourseProduct,
    CourseUnit,
    Exam,
    ExamError,
    ExamSettings,
    ExamVersion,
    get_contact_model,
    get_course_guidance_model,
    is_staff_or_team_member,
    is_team_member_not_staff,
    ProductExam,
    Question,
    Unit,
    UnitExam,
    UserAssessment,
    UserAssessmentAudit,
    UserCourse,
    UserExamVersion,
    UserExamVersionProduct,
    UserUnit,
    UserUnitAudit,
)
from exam.report import ReportAssessment, ReportUserExamVersion
from mail.service import queue_mail_template
from mail.tasks import process_mail
from stock.models import Product


def _contact(contact_pk):
    return get_contact_model().objects.get(pk=contact_pk)


def _submit_exam_for_marking(user_exam_version, products_for_user):
    with transaction.atomic():
        user_exam_version.mark()
        found = False
        # is this an LMS course?
        course_units = user_exam_version.find_course_units()
        user_course_pks = UserCourse.objects.current(
            user_exam_version.user
        ).values_list("course__pk", flat=True)
        for course_unit in course_units:
            # check the user is enrolled on the course
            if course_unit.course.pk in user_course_pks:
                found = True
                break
        # legacy (non LMS) courses
        if not found:
            UserExamVersionProduct.objects.link_user_exam_version_product(
                user_exam_version, products_for_user
            )


def _user_assessment_for_download(request, pk):
    try:
        user_assessment = UserAssessment.objects.get(pk=pk)
    except UserAssessment.DoesNotExist:
        raise PermissionDenied
    if (
        is_staff_or_team_member(request.user)
        or request.user == user_assessment.user_unit.user_course.user
    ):
        return user_assessment
    else:
        raise PermissionDenied


def _sort_current_to_mark(data):
    result = []
    for row in data:
        submitted = row["retake_submitted"]
        if not submitted:
            submitted = row["submitted"]
        result.append([submitted, row])
    result = sorted(result, key=itemgetter(0), reverse=True)
    return [x[1] for x in result]


@never_cache
def download_user_assessment_answer_file(request, pk):
    """Download the answer file."""
    user_assessment = _user_assessment_for_download(request, pk)
    if user_assessment.answer_file:
        return sendfile(
            request,
            user_assessment.answer_file.path,
            attachment=True,
            attachment_filename=user_assessment.answer_file_name().replace(
                "_", " "
            ),
        )
    else:
        raise ExamError(
            "User assessment {} does not have an answer file".format(
                user_assessment.pk
            )
        )


@never_cache
def download_user_assessment_retake_answer_file(request, pk):
    """Download the answer file."""
    user_assessment = _user_assessment_for_download(request, pk)
    if user_assessment.retake_answer_file:
        file_name = user_assessment.retake_answer_file_name().replace("_", " ")
        return sendfile(
            request,
            user_assessment.retake_answer_file.path,
            attachment=True,
            attachment_filename=file_name,
        )
    else:
        raise ExamError(
            "User assessment {} does not have an answer "
            "file for the retake".format(user_assessment.pk)
        )


@never_cache
def download_assessment_blank_form(request, pk):
    """Download the blank form for the assessment."""
    assessment = Assessment.objects.get(pk=pk)
    if assessment.is_user_enrolled_or_staff(request.user):
        if assessment.blank_form:
            mime_type, _ = mimetypes.guess_type(assessment.blank_form.path)
            return sendfile(
                request,
                assessment.blank_form.path,
                attachment=True,
                attachment_filename=Path(
                    assessment.blank_form_download_file_name()
                ),
                mimetype=mime_type,
            )
        else:
            raise ExamError(
                f"Assessment {assessment.pk} (for '{request.user.username}') "
                "does not have a blank form"
            )
    else:
        raise PermissionDenied


@never_cache
def download_assessment_example_form(request, pk):
    """Download the example form for the assessment."""
    assessment = Assessment.objects.get(pk=pk)
    if assessment.is_user_enrolled_or_staff(request.user):
        if assessment.example_form:
            mime_type, _ = mimetypes.guess_type(assessment.example_form.path)
            return sendfile(
                request,
                assessment.example_form.path,
                attachment=True,
                attachment_filename=Path(
                    assessment.example_form_download_file_name()
                ),
                mimetype=mime_type,
            )
        else:
            raise ExamError(
                f"Assessment {assessment.pk} (for '{request.user.username}') "
                "does not have an example form"
            )
    else:
        raise PermissionDenied


@never_cache
def download_unit_course_materials(request, pk):
    """Download the course materials for a unit."""
    has_perm = False
    if request.user.is_anonymous:
        pass
    else:
        try:
            unit = Unit.objects.get(pk=pk)
            has_perm = check_perm_user_unit(request.user, unit)
        except Unit.DoesNotExist:
            pass
    if has_perm:
        if unit.course_material:
            return sendfile(
                request,
                unit.course_material.path,
                attachment=True,
                attachment_filename=Path(
                    unit.course_material.name
                ).name.replace("_", " "),
            )
        else:
            raise ExamError(
                "Unit {} (for '{}') does not have any course "
                "material".format(unit.pk, request.user.username)
            )
    else:
        raise PermissionDenied


@never_cache
def download_assessment(request, assessment_pk):
    """Download a PDF report showing assessment."""
    try:
        assessment = Assessment.objects.get(pk=assessment_pk)
    except Assessment.DoesNotExist:
        raise PermissionDenied
    has_perm = request.user.is_staff
    if has_perm:
        pass
    elif request.user.is_anonymous:
        pass
    else:
        # is the user enrolled on a course?
        unit_pks = UserUnit.objects.filter(
            user_course__user=request.user
        ).values_list("unit__pk", flat=True)
        has_perm = assessment.unit.pk in unit_pks
    if has_perm:
        response = HttpResponse(content_type="application/pdf")
        file_name = assessment.assessment_download_file_name()
        response["Content-Disposition"] = f'attachment; filename="{file_name}"'
        report = ReportAssessment()
        report.report(assessment, request.user, response)
        return response
    else:
        raise PermissionDenied


@never_cache
def download_user_exam_version(request, pk):
    """Download a PDF report showing exam results for a user."""
    try:
        user_exam_version = UserExamVersion.objects.get(pk=pk)
    except UserExamVersion.DoesNotExist:
        raise PermissionDenied
    if request.user.is_staff or request.user == user_exam_version.user:
        response = HttpResponse(content_type="application/pdf")
        file_name = "test-{}.pdf".format(user_exam_version.pk)
        response["Content-Disposition"] = 'attachment; filename="{}"'.format(
            file_name
        )
        report = ReportUserExamVersion()
        report.report(user_exam_version, request.user, response)
        return response
    else:
        raise PermissionDenied


def _user_exam_version_redirect_url(user_exam_version):
    """Do we show the result or detail page?

    Show result page if the user has:

    - passed the exam
    - failed the exam retake

    """
    if user_exam_version.is_pass() or (
        user_exam_version.is_fail() and user_exam_version.previous_test()
    ):
        url = reverse(
            "web.exam.user.version.result", args=[user_exam_version.pk]
        )
    else:
        url = reverse(
            "web.exam.user.version.update", args=[user_exam_version.pk]
        )
    return url


# class UnitMixin:
#     def _unit(self):
#         unit_pk = self.kwargs["unit_pk"]
#         return Unit.objects.get(pk=unit_pk)
#
#     def _userunit(self, user_course, unit):
#         return UserUnit.objects.userunit_if_started(user_course, unit)
#
#     def get_success_url(self):
#         unit = self._unit()
#         return reverse("exam.unit.detail", args=[unit.pk])
#
#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         unit = self._unit()
#         userunit = None
#         # Can user take this Unit?
#         can_user_start = False
#         user_course = UserCourse.objects.is_user_enrolled(
#             self.request.user, unit.course
#         )
#         if user_course:
#             can_user_start = UserUnit.objects.can_user_start(user_course, unit)
#         if can_user_start:
#             # Has the user taken this Unit?
#             userunit = self._userunit(user_course, unit)
#         context.update(
#             dict(unit=unit, can_user_start=can_user_start, userunit=userunit)
#         )
#         return context


class AssessmentCreateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, CreateView
):
    form_class = AssessmentForm
    model = Assessment

    def _unit(self):
        unit_pk = self.kwargs["unit_pk"]
        return Unit.objects.get(pk=unit_pk)

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.unit = self._unit()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        unit = self._unit()
        courses_for_unit = CourseUnit.objects.courses_for_unit(self.object)
        context.update(dict(courses_for_unit=courses_for_unit, unit=unit))
        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({"unit": self._unit()})
        return kwargs

    def get_success_url(self):
        unit = self._unit()
        return reverse("exam.unit.detail", args=[unit.pk])


class AssessmentDeleteView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    form_class = AssessmentEmptyForm
    model = Assessment
    template_name = "exam/assessment_delete_form.html"

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.set_deleted(self.request.user)
        messages.info(self.request, "Deleted {}".format(self.object.name))
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse("exam.unit.detail", args=[self.object.unit.pk])


class AssessmentUpdateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    model = Assessment
    form_class = AssessmentForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        # is an update, so 'unit' is 'self.instance.unit' on the form
        kwargs.update({"unit": None})
        return kwargs

    def get_success_url(self):
        return reverse("exam.unit.detail", args=[self.object.unit.pk])


class ClassInformationCreateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, CreateView
):
    form_class = ClassInformationForm
    model = ClassInformation

    def get_success_url(self):
        return reverse("exam.class.information.list")


class ClassInformationListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    model = ClassInformation


class CourseCreateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, CreateView
):
    """Also see ``UserCourseCreateView``."""

    form_class = CourseForm
    model = Course

    def form_valid(self, form):
        self.object = form.save(commit=False)
        products = form.cleaned_data["products"]
        with transaction.atomic():
            self.object.is_tailored = False
            result = super().form_valid(form)
            for product in products:
                CourseProduct.objects.init_course_product(self.object, product)
        return result

    def get_success_url(self):
        return reverse("exam.course.detail", args=[self.object.pk])


class CourseDeleteView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    form_class = CourseEmptyForm
    model = Course
    template_name = "exam/course_delete_form.html"

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.set_deleted(self.request.user)
        messages.info(self.request, "Deleted {}".format(self.object.name))
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse("exam.course.list")


class CourseDetailView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, DetailView
):
    model = Course

    def _back_url_caption(self):
        if self.object.is_tailored:
            user_course = UserCourse.objects.get(course=self.object)
            contact = get_contact_model().objects.get(user=user_course.user)
            return (
                reverse("contact.detail", args=[contact.pk]),
                contact.full_name,
            )
        else:
            return reverse("exam.course.list"), "Courses"

    def _course_guidance(self):
        return get_course_guidance_model().objects.init_course_guidance(
            self.object, self.request.user
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        course_units = CourseUnit.objects.for_course(self.object)
        course_products = CourseProduct.objects.for_course(
            self.object
        ).order_by("product__name")
        back_url, caption = self._back_url_caption()
        context.update(
            dict(
                # assignment_or_assessment=self.object.get_assignment_or_assessment(
                #     1
                # ).title(),
                back_url=back_url,
                caption_back=caption,
                course_guidance=self._course_guidance(),
                course_units=course_units,
                course_products=course_products,
            )
        )
        return context


class CourseMixin:
    def _course(self):
        course_pk = self.kwargs["course_pk"]
        course = Course.objects.get(pk=course_pk)
        return course

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(course=self._course()))
        return context

    def get_success_url(self):
        course = self._course()
        return reverse("exam.course.detail", args=[course.pk])


class CourseUnitCreateView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    CourseMixin,
    BaseMixin,
    CreateView,
):
    form_class = CourseUnitCreateForm
    model = CourseUnit

    def form_valid(self, form):
        """Link the course to the unit (or undelete existing)."""
        self.object = form.save(commit=False)
        course = self._course()
        order = int(form.cleaned_data["ordering"])
        try:
            course_unit = CourseUnit.objects.get(
                course=course, unit=self.object.unit
            )
            if course_unit.is_deleted:
                course_unit.order = order
                course_unit.undelete()
            else:
                # we should never get here because the unit is already
                # linked to the course.  The unit should not appear in the
                # list of units on the 'CourseUnitForm' ('units_available').
                raise ExamError(
                    "Course unit {} exists, but is not deleted, so "
                    "how did we get here?".format(course_unit.pk)
                )
            return HttpResponseRedirect(self.get_success_url())
        except CourseUnit.DoesNotExist:
            self.object.course = self._course()
            self.object.order = order
            return super().form_valid(form)

    def get_form_kwargs(self):
        result = super().get_form_kwargs()
        result.update(dict(course=self._course()))
        return result


class CourseUnitDeleteView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    form_class = CourseUnitEmptyForm
    model = CourseUnit
    template_name = "exam/course_unit_delete_form.html"

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.set_deleted(self.request.user)
        messages.info(
            self.request,
            "Removed unit '{}' from '{}'".format(
                self.object.unit.name, self.object.course.name
            ),
        )
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse("exam.course.detail", args=[self.object.course.pk])


class CourseUnitUpdateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    form_class = CourseUnitUpdateForm
    model = CourseUnit
    template_name = "exam/course_unit_update_form.html"

    def form_valid(self, form):
        """Link the course to the unit (or undelete existing)."""
        self.object = form.save(commit=False)
        order = int(form.cleaned_data["ordering"])
        self.object.order = order
        return super().form_valid(form)

    def get_initial(self):
        result = super().get_initial()
        result.update(dict(ordering=self.object.order))
        return result

    def get_success_url(self):
        return reverse("exam.course.detail", args=[self.object.course.pk])


class CourseUpdateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    form_class = CourseForm
    model = Course

    def form_valid(self, form):
        """Link the course to the unit (or undelete existing)."""
        self.object = form.save(commit=False)
        products = form.cleaned_data["products"]
        with transaction.atomic():
            # delete existing
            qs = CourseProduct.objects.for_course(self.object)
            for course_product in qs:
                course_product.set_deleted(self.request.user)
            # init new
            for product in products:
                CourseProduct.objects.init_course_product(self.object, product)
            result = super().form_valid(form)
        return result

    def get_initial(self):
        result = super().get_initial()
        product_pks = CourseProduct.objects.for_course(self.object).values_list(
            "product__pk", flat=True
        )
        result.update(dict(products=list(product_pks)))
        return result

    def get_success_url(self):
        return reverse("exam.course.detail", args=[self.object.pk])


class CourseListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    def get_queryset(self):
        return Course.objects.current()


class ExamCreateMixin:
    form_class = ExamForm
    model = Exam

    def form_valid(self, form):
        with transaction.atomic():
            self.object = form.save()
            self.object.create_questions()
            products = form.cleaned_data["products"]
            for product in products:
                ProductExam.objects.init_product_exam(product, self.object)
        return HttpResponseRedirect(self.get_success_url())


class ExamCreateView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    ExamCreateMixin,
    BaseMixin,
    CreateView,
):
    def get_success_url(self):
        return reverse("exam.detail", args=[self.object.pk])


class ExamDetailMixin:
    model = Exam

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        edit_version = self.object.edit_version()
        can_publish, message = edit_version.can_publish()
        context.update(
            dict(
                can_publish=can_publish,
                edit_version=edit_version,
                exam_versions=self.object.exam_versions(),
                products=ProductExam.objects.products(self.object),
            )
        )
        if not can_publish:
            messages.info(self.request, message)
        return context


class ExamDetailView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    ExamDetailMixin,
    BaseMixin,
    DetailView,
):
    pass


class ExamListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    model = Exam


class ExamSettingsUpdateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    form_class = ExamSettingsForm
    template_name = "exam/exam_settings_form.html"

    def get_object(self):
        return ExamSettings.load()

    def get_success_url(self):
        return reverse("project.settings")


class ExamUpdateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    form_class = ExamForm
    model = Exam

    def form_valid(self, form):
        with transaction.atomic():
            self.object = form.save(commit=False)
            products = form.cleaned_data["products"]
            for product in products:
                ProductExam.objects.init_product_exam(product, self.object)
            # remove previous products
            original = ProductExam.objects.products(exam=self.object)
            for product in original:
                if product not in products:
                    ProductExam.objects.set_deleted(
                        self.request.user, product, self.object
                    )
            result = super().form_valid(form)
        return result

    def get_initial(self):
        result = super().get_initial()
        try:
            products = ProductExam.objects.products(exam=self.object)
            product_pks = [obj.pk for obj in products]
            result.update(dict(products=product_pks))
        except ProductExam.DoesNotExist:
            pass
        return result

    def get_success_url(self):
        return reverse("exam.detail", args=[self.object.pk])


class ExamVersionUserListMixin:
    paginate_by = 10

    def _exam_version(self):
        pk = self.kwargs["pk"]
        return ExamVersion.objects.get(pk=pk)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        exam_version = self._exam_version()
        context.update(dict(exam=exam_version.exam, exam_version=exam_version))
        return context

    def get_queryset(self):
        exam_version = self._exam_version()
        return exam_version.user_exam_versions()


class ExamVersionUserListView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    ExamVersionUserListMixin,
    BaseMixin,
    ListView,
):
    pass


class ExamVersionDetailMixin:
    model = ExamVersion

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        can_publish, message = self.object.can_publish()
        context.update(
            dict(
                can_publish=can_publish,
                questions=self.object.questions().order_by("number"),
            )
        )
        return context


class ExamVersionDetailView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    ExamVersionDetailMixin,
    BaseMixin,
    DetailView,
):
    pass


class ExamVersionPublishMixin:
    form_class = ExamVersionEmptyForm
    model = ExamVersion

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        can_publish, message = self.object.can_publish()
        is_valid = ProductExam.objects.is_valid(self.object.exam)
        context.update(
            dict(can_publish=can_publish, is_valid=is_valid, message=message)
        )
        if not can_publish:
            messages.error(self.request, message)
        return context

    def form_valid(self, form):
        self.object = form.save(commit=False)
        if not ProductExam.objects.is_valid(self.object.exam):
            raise ExamError(
                "No course permissions assigned to "
                "exam '{}'".format(self.object.exam.pk)
            )
        with transaction.atomic():
            self.object.publish()
            messages.info(
                self.request,
                "Test number {} has been published".format(self.object.pk),
            )
        return HttpResponseRedirect(self.get_success_url())


class ExamVersionPublishView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    ExamVersionPublishMixin,
    BaseMixin,
    UpdateView,
):
    template_name = "exam/exam_version_form_publish.html"

    def get_success_url(self):
        return reverse("exam.detail", args=[self.object.exam.pk])


class QuestionUpdateMixin:
    """Update the question and options.

    Formset code copied from:
    http://www.web-mech.com/blog/django-createviewupdateview-inline-formsets/

    """

    form_class = QuestionForm
    model = Question

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        formset = OptionFormSet(queryset=self.object.options())
        if not self.object.can_edit_answer():
            messages.warning(
                request,
                (
                    "The answer for this question cannot be changed "
                    "because it has already been published"
                ),
            )
        return self.render_to_response(
            self.get_context_data(form=form, formset=formset)
        )

    def get_form_kwargs(self):
        result = super().get_form_kwargs()
        result.update(dict(question=self.object))
        return result

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        formset = OptionFormSet(self.request.POST)
        if form.is_valid() and formset.is_valid():
            return self.form_valid(form, formset)
        return self.form_invalid(form, formset)

    def form_valid(self, form, formset):
        with transaction.atomic():
            formset.save()
            result = super().form_valid(form)
        return result

    def form_invalid(self, form, formset):
        return self.render_to_response(
            self.get_context_data(form=form, formset=formset)
        )


class QuestionUpdateView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    QuestionUpdateMixin,
    BaseMixin,
    UpdateView,
):
    def get_success_url(self):
        return reverse(
            "exam.version.detail", args=[self.object.exam_version.pk]
        )


class UnitCreateView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    BaseMixin,
    CreateView,
):
    form_class = UnitForm
    model = Unit


class UnitDeleteView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    form_class = UnitEmptyForm
    model = Unit
    template_name = "exam/unit_delete_form.html"

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.set_deleted(self.request.user)
        messages.info(self.request, "Deleted {}".format(self.object.name))
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        courses_for_unit = CourseUnit.objects.courses_for_unit(self.object)
        context.update(dict(courses_for_unit=courses_for_unit))
        return context

    def get_success_url(self):
        return reverse("exam.unit.list")


class UnitDetailView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, DetailView
):
    model = Unit

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        courses_for_unit = CourseUnit.objects.courses_for_unit(self.object)
        context.update(
            dict(
                assignment_or_assessment=self.object.get_assignment_or_assessment(
                    1
                ).title(),
                courses_for_unit=courses_for_unit,
            )
        )
        return context


class UnitExamDeleteView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    form_class = UnitExamEmptyForm
    model = UnitExam
    template_name = "exam/unit_exam_delete_form.html"

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.set_deleted(self.request.user)
        messages.info(
            self.request,
            "Deleted {} from {}".format(
                self.object.exam.name, self.object.unit.name
            ),
        )
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse("exam.unit.detail", args=[self.object.unit.pk])


class UnitExamUpdateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    """Update the exam for a unit.

    .. note:: The ``UnitExam`` model allows multiple exams for a unit, but this
              view restricts the unit to have a maximum of one exam for a unit.

    The student views should list all exams for a unit (in case we allow more
    than one in future).

    """

    form_class = UnitExamForm
    model = UnitExam

    def _unit(self):
        unit_pk = self.kwargs.get("unit_pk")
        if not unit_pk:
            raise ExamError("Cannot update the exam without a valid unit")
        return Unit.objects.get(pk=unit_pk)

    def _unit_exam(self):
        return UnitExam.objects.get(unit=self._unit())

    def form_valid(self, form):
        self.object = form.save(commit=False)
        try:
            unit_exam = self._unit_exam()
            unit_exam.exam = self.object.exam
            if unit_exam.is_deleted:
                unit_exam.undelete()
            else:
                unit_exam.save()
            return HttpResponseRedirect(self.get_success_url())
        except UnitExam.DoesNotExist:
            return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(unit=self._unit()))
        return context

    def get_initial(self):
        result = super().get_initial()
        try:
            unit_exam = self._unit_exam()
            if unit_exam.exam:
                if not unit_exam.is_deleted:
                    if not unit_exam.exam.is_deleted:
                        result.update(dict(exam=unit_exam.exam))
        except UnitExam.DoesNotExist:
            pass
        return result

    def get_object(self):
        return UnitExam(unit=self._unit())

    def get_success_url(self):
        return reverse("exam.unit.detail", args=[self.object.unit.pk])


class UnitListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    def get_queryset(self):
        return Unit.objects.current_with_assessment_count().order_by("name")


class UnitUpdateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    form_class = UnitForm
    model = Unit

    def get_success_url(self):
        return reverse("exam.unit.detail", args=[self.object.pk])


class UserAssessmentRetakeUpdateView(
    LoginRequiredMixin,
    UserPassesTestMixin,
    BaseMixin,
    UpdateView,
):
    """Comment on the retake."""

    model = UserAssessment
    form_class = UserAssessmentRetakeForm

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.retake_assessed = timezone.now()
        self.object.retake_assessor = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        return reverse("exam.user.unit.detail", args=[self.object.user_unit.pk])

    def test_func(self, user):
        return is_staff_or_team_member(user)


class UserAssessmentUpdateView(
    LoginRequiredMixin,
    UserPassesTestMixin,
    BaseMixin,
    UpdateView,
):
    """Comment on the assessment and ask the student to retake."""

    model = UserAssessment
    form_class = UserAssessmentForm

    def form_valid(self, form):
        """
        Added audit trail for UserUnit and UserAssessment, to allow
        updating of unit result and assessment comments.

        Requested as an open-ended feature.
        https://www.kbsoftware.co.uk/crm/ticket/6790/

        """
        self.object = form.save(commit=False)
        with transaction.atomic():
            self.object.assessed = timezone.now()
            self.object.assessor = self.request.user
            if self.object.can_retake:
                self.object.can_retake_date = timezone.now()
                self.object.can_retake_user = self.request.user
            UserAssessmentAudit.objects.create_user_assessment_audit(
                self.object,
                self.request.user,
                self.object.can_retake,
                self.object.error_count,
                self.object.assessor_comments,
            )
            result = super().form_valid(form)
        return result

    def get_success_url(self):
        return reverse("exam.user.unit.detail", args=[self.object.user_unit.pk])

    def test_func(self, user):
        return is_staff_or_team_member(user)


class UserCourseCoreCreateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, CreateView
):
    """Create a course (with 'core' units) for a contact.

    The ``UserCourseCreateView`` redirects to this view if the course has
    ``core`` (optional) units.

    """

    form_class = UserCourseCoreCreateForm
    model = UserCourse
    template_name = "exam/user_course_create_form.html"

    def _course(self):
        course_pk = self.kwargs["course_pk"]
        return Course.objects.get(pk=course_pk)

    def _course_units_core(self, course):
        return CourseUnit.objects.for_course(course).filter(core=True)

    def _payment_details(self):
        return escape(self.request.GET.get("payment_details")) or ""

    def form_valid(self, form):
        self.object = form.save(commit=False)
        contact = _contact(self.kwargs["contact_pk"])
        course = self._course()
        course_units_optional = form.cleaned_data["course_units"]
        with transaction.atomic():
            self.object.course = course
            self.object.user = contact.user
            self.object = form.save()
            # create core units for the course
            for course_unit in self._course_units_core(self.object.course):
                UserUnit.objects._create_user_unit(
                    self.object, course_unit.unit
                )
            # create optional units for the course
            for course_unit in course_units_optional:
                UserUnit.objects._create_user_unit(
                    self.object, course_unit.unit
                )
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        contact = _contact(self.kwargs["contact_pk"])
        course = self._course()
        context.update(
            dict(
                contact=contact,
                course=course,
                course_units_core=self._course_units_core(course),
                payment_details=self._payment_details(),
            )
        )
        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({"course": self._course()})
        return kwargs

    def get_initial(self):
        result = super().get_initial()
        result.update({"payment_details": self._payment_details()})
        return result

    def get_success_url(self):
        contact = _contact(self.kwargs["contact_pk"])
        return reverse("contact.detail", args=[contact.pk])


class UserCourseCreateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, CreateView
):
    """Create a course for a contact.

    Responds to the *Add Course* button on the contact detail page.

    Redirects to ``UserCourseCoreCreateView`` if the course has ``core``
    (optional) units.

    """

    form_class = UserCourseCreateForm
    model = UserCourse
    template_name = "exam/user_course_create_form.html"

    def _course_has_optional_units(self, course):
        """Does the course have optional units i.e. ``core=False``?"""
        return CourseUnit.objects.for_course(course).filter(core=False).exists()

    def form_valid(self, form):
        self.object = form.save(commit=False)
        contact = _contact(self.kwargs["contact_pk"])
        course_product = form.cleaned_data["course_product"]
        has_optional = self._course_has_optional_units(course_product.course)
        if has_optional:
            return HttpResponseRedirect(
                url_with_querystring(
                    reverse(
                        "exam.user.course.create.core",
                        args=[contact.pk, course_product.course.pk],
                    ),
                    payment_details=form.cleaned_data["payment_details"],
                )
            )
        else:
            with transaction.atomic():
                self.object.course = course_product.course
                self.object.user = contact.user
                self.object = form.save()
                # create core units for the course
                course_units = CourseUnit.objects.for_course(
                    self.object.course
                ).filter(core=True)
                for course_unit in course_units:
                    UserUnit.objects._create_user_unit(
                        self.object, course_unit.unit
                    )
            return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        contact = _contact(self.kwargs["contact_pk"])
        context.update(dict(contact=contact))
        return context

    def get_success_url(self):
        contact = _contact(self.kwargs["contact_pk"])
        return reverse("contact.detail", args=[contact.pk])


class UserCourseTailoredCreateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, CreateView
):
    """Create a tailored course for a user.

    .. tip:: The URL passes in a ``contact_pk`` (not a ``user_pk``)

    .. tip:: See ``CourseCreateView`` for creating non-tailored courses.

    """

    form_class = CourseForm
    model = Course

    def form_valid(self, form):
        self.object = form.save(commit=False)
        # this is a tailored course...
        # https://www.kbsoftware.co.uk/crm/ticket/5933/
        self.object.is_tailored = True
        with transaction.atomic():
            self.object = form.save()
            contact = _contact(self.kwargs["contact_pk"])
            user_course = UserCourse(user=contact.user, course=self.object)
            user_course.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        contact = _contact(self.kwargs["contact_pk"])
        context.update(dict(contact=contact))
        return context

    def get_success_url(self):
        contact = _contact(self.kwargs["contact_pk"])
        return reverse("contact.detail", args=[contact.pk])


class UserCourseDeleteView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    BaseMixin,
    UpdateView,
):
    model = UserCourse
    form_class = UserCourseEmptyForm
    template_name = "exam/user_course_delete.html"

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.set_deleted(self.request.user)
        return super().form_valid(form)

    def get_success_url(self):
        return reverse("user.redirect.contact", args=[self.object.user.pk])


class UserCourseDetailView(
    LoginRequiredMixin, UserPassesTestMixin, BaseMixin, DetailView
):
    """Assessor detail view of a course for a user.

    - Calls: ``UserUnitDetailView``.
    - Template: ``exam/usercourse_detail.html``.

    """

    model = UserCourse

    def get_context_data(self, **kwargs):
        """Only display course units with assessments.

        https://www.kbsoftware.co.uk/crm/ticket/5531/

        """
        context = super().get_context_data(**kwargs)
        user_units = UserUnit.objects.with_assessment(self.object)
        course_results, _ = self.object.course_results()
        context.update(
            dict(course_results=course_results, user_units=user_units)
        )
        return context

    def test_func(self, user):
        return is_staff_or_team_member(user)


class UserCourseListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    model = UserCourse
    paginate_by = 20

    def get_queryset(self):
        return UserCourse.objects.current().order_by("-created")


class UserCourseMarkingListView(
    LoginRequiredMixin, UserPassesTestMixin, BaseMixin, ListView
):
    """List of courses which are marked (or partly marked).

    ref email from Emma, 12/10/2023.  Would it be possible for us to have
    an *All Marking* section in the *Assessor Area* in a similar way to
    the *Queries Area* where it says *All Queries* and we can see who we
    have helped before?
    So the *team can take a look at previous feedback on
    assessments/assignments*.

    https://www.kbsoftware.co.uk/crm/ticket/6863/

    """

    paginate_by = 10
    template_name = "exam/user_course_marking_list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(
            dict(
                is_team_member_not_staff=is_team_member_not_staff(
                    self.request.user
                )
            )
        )
        return context

    def get_queryset(self):
        return UserCourse.objects.current_marking().order_by("-created")

    def test_func(self, user):
        return is_staff_or_team_member(user)


class UserCourseProfileUpdateView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    BaseMixin,
    UpdateView,
):
    model = UserCourse
    form_class = UserCourseProfileForm
    template_name = "exam/user_course_profile_form.html"

    def get_success_url(self):
        return reverse("exam.user.course.detail", args=[self.object.pk])


class UserCourseToMarkListView(
    LoginRequiredMixin, UserPassesTestMixin, BaseMixin, ListView
):
    model = UserCourse
    template_name = "exam/user_course_to_mark_list.html"

    def get_queryset(self):
        """Display submitted (user) units (list of dict)."""
        user_unit_data = UserUnit.objects.current_to_mark()
        return _sort_current_to_mark(user_unit_data)

    def test_func(self, user):
        return is_staff_or_team_member(user)


class UserCourseResultUpdateView(
    LoginRequiredMixin,
    UserPassesTestMixin,
    BaseMixin,
    UpdateView,
):
    """Update the result for the course.

    Will send an email to the student (``MAIL_TEMPLATE_USER_COURSE_RESULT``).

    .. tip:: See ``UserUnitResultUpdateView`` for marking the unit.

    """

    model = UserCourse
    form_class = UserCourseForm

    def form_valid(self, form):
        with transaction.atomic():
            self.object = form.save(commit=False)
            self.object.assessed = timezone.now()
            self.object.assessor = self.request.user
            queue_mail_template(
                self.object,
                UserCourse.MAIL_TEMPLATE_USER_COURSE_RESULT,
                {
                    self.object.user.email: dict(
                        name=self.object.user.first_name.capitalize(),
                    )
                },
            )
            transaction.on_commit(lambda: process_mail.send())
            result = super().form_valid(form)
        return result

    def get_success_url(self):
        return reverse("user.redirect.contact", args=[self.object.user.pk])

    def test_func(self, user):
        return is_staff_or_team_member(user)


class UserExamVersionUpdateMixin:
    """Let the student know which exam they are taking and the current status.

    Views using this mixin, should define a ``products_for_user`` method e.g::

      def products_for_user(self):
          qs = StudentCourse.objects.filter(user=user)
          return [obj.product for obj in qs]

    Use with a ``UpdateView`` e.g::

      class UserExamVersionUpdateView(
             LoginRequiredMixin, UserExamVersionUpdateMixin,
             PageFormMixin, UpdateView):

      url(regex=r'^user/test/(?P<pk>\d+)/$',
          view=UserExamVersionUpdateView.as_view(),
          name='web.exam.user.version.update'
      ),

    """

    model = UserExamVersion
    form_class = UserExamVersionEmptyForm

    def _products_for_user(self):
        try:
            return self.products_for_user()
        except AttributeError:
            raise ImproperlyConfigured(
                "Missing method - please define 'products_for_user'"
            )

    def form_valid(self, form):
        super().form_valid(form)
        _submit_exam_for_marking(self.object, self._products_for_user())
        return HttpResponseRedirect(
            reverse("web.exam.user.version.submitted", args=[self.object.pk])
        )

    def get_context_data(self, **kwargs):
        """Check exam product permissions."""
        context = super().get_context_data(**kwargs)
        if not self.check_perm_user_exam_version():
            raise PermissionDenied(
                "You do not have permission to access this exam"
            )
        results_in_full = self.object.results_in_full(self.request.user)
        context.update(dict(results_in_full=results_in_full))
        return context

    def get_success_url(self):
        return _user_exam_version_redirect_url(self.object)


class UserExamQuestionMixin:
    """Ask the student a question.

    To use this mix-in::

      class StudentQuestionView(
              LoginRequiredMixin, StudentQuestionMixin, BaseMixin, UpdateView):
          template_name = 'example/student_question_form.html'

      url(regex=r'^student/question/(?P<pk>\d+)/$',
          view=StudentQuestionView.as_view(),
          name='web.exam.user.question'
      ),

    """

    form_class = UserExamQuestionForm
    model = Question

    def _products_for_user(self):
        try:
            return self.products_for_user()
        except AttributeError:
            raise ImproperlyConfigured(
                "Missing method - please define 'products_for_user'"
            )

    def _user_exam_version(self):
        version_pk = self.kwargs["version_pk"]
        return UserExamVersion.objects.get(pk=version_pk)

    def form_valid(self, form):
        user_exam_version = self._user_exam_version()
        if "submit-for-marking" in self.request.POST:
            _submit_exam_for_marking(
                user_exam_version, self._products_for_user()
            )
            return HttpResponseRedirect(
                reverse(
                    "web.exam.user.version.submitted",
                    args=[user_exam_version.pk],
                )
            )
        else:
            with transaction.atomic():
                self.object = form.save()
                answer = form.cleaned_data["options"]
                user_exam_version.set_answer(self.object, answer)
            return HttpResponseRedirect(
                reverse(
                    "web.exam.user.question",
                    args=[user_exam_version.pk, self.object.pk],
                )
            )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user_exam_version = self._user_exam_version()
        # ``check_perm_user_exam_version`` provided by child class
        if not self.check_perm_user_exam_version(user_exam_version):
            raise PermissionDenied(
                "You do not have permission to access this exam"
            )
        context.update(
            dict(
                user_exam_version=user_exam_version,
                next_url=user_exam_version.next_url(self.object),
            )
        )
        return context

    def get_form_kwargs(self):
        result = super().get_form_kwargs()
        result.update(dict(question=self.object))
        return result

    def get_initial(self):
        result = super().get_initial()
        user_exam_version = self._user_exam_version()
        answer = user_exam_version.get_answer(self.object)
        if answer:
            result.update(dict(options=answer))
        return result


class UserExamRedirectView(LoginRequiredMixin, RedirectView):
    """Has the user taken the exam, or are they taking it next?"""

    permanent = False
    query_string = False

    def get_redirect_url(self, *args, **kwargs):
        pk = kwargs["pk"]
        exam = Exam.objects.get(pk=pk)
        user_exam_version = exam.user_exam_version(self.request.user)
        if not user_exam_version:
            # initialise an exam for this user
            exam.init_exam(self.request.user)
            # find the 'user_exam_version'
            user_exam_version = exam.user_exam_version(self.request.user)
            if not user_exam_version:
                raise ExamError(
                    "No user exam version for user '{}' "
                    "exam '{}'".format(self.request.user.username, exam.pk)
                )
        return _user_exam_version_redirect_url(user_exam_version)


class UserExamVersionResultMixin:
    """Display the result of an exam.

    To use this mix-in::

      class UserExamVersionResultView(
              LoginRequiredMixin, UserExamResultVersionMixin,
              PageFormMixin, DetailView):

    """

    model = UserExamVersion

    def get_context_data(self, **kwargs):
        """Check exam products (permissions)."""
        context = super().get_context_data(**kwargs)
        # ``check_perm_user_exam_version`` provided by child class
        if not self.check_perm_user_exam_version():
            raise PermissionDenied(
                "You do not have permission to access this exam"
            )
        results_in_full = self.object.results_in_full(self.request.user)
        context.update(dict(results_in_full=results_in_full))
        return context


class UserExamVersionRedirectView(LoginRequiredMixin, RedirectView):
    """The user is taking the exam.  Which question do we display first?"""

    permanent = False
    query_string = False

    def get_redirect_url(self, *args, **kwargs):
        pk = kwargs["pk"]
        user_exam_version = UserExamVersion.objects.get(pk=pk)
        question = user_exam_version.first_question()
        return reverse(
            "web.exam.user.question", args=[user_exam_version.pk, question.pk]
        )


class UserExamVersionResultCreateView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    RedirectNextMixin,
    BaseMixin,
    CreateView,
):
    form_class = UserExamVersionPercentForm
    model = UserExamVersion
    template_name = "exam/userexamversion_form_create.html"

    def _exam_version(self):
        pk = self.kwargs["exam_pk"]
        exam = Exam.objects.get(pk=pk)
        return exam.current_version()

    def _product(self):
        pk = self.kwargs["product_pk"]
        return Product.objects.get(pk=pk)

    def _user(self):
        pk = self.kwargs["user_pk"]
        return get_user_model().objects.get(pk=pk)

    def form_valid(self, form):
        with transaction.atomic():
            self.object = form.save(commit=False)
            self.object.exam_version = self._exam_version()
            self.object.user = self._user()
            percent = form.cleaned_data["percent"]
            legacy_retake = form.cleaned_data["retake"]
            self.object.set_percent_override(
                percent,
                self.request.user,
                legacy_result=True,
                legacy_retake=legacy_retake,
            )
            UserExamVersionProduct.objects.link_user_exam_version_product(
                self.object, [self._product()]
            )
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(
            dict(
                user=self._user(),
                exam_version=self._exam_version(),
                product=self._product(),
            )
        )
        return context

    def get_form_kwargs(self):
        result = super().get_form_kwargs()
        result.update(dict(legacy=True))
        return result

    def get_success_url(self):
        next_url = self.request.POST.get(REDIRECT_FIELD_NAME)
        if next_url:
            return next_url
        else:
            return reverse(
                "exam.version.user.list", args=[self.object.exam_version.pk]
            )


class UserExamVersionResultUpdateView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    RedirectNextMixin,
    BaseMixin,
    UpdateView,
):
    form_class = UserExamVersionPercentForm
    model = UserExamVersion

    def form_valid(self, form):
        with transaction.atomic():
            self.object = form.save(commit=False)
            percent = form.cleaned_data["percent"]
            kwargs = {}
            if self.object.legacy_result:
                kwargs["legacy_retake"] = form.cleaned_data["retake"]
            self.object.set_percent_override(
                percent, self.request.user, **kwargs
            )
        return HttpResponseRedirect(self.get_success_url())

    def get_form_kwargs(self):
        result = super().get_form_kwargs()
        result.update(dict(legacy=bool(self.object.legacy_result)))
        return result

    def get_initial(self):
        result = super().get_initial()
        result.update(
            dict(
                percent=self.object.result_as_percent(),
                retake=self.object.legacy_retake,
            )
        )
        return result

    def get_success_url(self):
        next_url = self.request.POST.get(REDIRECT_FIELD_NAME)
        if next_url:
            return next_url
        else:
            return reverse(
                "exam.version.user.list", args=[self.object.exam_version.pk]
            )


class UserExamVersionResultsView(
    LoginRequiredMixin,
    UserPassesTestMixin,
    RedirectNextMixin,
    BaseMixin,
    DetailView,
):
    model = UserExamVersion

    def get_context_data(self, **kwargs):
        """Check exam products (permissions)."""
        context = super().get_context_data(**kwargs)
        results_in_full = self.object.results_in_full(self.request.user)
        context.update(dict(results_in_full=results_in_full))
        return context

    def get_success_url(self):
        next_url = self.request.POST.get(REDIRECT_FIELD_NAME)
        if next_url:
            return next_url
        else:
            return reverse(
                "exam.version.user.list", args=[self.object.exam_version.pk]
            )

    def test_func(self, user):
        return is_staff_or_team_member(user)


class UserUnitCommentUpdateView(
    LoginRequiredMixin,
    UserPassesTestMixin,
    BaseMixin,
    UpdateView,
):
    """Assess the unit.

    Will send an email to the student (``MAIL_TEMPLATE_USER_UNIT_RESULT``).

    From the ticket, https://www.kbsoftware.co.uk/crm/ticket/6589/

    1. The assessor will need to be able to put in the feedback (for the
       assessments) and result (for the unit) before the unit comments
       are entered.
    2. I will then check everything and enter the unit comments.
    3. After the unit comments have been entered, everything should be
       displayed to the Student.

    .. tip:: The result is entered in the ``UserUnitResultUpdateView``.

    .. tip:: See ``UserCourseResultUpdateView`` for marking the course.

    """

    model = UserUnit
    form_class = UserUnitCommentForm
    template_name = "exam/user_unit_comment_form.html"

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.assessed = timezone.now()
        self.object.assessor = self.request.user
        with transaction.atomic():
            name = self.object.user_course.user.first_name.capitalize()
            queue_mail_template(
                self.object,
                UserUnit.MAIL_TEMPLATE_USER_UNIT_RESULT,
                {self.object.user_course.user.email: dict(name=name)},
            )
            transaction.on_commit(lambda: process_mail.send())
        return super().form_valid(form)

    def get_success_url(self):
        return reverse("exam.user.unit.detail", args=[self.object.pk])

    def test_func(self, user):
        return is_staff_or_team_member(user)


class UserUnitDetailView(
    LoginRequiredMixin, UserPassesTestMixin, BaseMixin, DetailView
):
    """Assessor detail view of a unit for a user.

    Called by the ``UserCourseDetailView``.

    """

    model = UserUnit

    def get_context_data(self, **kwargs):
        """Set the assessor - they are responsible for marking the unit."""
        context = super().get_context_data(**kwargs)
        assessor = []
        if self.object.assessor:
            if self.object.retake_assessed:
                assessor.append(
                    "Retake reviewed by {} ({} ago)".format(
                        self.object.retake_assessor.get_full_name(),
                        humanize.naturaldelta(
                            timezone.now() - self.object.retake_assessed
                        ),
                    )
                )
            if self.object.result_assessed:
                assessor.append(
                    "Result entered by {} ({} ago)".format(
                        self.object.result_assessor.get_full_name(),
                        humanize.naturaldelta(
                            timezone.now() - self.object.result_assessed
                        ),
                    )
                )
            if self.object.assessed:
                assessor.append(
                    "Assessed by {} ({} ago)".format(
                        self.object.assessor.get_full_name(),
                        humanize.naturaldelta(
                            timezone.now() - self.object.assessed
                        ),
                    )
                )
            if not assessor:
                assessor.append(
                    "Unit being marked by {}".format(
                        self.object.assessor.get_full_name(),
                    )
                )
        else:
            self.object.assessor = self.request.user
            self.object.save()
        context.update(dict(assessor=assessor))
        return context

    def test_func(self, user):
        return is_staff_or_team_member(user)


class UserUnitResultUpdateView(
    LoginRequiredMixin,
    UserPassesTestMixin,
    BaseMixin,
    UpdateView,
):
    """Update the result for the unit.

    1. The result is entered using this view.
    2. The unit is ``assessed`` in the ``UserUnitCommentUpdateView``.

    """

    model = UserUnit
    form_class = UserUnitResultForm
    template_name = "exam/user_unit_result_form.html"

    def form_valid(self, form):
        """
        Added audit trail for UserUnit and UserAssessment, to allow
        updating of unit result and assessment comments.

        Requested as an open-ended feature.
        https://www.kbsoftware.co.uk/crm/ticket/6790/

        """
        self.object = form.save(commit=False)
        with transaction.atomic():
            self.object.result_assessed = timezone.now()
            self.object.result_assessor = self.request.user
            UserUnitAudit.objects.create_user_unit_audit(
                self.object, self.request.user, self.object.result
            )
            result = super().form_valid(form)
        return result

    def get_success_url(self):
        return reverse("exam.user.unit.detail", args=[self.object.pk])

    def test_func(self, user):
        return is_staff_or_team_member(user)


class UserUnitRetakeCommentUpdateView(
    LoginRequiredMixin,
    UserPassesTestMixin,
    BaseMixin,
    UpdateView,
):
    model = UserUnit
    form_class = UserUnitRetakeCommentForm
    template_name = "exam/user_unit_retake_comment_form.html"

    def form_valid(self, form):
        self.object = form.save(commit=False)
        if self.object.retake_assessed:
            # just update the comment
            pass
        else:
            self.object.retake_assessed = timezone.now()
            self.object.retake_assessor = self.request.user
            with transaction.atomic():
                name = self.object.user_course.user.first_name.capitalize()
                queue_mail_template(
                    self.object,
                    UserUnit.MAIL_TEMPLATE_USER_UNIT_RETAKE,
                    {self.object.user_course.user.email: dict(name=name)},
                )
                transaction.on_commit(lambda: process_mail.send())
        return super().form_valid(form)

    def get_success_url(self):
        return reverse("exam.user.unit.detail", args=[self.object.pk])

    def test_func(self, user):
        return is_staff_or_team_member(user)
