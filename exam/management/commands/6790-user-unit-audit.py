# -*- encoding: utf-8 -*-
from django.conf import settings
from django.core.management.base import BaseCommand
from django.urls import reverse

from exam.models import UserAssessmentAudit, UserUnitAudit

from rich.pretty import pprint


class Command(BaseCommand):
    """
    Added audit trail for UserUnit and UserAssessment, to allow
    updating of unit result and assessment comments.

    Requested as an open-ended feature.
    https://www.kbsoftware.co.uk/crm/ticket/6790/

    """

    help = "'UserUnitAudit' - List audit entries for a specific user' #6790"

    def add_arguments(self, parser):
        parser.add_argument("user_name", type=str)

    def handle(self, *args, **options):
        self.stdout.write(f"{self.help}...")
        user_name = options["user_name"]

        qs = UserUnitAudit.objects.filter(
            user_unit__user_course__user__username=user_name
        ).order_by("created")
        for x in qs:
            user = x.user_unit.user_course.user.username
            changed_time = x.created.strftime("%H:%M:%S")
            changed_date = x.created.strftime("%m/%d/%Y")
            url = reverse("exam.user.unit.detail", args=[x.user_unit.pk])
            print(
                f"{changed_date} {changed_time} : result "
                f"changed to {x.result} by {x.user}. "
                f"Link to unit: {url}"
            )

        print("User assessment audit")

        qs = UserAssessmentAudit.objects.filter(
            user_assessment__user_unit__user_course__user__username=user_name
        ).order_by("created")
        for x in qs:
            user = x.user_assessment.user_unit.user_course.user.username
            comments = x.assessor_comments
            changed_time = x.created.strftime("%H:%M:%S")
            changed_date = x.created.strftime("%m/%d/%Y")
            url = reverse(
                "exam.user.assessment.update", args=[x.user_assessment.pk]
            )
            print(
                f"{changed_date} {changed_time} : assessment "
                f"comments changed to '{comments}' by {x.user}. "
                f"Link to assessment: {url}"
            )

        self.stdout.write(f"{self.help} - Complete...")
