# -*- encoding: utf-8 -*-
from django.urls import re_path

from exam.views import UserExamRedirectView, UserExamVersionRedirectView


urlpatterns = [
    re_path(
        r"^(?P<pk>\d+)/redirect/$",
        view=UserExamRedirectView.as_view(),
        name="exam.user.redirect",
    ),
    re_path(
        r"^user/exam/version/(?P<pk>\d+)/redirect/$",
        view=UserExamVersionRedirectView.as_view(),
        name="exam.user.version.redirect",
    ),
]
