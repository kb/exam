Multiple Choice Tests
*********************

Install
=======

Virtual Environment
-------------------

::

  virtualenv --python=python3 venv-exam
  # or
  python3 -m venv venv-exam
  source venv-exam/bin/activate

  pip install -r requirements/local.txt

Testing
=======

::

  find . -name '*.pyc' -delete
  py.test -x

To set demo questions and answers for an exam, find the primary key in the URL
(e.g. ``1``) and use in the ``demo_data_exam`` management command e.g::

  django-admin.py demo_data_exam 1

To set the correct answer for all the demo questions, find the primary key in
the URL (e.g. ``1``) and use in the ``demo_data_exam_answer`` management
command e.g::

  demo_data_exam_answer --exam-pk 1 --user-name staff

Usage
=====

::

  py.test -x && \
      touch temp.db && rm temp.db && \
      django-admin.py migrate --noinput && \
      django-admin.py demo_data_login && \
      django-admin.py runserver

Release
=======

https://www.kbsoftware.co.uk/docs/
