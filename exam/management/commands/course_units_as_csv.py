# -*- encoding: utf-8 -*-
import csv

from django.core.management.base import BaseCommand

from exam.models import Unit


class Command(BaseCommand):
    help = "Course units as CSV #5604"

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        qs = Unit.objects.exclude(deleted=True).order_by("course", "order")
        with open("course_units_as.csv", "w", newline="") as out:
            csv_writer = csv.writer(out, dialect="excel-tab")
            csv_writer.writerow(
                [
                    "Course",
                    "Course Name",
                    "Unit",
                    "Unit Name",
                    "Order",
                ]
            )
            for unit in qs:
                csv_writer.writerow(
                    [
                        unit.course.pk,
                        unit.course.name,
                        unit.pk,
                        unit.name,
                        unit.order,
                    ]
                )
        self.stdout.write("{} - Complete".format(self.help))
