# -*- encoding: utf-8 -*-
import json
import pathlib
import shutil

from django.core.management.base import BaseCommand
from rich.pretty import pprint

from exam.models import Course


class Command(BaseCommand):
    help = "Exam Course - dump images #6212"

    def add_arguments(self, parser):
        parser.add_argument("dump-image-folder", type=str)

    def _dump_image(self, course_pk, picture, image_type, dump_folder):
        from_file = pathlib.Path(picture.image.path)
        self.stdout.write(f"{from_file}")
        to_file = pathlib.Path(
            dump_folder, f"course-{image_type}-{course_pk}-{from_file.name}"
        )
        self.stdout.write(f"  {to_file}")
        shutil.copy(from_file, to_file)
        return {
            f"{image_type}": {
                "category": picture.category.slug if picture.category else "",
                "file_name": str(to_file),
                "title": picture.title,
                "user": picture.user.username,
            }
        }

    def handle(self, *args, **options):
        json_data = {}
        dump_folder = pathlib.Path(options["dump-image-folder"])
        self.stdout.write(f"{self.help}.  Dump images to '{dump_folder}'...")
        dump_folder.mkdir(exist_ok=True)
        for course in Course.objects.all().order_by("pk"):
            data = {}
            if course.banner:
                data.update(
                    self._dump_image(
                        course.pk, course.banner, "banner", dump_folder
                    )
                )
            if course.banner_mobile:
                data.update(
                    self._dump_image(
                        course.pk,
                        course.banner_mobile,
                        "banner_mobile",
                        dump_folder,
                    )
                )
            if course.picture:
                data.update(
                    self._dump_image(
                        course.pk, course.picture, "picture", dump_folder
                    )
                )
            json_data[course.pk] = data

        pprint(json_data, expand_all=True)
        json_file_name = "6212-dump-images-exam-course.json"
        with open(json_file_name, "w") as f:
            json.dump(json_data, f, indent=4)
        self.stdout.write(f"{self.help} - Complete")
        self.stdout.write(f"Dump images to '{dump_folder}'")
        self.stdout.write(f"Dump JSON to   '{json_file_name}'")
