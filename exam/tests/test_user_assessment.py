# -*- encoding: utf-8 -*-
import pytest
import pytz

from datetime import date
from django.core.files.uploadedfile import SimpleUploadedFile
from django.utils import timezone
from freezegun import freeze_time

from exam.models import ExamError, Unit, UserAssessment
from exam.tests.factories import (
    AssessmentFactory,
    CourseFactory,
    CourseUnitFactory,
    UnitFactory,
    UserAssessmentFactory,
    UserCourseFactory,
    UserUnitFactory,
)
from login.tests.factories import UserFactory


@pytest.mark.django_db
def test_answer_button():
    x = UserAssessmentFactory()
    assert "Start" == x.answer_button_caption()
    assert x.answer_button_enabled() is True
    assert x.feedback_enabled() is False
    assert x.feedback_enabled_for_retake() is False


@pytest.mark.django_db
def test_answer_button_legacy_user_course():
    """Handle legacy courses.

    https://www.kbsoftware.co.uk/crm/ticket/7017/

    .. tip:: Also see ``test_answer_button_retake_legacy_user_course``.

    """
    unit = UnitFactory(assignment_or_assessment=Unit.ASSESSMENT)
    with freeze_time(date(2024, 1, 1)):
        user_course = UserCourseFactory(
            user=UserFactory(),
            course=CourseFactory(),
            assessed=timezone.now(),
            result=12,
        )
    x = UserAssessmentFactory(
        user_unit=UserUnitFactory(
            unit=unit,
            user_course=user_course,
            assessed=timezone.now(),
            submitted=timezone.now(),
        ),
        answer_submitted=timezone.now(),
        has_passed=False,
    )
    assert "Submitted" == x.answer_button_caption()
    assert x.answer_button_enabled() is False
    assert x.feedback_enabled() is False
    assert x.feedback_enabled_for_retake() is False


@pytest.mark.django_db
def test_answer_button_answer_submitted():
    x = UserAssessmentFactory(answer_submitted=timezone.now())
    assert "Uploaded" == x.answer_button_caption()
    assert x.answer_button_enabled() is True
    assert x.feedback_enabled() is False
    assert x.feedback_enabled_for_retake() is False


@pytest.mark.django_db
def test_answer_button_answer_and_unit_submitted():
    """

    .. warning:: Not sure this is a valid state as ``can_retake`` would have
                 been set to ``True`` before the unit was assessed.

    """
    unit = UnitFactory(assignment_or_assessment=Unit.ASSESSMENT)
    user_unit = UserUnitFactory(
        unit=unit, submitted=timezone.now(), assessed=timezone.now(), result=1
    )
    x = UserAssessmentFactory(
        user_unit=user_unit,
        answer_submitted=timezone.now(),
        can_retake=False,
        has_passed=False,
        assessor_comments="assessor-comments",
    )
    assert "FAIL" == x.answer_button_caption()
    assert x.answer_button_enabled() is False
    assert x.feedback_enabled() is False
    assert x.feedback_enabled_for_retake() is False


@pytest.mark.django_db
def test_answer_button_answer_and_unit_submitted_and_pass():
    unit = UnitFactory(assignment_or_assessment=Unit.ASSESSMENT)
    x = UserAssessmentFactory(
        user_unit=UserUnitFactory(
            unit=unit,
            submitted=timezone.now(),
            assessed=timezone.now(),
            result=1,
        ),
        answer_submitted=timezone.now(),
        has_passed=True,
    )
    assert "PASS" == x.answer_button_caption()
    assert x.answer_button_enabled() is False
    assert x.feedback_enabled() is False
    assert x.feedback_enabled_for_retake() is False


@pytest.mark.django_db
def test_answer_button_can_retake_not_submitted():
    unit = UnitFactory(assignment_or_assessment=Unit.ASSESSMENT)
    user_unit = UserUnitFactory(
        unit=unit,
        submitted=timezone.now(),
        retake_assessed=timezone.now(),
        retake_submitted=None,
    )
    assessment = AssessmentFactory(unit=user_unit.unit)
    x = UserAssessmentFactory(
        user_unit=user_unit,
        assessment=assessment,
        can_retake=True,
        retake_submitted=None,
    )
    assert "Retake" == x.answer_button_caption()
    assert x.answer_button_enabled() is True
    assert x.feedback_enabled() is False
    assert x.feedback_enabled_for_retake() is False


@pytest.mark.django_db
def test_answer_button_can_retake():
    unit = UnitFactory(assignment_or_assessment=Unit.ASSESSMENT)
    user_unit = UserUnitFactory(
        unit=unit,
        submitted=timezone.now(),
        retake_assessed=timezone.now(),
        retake_submitted=None,
    )
    assessment = AssessmentFactory(unit=user_unit.unit)
    x = UserAssessmentFactory(
        user_unit=user_unit,
        assessment=assessment,
        can_retake=True,
        retake_submitted=timezone.now(),
    )
    assert "Retake Uploaded" == x.answer_button_caption()
    assert x.answer_button_enabled() is True
    assert x.feedback_enabled() is False
    assert x.feedback_enabled_for_retake() is False


@pytest.mark.django_db
def test_answer_button_retake_submitted():
    unit = UnitFactory(assignment_or_assessment=Unit.ASSESSMENT)
    user_unit = UserUnitFactory(
        unit=unit,
        submitted=timezone.now(),
        retake_assessed=timezone.now(),
        retake_submitted=None,
    )
    assessment = AssessmentFactory(unit=user_unit.unit)
    x = UserAssessmentFactory(
        user_unit=user_unit,
        assessment=assessment,
        can_retake=True,
        retake_submitted=timezone.now(),
    )
    assert "Retake Uploaded" == x.answer_button_caption()
    assert x.answer_button_enabled() is True
    assert x.feedback_enabled() is False
    assert x.feedback_enabled_for_retake() is False


@pytest.mark.django_db
def test_answer_button_retake_answer_submitted_and_unit_submitted():
    user_unit = UserUnitFactory(
        submitted=timezone.now(),
        retake_assessed=timezone.now(),
        retake_submitted=timezone.now(),
    )
    assessment = AssessmentFactory(unit=user_unit.unit)
    x = UserAssessmentFactory(
        user_unit=user_unit,
        assessment=assessment,
        can_retake=True,
        retake_submitted=timezone.now(),
    )
    assert "Retake Submitted" == x.answer_button_caption()
    assert x.answer_button_enabled() is False
    assert x.feedback_enabled() is False
    assert x.feedback_enabled_for_retake() is False


@pytest.mark.django_db
def test_answer_button_retake_and_retake_submitted():
    user_unit = UserUnitFactory(
        retake_assessed=timezone.now(), retake_submitted=timezone.now()
    )
    assessment = AssessmentFactory(unit=user_unit.unit)
    x = UserAssessmentFactory(
        user_unit=user_unit,
        assessment=assessment,
        can_retake=True,
        retake_submitted=timezone.now(),
    )
    assert "Retake Submitted" == x.answer_button_caption()
    assert x.answer_button_enabled() is False
    assert x.feedback_enabled() is False
    assert x.feedback_enabled_for_retake() is False


@pytest.mark.django_db
def test_answer_button_retake_legacy_user_course():
    """Handle legacy courses.

    https://www.kbsoftware.co.uk/crm/ticket/7017/

    .. tip:: Also see ``test_answer_button_legacy_user_course``.

    """
    unit = UnitFactory(assignment_or_assessment=Unit.ASSESSMENT)
    with freeze_time(date(2024, 1, 1)):
        user_course = UserCourseFactory(
            user=UserFactory(),
            course=CourseFactory(),
            assessed=timezone.now(),
            result=12,
        )
    user_unit = UserUnitFactory(
        unit=unit,
        user_course=user_course,
        submitted=timezone.now(),
        assessed=timezone.now(),
        retake_assessed=timezone.now(),
        retake_submitted=timezone.now(),
    )
    assessment = AssessmentFactory(unit=user_unit.unit)
    x = UserAssessmentFactory(
        user_unit=user_unit,
        assessment=assessment,
        answer_submitted=timezone.now(),
        has_passed=False,
        can_retake=True,
        retake_submitted=timezone.now(),
    )
    assert "Retake Submitted" == x.answer_button_caption()
    assert x.answer_button_enabled() is False
    assert x.feedback_enabled() is False
    assert x.feedback_enabled_for_retake() is False


@pytest.mark.django_db
def test_answer_file_name():
    user_assessment = UserAssessmentFactory()
    assert "the_answer_file" in user_assessment.answer_file_name()


@pytest.mark.django_db
def test_answer_file_name_original_file_name():
    user_assessment = UserAssessmentFactory(
        answer_original_file_name="apple.pdf"
    )
    assert "apple.pdf" in user_assessment.answer_file_name()


@pytest.mark.django_db
def test_create_user_assessment():
    user_unit = UserUnitFactory()
    assessment = AssessmentFactory(unit=user_unit.unit)
    x = UserAssessment.objects.create_user_assessment(user_unit, assessment)
    assert x.pk > 0
    assert user_unit == x.user_unit
    assert assessment == x.assessment
    # assert x.assessor is None
    # assert "" == x.assessor_comments
    # assert "" == x.answer
    assert "" == x.answer_file.name
    assert "" == x.answer_original_file_name
    assert x.answer_submitted is None
    assert x.can_retake is False


@pytest.mark.django_db
def test_factory():
    UserAssessmentFactory()


@pytest.mark.django_db
def test_feedback_enabled():
    unit = UnitFactory(assignment_or_assessment=Unit.ASSESSMENT)
    user_assessment = UserAssessmentFactory(
        user_unit=UserUnitFactory(unit=unit, assessed=timezone.now(), result=1),
        assessed=timezone.now(),
        has_passed=True,
        assessor_comments="assessor-comments",
    )
    assert user_assessment.feedback_enabled() is True
    assert user_assessment.feedback_enabled_for_retake() is False


@pytest.mark.django_db
@pytest.mark.parametrize(
    "can_retake,grade,user_unit_assessed,retake_grade,expect",
    [
        (False, 20, None, None, False),
        (False, None, None, None, False),
        (True, None, None, None, False),
        (True, None, timezone.now(), 21, True),
        (True, None, timezone.now(), None, True),
    ],
)
def test_feedback_enabled_assignment(
    can_retake, grade, user_unit_assessed, retake_grade, expect
):
    unit = UnitFactory(assignment_or_assessment=Unit.ASSIGNMENT)
    result = None
    if user_unit_assessed:
        result = 1
    user_assessment = UserAssessmentFactory(
        user_unit=UserUnitFactory(
            unit=unit,
            assessed=user_unit_assessed,
            result=result,
            retake_assessed=None,
        ),
        assessed=timezone.now(),
        can_retake=can_retake,
        grade=grade,
        assessor_comments="assessor-comments",
        retake_assessed=timezone.now(),
        retake_grade=retake_grade,
        retake_comments="retake-comments",
    )
    assert user_assessment.feedback_enabled() is expect
    assert user_assessment.feedback_enabled_for_retake() is False


@pytest.mark.django_db
@pytest.mark.parametrize(
    "can_retake,has_passed,user_unit_assessed,retake_has_passed,expect",
    [
        (False, False, None, False, False),
        (False, True, None, False, False),
        (True, False, None, False, False),
        (True, False, timezone.now(), False, True),
        (True, False, timezone.now(), True, True),
    ],
)
def test_feedback_enabled_assessment(
    can_retake, has_passed, user_unit_assessed, retake_has_passed, expect
):
    """Test for assessments with assessor comments already entered.

    .. tip:: An assessment will use ``has_passed`` and ``retake_has_passed``

    """
    unit = UnitFactory(assignment_or_assessment=Unit.ASSESSMENT)
    result = None
    if user_unit_assessed:
        result = 1
    user_assessment = UserAssessmentFactory(
        user_unit=UserUnitFactory(
            unit=unit,
            assessed=user_unit_assessed,
            result=result,
            retake_assessed=None,
        ),
        assessed=timezone.now(),
        can_retake=can_retake,
        has_passed=has_passed,
        assessor_comments="assessor-comments",
        retake_assessed=timezone.now(),
        retake_has_passed=retake_has_passed,
        retake_comments="retake-comments",
    )
    assert user_assessment.feedback_enabled() is expect
    assert user_assessment.feedback_enabled_for_retake() is False


@pytest.mark.django_db
def test_feedback_enabled_not_for_user_unit():
    user_assessment = UserAssessmentFactory(
        user_unit=UserUnitFactory(unit=UnitFactory(), assessed=None),
    )
    assert user_assessment.feedback_enabled() is False
    assert user_assessment.feedback_enabled_for_retake() is False


@pytest.mark.django_db
def test_init_user_assessment():
    user_unit = UserUnitFactory()
    assessment = AssessmentFactory(unit=user_unit.unit)
    x = UserAssessment.objects.init_user_assessment(user_unit, assessment)
    assert x.pk > 0
    assert user_unit == x.user_unit
    assert assessment == x.assessment
    # assert x.assessor is None
    # assert "" == x.assessor_comments
    # assert "" == x.answer
    assert "" == x.answer_file.name
    assert "" == x.answer_original_file_name
    assert x.answer_submitted is None
    assert x.can_retake is False
    x2 = UserAssessment.objects.init_user_assessment(user_unit, assessment)
    assert x.pk == x2.pk


# 10/02/2022, The unit is marked (not the assessment)
# @pytest.mark.parametrize("can_retake", [False, True])
# @pytest.mark.django_db
# def test_mark(can_retake):
#     assessor = UserFactory(is_staff=True)
#     user_assessment = UserAssessmentFactory()
#     # test
#     user_assessment.mark(assessor, "Orange", can_retake)
#     # check
#     user_assessment.refresh_from_db()
#     assert assessor == user_assessment.assessor
#     assert "Orange" == user_assessment.assessor_comments
#     assert timezone.now().date() == user_assessment.assessed.date()
#     assert user_assessment.can_retake is can_retake
#
#
# 10/02/2022, The unit is marked (not the assessment)
# @pytest.mark.django_db
# def test_mark_without_user_unit():
#     """
#
#     Is it possible to mark an assessment without a ``UserUnit`` record?
#
#     """
#     assessor = UserFactory(is_staff=True)
#
#     user_unit = UserUnitFactory()
#     assessment = AssessmentFactory(unit=user_unit.unit)
#     user_assessment = UserAssessmentFactory(assessment=assessment)
#     # test
#     user_assessment.mark(assessor, "Orange", can_retake=True)
#
#
# 10/02/2022, The unit is marked (not the assessment)
# @pytest.mark.django_db
# def test_mark_can_retake():
#     """
#     Copy from ``test_lms_models_userassessment_objects_resit_assessment``
#     (``lms/tests/test_lms_models.py``)
#
#     """
#     assessor = UserFactory(is_staff=True)
#     course = CourseFactory(name="Farming")
#     unit = UnitFactory(name="Water Supply")
#     CourseUnitFactory(course=course, unit=unit, order=1)
#     user_unit = UserUnitFactory(
#         user_course=UserCourseFactory(user=UserFactory(), course=course),
#         unit=unit,
#     )
#     assessment = AssessmentFactory(unit=user_unit.unit)
#     assert 0 == UserAssessment.objects.count()
#     user_assessment = UserAssessmentFactory(assessment=assessment)
#     assert 1 == UserAssessment.objects.count()
#     # test
#     user_assessment.mark(assessor, "Orange", can_retake=True)
#     # check
#     user_assessment.refresh_from_db()
#     assert user_assessment.can_retake is True
#     user_assessment_2 = UserAssessment.objects.sit_assessment(
#         user_unit, assessment, "My retake", None
#     )
#     assert 2 == UserAssessment.objects.count()
#     user_assessment_2.refresh_from_db()
#     assert user_assessment_2.can_retake is False
#     assert user_assessment_2.assessor is None


@pytest.mark.django_db
def test_answer_button_caption_assessment_retake_submitted():
    unit = UnitFactory(assignment_or_assessment=Unit.ASSESSMENT)
    user_unit = UserUnitFactory(
        unit=unit,
        submitted=timezone.now(),
        retake_submitted=timezone.now(),
        retake_assessed=timezone.now(),
    )
    assessment = AssessmentFactory(unit=user_unit.unit)
    user_assessment = UserAssessmentFactory(
        user_unit=user_unit,
        assessment=assessment,
        has_passed=False,
        can_retake=True,
        answer_submitted=timezone.now(),
        retake_submitted=timezone.now(),
        retake_grade=None,
        retake_has_passed=False,
    )
    assert "Retake Submitted" == user_assessment.answer_button_caption()


@pytest.mark.django_db
def test_answer_button_caption_assessment_retake_uploaded():
    unit = UnitFactory(assignment_or_assessment=Unit.ASSESSMENT)
    user_unit = UserUnitFactory(
        unit=unit, retake_assessed=timezone.now(), retake_submitted=None
    )
    assessment = AssessmentFactory(unit=user_unit.unit)
    user_assessment = UserAssessmentFactory(
        user_unit=user_unit,
        assessment=assessment,
        has_passed=False,
        can_retake=True,
        retake_submitted=timezone.now(),
        retake_grade=None,
        retake_has_passed=False,
    )
    assert "Retake Uploaded" == user_assessment.answer_button_caption()


@pytest.mark.django_db
def test_answer_button_caption_assessment_submitted():
    unit = UnitFactory(assignment_or_assessment=Unit.ASSESSMENT)
    user_unit = UserUnitFactory(unit=unit, submitted=timezone.now())
    user_assessment = UserAssessmentFactory(
        user_unit=user_unit,
        answer_submitted=timezone.now(),
        has_passed=False,
        can_retake=False,
        retake_submitted=None,
        retake_has_passed=False,
    )
    assert "Submitted" == user_assessment.answer_button_caption()


@pytest.mark.django_db
def test_answer_button_caption_assessment_uploaded():
    unit = UnitFactory(assignment_or_assessment=Unit.ASSESSMENT)
    user_unit = UserUnitFactory(unit=unit, submitted=None)
    user_assessment = UserAssessmentFactory(
        user_unit=user_unit,
        answer_submitted=timezone.now(),
        has_passed=False,
        can_retake=False,
        retake_submitted=None,
        retake_has_passed=False,
    )
    assert "Uploaded" == user_assessment.answer_button_caption()


@pytest.mark.django_db
def test_answer_button_caption_assignment_retake_fail():
    unit = UnitFactory(assignment_or_assessment=Unit.ASSIGNMENT)
    user_unit = UserUnitFactory(
        unit=unit,
        retake_assessed=timezone.now(),
        retake_submitted=timezone.now(),
        assessed=timezone.now(),
        result=1,
    )
    assessment = AssessmentFactory(unit=user_unit.unit)
    user_assessment = UserAssessmentFactory(
        user_unit=user_unit,
        assessment=assessment,
        has_passed=False,
        can_retake=True,
        retake_submitted=timezone.now(),
        retake_grade=75,
        retake_has_passed=False,
    )
    assert "FAIL" == user_assessment.answer_button_caption()


@pytest.mark.django_db
def test_answer_button_caption_assignment_retake_pass():
    unit = UnitFactory(assignment_or_assessment=Unit.ASSIGNMENT)
    user_unit = UserUnitFactory(
        unit=unit,
        retake_assessed=timezone.now(),
        retake_submitted=timezone.now(),
        assessed=timezone.now(),
        result=1,
    )
    assessment = AssessmentFactory(unit=user_unit.unit)
    user_assessment = UserAssessmentFactory(
        user_unit=user_unit,
        assessment=assessment,
        has_passed=False,
        can_retake=True,
        retake_submitted=timezone.now(),
        retake_grade=85,
        retake_has_passed=False,
    )
    assert "PASS" == user_assessment.answer_button_caption()


@pytest.mark.django_db
def test_answer_button_caption_assignment_retake_submitted():
    unit = UnitFactory(assignment_or_assessment=Unit.ASSIGNMENT)
    user_unit = UserUnitFactory(
        unit=unit,
        retake_assessed=timezone.now(),
        retake_submitted=timezone.now(),
    )
    assessment = AssessmentFactory(unit=user_unit.unit)
    user_assessment = UserAssessmentFactory(
        user_unit=user_unit,
        assessment=assessment,
        has_passed=False,
        can_retake=True,
        retake_submitted=timezone.now(),
        retake_grade=None,
        retake_has_passed=False,
    )
    assert "Retake Submitted" == user_assessment.answer_button_caption()


@pytest.mark.django_db
def test_answer_button_caption_assignment_retake_uploaded():
    unit = UnitFactory(assignment_or_assessment=Unit.ASSIGNMENT)
    user_unit = UserUnitFactory(
        unit=unit, retake_assessed=timezone.now(), retake_submitted=None
    )
    assessment = AssessmentFactory(unit=user_unit.unit)
    user_assessment = UserAssessmentFactory(
        user_unit=user_unit,
        assessment=assessment,
        has_passed=False,
        can_retake=True,
        retake_submitted=timezone.now(),
        retake_grade=None,
        retake_has_passed=False,
    )
    assert "Retake Uploaded" == user_assessment.answer_button_caption()


@pytest.mark.django_db
def test_answer_button_caption_assignment_submitted():
    unit = UnitFactory(assignment_or_assessment=Unit.ASSIGNMENT)
    user_unit = UserUnitFactory(unit=unit, submitted=timezone.now())
    user_assessment = UserAssessmentFactory(
        user_unit=user_unit,
        answer_submitted=timezone.now(),
        has_passed=False,
        can_retake=False,
        retake_submitted=None,
        retake_has_passed=False,
    )
    assert "Submitted" == user_assessment.answer_button_caption()


@pytest.mark.django_db
def test_answer_button_caption_assignment_uploaded():
    unit = UnitFactory(assignment_or_assessment=Unit.ASSIGNMENT)
    user_unit = UserUnitFactory(unit=unit, submitted=None)
    user_assessment = UserAssessmentFactory(
        user_unit=user_unit,
        answer_submitted=timezone.now(),
        has_passed=False,
        can_retake=False,
        retake_submitted=None,
        retake_has_passed=False,
    )
    assert "Uploaded" == user_assessment.answer_button_caption()


@pytest.mark.django_db
@pytest.mark.parametrize(
    "can_retake,has_passed,retake_has_passed,retake_assessed,expect",
    [
        (False, False, False, None, "FAIL"),
        (False, True, False, None, "PASS"),
        (True, False, False, None, "FAIL"),
        (True, False, False, timezone.now(), "FAIL"),
        (True, False, True, timezone.now(), "PASS"),
    ],
)
def test_answer_button_caption_assessment_answer_submitted(
    can_retake,
    has_passed,
    retake_has_passed,
    retake_assessed,
    expect,
):
    unit = UnitFactory(assignment_or_assessment=Unit.ASSESSMENT)
    user_unit = UserUnitFactory(
        unit=unit,
        submitted=timezone.now(),
        assessed=timezone.now(),
        result=1,
        retake_submitted=timezone.now(),
        retake_assessed=retake_assessed,
    )
    assessment = AssessmentFactory(unit=user_unit.unit)
    # 'retake_submitted' is not used by 'answer_button_caption'
    user_assessment = UserAssessmentFactory(
        user_unit=user_unit,
        assessment=assessment,
        has_passed=has_passed,
        answer_submitted=timezone.now(),
        can_retake=can_retake,
        retake_submitted=timezone.now(),
        retake_has_passed=retake_has_passed,
    )
    assert expect == user_assessment.answer_button_caption()


@pytest.mark.django_db
@pytest.mark.parametrize(
    "grade,expect",
    [
        (90, "PASS"),
        (79, "FAIL"),
        (None, "Submitted"),
    ],
)
def test_answer_button_caption_assignment(grade, expect):
    unit = UnitFactory(assignment_or_assessment=Unit.ASSIGNMENT)
    user_asssignment = UserAssessmentFactory(
        user_unit=UserUnitFactory(
            unit=unit,
            submitted=timezone.now(),
            assessed=timezone.now(),
            result=1,
        ),
        answer_submitted=timezone.now(),
        has_passed=False,
        grade=grade,
        can_retake=False,
        retake_submitted=None,
        retake_grade=None,
        retake_has_passed=False,
    )
    assert expect == user_asssignment.answer_button_caption()


@pytest.mark.django_db
def test_retake_answer_file_name():
    user_assessment = UserAssessmentFactory(
        retake_answer_file=SimpleUploadedFile(
            "my_retake_answer_file.doc", b"file contents"
        ),
    )
    assert "my_retake_answer_file" in user_assessment.retake_answer_file_name()


@pytest.mark.django_db
def test_retake_answer_file_name_original_file_name():
    user_assessment = UserAssessmentFactory(
        retake_answer_original_file_name="apple.pdf"
    )
    assert "apple.pdf" in user_assessment.retake_answer_file_name()


# @pytest.mark.django_db
# def test_set_can_retake():
#    user = UserFactory()
#    user_assessment = UserAssessmentFactory()
#    user_assessment.set_can_retake(user)
#    assert user_assessment.can_retake is True
#    assert user == user_assessment.can_retake_user
#    assert timezone.now().date() == user_assessment.can_retake_date.date()


# @pytest.mark.django_db
# def test_set_can_retake_already_set():
#    user_assessment = UserAssessment(can_retake=True)
#    with pytest.raises(ExamError) as e:
#        user_assessment.set_can_retake(UserFactory())
#    assert (
#        "Cannot retake student assessment '{}'. "
#        "The retake has already been approved.".format(user_assessment.pk)
#    ) in str(e.value)


@pytest.mark.django_db
def test_sat_user_assessment():
    course = CourseFactory(name="Farming")
    user = UserFactory()
    user_course = UserCourseFactory(user=user, course=course)
    unit = UnitFactory(name="Water Supply")
    assessment = AssessmentFactory(unit=unit)
    user_unit = UserUnitFactory(user_course=user_course, unit=unit)
    user_assessment = UserAssessmentFactory(
        user_unit=user_unit, assessment=assessment, can_retake=True
    )
    assert user_assessment == UserAssessment.objects.sat_user_assessment(
        user_course, assessment
    )


@pytest.mark.django_db
def test_sat_user_assessment_duplicate():
    """Check for duplicate 'UserAssessment' records.

    .. tip:: Check the method for more information...

    """
    course = CourseFactory(name="Farming")
    user = UserFactory()
    user_course = UserCourseFactory(user=user, course=course)
    unit = UnitFactory(name="Water Supply")
    assessment = AssessmentFactory(unit=unit)
    user_unit = UserUnitFactory(user_course=user_course, unit=unit)
    user_assessment_1 = UserAssessmentFactory(
        user_unit=user_unit, assessment=assessment, can_retake=True
    )
    user_assessment_1.set_deleted(user)
    user_assessment_2 = UserAssessmentFactory(
        user_unit=user_unit, assessment=assessment, can_retake=True
    )
    assert user_assessment_2 == UserAssessment.objects.sat_user_assessment(
        user_course, assessment
    )


@pytest.mark.django_db
def test_sit_assessment():
    course = CourseFactory(name="Farming")
    unit = UnitFactory(name="Water Supply")
    CourseUnitFactory(course=course, unit=unit)
    user_unit = UserUnitFactory(
        user_course=UserCourseFactory(user=UserFactory(), course=course),
        unit=unit,
    )
    assessment = AssessmentFactory(unit=unit)
    assert 0 == UserAssessment.objects.count()
    x = UserAssessment.objects.sit_assessment(
        user_unit,
        assessment,
        SimpleUploadedFile("my_answer_file.doc", b"file contents"),
    )
    assert user_unit == x.user_unit
    assert assessment == x.assessment
    assert "my_answer_file" in x.answer_file.name
    assert "my_answer_file" in x.answer_original_file_name
    assert "" == x.retake_answer_file.name
    assert "" == x.retake_answer_original_file_name
    assert timezone.now().date() == x.answer_submitted.date()
    assert x.can_retake is False
    # resubmit should not add more records
    UserAssessment.objects.sit_assessment(user_unit, assessment, "")
    UserAssessment.objects.sit_assessment(user_unit, assessment, "")
    assert 1 == UserAssessment.objects.count()


@pytest.mark.django_db
def test_sit_assessment_can_retake():
    course = CourseFactory(name="Farming")
    unit = UnitFactory(name="Water Supply")
    CourseUnitFactory(course=course, unit=unit)
    user_unit = UserUnitFactory(
        user_course=UserCourseFactory(user=UserFactory(), course=course),
        unit=unit,
    )
    assessment = AssessmentFactory(unit=unit)
    UserAssessmentFactory(
        user_unit=user_unit, assessment=assessment, can_retake=True
    )
    assert 1 == UserAssessment.objects.count()
    x = UserAssessment.objects.sit_assessment(
        user_unit,
        assessment,
        SimpleUploadedFile("the_retake_answer_file.doc", b"file contents"),
    )
    assert 1 == UserAssessment.objects.count()
    assert x.can_retake is True
    assert user_unit == x.user_unit
    assert assessment == x.assessment
    # created by the factory
    assert "the_answer_file" in x.answer_file.name
    assert "the_answer_file" in x.answer_original_file_name
    assert "the_retake_answer_file" in x.retake_answer_file.name
    assert "the_retake_answer_file" in x.retake_answer_original_file_name
    assert x.answer_submitted is None
    assert timezone.now().date() == x.retake_submitted.date()


@pytest.mark.django_db
def test_sit_assessment_no_user():
    course = CourseFactory(name="Farming")
    unit = UnitFactory(name="Water Supply")
    CourseUnitFactory(course=course, unit=unit)
    user_unit = UserUnitFactory(
        user_course=UserCourseFactory(
            user=UserFactory(), course=CourseFactory(name="IT")
        ),
        unit=unit,
    )
    assessment = AssessmentFactory(unit=unit)
    with pytest.raises(ExamError) as e:
        UserAssessment.objects.sit_assessment(user_unit, assessment, "")
    assert "'CourseUnit', 'Water Supply' is not linked to 'IT'" in str(e.value)


@pytest.mark.parametrize(
    "can_retake,expect",
    [
        (False, "Apple: Fruit (for 'patrick')"),
        (True, "Apple: Fruit (for 'patrick') (Retake)"),
    ],
)
@pytest.mark.django_db
def test_str(can_retake, expect):
    unit = UnitFactory(name="Fruit")
    CourseUnitFactory(course=CourseFactory(name="Tree"), unit=unit, order=1)
    assessment = AssessmentFactory(unit=unit, name="Apple")
    user_unit = UserUnitFactory(
        unit=unit,
        user_course=UserCourseFactory(user=UserFactory(username="patrick")),
    )
    user_assessment = UserAssessmentFactory(
        user_unit=user_unit, assessment=assessment, can_retake=can_retake
    )
    assert expect == str(user_assessment)
