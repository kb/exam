User Feedback
*************

- Students can see feedback before unit is assessed, `Ticket 7006`_
- Documentation prepared with the help of this management command::

    exam/management/commands/7006-student-can-see-feedback.py

- For requirements, see emails added to the WIP_ section below.

Before Marking
==============

.. note:: For `Pass First Time`_ and Retake_

Assessment has been uploaded and submitted by the student:

.. image:: ./misc/2024-02-06-user-unit-mark-init.png

.. code-block:: text

  UserUnit
  retake_assessed                 None
  is_marking_finished_retake      False

  result                          None
  result_assessed                 None
  assessed                        None
  is_marking_finished             False

.. code-block:: text

  UserAssessment
  can_retake                      False
  has_passed                      False
  assessed                        None
  assessor_comments
  feedback_enabled_for_retake     False

  retake_assessed                 None
  answer_button_caption           Submitted
  feedback_enabled                False

.. tip:: This isn't a clean data-set, so some error counts have already been
         entered.

Pass First Time
===============

Pass assessment (no retake)
---------------------------

Tick the *Has passed* box and enter a comment.

.. image:: ./misc/2024-02-06-user-unit-mark-pass.png

.. image:: ./misc/2024-02-06-user-unit-mark-pass-summary.png

.. code-block:: text

  UserUnit (no change)

.. code-block:: text

  UserAssessment
  has_passed                      True
  assessed                        2024-02-06 13:09:33.810801+00:00
  assessor_comments               You have passed this assessment.

Result (no retake)
------------------

Enter the *Result*.

.. note:: The comments also needs to be entered before the unit is *Marked*.
          (see next step).

.. image:: ./misc/2024-02-06-user-unit-result.png

.. image:: ./misc/2024-02-06-user-unit-result-summary.png

.. code-block:: text

  UserUnit
  result                          65
  result_assessed                 2024-02-06 13:18:00.246296+00:00

.. code-block:: text

  UserAssessment (no change)

Mark - add comments (no retake)
-------------------------------

Add the comments to *complete* the marking.

.. image:: ./misc/2024-02-06-user-unit-comment.png

.. image:: ./misc/2024-02-06-user-unit-comment-summary.png

Feedback is displayed to the student

.. image:: ./misc/2024-02-06-user-unit-comment-assessment.png

.. code-block:: text

  UserUnit
  assessed                        2024-02-06 13:26:10.405591+00:00
  is_marking_finished             True

.. code-block:: text

  UserAssessment
  retake_assessed                 None
  answer_button_caption           PASS
  feedback_enabled                True

Retake
======

.. note:: Starting from `Before Marking`_

*Can retake*, to indicate a retake is required for the assessment.

.. image:: ./misc/2024-02-06-user-unit-retake.png

.. image:: ./misc/2024-02-06-user-unit-retake-summary.png

.. code-block:: text

  UserUnit (no change)

.. code-block:: text

  UserAssessment
  can_retake                      True
  assessed                        2024-02-06 13:53:04.304537+00:00
  assessor_comments               Please try again...

Retake Comments (unit)
----------------------

To complete the marking for the retake, enter *Retake Comments*

.. image:: ./misc/2024-02-06-user-unit-retake-comments.png

.. image:: ./misc/2024-02-06-user-unit-retake-comments-summary.png

.. image:: ./misc/2024-02-06-user-unit-retake-comments-assessment-summary.png

.. code-block:: text

  UserUnit
  retake_assessed                 2024-02-06 15:05:28.150302+00:00
  is_marking_finished_retake      True

.. code-block:: text

  UserAssessment
  feedback_enabled_for_retake     True
  answer_button_caption           Retake Submitted

Mark Retake (assessment)
------------------------

.. image:: ./misc/2024-02-06-user-unit-retake-mark-assessment-retake.png

.. image:: ./misc/2024-02-06-user-unit-retake-mark-assessment-retake-summary.png

.. code-block:: text

  UserUnit (no change)

.. code-block:: text

  UserAssessment
  has_passed                      False
  retake_assessed                 2024-02-06 15:15:15.386671+00:00
  retake_has_passed               True
  feedback_enabled                False

Result (unit with a retake)
---------------------------

Same as `Result (no retake)`_

Mark - add comments (unit with a retake)
----------------------------------------

Add the comments to *complete* the marking.

.. image:: ./misc/2024-02-06-user-unit-comment.png

.. image:: ./misc/2024-02-06-user-unit-retake-comment-summary.png

Feedback is displayed to the student

.. image:: ./misc/2024-02-06-user-unit-retake-marked-assessment-summary.png

.. code-block:: text

  UserUnit
  result                          23
  result_assessed                 2024-02-06 15:31:07.481718+00:00
  assessed                        2024-02-06 15:32:29.945308+00:00
  is_marking_finished             True

.. code-block:: text

  UserAssessment
  can_retake                      True
  has_passed                      False
  feedback_enabled_for_retake     True
  answer_button_caption           PASS
  feedback_enabled                True

WIP
===

email from Megan, 01/02/2024
----------------------------

... when I add the Unit comments that means I have finished marking the
retake.

Basically, for the first submission when I finish marking I either add unit
comments if they have passed or retake comments if they have not.
For the second submission when I finish marking I add unit comments.

email to Megan, 01/02/2024
--------------------------

I have a question:

Imagine you are marking a unit with three assessments and you ask the student
to retake one of them. How do we know when you have finished reviewing the
other two assessments (amending feedback and recalculating error totals)?

Is it when you enter the retake comments for the unit?

.. image:: ./misc/2024-02-01-retake-comments.png

email from Megan, 31/01/2024
----------------------------

We do not want Students to see anything that we put on the Assessors Area as
we go along.

When I mark I often have to flick between assessments, amend feedback I have
already written and recalculate error totals as I go along.
Until I have marked the whole unit nothing I have added to the individual
assessment boxes is final, it is all subject to change.
Only when I am happy with everything, can I go and add the overall mark and
comments.
Adding the comments, or requesting a retake is the point when marking is
finished and ready for the Students to see their feedback etc.
That is why I was so concerned that Students were seeing things I was adding
before I got to this stage.

This is the same with assignments. Even though it is one document, we do not
want students seeing anything Seamus adds as feedback/mark before I have
checked it.
So again, they should see nothing until we add the overall comments or say
they can retake.    

Reading that email from Emma, I read that to mean that she wants Students to
see the word PASS next to their overall mark when they receive it, to make it
clear that they have passed and make it consistent with how we display the
Achievement Test results.
She wouldn't have wanted Students to see each assessment instantly because she
would have known that assessments can not be marked individually like that. 

I think that more confusion has arisen because I am aware that she made a
further request involving when Students see the word 'PASS' on their work.
At some point, I know she also asked that when Students retake their work we
start showing them all their feedback not just the feedback associated with
the assessments that we have requested that they retake.
This meant that we needed to be able to mark an individual assessment as a
PASS to make it clear that they do not need to resubmit this one.
Have these two requests got mixed up and made into one maybe?
I was not part of these conversations so I am only assuming. 

It is my understanding that what is needed is two separate things:

- Firstly, when Students are successful and they get their overall results
  (which they get for the complete unit at once, not individually)
  they also get told whether their percentage result is a
  PASS (80% and above), or FAIL.  
- Secondly, when a Student is asked to retake assessments they need to be
  shown not only the feedback for assessments they need to resubmit but also
  the feedback for assessments that they have passed.
  These need to be marked as PASS to make it clear they do not need to
  resubmit these assessments.

Sorry to make things more confusing, but I think Emma also asked for a further
update that she thought was associated with this update last week.
This was the update that changed the wording to show whether a Student had
uploaded work or submitted it.
At the moment when a Student uploads a document the text next to that
assessment/assignment changes to submitted straight away.
We wanted to update it so that it said uploaded and then only changed to
submitted once they clicked the 'Submit Coursework' button.
Emma thought this request might be associated with the current update as they
all involve the word that is shown next to the assessment.
If this is not the case then please treat this section as separate.


.. _`Ticket 7006`: https://www.kbsoftware.co.uk/crm/ticket/7006/
