Exam
****

.. highlight:: python

To list the exams taken by a user::

  from exam.models import UserExamVersion

  for item in UserExamVersion.objects.all().order_by('pk'):
      print(item.pk, item.user, item.exam_version.exam, item.result, item.user.first_name)

To get the calculated result for a user exam version::

  user_exam_version = UserExamVersion.objects.get(pk=79)
  user_exam_version._result()

To change an answer, get the question number (in this example ``6``)::

  from exam.models import Selection

  selection = Selection.objects.get(
      user_exam_version=user_exam_version,
      answer__question__number=6,
  )

  # the answer given by the user is
  selection.answer
  # the correct answer is
  selection.answer.question.answer

  # to set the correct answer
  selection.answer = selection.answer.question.answer
  selection.save()

To set the result::

  user_exam_version = UserExamVersion.objects.get(pk=79)
  user_exam_version.user.username
  # user_exam_version.result = 18
  user_exam_version.result = user_exam_version._result()
  user_exam_version.save()
