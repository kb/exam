# -*- encoding: utf-8 -*-
import datetime
import pytest

from datetime import date
from django.core.files.uploadedfile import SimpleUploadedFile
from django.urls import reverse
from django.utils import timezone
from freezegun import freeze_time
from http import HTTPStatus

from exam.models import (
    Assessment,
    Course,
    CourseProduct,
    CourseUnit,
    Exam,
    ExamError,
    ProductExam,
    Unit,
    UnitExam,
    UserCourse,
    UserExamVersion,
    UserExamVersionProduct,
    UserAssessmentAudit,
    UserUnit,
    UserUnitAudit,
)
from exam.tests.factories import (
    AssessmentFactory,
    CourseFactory,
    CourseProductFactory,
    CourseUnitFactory,
    ExamFactory,
    ExamSettingsFactory,
    ExamVersionFactory,
    OptionFactory,
    ProductExamFactory,
    QuestionFactory,
    SelectionFactory,
    UnitExamFactory,
    UnitFactory,
    UserAssessmentFactory,
    UserCourseFactory,
    UserExamVersionFactory,
    UserUnitFactory,
)
from exam.tests.scenario import set_correct_answers_all, setup_exam
from exam.views import _sort_current_to_mark, _submit_exam_for_marking
from login.tests.factories import TEST_PASSWORD, UserFactory
from mail.models import Message
from mail.tests.factories import MailTemplateFactory
from stock.tests.factories import ProductCategoryFactory, ProductFactory


def _question_data(question, answer, opt1, opt2):
    return {
        # question
        "question": "Apple",
        "answer": answer.pk,
        # management form
        "form-INITIAL_FORMS": "2",
        "form-MAX_NUM_FORMS": "1000",
        "form-MIN_NUM_FORMS": "0",
        "form-TOTAL_FORMS": "2",
        # formset
        "form-0-id": opt1.pk,
        "form-0-option": "Crumble",
        "form-1-id": opt2.pk,
        "form-1-option": "Pie",
    }


@pytest.mark.django_db
def test_assessment_create(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    data = {
        "instructions": "Fruit",
        "name": "Apple",
        "order": "1",
        "assessment_type": "w",
        "what_to_do": "What!",
    }
    unit = UnitFactory()
    AssessmentFactory(unit=unit, order=2)
    with pytest.raises(Assessment.DoesNotExist):
        Assessment.objects.get(name="Apple")
    url = reverse("exam.assessment.create", args=[unit.pk])
    response = client.post(url, data=data)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert reverse("exam.unit.detail", args=[unit.pk]) == response.url
    assert 2 == Assessment.objects.count()
    assessment = Assessment.objects.get(name="Apple")
    assert 1 == assessment.order
    assert "Apple" == assessment.name
    assert "Fruit" == assessment.instructions
    assert "w" == assessment.assessment_type
    assert "What!" == assessment.what_to_do


@pytest.mark.django_db
def test_assessment_create_duplicate_order(client):
    unit = UnitFactory()
    AssessmentFactory(unit=unit, order=2)
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    data = {
        "instructions": "Fruit",
        "name": "Apple",
        "order": "2",
        "assessment_type": "w",
        "what_to_do": "What!",
    }
    url = reverse("exam.assessment.create", args=[unit.pk])
    response = client.post(url, data=data)
    assert HTTPStatus.OK == response.status_code
    assert {
        "order": [
            (
                "An assessment already exists with order 2. "
                "The next available 'order' number is 3"
            )
        ]
    } == response.context["form"].errors


@pytest.mark.django_db
def test_assessment_create_duplicate_order_deleted(caplog, client):
    """The duplicate record is deleted."""
    user = UserFactory(is_staff=True)
    unit = UnitFactory(name="Dairy Farming")
    assessment_1 = AssessmentFactory(unit=unit, order=Assessment.HIGH_ORDER + 1)
    assessment_1.set_deleted(user)
    assessment_2 = AssessmentFactory(unit=unit, name="Milking", order=2)
    assessment_2.set_deleted(user)
    AssessmentFactory(unit=unit, order=12)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    data = {
        "instructions": "Fruit",
        "name": "Apple",
        "order": "2",
        "assessment_type": "w",
        "what_to_do": "What!",
    }
    url = reverse("exam.assessment.create", args=[unit.pk])
    response = client.post(url, data=data)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert reverse("exam.unit.detail", args=[unit.pk]) == response.url
    assessment_2.refresh_from_db()
    assert Assessment.HIGH_ORDER + 2 == assessment_2.order
    logs = [x.message for x in caplog.records if x.levelname == "INFO"]
    assert 1 == len(logs)
    assert (
        f"Updated 'order' from 2 to 1000001 for deleted assessment "
        f"'Milking' ({assessment_2.pk}) for 'Dairy Farming' unit ({unit.pk})"
    ) in logs[0]


@pytest.mark.django_db
def test_assessment_delete(client):
    user = UserFactory(is_staff=True)
    assessment = AssessmentFactory()
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("exam.assessment.delete", args=[assessment.pk])
    response = client.post(url)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert (
        reverse("exam.unit.detail", args=[assessment.unit.pk]) == response.url
    )
    assert 1 == Assessment.objects.count()
    assessment.refresh_from_db()
    assert assessment.is_deleted is True


@pytest.mark.django_db
def test_assessment_update(client):
    user = UserFactory(is_staff=True)
    assessment = AssessmentFactory(
        name="Orange",
        instructions="Colour",
        order=12,
        assessment_type="u",
        what_to_do="Peach",
    )
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    data = {
        "name": "Apple",
        "instructions": "Fruit",
        "order": "12",
        "assessment_type": "w",
        "what_to_do": "Nectarine",
    }
    url = reverse("exam.assessment.update", args=[assessment.pk])
    response = client.post(url, data=data)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert (
        reverse("exam.unit.detail", args=[assessment.unit.pk]) == response.url
    )
    assert 1 == Assessment.objects.count()
    assessment.refresh_from_db()
    assert 12 == assessment.order
    assert "Apple" == assessment.name
    assert "Fruit" == assessment.instructions
    assert "w" == assessment.assessment_type
    assert "Nectarine" == assessment.what_to_do


@pytest.mark.django_db
def test_assessment_update_duplicate_order(client):
    user = UserFactory(is_staff=True)
    unit = UnitFactory()
    assessment = AssessmentFactory(unit=unit, order=4)
    assessment = AssessmentFactory(
        unit=unit,
        name="Orange",
        order=3,
        instructions="Colour",
        assessment_type="u",
        what_to_do="Peach",
    )
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    data = {
        "name": "Apple",
        "instructions": "Fruit",
        "order": "4",
        "assessment_type": "w",
        "what_to_do": "Nectarine",
    }
    url = reverse("exam.assessment.update", args=[assessment.pk])
    response = client.post(url, data=data)
    assert HTTPStatus.OK == response.status_code
    assert {
        "order": [
            (
                "An assessment already exists with order 4. "
                "The next available 'order' number is 5"
            )
        ]
    } == response.context["form"].errors


@pytest.mark.django_db
def test_course_create(client):
    product = ProductFactory()
    ExamSettingsFactory(course=product.category.product_type)
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    assert 0 == Course.objects.count()
    assert 0 == CourseProduct.objects.count()
    data = {
        "name": "Name",
        "title": "Title",
        "description": "Description",
        "assignment_or_assessment": "assignment",
    }
    url = reverse("exam.course.create")
    response = client.post(url, data=data)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert 1 == Course.objects.count()
    course = Course.objects.first()
    assert reverse("exam.course.detail", args=[course.pk]) == response.url
    assert "Name" == course.name
    assert "Title" == course.title
    assert "Description" == course.description
    # assert "assignment" == course.get_assignment_or_assessment(1)
    assert course.is_tailored is False
    assert 0 == CourseProduct.objects.count()


@pytest.mark.django_db
def test_course_create_with_product(client):
    product = ProductFactory()
    ExamSettingsFactory(course=product.category.product_type)
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    assert 0 == Course.objects.count()
    assert 0 == CourseProduct.objects.count()
    data = {
        "name": "Name",
        "title": "Title",
        "description": "Description",
        "assignment_or_assessment": "assignment",
        "products": [product.pk],
    }
    url = reverse("exam.course.create")
    response = client.post(url, data=data)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert 1 == Course.objects.count()
    course = Course.objects.first()
    assert reverse("exam.course.detail", args=[course.pk]) == response.url
    assert "Name" == course.name
    assert "Title" == course.title
    assert "Description" == course.description
    # assert "assignment" == course.get_assignment_or_assessment(1)
    assert course.is_tailored is False
    assert 1 == CourseProduct.objects.count()
    course_product = CourseProduct.objects.first()
    assert course == course_product.course
    assert product == course_product.product


@pytest.mark.django_db
def test_course_delete(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    course = CourseFactory()
    url = reverse("exam.course.delete", args=[course.pk])
    response = client.post(url)
    assert HTTPStatus.FOUND == response.status_code
    assert reverse("exam.course.list") == response.url
    course.refresh_from_db()
    assert course.is_deleted is True


@pytest.mark.django_db
def test_course_detail(client):
    user = UserFactory(is_staff=True)
    course = CourseFactory()
    unit_a = UnitFactory(name="a")
    CourseUnitFactory(course=course, unit=unit_a)
    unit_b = UnitFactory(name="b")
    CourseUnitFactory(course=course, unit=unit_b)
    unit_b.set_deleted(user)
    unit_c = UnitFactory(name="c")
    CourseUnitFactory(course=course, unit=unit_c)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(reverse("exam.course.detail", args=[course.pk]))
    assert HTTPStatus.OK == response.status_code
    assert "course" in response.context
    assert "object" in response.context
    assert ["a", "c"] == [x.unit.name for x in response.context["course_units"]]


@pytest.mark.django_db
def test_course_list(client):
    user = UserFactory(is_staff=True)
    CourseFactory(name="a")
    course = CourseFactory(name="b")
    course.set_deleted(user)
    CourseFactory(name="c")
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("exam.course.list")
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    assert 2 == len(response.context["object_list"])


@pytest.mark.django_db
def test_course_unit_create(client):
    category = ProductCategoryFactory()
    product = ProductFactory(name="Farming", category=category)
    ExamSettingsFactory(unit=category.product_type)
    course = CourseFactory()
    unit = UnitFactory()
    user = UserFactory(is_staff=True)
    assert 0 == CourseUnit.objects.count()
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("exam.course.unit.create", args=[course.pk])
    response = client.post(
        url,
        {
            "unit": unit.pk,
            "name": "Pear",
            "ordering": 11,
            "product": product.pk,
        },
    )
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert reverse("exam.course.detail", args=[course.pk]) == response.url
    assert 1 == CourseUnit.objects.count()
    course_unit = CourseUnit.objects.first()
    assert unit == course_unit.unit
    assert course == course_unit.course
    assert 11 == course_unit.order
    assert "Pear" == course_unit.name
    assert product == course_unit.product
    assert course_unit.core is False


@pytest.mark.django_db
def test_course_unit_create_exists(client):
    """The 'course_unit' was created previously, but then deleted."""
    ExamSettingsFactory()
    course = CourseFactory()
    unit = UnitFactory()
    user = UserFactory(is_staff=True)
    # delete the course unit
    course_unit = CourseUnitFactory(course=course, unit=unit)
    course_unit.set_deleted(user)
    assert 1 == CourseUnit.objects.count()
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("exam.course.unit.create", args=[course.pk])
    response = client.post(
        url, {"unit": unit.pk, "name": "Grape", "ordering": 11, "core": True}
    )
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert reverse("exam.course.detail", args=[course.pk]) == response.url
    assert 1 == CourseUnit.objects.count()
    course_unit = CourseUnit.objects.first()
    assert unit == course_unit.unit
    assert course == course_unit.course
    assert 11 == course_unit.order


@pytest.mark.django_db
def test_course_unit_create_get(client):
    ExamSettingsFactory()
    course = CourseFactory()
    # unit = UnitFactory()
    user = UserFactory(is_staff=True)
    assert 0 == CourseUnit.objects.count()
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("exam.course.unit.create", args=[course.pk])
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    form = response.context["form"]
    ordering = form.fields["ordering"]
    assert [
        (1, 1),
        (2, 2),
        (3, 3),
        (4, 4),
        (5, 5),
        (6, 6),
        (7, 7),
        (8, 8),
        (9, 9),
        (10, 10),
        (11, 11),
        (12, 12),
        (13, 13),
        (14, 14),
        (15, 15),
    ] == ordering.choices


@pytest.mark.django_db
def test_course_unit_delete(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    course = CourseFactory()
    unit = UnitFactory()
    course_unit = CourseUnitFactory(course=course, unit=unit, order=1)
    url = reverse("exam.course.unit.delete", args=[course_unit.pk])
    response = client.post(url)
    assert HTTPStatus.FOUND == response.status_code
    assert reverse("exam.course.detail", args=[course.pk]) == response.url
    # course
    course.refresh_from_db()
    assert course.is_deleted is False
    # unit
    unit.refresh_from_db()
    assert unit.is_deleted is False
    # course unit
    course_unit.refresh_from_db()
    assert course_unit.is_deleted is True


@pytest.mark.django_db
def test_course_unit_update(client):
    category = ProductCategoryFactory()
    product = ProductFactory(name="Farming", category=category)
    ExamSettingsFactory(unit=category.product_type)
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    course = CourseFactory()
    unit = UnitFactory()
    course_unit = CourseUnitFactory(
        course=course, unit=unit, order=1, product=None
    )
    assert course_unit.core is True
    CourseUnitFactory(course=course, unit=UnitFactory(), order=2)
    CourseUnitFactory(course=course, unit=UnitFactory(), order=3)
    url = reverse("exam.course.unit.update", args=[course_unit.pk])
    response = client.post(
        url,
        {"ordering": 5, "name": "Apple", "core": False, "product": product.pk},
    )
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert reverse("exam.course.detail", args=[course.pk]) == response.url
    course_unit.refresh_from_db()
    assert 5 == course_unit.order
    assert "Apple" == course_unit.name
    assert course_unit.core is False
    assert product == course_unit.product


@pytest.mark.django_db
def test_course_unit_update_core_no_product(client):
    """Update the course unit.

    .. note:: ``core`` units do *not* need a product.

    """
    ExamSettingsFactory()
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    course = CourseFactory()
    unit = UnitFactory()
    course_unit = CourseUnitFactory(
        course=course, unit=unit, order=1, product=None
    )
    assert course_unit.core is True
    CourseUnitFactory(course=course, unit=UnitFactory(), order=2)
    CourseUnitFactory(course=course, unit=UnitFactory(), order=3)
    url = reverse("exam.course.unit.update", args=[course_unit.pk])
    response = client.post(
        url,
        {"ordering": 5, "name": "Apple", "core": True},
    )
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert reverse("exam.course.detail", args=[course.pk]) == response.url
    course_unit.refresh_from_db()
    assert 5 == course_unit.order
    assert "Apple" == course_unit.name
    assert course_unit.core is True
    assert course_unit.product is None


@pytest.mark.django_db
def test_course_unit_update_invalid_core_no_product(client):
    """If the unit is not 'core', then it must have a product."""
    ExamSettingsFactory()
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    course = CourseFactory()
    unit = UnitFactory()
    course_unit = CourseUnitFactory(
        course=course, unit=unit, order=1, product=None
    )
    assert course_unit.core is True
    CourseUnitFactory(course=course, unit=UnitFactory(), order=2)
    CourseUnitFactory(course=course, unit=UnitFactory(), order=3)
    url = reverse("exam.course.unit.update", args=[course_unit.pk])
    response = client.post(url, {"ordering": 5, "name": "Apple", "core": False})
    assert HTTPStatus.OK == response.status_code
    form = response.context["form"]
    assert {"__all__": ["Optional units must have a product"]} == form.errors


@pytest.mark.django_db
def test_course_unit_update_get(client):
    ExamSettingsFactory()
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    course = CourseFactory()
    unit = UnitFactory()
    course_unit = CourseUnitFactory(
        course=course, unit=unit, name="Orange", order=4
    )
    CourseUnitFactory(course=course, unit=UnitFactory(), order=2)
    CourseUnitFactory(course=course, unit=UnitFactory(), order=3)
    url = reverse("exam.course.unit.update", args=[course_unit.pk])
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    form = response.context["form"]
    assert {
        "ordering": 4,
        "name": "Orange",
        "core": True,
        "product": None,
    } == form.initial
    ordering = form.fields["ordering"]
    assert [
        (1, 1),
        (4, 4),
        (5, 5),
        (6, 6),
        (7, 7),
        (8, 8),
        (9, 9),
        (10, 10),
        (11, 11),
        (12, 12),
        (13, 13),
        (14, 14),
        (15, 15),
        (16, 16),
        (17, 17),
    ] == ordering.choices


@pytest.mark.django_db
def test_course_update(client):
    product = ProductFactory()
    ExamSettingsFactory(course=product.category.product_type)
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    course = CourseFactory()
    course_product = CourseProductFactory(course=course, product=product)
    data = {
        "name": "New Course Name",
        "description": "New Course Description",
        "title": "New Course Title",
        "assignment_or_assessment": "assignment",
    }
    url = reverse("exam.course.update", args=[course.pk])
    response = client.post(url, data=data)
    assert HTTPStatus.FOUND == response.status_code
    assert reverse("exam.course.detail", args=[course.pk]) == response.url
    course.refresh_from_db()
    assert "New Course Name" == course.name
    assert "New Course Description" == course.description
    assert "New Course Title" == course.title
    # assert "assignment" == course.get_assignment_or_assessment(1)
    assert 1 == CourseProduct.objects.count()
    course_product = CourseProduct.objects.first()
    assert course_product.is_deleted is True


@pytest.mark.django_db
def test_course_update_get(client):
    category = ProductCategoryFactory()
    product = ProductFactory(name="a", category=category)
    ExamSettingsFactory(course=category.product_type)
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    course = CourseFactory()
    # included (because already linked to this course)
    course_product_1 = CourseProductFactory(course=course, product=product)
    # not included (because linked to another course)
    course_product_2 = CourseProductFactory(
        course=CourseFactory(),
        product=ProductFactory(category=category, name="b"),
    )
    # included (because the product has not been deleted)
    course_product_3 = CourseProductFactory(
        course=course,
        product=ProductFactory(category=category, name="c"),
    )
    course_product_3.set_deleted(UserFactory())
    # included (because already linked to this course)
    course_product_4 = CourseProductFactory(
        course=course,
        product=ProductFactory(category=category, name="d"),
    )
    # included (not linked to any other products)
    ProductFactory(category=category, name="e"),
    # not included (because is not the correct 'product_type')
    ProductFactory(category=ProductCategoryFactory(), name="f"),
    url = reverse("exam.course.update", args=[course.pk])
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    form = response.context["form"]
    qs = form.fields["products"].queryset
    assert ["a", "c", "d", "e"] == [x.name for x in qs]
    # assert "assessment" == form.fields["assignment_or_assessment"].initial


@pytest.mark.django_db
def test_course_update_with_products(client):
    product = ProductFactory()
    ExamSettingsFactory(course=product.category.product_type)
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    course = CourseFactory()
    data = {
        "name": "New Course Name",
        "description": "New Course Description",
        "products": [product.pk],
        "title": "New Course Title",
        "assignment_or_assessment": "assessment",
    }
    url = reverse("exam.course.update", args=[course.pk])
    response = client.post(url, data=data)
    assert HTTPStatus.FOUND == response.status_code
    assert reverse("exam.course.detail", args=[course.pk]) == response.url
    course.refresh_from_db()
    assert "New Course Name" == course.name
    assert "New Course Description" == course.description
    assert "New Course Title" == course.title
    # assert "assessment" == course.get_assignment_or_assessment(1)
    assert 1 == CourseProduct.objects.count()
    course_product = CourseProduct.objects.first()
    assert course_product.is_deleted is False
    assert course == course_product.course
    assert product == course_product.product


@pytest.mark.django_db
def test_download_unit_course_materials(client):
    user = UserFactory(is_staff=False)
    course = CourseFactory()
    unit = UnitFactory()
    CourseUnitFactory(course=course, unit=unit, order=1)
    user_course = UserCourse.objects.enrol(user, course)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    with pytest.raises(ExamError) as e:
        client.get(
            reverse(
                "exam.unit.download.course.materials",
                args=[unit.pk],
            )
        )
    # this assert is what we want to prove the test has passed
    assert (
        "Unit {} (for '{}') does not have any course "
        "material".format(unit.pk, user.username)
    ) in str(e.value)


@pytest.mark.django_db
def test_download_unit_course_materials_is_staff(client):
    user = UserFactory(is_staff=True)
    course = CourseFactory()
    unit = UnitFactory()
    CourseUnitFactory(course=course, unit=unit, order=1)
    # user is not registered for this course!
    # user_course = UserCourse.objects.enrol(user, course)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    with pytest.raises(ExamError) as e:
        client.get(
            reverse(
                "exam.unit.download.course.materials",
                args=[unit.pk],
            )
        )
    # this assert is what we want to prove the test has passed
    assert (
        "Unit {} (for '{}') does not have any course "
        "material".format(unit.pk, user.username)
    ) in str(e.value)


@pytest.mark.django_db
def test_download_unit_course_materials_not_registered(client):
    user = UserFactory(is_staff=False)
    course = CourseFactory()
    unit = UnitFactory()
    CourseUnitFactory(course=course, unit=unit, order=1)
    # user is not registered for this course!
    # user_course = UserCourse.objects.enrol(user, course)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(
        reverse(
            "exam.unit.download.course.materials",
            args=[unit.pk],
        )
    )
    assert HTTPStatus.FORBIDDEN == response.status_code


@pytest.mark.django_db
def test_user_assessment_download_blank_form(client):
    user = UserFactory(is_staff=False, username="patrick")
    course = CourseFactory()
    unit = UnitFactory()
    CourseUnitFactory(course=course, unit=unit, order=1)
    assessment = AssessmentFactory(
        unit=unit,
        name="2a | Letter of Claim",
        blank_form=SimpleUploadedFile("my_blank_form.pdf", b"file contents"),
    )
    UserCourse.objects.enrol(user, course)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(
        reverse("exam.assessment.download.blank.form", args=[assessment.pk])
    )
    assert HTTPStatus.OK == response.status_code
    assert "application/pdf" == response.headers.get("Content-Type")
    assert (
        'attachment; filename="2a-Letter-of-Claim-Blank-Form.pdf"'
        == response.headers.get("Content-Disposition")
    )


@pytest.mark.django_db
def test_user_assessment_download_blank_form_does_not_exist(client):
    user = UserFactory(is_staff=False, username="patrick")
    course = CourseFactory()
    unit = UnitFactory()
    CourseUnitFactory(course=course, unit=unit, order=1)
    assessment = AssessmentFactory(unit=unit, blank_form=None)
    UserCourse.objects.enrol(user, course)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    with pytest.raises(ExamError) as e:
        client.get(
            reverse(
                "exam.assessment.download.blank.form",
                args=[assessment.pk],
            )
        )
    assert (
        "Assessment {} (for '{}') does not have a blank "
        "form".format(assessment.pk, user.username)
    ) in str(e.value)


@pytest.mark.django_db
def test_user_assessment_download_example_form(client):
    user = UserFactory(is_staff=False)
    course = CourseFactory()
    unit = UnitFactory()
    CourseUnitFactory(course=course, unit=unit, order=1)
    assessment = AssessmentFactory(
        unit=unit,
        name="2a | Letter of Claim",
        blank_form=SimpleUploadedFile("my_blank_form.docx", b"file contents"),
    )
    UserCourse.objects.enrol(user, course)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(
        reverse("exam.assessment.download.example.form", args=[assessment.pk])
    )
    assert (
        # "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
        "application/octet-stream"
        == response.headers.get("Content-Type")
    )
    assert (
        #'attachment; filename="2a-Letter-of-Claim-Example-Form.docx"'
        'attachment; filename="2a-Letter-of-Claim-Example-Form.dat"'
        == response.headers.get("Content-Disposition")
    )


@pytest.mark.django_db
def test_user_assessment_download_example_form_does_not_exist(client):
    user = UserFactory(is_staff=False)
    course = CourseFactory()
    unit = UnitFactory()
    CourseUnitFactory(course=course, unit=unit, order=1)
    assessment = AssessmentFactory(unit=unit, example_form=None)
    UserCourse.objects.enrol(user, course)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    with pytest.raises(ExamError) as e:
        client.get(
            reverse(
                "exam.assessment.download.example.form",
                args=[assessment.pk],
            )
        )
    assert (
        "Assessment {} (for '{}') does not have an example "
        "form".format(assessment.pk, user.username)
    ) in str(e.value)


@pytest.mark.django_db
def test_sort_current_to_mark():
    course_a = CourseFactory()
    course_b = CourseFactory()
    unit_1 = UnitFactory()
    unit_2 = UnitFactory()
    CourseUnitFactory(course=course_a, unit=unit_1, name="Apple")
    CourseUnitFactory(course=course_b, unit=unit_2, name="Orange")
    user = UserFactory()
    # course a
    UserUnitFactory(
        user_course=UserCourseFactory(
            course=course_a, user=user, assessor_comments="a"
        ),
        unit=unit_1,
        submitted=timezone.now(),
    )
    # course b
    UserUnitFactory(
        user_course=UserCourseFactory(
            course=course_b, user=user, assessor_comments="b"
        ),
        unit=unit_2,
        retake_submitted=timezone.now(),
    )
    # test
    user_units = UserUnit.objects.current_to_mark()
    result = [x["unit_name"] for x in _sort_current_to_mark(user_units)]
    assert ["Orange", "Apple"] == result


@pytest.mark.django_db
def test_exam_create(client):
    product = ProductFactory()
    ExamSettingsFactory(course=product.category.product_type)
    # login
    u = UserFactory(is_staff=True)
    assert client.login(username=u.username, password=TEST_PASSWORD) is True
    # check
    assert 0 == Exam.objects.count()
    # test
    url = reverse("exam.create")
    data = {"name": "Apple", "products": [product.pk]}
    response = client.post(url, data)
    # check
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert 1 == Exam.objects.count()
    exam = Exam.objects.first()
    expect = reverse("exam.detail", args=[exam.pk])
    assert expect == response["Location"]
    exam.refresh_from_db()
    assert "Apple" == exam.name
    assert 1 == ProductExam.objects.count()
    product_exam = ProductExam.objects.first()
    assert exam == product_exam.exam
    assert product == product_exam.product


@pytest.mark.django_db
def test_exam_update(client):
    product = ProductFactory()
    ExamSettingsFactory(course=product.category.product_type)
    exam = ExamFactory(name="Orange")
    # login
    u = UserFactory(is_staff=True)
    assert client.login(username=u.username, password=TEST_PASSWORD) is True
    # check
    assert 0 == ProductExam.objects.count()
    # test
    url = reverse("exam.update", args=[exam.pk])
    data = {"name": "Apple", "products": [product.pk]}
    response = client.post(url, data)
    # check
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    expect = reverse("exam.detail", args=[exam.pk])
    assert expect == response["Location"]
    exam.refresh_from_db()
    assert "Apple" == exam.name
    assert 1 == ProductExam.objects.count()
    product_exam = ProductExam.objects.first()
    assert exam == product_exam.exam
    assert product == product_exam.product


@pytest.mark.django_db
def test_exam_update_not_legacy(client):
    """Update an exam with a legacy ``ProductExam``.

    If the link between a product and exam was created during a data migration,
    then clear the ``legacy`` flag when the user updates the link.

    """
    product = ProductFactory()
    ExamSettingsFactory(course=product.category.product_type)
    exam = ExamFactory(name="Orange")
    ProductExamFactory(product=product, exam=exam, order=1, legacy=True)
    # login
    u = UserFactory(is_staff=True)
    assert client.login(username=u.username, password=TEST_PASSWORD) is True
    # test
    url = reverse("exam.update", args=[exam.pk])
    data = {"name": "Apple", "products": [product.pk]}
    response = client.post(url, data)
    # check
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    expect = reverse("exam.detail", args=[exam.pk])
    assert expect == response["Location"]
    product_exam = ProductExam.objects.first()
    assert product_exam.legacy is False


@pytest.mark.django_db
def test_question_update(client):
    exam_version = ExamVersionFactory(date_published=None)
    question = QuestionFactory(exam_version=exam_version)
    opt1 = OptionFactory(number=1, question=question)
    opt2 = OptionFactory(number=2, question=question)
    # login
    u = UserFactory(is_staff=True)
    assert client.login(username=u.username, password=TEST_PASSWORD) is True
    # test
    url = reverse("exam.question.update", args=[question.pk])
    response = client.post(url, _question_data(question, opt2, opt1, opt2))
    # check
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    expect = reverse("exam.version.detail", args=[exam_version.pk])
    assert expect == response["Location"]
    # check question
    question.refresh_from_db()
    assert "Apple" == question.question
    assert opt2 == question.answer
    # check options
    options = question.options()
    assert ["Crumble", "Pie"] == [obj.option for obj in options]
    assert [1, 2] == [obj.number for obj in options]


@pytest.mark.django_db
def test_submit_exam_for_marking():
    MailTemplateFactory(slug=UserExamVersion.MAIL_TEMPLATE_PASS)
    product = ProductFactory()
    user = UserFactory()
    exam_version = setup_exam()
    exam_version.exam.init_exam(user)
    ProductExamFactory(product=product, exam=exam_version.exam)
    user_exam_version = UserExamVersionFactory(
        user=user, exam_version=exam_version
    )
    set_correct_answers_all(user_exam_version)
    assert 0 == UserExamVersionProduct.objects.count()
    _submit_exam_for_marking(user_exam_version, [product])
    UserExamVersionProduct.objects.get(
        user_exam_version=user_exam_version, product=product
    )


@pytest.mark.django_db
def test_submit_exam_for_marking_lms():
    MailTemplateFactory(slug=UserExamVersion.MAIL_TEMPLATE_FAIL_INITIAL)
    user = UserFactory(is_staff=False)
    exam = ExamFactory()
    exam_version = ExamVersionFactory(exam=exam)
    # question_1
    question = QuestionFactory(exam_version=exam_version)
    answer = OptionFactory(question=question)
    question.answer = answer
    question.save()
    # lms
    course = CourseFactory()
    unit = UnitFactory()
    CourseUnitFactory(course=course, unit=unit)
    UnitExamFactory(unit=unit, exam=exam_version.exam)
    # lms - user
    UserCourseFactory(user=user, course=course)
    # user_exam_version
    user_exam_version = UserExamVersionFactory(
        user=user,
        exam_version=exam_version,
    )
    SelectionFactory(user_exam_version=user_exam_version, answer=answer)
    assert 0 == UserExamVersionProduct.objects.count()
    _submit_exam_for_marking(user_exam_version, [])
    assert 0 == UserExamVersionProduct.objects.count()


@pytest.mark.django_db
def test_submit_exam_for_marking_no_matching_products():
    """Cannot link if the exam is not linked to a product."""
    MailTemplateFactory(slug=UserExamVersion.MAIL_TEMPLATE_PASS)
    product = ProductFactory()
    user = UserFactory()
    exam_version = setup_exam()
    exam_version.exam.init_exam(user)
    user_exam_version = UserExamVersionFactory(
        user=user, exam_version=exam_version
    )
    set_correct_answers_all(user_exam_version)
    assert 0 == UserExamVersionProduct.objects.count()
    with pytest.raises(ExamError) as e:
        _submit_exam_for_marking(user_exam_version, [product])
    assert "Cannot find any matching products for user exam version" in str(
        e.value
    )
    assert 0 == UserExamVersionProduct.objects.count()


@pytest.mark.django_db
def test_submit_exam_for_marking_is_staff():
    """No need to link if the user is a member of staff."""
    MailTemplateFactory(slug=UserExamVersion.MAIL_TEMPLATE_PASS)
    course = CourseFactory()
    course_product = CourseProductFactory(course=course)
    unit = UnitFactory()
    CourseUnitFactory(course=course, unit=unit)
    user = UserFactory(is_staff=True)
    UserCourseFactory(user=user, course=course)
    exam_version = setup_exam()
    exam_version.exam.init_exam(user)
    UnitExamFactory(unit=unit, exam=exam_version.exam)
    user_exam_version = UserExamVersionFactory(
        user=user, exam_version=exam_version
    )
    set_correct_answers_all(user_exam_version)
    assert 0 == UserExamVersionProduct.objects.count()
    _submit_exam_for_marking(user_exam_version, [course_product.product])
    assert 0 == UserExamVersionProduct.objects.count()


@pytest.mark.django_db
def test_unit_create(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("exam.unit.create")
    assert 0 == Unit.objects.count()
    response = client.post(
        url,
        {
            "name": "Apple",
            "assignment_or_assessment": Unit.ASSIGNMENT,
        },
    )
    assert HTTPStatus.FOUND == response.status_code
    assert 1 == Unit.objects.count()
    unit = Unit.objects.get(name="Apple")
    assert reverse("exam.unit.detail", args=[unit.pk]) == response.url
    assert Unit.ASSIGNMENT == unit.get_assignment_or_assessment(1)


@pytest.mark.django_db
def test_unit_delete(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    unit = UnitFactory()
    response = client.post(reverse("exam.unit.delete", args=[unit.pk]))
    assert HTTPStatus.FOUND == response.status_code
    assert reverse("exam.unit.list") == response.url
    unit.refresh_from_db()
    assert unit.is_deleted is True


@pytest.mark.django_db
def test_unit_detail(client):
    user = UserFactory(is_staff=True)
    unit = UnitFactory()
    # a couple of courses for 'courses_for_unit'
    CourseUnitFactory(course=CourseFactory(), unit=unit)
    CourseUnitFactory(course=CourseFactory(), unit=unit)
    AssessmentFactory(unit=unit, name="a")
    assessment = AssessmentFactory(unit=unit, name="b")
    assessment.set_deleted(user)
    AssessmentFactory(unit=unit, name="c")
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(reverse("exam.unit.detail", args=[unit.pk]))
    assert HTTPStatus.OK == response.status_code
    assert "unit" in response.context
    assert "object" in response.context
    assert unit == response.context["unit"]
    assert unit == response.context["object"]


@pytest.mark.django_db
def test_unit_exam_delete(client):
    user = UserFactory(is_staff=True)
    course = CourseFactory()
    exam = ExamFactory()
    unit = UnitFactory()
    CourseUnitFactory(course=course, unit=unit)
    unit_exam = UnitExamFactory(unit=unit, exam=exam)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    assert 1 == UnitExam.objects.count()
    response = client.post(
        reverse("exam.unit.exam.delete", args=[unit_exam.pk])
    )
    assert HTTPStatus.FOUND == response.status_code
    assert reverse("exam.unit.detail", args=[unit.pk]) == response.url
    assert 1 == UnitExam.objects.count()
    unit_exam.refresh_from_db()
    assert unit_exam.is_deleted is True
    assert user == unit_exam.user_deleted
    assert unit_exam.exam.is_deleted is False


@pytest.mark.django_db
def test_unit_exam_update(client):
    user = UserFactory(is_staff=True)
    course = CourseFactory()
    exam = ExamFactory()
    unit = UnitFactory()
    CourseUnitFactory(course=course, unit=unit)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    assert 0 == UnitExam.objects.count()
    response = client.post(
        reverse("exam.unit.exam.update", args=[unit.pk]), {"exam": exam.pk}
    )
    assert HTTPStatus.FOUND == response.status_code
    assert reverse("exam.unit.detail", args=[unit.pk]) == response.url
    assert 1 == UnitExam.objects.count()
    unit_exam = UnitExam.objects.first()
    assert unit == unit_exam.unit
    assert exam == unit_exam.exam


@pytest.mark.django_db
def test_unit_exam_update_change(client):
    user = UserFactory(is_staff=True)
    course = CourseFactory()
    exam_1 = ExamFactory()
    exam_2 = ExamFactory()
    unit = UnitFactory()
    CourseUnitFactory(course=course, unit=unit)
    UnitExamFactory(unit=unit, exam=exam_1)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    assert 1 == UnitExam.objects.count()
    response = client.post(
        reverse("exam.unit.exam.update", args=[unit.pk]), {"exam": exam_2.pk}
    )
    assert HTTPStatus.FOUND == response.status_code
    assert reverse("exam.unit.detail", args=[unit.pk]) == response.url
    assert 1 == UnitExam.objects.count()
    unit_exam = UnitExam.objects.first()
    assert unit == unit_exam.unit
    assert exam_2 == unit_exam.exam


@pytest.mark.django_db
def test_unit_exam_update_deleted(client):
    user = UserFactory(is_staff=True)
    exam = ExamFactory()
    unit = UnitFactory()
    unit_exam = UnitExamFactory(unit=unit, exam=exam)
    unit_exam.set_deleted(UserFactory())
    unit_exam.refresh_from_db()
    assert unit_exam.is_deleted is True
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.post(
        reverse("exam.unit.exam.update", args=[unit.pk]), {"exam": exam.pk}
    )
    assert HTTPStatus.FOUND == response.status_code
    assert reverse("exam.unit.detail", args=[unit.pk]) == response.url
    unit_exam.refresh_from_db()
    assert unit_exam.is_deleted is False


@pytest.mark.django_db
def test_unit_exam_update_get(client):
    user = UserFactory(is_staff=True)
    exam = ExamFactory()
    unit = UnitFactory()
    unit_exam = UnitExamFactory(unit=unit, exam=exam)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(reverse("exam.unit.exam.update", args=[unit.pk]))
    assert HTTPStatus.OK == response.status_code
    assert "form" in response.context
    form = response.context["form"]
    assert exam == form.initial["exam"]


@pytest.mark.django_db
def test_unit_exam_update_missing_exam(client):
    user = UserFactory(is_staff=True)
    course = CourseFactory()
    unit = UnitFactory()
    CourseUnitFactory(course=course, unit=unit)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    assert 0 == UnitExam.objects.count()
    response = client.post(reverse("exam.unit.exam.update", args=[unit.pk]), {})
    assert HTTPStatus.OK == response.status_code
    assert {"exam": ["This field is required."]} == response.context[
        "form"
    ].errors


@pytest.mark.django_db
def test_unit_update(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    course = CourseFactory()
    unit = UnitFactory(name="Apple")
    CourseUnitFactory(course=course, unit=unit, order=1)
    url = reverse("exam.unit.update", args=[unit.pk])
    response = client.post(
        url,
        {
            "name": "Orange",
            "assignment_or_assessment": Unit.ASSIGNMENT,
        },
    )
    assert HTTPStatus.FOUND == response.status_code
    assert reverse("exam.unit.detail", args=[unit.pk]) == response.url
    assert 1 == Unit.objects.count()
    unit.refresh_from_db()
    assert "Orange" == unit.name
    assert Unit.ASSIGNMENT == unit.get_assignment_or_assessment(1)


@pytest.mark.django_db
def test_unit_update_get(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    course = CourseFactory()
    unit = UnitFactory(name="Apple")
    CourseUnitFactory(course=course, unit=unit, order=1)
    response = client.get(reverse("exam.unit.update", args=[unit.pk]))
    assert HTTPStatus.OK == response.status_code
    form = response.context["form"]
    assert Unit.ASSESSMENT == form.fields["assignment_or_assessment"].initial


@pytest.mark.django_db
def test_user_assessment_retake_update_assessment(client):
    """Comment on the retake."""
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    course = CourseFactory(name="Course")
    unit = UnitFactory(name="Unit 1", assignment_or_assessment=Unit.ASSESSMENT)
    CourseUnitFactory(course=course, unit=unit, order=1)
    assessment = AssessmentFactory(unit=unit)
    user_course = UserCourse.objects.enrol(user, course)
    user_unit = UserUnitFactory(user_course=user_course, unit=unit)
    user_assessment = UserAssessmentFactory(
        user_unit=user_unit,
        assessment=assessment,
        has_passed=False,
        retake_has_passed=False,
    )
    response = client.post(
        reverse(
            "exam.user.assessment.update.retake", args=[user_assessment.pk]
        ),
        data={
            "has_passed": True,
            "retake_comments": "I marked the retake",
            "retake_error_count": 3,
            "retake_has_passed": True,
        },
    )
    assert HTTPStatus.FOUND == response.status_code
    assert reverse("exam.user.unit.detail", args=[user_unit.pk]) == response.url
    user_assessment.refresh_from_db()
    # retake
    assert user == user_assessment.retake_assessor
    assert date.today() == user_assessment.retake_assessed.date()
    assert "I marked the retake" == user_assessment.retake_comments
    assert 3 == user_assessment.retake_error_count
    assert user_assessment.retake_grade is None
    # assessment
    assert user_assessment.assessed is None
    assert user_assessment.assessor is None
    assert user_assessment.error_count is None
    assert "" == user_assessment.assessor_comments
    # can_retake
    assert user_assessment.can_retake is False
    assert user_assessment.can_retake_date is None
    assert user_assessment.can_retake_user is None
    # retake, so do not update
    assert user_assessment.has_passed is False
    # 'has_passed' and 'retake_has_passed' (for assessments only)
    assert user_assessment.retake_has_passed is True


@pytest.mark.django_db
def test_user_assessment_retake_update_assignment(client):
    """Comment on the retake."""
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    course = CourseFactory(name="Course")
    unit = UnitFactory(name="Unit 1", assignment_or_assessment=Unit.ASSIGNMENT)
    CourseUnitFactory(course=course, unit=unit, order=1)
    assessment = AssessmentFactory(unit=unit)
    user_course = UserCourse.objects.enrol(user, course)
    user_unit = UserUnitFactory(user_course=user_course, unit=unit)
    user_assessment = UserAssessmentFactory(
        user_unit=user_unit,
        assessment=assessment,
        has_passed=False,
        retake_has_passed=False,
    )
    response = client.post(
        reverse(
            "exam.user.assessment.update.retake", args=[user_assessment.pk]
        ),
        data={
            "has_passed": True,
            "retake_comments": "I marked the retake",
            "retake_grade": 33,
            "retake_has_passed": True,
        },
    )
    assert HTTPStatus.FOUND == response.status_code
    assert reverse("exam.user.unit.detail", args=[user_unit.pk]) == response.url
    user_assessment.refresh_from_db()
    # retake
    assert user == user_assessment.retake_assessor
    assert date.today() == user_assessment.retake_assessed.date()
    assert "I marked the retake" == user_assessment.retake_comments
    assert user_assessment.retake_error_count is None
    assert 33 == user_assessment.retake_grade
    # assessment
    assert user_assessment.assessed is None
    assert user_assessment.assessor is None
    assert user_assessment.error_count is None
    assert "" == user_assessment.assessor_comments
    # can_retake
    assert user_assessment.can_retake is False
    assert user_assessment.can_retake_date is None
    assert user_assessment.can_retake_user is None
    # 'has_passed' and 'retake_has_passed' (assessments only - do not update)
    assert user_assessment.has_passed is False
    assert user_assessment.retake_has_passed is False


@pytest.mark.django_db
def test_user_assessment_retake_update_get_assignment(client):
    """Comment on the retake."""
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    course = CourseFactory(name="Course")
    unit = UnitFactory(assignment_or_assessment=Unit.ASSIGNMENT)
    CourseUnitFactory(course=course, unit=unit, order=1)
    assessment = AssessmentFactory(unit=unit)
    user_course = UserCourse.objects.enrol(user, course)
    user_unit = UserUnitFactory(user_course=user_course, unit=unit)
    user_assessment = UserAssessmentFactory(
        user_unit=user_unit, assessment=assessment
    )
    response = client.get(
        reverse(
            "exam.user.assessment.update.retake", args=[user_assessment.pk]
        ),
    )
    assert HTTPStatus.OK == response.status_code
    assert "form" in response.context
    form = response.context["form"]
    assert ["retake_grade", "retake_comments"] == list(form.fields.keys())


@pytest.mark.django_db
def test_user_assessment_retake_update_get_assessment(client):
    """Comment on the retake."""
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    course = CourseFactory(name="Course")
    unit = UnitFactory(assignment_or_assessment=Unit.ASSESSMENT)
    CourseUnitFactory(course=course, unit=unit, order=1)
    assessment = AssessmentFactory(unit=unit)
    user_course = UserCourse.objects.enrol(user, course)
    user_unit = UserUnitFactory(user_course=user_course, unit=unit)
    user_assessment = UserAssessmentFactory(
        user_unit=user_unit, assessment=assessment
    )
    response = client.get(
        reverse(
            "exam.user.assessment.update.retake", args=[user_assessment.pk]
        ),
    )
    assert HTTPStatus.OK == response.status_code
    assert "form" in response.context
    form = response.context["form"]
    # 'has_passed' and 'retake_has_passed' (for assessments only)
    assert [
        "retake_error_count",
        "retake_comments",
        "retake_has_passed",
    ] == list(form.fields.keys())


@pytest.mark.django_db
def test_user_assessment_retake_update_get_assessment_legacy_course(client):
    """Comment on the retake for a legacy course.

    https://www.kbsoftware.co.uk/crm/ticket/7017/

    """
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    course = CourseFactory(name="Course")
    unit = UnitFactory(assignment_or_assessment=Unit.ASSESSMENT)
    CourseUnitFactory(course=course, unit=unit, order=1)
    assessment = AssessmentFactory(unit=unit)
    with freeze_time(date(2024, 1, 1)):
        user_course = UserCourseFactory(
            user=user,
            course=course,  # assessed=timezone.now(), result=12
        )
    user_unit = UserUnitFactory(
        user_course=user_course, unit=unit, assessed=timezone.now(), result=12
    )
    user_assessment = UserAssessmentFactory(
        user_unit=user_unit, assessment=assessment
    )
    response = client.get(
        reverse(
            "exam.user.assessment.update.retake", args=[user_assessment.pk]
        ),
    )
    assert HTTPStatus.OK == response.status_code
    assert "form" in response.context
    form = response.context["form"]
    # 'has_passed' and 'retake_has_passed' (for assessments only)
    assert [
        "retake_error_count",
        "retake_comments",
    ] == list(form.fields.keys())


@pytest.mark.django_db
@pytest.mark.parametrize(
    "can_retake,has_passed", [(False, False), (True, False), (False, True)]
)
def test_user_assessment_update_assessment(client, can_retake, has_passed):
    """Comment on the assessment and ask the student to retake.

    Added audit trail for ``UserUnit`` and ``UserAssessment``, to allow
    updating of unit result and assessment comments.

    Requested as an open-ended feature.
    https://www.kbsoftware.co.uk/crm/ticket/6790/

    The template, ``exam/templates/exam/userassessment_form.html`` was setup
    to prevent the user changing the grade, but the form had no validation
    code to check.  The template code has now been removed.

    """
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    course = CourseFactory(name="Course")
    unit = UnitFactory(name="Unit 1", assignment_or_assessment=Unit.ASSESSMENT)
    CourseUnitFactory(course=course, unit=unit, order=1)
    assessment = AssessmentFactory(unit=unit)
    user_course = UserCourse.objects.enrol(user, course)
    user_unit = UserUnitFactory(user_course=user_course, unit=unit)
    user_assessment = UserAssessmentFactory(
        user_unit=user_unit, assessment=assessment
    )
    assert 0 == UserAssessmentAudit.objects.count()
    response = client.post(
        reverse("exam.user.assessment.update", args=[user_assessment.pk]),
        data={
            "assessor_comments": "I marked the assessment",
            "can_retake": can_retake,
            "error_count": 2,
            "has_passed": has_passed,
            "retake_has_passed": True,
        },
    )
    assert HTTPStatus.FOUND == response.status_code
    assert reverse("exam.user.unit.detail", args=[user_unit.pk]) == response.url
    user_assessment.refresh_from_db()
    # assessment
    assert date.today() == user_assessment.assessed.date()
    assert user == user_assessment.assessor
    assert "I marked the assessment" == user_assessment.assessor_comments
    assert 2 == user_assessment.error_count
    assert user_assessment.grade is None
    assert 1 == UserAssessmentAudit.objects.count()
    user_assessment_audit = UserAssessmentAudit.objects.first()
    assert user_assessment_audit.can_retake is can_retake
    assert 2 == user_assessment_audit.error_count
    assert "I marked the assessment" == user_assessment_audit.assessor_comments
    # can_retake
    if can_retake:
        assert user_assessment.can_retake is True
        assert date.today() == user_assessment.can_retake_date.date()
        assert user == user_assessment.can_retake_user
    else:
        assert user_assessment.can_retake is False
        assert user_assessment.can_retake_date is None
        assert user_assessment.can_retake_user is None
    # retake
    assert user_assessment.has_passed is has_passed
    # not a retake, so do not update
    assert user_assessment.retake_has_passed is False
    assert user_assessment.retake_assessor is None
    assert user_assessment.retake_assessed is None
    assert "" == user_assessment.retake_comments
    assert user_assessment.retake_error_count is None


@pytest.mark.django_db
def test_user_assessment_update_assessment_pass_and_retake(client):
    """A user cannot pass an assessment and be asked to retake it."""
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    course = CourseFactory(name="Course")
    unit = UnitFactory(name="Unit 1", assignment_or_assessment=Unit.ASSESSMENT)
    CourseUnitFactory(course=course, unit=unit, order=1)
    assessment = AssessmentFactory(unit=unit)
    user_course = UserCourse.objects.enrol(user, course)
    user_unit = UserUnitFactory(user_course=user_course, unit=unit)
    user_assessment = UserAssessmentFactory(
        user_unit=user_unit, assessment=assessment
    )
    assert 0 == UserAssessmentAudit.objects.count()
    response = client.post(
        reverse("exam.user.assessment.update", args=[user_assessment.pk]),
        data={
            "assessor_comments": "I marked the assessment",
            "can_retake": True,
            "error_count": 2,
            "has_passed": True,
            "retake_has_passed": True,
        },
    )
    assert HTTPStatus.OK == response.status_code
    assert {
        "__all__": [
            "A student cannot be asked to retake an assessment they have passed"
        ]
    } == response.context["form"].errors


@pytest.mark.django_db
@pytest.mark.parametrize("can_retake", [False, True])
def test_user_assessment_update_assignment(client, can_retake):
    """Comment on the assessment and ask the student to retake.

    - ``has_passed`` should not be updated for assignments.

    """
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    course = CourseFactory(name="Course")
    unit = UnitFactory(name="Unit 1", assignment_or_assessment=Unit.ASSIGNMENT)
    CourseUnitFactory(course=course, unit=unit, order=1)
    assessment = AssessmentFactory(unit=unit)
    user_course = UserCourse.objects.enrol(user, course)
    user_unit = UserUnitFactory(user_course=user_course, unit=unit)
    user_assessment = UserAssessmentFactory(
        user_unit=user_unit, assessment=assessment, has_passed=False
    )
    response = client.post(
        reverse("exam.user.assessment.update", args=[user_assessment.pk]),
        data={
            "assessor_comments": "I marked the assessment",
            "can_retake": can_retake,
            "grade": 22,
            "has_passed": True,
        },
    )
    assert HTTPStatus.FOUND == response.status_code
    assert reverse("exam.user.unit.detail", args=[user_unit.pk]) == response.url
    user_assessment.refresh_from_db()
    # assessment
    assert date.today() == user_assessment.assessed.date()
    assert user == user_assessment.assessor
    assert "I marked the assessment" == user_assessment.assessor_comments
    assert user_assessment.error_count is None
    assert 22 == user_assessment.grade
    # can_retake
    if can_retake:
        assert user_assessment.can_retake is True
        assert date.today() == user_assessment.can_retake_date.date()
        assert user == user_assessment.can_retake_user
    else:
        assert user_assessment.can_retake is False
        assert user_assessment.can_retake_date is None
        assert user_assessment.can_retake_user is None
    # retake
    assert user_assessment.has_passed is False
    assert user_assessment.retake_assessor is None
    assert user_assessment.retake_assessed is None
    assert "" == user_assessment.retake_comments
    assert user_assessment.retake_error_count is None


@pytest.mark.django_db
def test_user_assessment_update_get_assessment(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    course = CourseFactory(name="Course")
    unit = UnitFactory(assignment_or_assessment=Unit.ASSESSMENT)
    CourseUnitFactory(course=course, unit=unit, order=1)
    assessment = AssessmentFactory(unit=unit)
    user_course = UserCourse.objects.enrol(user, course)
    user_unit = UserUnitFactory(user_course=user_course, unit=unit)
    user_assessment = UserAssessmentFactory(
        user_unit=user_unit, assessment=assessment
    )
    response = client.get(
        reverse("exam.user.assessment.update", args=[user_assessment.pk])
    )
    assert HTTPStatus.OK == response.status_code
    assert "form" in response.context
    form = response.context["form"]
    assert [
        "can_retake",
        "error_count",
        "assessor_comments",
        "has_passed",
    ] == list(form.fields.keys())


@pytest.mark.django_db
def test_user_assessment_update_get_assessment_legacy_course(client):
    """Comment on the legacy course.

    https://www.kbsoftware.co.uk/crm/ticket/7017/

    """
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    course = CourseFactory(name="Course")
    unit = UnitFactory(assignment_or_assessment=Unit.ASSESSMENT)
    CourseUnitFactory(course=course, unit=unit, order=1)
    assessment = AssessmentFactory(unit=unit)
    with freeze_time(date(2024, 1, 1)):
        user_course = UserCourseFactory(
            user=user,
            course=course,  # assessed=timezone.now(), result=12
        )
    user_unit = UserUnitFactory(
        user_course=user_course, unit=unit, assessed=timezone.now(), result=12
    )
    user_assessment = UserAssessmentFactory(
        user_unit=user_unit, assessment=assessment
    )
    response = client.get(
        reverse("exam.user.assessment.update", args=[user_assessment.pk])
    )
    assert HTTPStatus.OK == response.status_code
    assert "form" in response.context
    form = response.context["form"]
    assert [
        "can_retake",
        "error_count",
        "assessor_comments",
    ] == list(form.fields.keys())


@pytest.mark.django_db
def test_user_assessment_update_get_assignment(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    course = CourseFactory(name="Course")
    unit = UnitFactory(assignment_or_assessment=Unit.ASSIGNMENT)
    CourseUnitFactory(course=course, unit=unit, order=1)
    assessment = AssessmentFactory(unit=unit)
    user_course = UserCourse.objects.enrol(user, course)
    user_unit = UserUnitFactory(user_course=user_course, unit=unit)
    user_assessment = UserAssessmentFactory(
        user_unit=user_unit, assessment=assessment
    )
    response = client.get(
        reverse("exam.user.assessment.update", args=[user_assessment.pk])
    )
    assert HTTPStatus.OK == response.status_code
    assert "form" in response.context
    form = response.context["form"]
    # 'has_passed' and 'retake_has_passed' (for assessments only)
    assert ["can_retake", "grade", "assessor_comments"] == list(
        form.fields.keys()
    )


@pytest.mark.django_db
def test_usercourse_detail(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    user_course = UserCourseFactory()
    # a (has an assessment)
    unit_a = UnitFactory()
    CourseUnitFactory(course=user_course.course, unit=unit_a, order=1)
    AssessmentFactory(unit=unit_a, name="assessment_a", order=1)
    UserUnitFactory(
        user_course=user_course,
        unit=unit_a,
        assessor_comments="a",
    )
    # b (does not have an assessment)
    unit_b = UnitFactory()
    CourseUnitFactory(course=user_course.course, unit=unit_b, order=3)
    UserUnitFactory(
        user_course=user_course,
        unit=unit_b,
        assessor_comments="b",
    ).set_deleted(user)
    # c (does not have an assessment)
    unit_c = UnitFactory()
    CourseUnitFactory(course=user_course.course, unit=unit_c, order=2)
    # AssessmentFactory(unit=unit_c, name="assessment_c", order=1)
    UserUnitFactory(
        user_course=user_course,
        unit=unit_c,
        assessor_comments="c",
    )
    # d (has an assessment)
    unit_d = UnitFactory()
    CourseUnitFactory(course=user_course.course, unit=unit_d, order=4)
    AssessmentFactory(unit=unit_d, name="assessment_d", order=1)
    UserUnitFactory(
        user_course=user_course,
        unit=unit_d,
        assessor_comments="d",
    )
    # e (does not have an assessment)
    unit_e = UnitFactory()
    CourseUnitFactory(course=user_course.course, unit=unit_e, order=5)
    unit_e.set_deleted(user)
    UserUnitFactory(user_course=user_course, unit=unit_e, assessor_comments="e")
    response = client.get(
        reverse("exam.user.course.detail", args=[user_course.pk])
    )
    assert HTTPStatus.OK == response.status_code
    assert "user_units" in response.context
    assert ["a", "d"] == [
        x.assessor_comments for x in response.context["user_units"]
    ]


@pytest.mark.django_db
def test_user_course_list(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    user_course_a = UserCourseFactory(
        course=CourseFactory(name="c"), assessor_comments="c"
    )
    UserCourseFactory(assessor_comments="b").set_deleted(user)
    user_course_c = UserCourseFactory(
        course=CourseFactory(name="a"), assessor_comments="a"
    )
    response = client.get(reverse("exam.user.course.list"))
    assert HTTPStatus.OK == response.status_code
    assert "usercourse_list" in response.context
    qs = response.context["usercourse_list"]
    # ordered by course name
    assert ["a", "c"] == [x.assessor_comments for x in qs]


@pytest.mark.django_db
def test_user_course_list_to_mark(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    user_course = UserCourseFactory(assessor_comments="a")
    UserCourseFactory(assessor_comments="b").set_deleted(user)
    user_course_c = UserCourseFactory(assessor_comments="c")
    unit = UnitFactory(name="Farming")
    CourseUnitFactory(course=user_course_c.course, unit=unit, name="Dairy")
    # only list courses where the unit is submitted
    user_unit_c = UserUnitFactory(
        user_course=user_course_c,
        unit=unit,
        submitted=timezone.now(),
    )
    UserAssessmentFactory(
        user_unit=user_unit_c, answer_submitted=timezone.now()
    )
    response = client.get(reverse("exam.user.course.list.to.mark"))
    assert HTTPStatus.OK == response.status_code
    assert "object_list" in response.context
    qs = response.context["object_list"]
    assert ["Dairy"] == [x["unit_name"] for x in qs]


@pytest.mark.django_db
def test_user_course_delete(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    user_course = UserCourseFactory()
    assert user_course.is_deleted is False
    url = reverse("exam.user.course.delete", args=[user_course.pk])
    response = client.post(url)
    assert HTTPStatus.FOUND == response.status_code
    assert (
        reverse("user.redirect.contact", args=[user_course.user.pk])
        == response.url
    )
    user_course.refresh_from_db()
    assert user_course.is_deleted is True


@pytest.mark.django_db
def test_user_course_update(client):
    MailTemplateFactory(slug=UserCourse.MAIL_TEMPLATE_USER_COURSE_RESULT)
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    user_course = UserCourseFactory(user=UserFactory(first_name="Patrick"))
    url = reverse("exam.user.course.update.result", args=[user_course.pk])
    response = client.post(
        url, {"assessor_comments": "Very impressive!", "result": 78}
    )
    assert HTTPStatus.FOUND == response.status_code
    assert (
        reverse("user.redirect.contact", args=[user_course.user.pk])
        == response.url
    )
    user_course.refresh_from_db()
    assert 78 == user_course.result
    assert user == user_course.assessor
    assert timezone.now().date() == user_course.assessed.now().date()
    assert "Very impressive!" == user_course.assessor_comments
    # check email template context
    assert 1 == Message.objects.count()
    message = Message.objects.first()
    assert UserCourse.MAIL_TEMPLATE_USER_COURSE_RESULT == message.template.slug
    assert 1 == message.mail_set.count()
    mail = message.mail_set.first()
    assert 1 == mail.mailfield_set.count()
    assert {"name": "Patrick"} == {
        f.key: f.value for f in mail.mailfield_set.all()
    }


# @pytest.mark.django_db
# def test_user_course_list_user(client):
#     user = UserFactory(is_staff=True)
#     assert client.login(username=user.username, password=TEST_PASSWORD) is True
#     user_course = UserCourseFactory(assessor_comments="a")
#     UserCourseFactory(assessor_comments="b").set_deleted(user)
#     UserCourseFactory(assessor_comments="c")
#     url = url_with_querystring(
#         reverse("exam.user.course.list"), user=user_course.user.pk
#     )
#     response = client.get(url)
#     assert HTTPStatus.OK == response.status_code
#     assert "usercourse_list" in response.context
#     qs = response.context["usercourse_list"]
#     assert ["a"] == [x.assessor_comments for x in qs]
#     assert "product_filter" in response.context
#     assert None == response.context["product_filter"]
#     assert "user_filter" in response.context
#     assert user_course.user == response.context["user_filter"]


@pytest.mark.django_db
def test_user_exam_version_result_create(client):
    user = UserFactory(is_staff=True)
    exam_version = setup_exam()
    product = ProductFactory()
    # link the product and exam, so we can record the result
    ProductExamFactory(product=product, exam=exam_version.exam)
    url = reverse(
        "exam.user.version.result.create",
        args=[exam_version.exam.pk, user.pk, product.pk],
    )
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.post(url, {"percent": 10, "retake": True})
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    user_exam_version = UserExamVersion.objects.get(
        user=user, exam_version=exam_version
    )
    expect = reverse(
        "exam.version.user.list", args=[user_exam_version.exam_version.pk]
    )
    assert expect == response["Location"]
    assert user_exam_version.legacy_result is True
    assert user_exam_version.legacy_retake is True
    assert 10 == user_exam_version.percent_override


@pytest.mark.django_db
def test_user_exam_version_result_update(client):
    user = UserFactory(is_staff=True)
    exam_version = setup_exam()
    user_exam_version = UserExamVersionFactory(
        exam_version=exam_version, user=user
    )
    user_exam_version.set_result(5)
    url = reverse(
        "exam.user.version.result.update", args=[user_exam_version.pk]
    )
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.post(url, {"percent": 10})
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    expect = reverse(
        "exam.version.user.list", args=[user_exam_version.exam_version.pk]
    )
    assert expect == response["Location"]
    user_exam_version.refresh_from_db()
    assert 10 == user_exam_version.percent_override


@pytest.mark.django_db
def test_user_exam_version_result_update_result_too_big(client):
    user = UserFactory(is_staff=True)
    exam_version = setup_exam()
    user_exam_version = UserExamVersionFactory(
        exam_version=exam_version, user=user
    )
    user_exam_version.set_result(5)
    url = reverse(
        "exam.user.version.result.update", args=[user_exam_version.pk]
    )
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.post(url, {"percent": 101})
    assert HTTPStatus.OK == response.status_code
    assert "percent" in response.context["form"].errors
    assert (
        "The percentage mark must be between 0 and 100"
    ) == response.context["form"].errors["percent"][0]


@pytest.mark.django_db
def test_user_exam_version_result_update_not_marked(client):
    user = UserFactory(is_staff=True)
    exam_version = setup_exam()
    user_exam_version = UserExamVersionFactory(
        exam_version=exam_version, user=user
    )
    url = reverse(
        "exam.user.version.result.update", args=[user_exam_version.pk]
    )
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    with pytest.raises(ExamError) as e:
        client.post(url, {"percent": 10})
    assert ("cannot override an exam result until it has been marked") in str(
        e.value
    )


@pytest.mark.django_db
def test_user_unit_detail(client):
    user = UserFactory(is_staff=True)
    user_unit = UserUnitFactory(assessor=None)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(reverse("exam.user.unit.detail", args=[user_unit.pk]))
    assert HTTPStatus.OK == response.status_code
    user_unit.refresh_from_db()
    assert user == user_unit.assessor
    assert "assessor" in response.context
    assert [] == response.context["assessor"]


@pytest.mark.django_db
def test_user_unit_detail_assessor_already_set(client):
    user = UserFactory(is_staff=True)
    assessor = UserFactory(is_staff=True, first_name="Patrick")
    user_unit = UserUnitFactory(assessor=assessor)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(reverse("exam.user.unit.detail", args=[user_unit.pk]))
    assert HTTPStatus.OK == response.status_code
    user_unit.refresh_from_db()
    assert assessor == user_unit.assessor
    assert ["Unit being marked by Patrick"] == response.context["assessor"]


@pytest.mark.django_db
def test_user_unit_detail_assessor_retake(client):
    user = UserFactory(is_staff=True)
    assessor = UserFactory(is_staff=True, first_name="Patrick")
    with freeze_time(
        datetime.datetime(2023, 1, 1, 1, 1, 1, tzinfo=datetime.timezone.utc)
    ):
        user_unit = UserUnitFactory(
            assessor=assessor,
            retake_assessed=timezone.now(),
            retake_assessor=UserFactory(first_name="Chris"),
        )
        assert (
            client.login(username=user.username, password=TEST_PASSWORD) is True
        )
        response = client.get(
            reverse("exam.user.unit.detail", args=[user_unit.pk])
        )
    assert HTTPStatus.OK == response.status_code
    user_unit.refresh_from_db()
    assert assessor == user_unit.assessor
    assert ["Retake reviewed by Chris (a moment ago)"] == response.context[
        "assessor"
    ]


@pytest.mark.django_db
def test_user_unit_detail_assessor_retake_result(client):
    user = UserFactory(is_staff=True)
    assessor = UserFactory(is_staff=True, first_name="Patrick")
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    with freeze_time(
        datetime.datetime(2023, 1, 1, 1, 1, 1, tzinfo=datetime.timezone.utc)
    ):
        user_unit = UserUnitFactory(
            assessor=assessor,
            retake_assessed=timezone.now(),
            retake_assessor=UserFactory(first_name="Chris"),
            result_assessed=timezone.now(),
            result_assessor=UserFactory(first_name="Peter"),
        )
        response = client.get(
            reverse("exam.user.unit.detail", args=[user_unit.pk])
        )
    assert HTTPStatus.OK == response.status_code
    user_unit.refresh_from_db()
    assert assessor == user_unit.assessor
    assert [
        "Retake reviewed by Chris (a moment ago)",
        "Result entered by Peter (a moment ago)",
    ] == response.context["assessor"]


@pytest.mark.django_db
def test_user_unit_detail_assessor_retake_result_assessed(client):
    user = UserFactory(is_staff=True)
    assessor = UserFactory(is_staff=True, first_name="Patrick")
    with freeze_time(
        datetime.datetime(2023, 1, 1, 1, 1, 1, tzinfo=datetime.timezone.utc)
    ):
        user_unit = UserUnitFactory(
            assessor=assessor,
            assessed=timezone.now(),
            retake_assessed=timezone.now(),
            retake_assessor=UserFactory(first_name="Chris"),
            result_assessed=timezone.now(),
            result_assessor=UserFactory(first_name="Peter"),
        )
        assert (
            client.login(username=user.username, password=TEST_PASSWORD) is True
        )
        response = client.get(
            reverse("exam.user.unit.detail", args=[user_unit.pk])
        )
        assert HTTPStatus.OK == response.status_code
        user_unit.refresh_from_db()
        assert assessor == user_unit.assessor
        assert [
            "Retake reviewed by Chris (a moment ago)",
            "Result entered by Peter (a moment ago)",
            "Assessed by Patrick (a moment ago)",
        ] == response.context["assessor"]


@pytest.mark.django_db
def test_user_unit_update_comment(client):
    MailTemplateFactory(slug=UserUnit.MAIL_TEMPLATE_USER_UNIT_RESULT)
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    user_course = UserCourseFactory(user=UserFactory(first_name="Patrick"))
    unit = UnitFactory()
    CourseUnitFactory(course=user_course.course, unit=unit, order=1)
    user_unit = UserUnitFactory(user_course=user_course, unit=unit, result=88)
    url = reverse("exam.user.unit.update.comment", args=[user_unit.pk])
    data = {"assessor_comments": "Marked"}  # , "result": "100"}
    response = client.post(url, data=data)
    assert HTTPStatus.FOUND == response.status_code
    assert reverse("exam.user.unit.detail", args=[user_unit.pk]) == response.url
    user_unit.refresh_from_db()
    assert user == user_unit.assessor
    assert date.today() == user_unit.assessed.date()
    assert "Marked" == user_unit.assessor_comments
    # check email template context
    assert 1 == Message.objects.count()
    message = Message.objects.first()
    assert 1 == message.mail_set.count()
    mail = message.mail_set.first()
    assert UserUnit.MAIL_TEMPLATE_USER_UNIT_RESULT == message.template.slug
    assert 1 == mail.mailfield_set.count()
    assert {"name": "Patrick"} == {
        f.key: f.value for f in mail.mailfield_set.all()
    }


@pytest.mark.django_db
def test_user_unit_update_comment_no_result(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    user_course = UserCourseFactory()
    unit = UnitFactory()
    CourseUnitFactory(course=user_course.course, unit=unit, order=1)
    user_unit = UserUnitFactory(user_course=user_course, unit=unit, result=None)
    url = reverse("exam.user.unit.update.comment", args=[user_unit.pk])
    data = {"assessor_comments": "Marked"}  # , "result": "100"}
    response = client.post(url, data=data)
    assert HTTPStatus.OK == response.status_code
    form = response.context["form"]
    assert {
        "__all__": ["The unit has no result. The unit cannot be assessed."],
    } == form.errors


@pytest.mark.django_db
def test_user_unit_update_result(client):
    """

    .. tip:: Testing ``UserUnitResultUpdateView``.

    """
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    user_course = UserCourseFactory(user=UserFactory(first_name="Patrick"))
    unit = UnitFactory()
    CourseUnitFactory(course=user_course.course, unit=unit, order=1)
    user_unit = UserUnitFactory(user_course=user_course, unit=unit)
    assert 0 == UserUnitAudit.objects.count()
    url = reverse("exam.user.unit.update.result", args=[user_unit.pk])
    data = {"result": "100"}
    response = client.post(url, data=data)
    assert HTTPStatus.FOUND == response.status_code
    assert reverse("exam.user.unit.detail", args=[user_unit.pk]) == response.url
    user_unit.refresh_from_db()
    assert user_unit.assessed is None
    assert date.today() == user_unit.result_assessed.date()
    assert user == user_unit.result_assessor
    assert 100 == user_unit.result
    # check email template context
    assert 0 == Message.objects.count()
    assert 1 == UserUnitAudit.objects.count()
    user_unit_audit = UserUnitAudit.objects.first()
    assert user == user_unit_audit.user
    assert user_unit == user_unit_audit.user_unit
    assert 100 == user_unit_audit.result
    assert timezone.now().date() == user_unit_audit.created.date()


@pytest.mark.django_db
def test_user_unit_update_result_already_assessed(client):
    """

    The unit for this student has already been assessed, so the result cannot
    be updated.

    .. tip:: Testing ``UserUnitResultUpdateView``.

    """
    MailTemplateFactory(slug=UserUnit.MAIL_TEMPLATE_USER_UNIT_RESULT)
    user = UserFactory(is_staff=True)
    user_assessor = UserFactory()
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    user_course = UserCourseFactory(user=UserFactory(first_name="Patrick"))
    unit = UnitFactory()
    CourseUnitFactory(course=user_course.course, unit=unit, order=1)
    assessed = datetime.datetime(
        2023, 7, 1, 6, 0, 0, tzinfo=datetime.timezone.utc
    )
    user_unit = UserUnitFactory(
        user_course=user_course,
        unit=unit,
        assessed=assessed,
        assessor=user_assessor,
        result_assessed=assessed,
    )
    assert 0 == UserUnitAudit.objects.count()
    url = reverse("exam.user.unit.update.result", args=[user_unit.pk])
    data = {"result": "100"}
    response = client.post(url, data=data)
    assert HTTPStatus.FOUND == response.status_code
    # form = response.context["form"]
    # assert {
    #     "__all__": [
    #         "Cannot change the result.  The unit has already been assessed"
    #     ],
    # } == form.errors
    user_unit.refresh_from_db()
    # make sure the 'assessed' date does not change when the result is updated
    assert assessed == user_unit.assessed
    # make sure the 'assessor' does not change when the result is updated
    assert user_assessor == user_unit.assessor
    assert date.today() == user_unit.result_assessed.date()
    assert user == user_unit.result_assessor
    assert 1 == UserUnitAudit.objects.count()
    user_unit_audit = UserUnitAudit.objects.first()
    assert user == user_unit_audit.user
    assert user_unit == user_unit_audit.user_unit
    assert 100 == user_unit_audit.result
    assert timezone.now().date() == user_unit_audit.created.date()


@pytest.mark.django_db
def test_user_unit_update_retake_comment(client):
    MailTemplateFactory(slug=UserUnit.MAIL_TEMPLATE_USER_UNIT_RETAKE)
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    user_course = UserCourseFactory(user=UserFactory(first_name="Patrick"))
    unit = UnitFactory()
    CourseUnitFactory(course=user_course.course, unit=unit, order=1)
    user_unit = UserUnitFactory(user_course=user_course, unit=unit)
    url = reverse("exam.user.unit.update.comment.retake", args=[user_unit.pk])
    data = {"retake_comments": "Marked"}  # , "result": "100"}
    response = client.post(url, data=data)
    assert HTTPStatus.FOUND == response.status_code
    assert reverse("exam.user.unit.detail", args=[user_unit.pk]) == response.url
    user_unit.refresh_from_db()
    assert "Marked" == user_unit.retake_comments
    assert user_unit.retake_assessed.date() == timezone.now().date()
    assert user == user_unit.retake_assessor
    # check email template context
    assert 1 == Message.objects.count()
    message = Message.objects.first()
    assert 1 == message.mail_set.count()
    mail = message.mail_set.first()
    assert UserUnit.MAIL_TEMPLATE_USER_UNIT_RETAKE == message.template.slug
    assert 1 == mail.mailfield_set.count()
    assert {"name": "Patrick"} == {
        f.key: f.value for f in mail.mailfield_set.all()
    }


@pytest.mark.django_db
def test_user_unit_update_retake_comment_again(client):
    user = UserFactory(is_staff=True)
    first_user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    user_course = UserCourseFactory(user=UserFactory(first_name="Patrick"))
    unit = UnitFactory()
    CourseUnitFactory(course=user_course.course, unit=unit, order=1)
    frozen_at = datetime.datetime(
        2023, 1, 1, 1, 1, 1, tzinfo=datetime.timezone.utc
    )
    with freeze_time(frozen_at):
        user_unit = UserUnitFactory(
            user_course=user_course,
            unit=unit,
            retake_assessed=timezone.now(),
            retake_assessor=first_user,
        )
    url = reverse("exam.user.unit.update.comment.retake", args=[user_unit.pk])
    data = {"retake_comments": "Marked"}  # , "result": "100"}
    response = client.post(url, data=data)
    assert HTTPStatus.FOUND == response.status_code
    assert reverse("exam.user.unit.detail", args=[user_unit.pk]) == response.url
    user_unit.refresh_from_db()
    assert "Marked" == user_unit.retake_comments
    assert frozen_at == user_unit.retake_assessed
    assert first_user == user_unit.retake_assessor
    # check email template context
    assert 0 == Message.objects.count()


@pytest.mark.django_db
def test_user_unit_update_missing_data(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    user_course = UserCourseFactory()
    unit = UnitFactory()
    CourseUnitFactory(course=user_course.course, unit=unit, order=1)
    user_unit = UserUnitFactory(user_course=user_course, unit=unit, result=88)
    url = reverse("exam.user.unit.update.comment", args=[user_unit.pk])
    response = client.post(url, data={})
    assert HTTPStatus.OK == response.status_code
    form = response.context["form"]
    assert {
        "assessor_comments": ["This field is required."],
        # "result": ["This field is required."],
    } == form.errors


@pytest.mark.django_db
def test_user_unit_update_get(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    user_course = UserCourseFactory()
    unit = UnitFactory()
    CourseUnitFactory(course=user_course.course, unit=unit, order=1)
    user_unit = UserUnitFactory(user_course=user_course, unit=unit)
    url = reverse("exam.user.unit.update.comment", args=[user_unit.pk])
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    # assert "user_course" in response.context
    # assert user_course == response.context["user_course"]
