# -*- encoding: utf-8 -*-
from operator import truediv
from pickle import FALSE
from this import d
import pytest

from django.utils import timezone
from freezegun import freeze_time

from exam.models import ExamError, UserAssessment, UserCourse, UserUnit
from exam.tests.factories import (
    AssessmentFactory,
    CourseFactory,
    CourseUnitFactory,
    ExamFactory,
    ExamVersionFactory,
    UnitExamFactory,
    UnitFactory,
    UserAssessmentFactory,
    UserCourseFactory,
    UserExamVersionFactory,
    UserUnitFactory,
)
from login.tests.factories import UserFactory


@pytest.mark.parametrize(
    "answer_submitted,expect",
    [
        (None, True),  # True condition
        (timezone.now(), False),
    ],
)
@pytest.mark.django_db
def test_user_assessment_can_user_start(answer_submitted, expect):
    user_assessment = UserAssessmentFactory(answer_submitted=answer_submitted)
    assert user_assessment.can_user_start() is expect


@pytest.mark.parametrize(
    "answer_submitted,assessed,expect",
    [
        (timezone.now(), None, True),  # True condition
        (None, None, False),
        (timezone.now(), timezone.now(), False),
    ],
)
@pytest.mark.django_db
def test_user_assessment_can_mark(answer_submitted, assessed, expect):
    # Assessment submitted and not assessed so can mark
    user_assessment = UserAssessmentFactory(
        answer_submitted=answer_submitted, assessed=assessed
    )
    assert user_assessment.can_mark() is expect


@pytest.mark.parametrize(
    "can_retake,assessed,expect",
    [
        (True, timezone.now(), True),
        (False, timezone.now(), False),
        (True, None, False),
    ],
)
@pytest.mark.django_db
def test_user_assessment_is_retake(can_retake, assessed, expect):
    user_unit = UserUnitFactory(assessed=assessed)
    user_assessment = UserAssessmentFactory(
        user_unit=user_unit, can_retake=can_retake
    )
    assert user_assessment.is_retake() is expect


@pytest.mark.parametrize(
    "retake_submitted, assessed, can_retake,expect",
    [
        (None, timezone.now(), True, True),  # True condition
        (timezone.now(), timezone.now(), True, False),
        (timezone.now(), timezone.now(), False, False),
        (timezone.now(), None, True, False),
    ],
)
@pytest.mark.django_db
def test_user_assessment_can_user_start_retake(
    retake_submitted, assessed, can_retake, expect
):
    user_unit = UserUnitFactory(
        retake_submitted=retake_submitted, assessed=assessed
    )
    user_assessment = UserAssessmentFactory(
        user_unit=user_unit, can_retake=can_retake
    )
    assert user_assessment.can_user_start_retake() is expect


@pytest.mark.parametrize(
    "retake_submitted, retake_assessed, expect",
    [
        (timezone.now(), None, True),  # True condition
        (timezone.now(), timezone.now(), False),
        (None, None, False),
        (None, timezone.now(), False),
    ],
)
@pytest.mark.django_db
def test_user_assessment_can_mark_retake(
    retake_submitted, retake_assessed, expect
):
    user_assessment = UserAssessmentFactory(
        retake_submitted=retake_submitted, retake_assessed=retake_assessed
    )
    assert user_assessment.can_mark_retake() is expect


@pytest.mark.parametrize(
    "answer_submitted, submitted, expect",
    [
        (timezone.now(), None, True),  # True condition
        (timezone.now(), timezone.now(), False),
    ],
)
@pytest.mark.django_db
def test_unit_can_submit(answer_submitted, submitted, expect):
    """Can the user submit the unit for the first time.
    All assessments must have been completed.
    Unit must not have already been submitted.
    """
    unit = UnitFactory(name="unit1")
    assessment = AssessmentFactory(unit=unit)
    user_unit = UserUnitFactory(unit=unit, submitted=submitted)
    user_assessment = UserAssessmentFactory(
        user_unit=user_unit,
        assessment=assessment,
        answer_submitted=answer_submitted,
    )
    assert user_unit.can_submit() is expect


@pytest.mark.parametrize(
    "answer_submitted, retake_submitted, expect",
    [
        (timezone.now(), None, True),  # True condition
        (timezone.now(), timezone.now(), False),
    ],
)
@pytest.mark.django_db
def test_unit_can_submit_retake(answer_submitted, retake_submitted, expect):
    """Can the user submit a unit retake.
    If unit retake not yet submitted and all assessments are complete
    then unit retake can be submitted, otherwise it cannot
    """
    unit = UnitFactory(name="unit1")
    assessment = AssessmentFactory(unit=unit)
    user_unit = UserUnitFactory(unit=unit, retake_submitted=retake_submitted)
    user_assessment = UserAssessmentFactory(
        user_unit=user_unit,
        assessment=assessment,
        answer_submitted=answer_submitted,
    )
    assert user_unit.can_submit_retake() is expect


@pytest.mark.parametrize(
    "submitted, assessed, expect",
    [
        (timezone.now(), None, True),  # True condition
        (timezone.now(), timezone.now(), False),
    ],
)
@pytest.mark.django_db
def test_unit_can_mark(submitted, assessed, expect):
    """Can the assessor mark the unit.
    If unit submitted and unit not marked, unit can be marked
    otherwise it cannot.
    """
    user_unit = UserUnitFactory(submitted=submitted, assessed=assessed)
    assert user_unit.can_mark() is expect


@pytest.mark.django_db
def test_answer_button_enabled():
    user_assessment = UserAssessmentFactory()
    assert user_assessment.answer_button_enabled() is True


@pytest.mark.django_db
def test_answer_button_enabled_not():
    # unit submitted - cannot answer
    user_unit = UserUnitFactory(submitted=timezone.now())
    user_assessment = UserAssessmentFactory(
        user_unit=user_unit, answer_submitted=timezone.now()
    )
    assert user_assessment.answer_button_enabled() is False

    # submitted and assessed - cannot answer
    user_unit2 = UserUnitFactory(
        submitted=timezone.now(), assessed=timezone.now()
    )
    user_assessment2 = UserAssessmentFactory(user_unit=user_unit2)
    assert user_assessment2.answer_button_enabled() is False


@pytest.mark.django_db
def test_unit_assessments_complete():
    assert UserUnitFactory().is_complete_assessments() is True
