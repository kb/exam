# -*- encoding: utf-8 -*-
import logging

from django import forms
from django.forms import modelformset_factory

from base.form_utils import RequiredFieldForm, FileDropInput
from stock.models import Product
from .models import (
    Assessment,
    ClassInformation,
    Course,
    CourseProduct,
    CourseUnit,
    Exam,
    ExamSettings,
    ExamVersion,
    Option,
    Question,
    Unit,
    UnitExam,
    UserAssessment,
    UserCourse,
    UserExamVersion,
    UserUnit,
)


logger = logging.getLogger(__name__)


def _course_products():
    settings = ExamSettings.objects.settings()
    return (
        Product.objects.current()
        .filter(category__product_type=settings.course)
        .order_by("name")
    )


def _unit_products():
    settings = ExamSettings.objects.settings()
    return (
        Product.objects.current()
        .filter(category__product_type=settings.unit)
        .order_by("name")
    )


class CourseProductModelChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, course_product):
        return f"{course_product.product.name} ({course_product.course.name})"


class CourseUnitModelMultipleChoiceField(forms.ModelMultipleChoiceField):
    def label_from_instance(self, course_unit):
        return f"{course_unit.unit.name}"


class AssessmentEmptyForm(forms.ModelForm):
    class Meta:
        model = Assessment
        fields = ()


class AssessmentForm(RequiredFieldForm):
    def __init__(self, *args, **kwargs):
        self.unit = kwargs.pop("unit")
        super().__init__(*args, **kwargs)
        for name in ("assessment_criteria", "instructions"):
            f = self.fields[name]
            f.widget.attrs.update({"class": "pure-input-1", "rows": 5})
        for name in ("assessment_type", "name", "order", "what_to_do"):
            f = self.fields[name]
            f.widget.attrs.update({"class": "pure-input-1"})
        self.fields["blank_form"].widget = FileDropInput(
            zone_id="filedrop-1-zone",
            default_text="Drop the blank form here...",
            click_text="or click here to select the form",
        )
        self.fields["example_form"].widget = FileDropInput(
            zone_id="filedrop-2-zone",
            default_text="Drop the example form here...",
            click_text="or click here to select the form",
        )

    class Meta:
        model = Assessment
        fields = (
            "name",
            "order",
            "assessment_type",
            "what_to_do",
            "instructions",
            "assessment_criteria",
            "blank_form",
            "example_form",
        )

    def _update_order_on_deleted(self, assessment):
        """Update the 'order' on a deleted assessment."""
        original_order = assessment.order
        last_assessment = (
            Assessment.objects.filter(unit=assessment.unit)
            .filter(order__gte=Assessment.HIGH_ORDER)
            .order_by("order")
            .last()
        )
        if last_assessment:
            assessment.order = last_assessment.order + 1
        else:
            assessment.order = Assessment.HIGH_ORDER + 1
        logger.info(
            f"Updated 'order' from {original_order} to {assessment.order} "
            f"for deleted assessment '{assessment.name}' ({assessment.pk}) "
            f"for '{assessment.unit.name}' unit ({assessment.unit.pk})"
        )
        assessment.save()

    def clean_order(self):
        data = self.cleaned_data.get("order")
        check_order = True
        if self.unit:
            # create
            unit = self.unit
        else:
            # update
            unit = self.instance.unit
            if data == self.instance.order:
                check_order = False
        if check_order:
            assessment = None
            try:
                assessment = Assessment.objects.get(unit=unit, order=data)
                if assessment and assessment.is_deleted:
                    # update the 'order' on the deleted assessment so it
                    # is *out of range*...
                    self._update_order_on_deleted(assessment)
                else:
                    # tell the user to change the 'order'
                    final_assessment = (
                        Assessment.objects.current(unit)
                        .order_by("order")
                        .last()
                    )
                    if final_assessment:
                        message = (
                            "An assessment already exists with "
                            f"order {data}. The next available "
                            f"'order' number is {final_assessment.order + 1}"
                        )
                        raise forms.ValidationError(
                            message, code="assessment__order__exists"
                        )
            except Assessment.DoesNotExist:
                pass
        return data


class AssessmentAnswerFileForm(forms.ModelForm):
    """

    03/06/2021, Was a ``ModelForm``, but the model in the
    ``StudentSitAssessmentFormView`` is not a ``UserAssessment``.
    Try using the ``Assessment`` model with a couple of answer fields.

    .. todo:: Need to check file upload is still working!

    """

    answer_file = forms.FileField(required=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["answer_file"].widget = FileDropInput(
            default_text=" ",
            current_file_text=" ",
            click_text="Select your Document",
        )

    class Meta:
        model = Assessment
        fields = ("answer_file",)


class ClassInformationForm(RequiredFieldForm):
    """

    .. note:: Copied from 'member.DiplomaClassForm'

    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        course = self.fields["course"]
        # was 'Product.objects.filter(category__slug="diploma", legacy=False)
        course.queryset = Course.objects.filter(name__icontains="diploma")
        course.label = "Course"
        self.fields["course"].widget.attrs.update({"class": "pure-input-1"})
        self.fields["start_date"].widget.attrs.update(
            {"class": "pure-input-1 datepicker"}
        )
        self.fields["description"].widget.attrs.update(
            {"class": "pure-input-1", "rows": 3}
        )

    class Meta:
        model = ClassInformation
        fields = ("start_date", "course", "description")


class CourseEmptyForm(forms.ModelForm):
    class Meta:
        model = Course
        fields = ()


class CourseForm(forms.ModelForm):
    """Create and update a course.

    The ``products`` are linked to the course using the ``CourseProduct``
    model.

    """

    products = forms.ModelMultipleChoiceField(
        queryset=Product.objects.none(), required=False
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name in ["name", "title", "description"]:
            f = self.fields[name]
            f.widget.attrs.update({"class": "pure-input-1"})
        f = self.fields["products"]
        if self.instance:
            product_pks_to_exclude = (
                CourseProduct.objects.current()
                .exclude(course__pk=self.instance.pk)
                .values_list("product__pk", flat=True)
            )
            f.queryset = _course_products().exclude(
                pk__in=product_pks_to_exclude
            )
        else:
            f.queryset = _course_products()
        f.widget.attrs.update({"class": "chosen-select pure-input-1"})

    class Meta:
        model = Course
        fields = (
            "name",
            "title",
            "description",
            "products",
        )


class CourseUnitEmptyForm(forms.ModelForm):
    class Meta:
        model = CourseUnit
        fields = ()


class CourseUnitMixinForm(forms.ModelForm):
    ordering = forms.ChoiceField(choices=[], label="Order")

    def _ordering(self, course, order=None):
        """Remove existing values for the 'order'."""
        result = []
        if order:
            result.append(order)
        course_units = CourseUnit.objects.for_course(course)
        unit_ordering = course_units.values_list("order", flat=True)
        count = 1
        while True:
            if count in unit_ordering:
                pass
            else:
                result.append(count)
            count = count + 1
            if len(result) > 14:
                break
        return [(x, x) for x in sorted(result)]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # name
        f = self.fields["name"]
        f.widget.attrs.update({"class": "pure-input-1"})
        # product
        f = self.fields["product"]
        f.widget.attrs.update({"class": "chosen-select pure-input-1-4"})
        f.queryset = _unit_products()

    def clean(self):
        cleaned_data = super().clean()
        core = self.cleaned_data.get("core")
        if core:
            pass
        else:
            product = self.cleaned_data.get("product")
            if not product:
                raise forms.ValidationError(
                    "Optional units must have a product",
                    code="course_unit__core_not_product",
                )
        return cleaned_data


class CourseUnitCreateForm(CourseUnitMixinForm):

    def _units(self, course):
        return CourseUnit.objects.units_available(course)

    def __init__(self, *args, **kwargs):
        course = kwargs.pop("course")
        super().__init__(*args, **kwargs)
        # unit
        f = self.fields["unit"]
        f.queryset = self._units(course)
        f.widget.attrs.update({"class": "chosen-select pure-input-1-2"})
        # ordering
        f = self.fields["ordering"]
        f.choices = self._ordering(course)
        f.widget.attrs.update({"class": "chosen-select pure-input-1-4"})

    class Meta:
        model = CourseUnit
        fields = ("name", "unit", "ordering", "core", "product")


class CourseUnitUpdateForm(CourseUnitMixinForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # ordering
        f = self.fields["ordering"]
        f.choices = self._ordering(self.instance.course, self.instance.order)
        f.widget.attrs.update({"class": "chosen-select pure-input-1-4"})

    class Meta:
        model = CourseUnit
        fields = ("name", "ordering", "core", "product")


class ExamEmptyForm(forms.ModelForm):
    class Meta:
        model = Exam
        fields = ()


class ExamForm(forms.ModelForm):
    products = forms.ModelMultipleChoiceField(
        label="Courses", queryset=Product.objects.none(), required=True
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name in ["name", "description"]:
            f = self.fields[name]
            f.widget.attrs.update({"class": "pure-input-1"})
        f = self.fields["products"]
        f.queryset = _course_products()
        f.widget.attrs.update({"class": "chosen-select pure-input-1-2"})

    class Meta:
        model = Exam
        fields = ("name", "description")


class ExamSettingsForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        names = (
            "fail_but_can_retake",
            "retake_warning",
            "fail_retake",
            "passed",
        )
        for name in names:
            self.fields[name].widget.attrs.update(
                {"class": "pure-input-1", "rows": 5}
            )
        for name in ("course", "unit"):
            f = self.fields[name]
            f.widget.attrs.update({"class": "chosen-select pure-input-1-4"})

    class Meta:
        model = ExamSettings
        fields = (
            "fail_but_can_retake",
            "retake_warning",
            "fail_retake",
            "passed",
            "course",
            "unit",
        )


class ExamVersionEmptyForm(forms.ModelForm):
    class Meta:
        model = ExamVersion
        fields = ()


class OptionForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        f = self.fields["option"]
        f.widget.attrs.update({"class": "pure-input-1", "rows": 2})
        f.label = ""

    class Meta:
        model = Option
        fields = ("option",)


OptionFormSet = modelformset_factory(Option, extra=0, form=OptionForm)


class QuestionForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        question = kwargs.pop("question")
        super().__init__(*args, **kwargs)
        f = self.fields["question"]
        f.widget.attrs.update({"class": "pure-input-1", "rows": 2})
        f = self.fields["answer"]
        f.label = "Correct Answer"
        f.label_from_instance = lambda obj: "Answer {}".format(obj.number)
        f.queryset = question.options()
        f.required = True
        if not question.can_edit_answer():
            f.disabled = True

    class Meta:
        model = Question
        fields = ("question", "answer")


class UnitEmptyForm(forms.ModelForm):
    class Meta:
        model = Unit
        fields = ()


class UnitExamEmptyForm(forms.ModelForm):
    class Meta:
        model = UnitExam
        fields = ()


class UnitExamForm(forms.ModelForm):
    class Meta:
        model = UnitExam
        fields = ("exam",)


class UnitForm(RequiredFieldForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        f = self.fields["assignment_or_assessment"]
        f.widget.attrs.update({"class": "chosen-select pure-input-1-4"})
        for name in ("name",):
            f = self.fields[name]
            f.widget.attrs.update({"class": "pure-input-1"})

    class Meta:
        model = Unit
        fields = (
            "name",
            "description",
            "assignment_or_assessment",
            "course_material",
        )


class UserAssessmentForm(RequiredFieldForm):
    """User assessment form."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        f = self.fields["assessor_comments"]
        f.label = "Comments"
        f.widget.attrs.update({"class": "pure-input-1"})
        user_unit = self.instance.user_unit
        if user_unit.unit.is_assignment():
            del self.fields["error_count"]
            del self.fields["has_passed"]
            self.fields["grade"].help_text = ""
        else:
            del self.fields["grade"]
            self.fields["error_count"].help_text = ""
            if not user_unit.is_current_user_assessment():
                del self.fields["has_passed"]

    class Meta:
        model = UserAssessment
        fields = (
            "can_retake",
            "error_count",
            "grade",
            "assessor_comments",
            "has_passed",
        )

    def clean(self):
        cleaned_data = super().clean()
        user_unit = self.instance.user_unit
        if user_unit.unit.is_assignment():
            pass
        else:
            can_retake = self.cleaned_data.get("can_retake")
            has_passed = self.cleaned_data.get("has_passed")
            if can_retake and has_passed:
                raise forms.ValidationError(
                    (
                        "A student cannot be asked to retake "
                        "an assessment they have passed"
                    ),
                    code="user_assessment__cannot_retake_and_pass",
                )
        return cleaned_data


class UserAssessmentRetakeForm(RequiredFieldForm):
    """User assessment form."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        f = self.fields["retake_comments"]
        f.label = "Retake Comments"
        f.widget.attrs.update({"class": "pure-input-1"})
        user_unit = self.instance.user_unit
        if user_unit.unit.is_assignment():
            del self.fields["retake_has_passed"]
            del self.fields["retake_error_count"]
            self.fields["retake_grade"].help_text = ""
        else:
            del self.fields["retake_grade"]
            self.fields["retake_error_count"].help_text = ""
            if not user_unit.is_current_user_assessment():
                del self.fields["retake_has_passed"]

    class Meta:
        model = UserAssessment
        fields = (
            "retake_error_count",
            "retake_grade",
            "retake_comments",
            "retake_has_passed",
        )


class UserCourseCoreCreateForm(RequiredFieldForm):
    course_units = CourseUnitModelMultipleChoiceField(
        queryset=CourseUnit.objects.none(), required=False
    )

    def __init__(self, *args, **kwargs):
        course = kwargs.pop("course")
        super().__init__(*args, **kwargs)
        f = self.fields["course_units"]
        f.label = "Please choose three of the optional units"
        f.queryset = (
            CourseUnit.objects.for_course(course)
            .filter(core=False)
            .order_by("order")
        )
        f.widget.attrs.update({"class": "chosen-select pure-input-1"})
        # payment_details
        f = self.fields["payment_details"]
        f.widget.attrs.update({"class": "pure-input-2-3"})

    class Meta:
        model = UserCourse
        fields = (
            "course_units",
            "payment_details",
        )

    # def clean_course_units(self):
    # 22/07/2024, Remove the check for three optional units.
    # Foundation and Advanced Courses can have 0, 1, 2 or 3 optional units.
    # https://www.kbsoftware.co.uk/crm/ticket/7270/
    #    data = self.cleaned_data.get("course_units")
    #    if data.count() == 3:
    #        pass
    #    else:
    #        raise forms.ValidationError(
    #            "Please select three of the optional units",
    #            code="usercourse__course_unit__optional",
    #        )
    #    return data


class UserCourseCreateForm(RequiredFieldForm):
    course_product = CourseProductModelChoiceField(
        queryset=CourseProduct.objects.none(), required=True
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        f = self.fields["course_product"]
        f.queryset = CourseProduct.objects.current()
        f.widget.attrs.update({"class": "chosen-select pure-input-1"})
        # payment_details
        f = self.fields["payment_details"]
        f.widget.attrs.update({"class": "pure-input-2-3"})

    class Meta:
        model = UserCourse
        fields = (
            "course_product",
            "payment_details",
        )


class UserCourseEmptyForm(forms.ModelForm):
    class Meta:
        model = UserCourse
        fields = ()


class UserCourseForm(RequiredFieldForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        f = self.fields["assessor_comments"]
        f.label = "Comments"
        f.widget.attrs.update({"class": "pure-input-1"})
        for name in ("assessor_comments", "result"):
            self.fields[name].required = True

    class Meta:
        model = UserCourse
        fields = ("assessor_comments", "result")


class UserCourseProfileForm(RequiredFieldForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name in ("additional_notes", "performance_notes"):
            f = self.fields[name]
            f.widget.attrs.update({"class": "pure-input-1", "rows": 5})
        # payment_details
        f = self.fields["payment_details"]
        f.widget.attrs.update({"class": "pure-input-2-3"})
        # selected_class
        f = self.fields["selected_class"]
        f.label = "Diploma Class"
        f.queryset = ClassInformation.objects.filter(
            course=self.instance.course
        )
        f.widget.attrs.update({"class": "chosen-select pure-input-1-4"})

    class Meta:
        model = UserCourse
        fields = (
            # "enrol_date",
            "payment_details",
            "selected_class",
            "extension_expires",
            "performance_notes",
            "additional_notes",
            "cancellation_date",
        )


class UserExamQuestionForm(forms.ModelForm):
    options = forms.ModelChoiceField(
        label="Choose Your Answer",
        queryset=Option.objects.none(),
        widget=forms.RadioSelect,
    )

    def __init__(self, *args, **kwargs):
        question = kwargs.pop("question")
        super().__init__(*args, **kwargs)
        f = self.fields["options"]
        f.empty_label = None
        f.label = question.question
        f.label_from_instance = lambda obj: "{}".format(obj.option)
        f.queryset = question.options()

    class Meta:
        model = Question
        fields = ("options",)


class UserExamVersionEmptyForm(forms.ModelForm):
    class Meta:
        model = UserExamVersion
        fields = ()


class UserExamVersionPercentForm(forms.ModelForm):
    percent = forms.IntegerField()

    def __init__(self, *args, **kwargs):
        legacy = kwargs.pop("legacy")
        super().__init__(*args, **kwargs)
        if legacy:
            self.fields.update({"retake": forms.BooleanField(required=False)})

    class Meta:
        model = UserExamVersion
        fields = ("percent",)

    def clean_percent(self):
        data = self.cleaned_data.get("percent")
        if data < 0 or data > 100:
            message = "The percentage mark must be between 0 and 100"
            raise forms.ValidationError(message, code="exam__percent__range")
        return data


class UserUnitCommentForm(RequiredFieldForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        f = self.fields["assessor_comments"]
        f.label = "Comments"
        f.widget.attrs.update({"class": "pure-input-1"})
        f.required = True

    class Meta:
        model = UserUnit
        fields = ("assessor_comments",)

    def clean(self):
        cleaned_data = super().clean()
        if not self.instance.result:
            raise forms.ValidationError(
                "The unit has no result. The unit cannot be assessed.",
                code="user_unit__no_result",
            )
        return cleaned_data


class UserUnitResultForm(RequiredFieldForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        f = self.fields["result"]
        f.required = True

    class Meta:
        model = UserUnit
        fields = ("result",)

    # def clean(self):
    #     cleaned_data = super().clean()
    #     # if self.instance.assessed:
    #     #     raise forms.ValidationError(
    #     #         "Cannot change the result.  The unit has already been assessed",
    #     #         code="user_unit__already_assessed",
    #     #     )
    #     return cleaned_data


class UserUnitRetakeCommentForm(RequiredFieldForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        f = self.fields["retake_comments"]
        f.label = "Comments"
        f.widget.attrs.update({"class": "pure-input-1"})
        f.required = True

    class Meta:
        model = UserUnit
        fields = ("retake_comments",)
