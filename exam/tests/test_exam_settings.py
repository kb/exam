# -*- encoding: utf-8 -*-
import pytest

from exam.models import ExamSettings


@pytest.mark.django_db
def test_save():
    obj = ExamSettings.load()
    obj.retake_warning = "Be Careful"
    obj.save()
    assert "Be Careful" == obj.retake_warning


@pytest.mark.django_db
def test_str():
    obj = ExamSettings.load()
    assert "Exam Settings" in str(obj)
