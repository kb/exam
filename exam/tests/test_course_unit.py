# -*- encoding: utf-8 -*-
import pytest

from django.db.utils import IntegrityError

from exam.models import CourseUnit
from exam.tests.factories import CourseFactory, CourseUnitFactory, UnitFactory
from login.tests.factories import UserFactory


@pytest.mark.django_db
def test_courses_for_unit():
    unit = UnitFactory()
    user = UserFactory(is_staff=True)
    CourseUnitFactory(course=CourseFactory(name="c1"), unit=unit)
    CourseUnitFactory(course=CourseFactory(name="c2"), unit=UnitFactory())
    course = CourseFactory(name="c3")
    CourseUnitFactory(course=course, unit=unit)
    course.set_deleted(user)
    CourseUnitFactory(course=CourseFactory(name="c4"), unit=unit)
    qs = CourseUnit.objects.courses_for_unit(unit)
    assert ["c1", "c4"] == [x.course.name for x in qs]


@pytest.mark.django_db
def test_courses_for_unit_deleted():
    unit = UnitFactory()
    user = UserFactory(is_staff=True)
    unit.set_deleted(user)
    CourseUnitFactory(course=CourseFactory(name="c1"), unit=unit)
    qs = CourseUnit.objects.courses_for_unit(unit)
    assert [] == [x.course.name for x in qs]


@pytest.mark.django_db
def test_create_course_unit():
    course = CourseFactory()
    unit = UnitFactory()
    x = CourseUnit.objects.create_course_unit(course, unit, 8)
    assert 8 == x.order
    assert course == x.course
    assert unit == x.unit


@pytest.mark.django_db
def test_for_course():
    course = CourseFactory()
    unit_a = UnitFactory(name="a")
    CourseUnitFactory(course=course, unit=unit_a, order=1)
    unit_b = UnitFactory(name="b")
    unit_c = UnitFactory(name="c")
    unit_c.set_deleted(UserFactory())
    unit_d = UnitFactory(name="d")
    CourseUnitFactory(course=course, unit=unit_d, order=2)
    unit_e = UnitFactory(name="e")
    # another course
    unit_f = UnitFactory(name="f")
    CourseUnitFactory(course=CourseFactory(), unit=unit_f, order=1)
    # deleted course unit
    unit_g = UnitFactory(name="g")
    course_unit = CourseUnitFactory(course=course, unit=unit_g, order=3)
    course_unit.set_deleted(UserFactory())
    assert ["a", "d"] == [
        x.unit.name for x in CourseUnit.objects.for_course(course)
    ]


@pytest.mark.django_db
def test_init_course_unit():
    course = CourseFactory()
    unit = UnitFactory()
    x = CourseUnit.objects.init_course_unit(course, unit, 8)
    assert 8 == x.order
    assert course == x.course
    assert unit == x.unit


@pytest.mark.django_db
def test_init_course_unit_exists():
    course = CourseFactory()
    unit = UnitFactory()
    CourseUnitFactory(course=course, unit=unit, order=23)
    x = CourseUnit.objects.init_course_unit(course, unit, 8)
    assert 1 == CourseUnit.objects.count()
    assert 8 == x.order
    assert course == x.course
    assert unit == x.unit


@pytest.mark.django_db
def test_unique_together():
    course = CourseFactory()
    unit = UnitFactory()
    CourseUnitFactory(course=course, unit=unit)
    CourseUnitFactory(course=CourseFactory(), unit=unit)
    with pytest.raises(IntegrityError):
        CourseUnitFactory(course=course, unit=unit)


@pytest.mark.django_db
def test_unique_together_order():
    """We no longer have 'unique_together' on the 'order' field."""
    course = CourseFactory()
    CourseUnitFactory(course=course, unit=UnitFactory(name="b"), order=1)
    CourseUnitFactory(course=course, unit=UnitFactory(name="c"), order=2)
    # with pytest.raises(IntegrityError):
    CourseUnitFactory(course=course, unit=UnitFactory(name="a"), order=1)
    assert ["a", "b", "c"] == [x.unit.name for x in CourseUnit.objects.all()]


@pytest.mark.django_db
def test_units_available():
    course = CourseFactory()
    unit_a = UnitFactory(name="a")
    CourseUnitFactory(course=course, unit=unit_a, order=1)
    unit_b = UnitFactory(name="b")
    unit_c = UnitFactory(name="c")
    unit_c.set_deleted(UserFactory())
    unit_d = UnitFactory(name="d")
    CourseUnitFactory(course=course, unit=unit_d, order=2)
    unit_e = UnitFactory(name="e")
    # another course
    unit_f = UnitFactory(name="f")
    CourseUnitFactory(course=CourseFactory(), unit=unit_f, order=1)
    # deleted course unit
    unit_g = UnitFactory(name="g")
    course_unit = CourseUnitFactory(course=course, unit=unit_g, order=3)
    course_unit.set_deleted(UserFactory())
    assert ["b", "e", "f", "g"] == [
        x.name for x in CourseUnit.objects.units_available(course)
    ]
