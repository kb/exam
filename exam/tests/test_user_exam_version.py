# -*- encoding: utf-8 -*-
"""A user takes an exam.

User flow:

The user is logged into the system and is taking an exam.  They start by
choosing the exam.  The system will create a row in the ``UserExamVersion``
table (``test_sit_exam``).

When the user answers the first question, a clock starts.  They have 24 hours
to complete the test.  After 24 hours, the answers are cleared.

  I am questioning this requirement, see *Questions, w/c 01/09/2016* in
  https://docs.google.com/a/kbsoftware.co.uk/document/d/1CWAus5Jm00PjtPtYlsxaKNBxKkk9NF7c1z4_AG5BgtY/edit?usp=sharing

"""
import pytest

from django.urls import reverse
from django.utils import timezone

from exam.models import ExamError, Option, Question, UserExamVersion
from exam.tests.factories import (
    CourseUnitFactory,
    ExamFactory,
    ExamVersionFactory,
    OptionFactory,
    QuestionFactory,
    SelectionFactory,
    UnitExamFactory,
    UnitFactory,
    UserExamVersionFactory,
)
from exam.tests.scenario import (
    set_correct_answers,
    set_correct_answers_all,
    set_incorrect_answers,
    setup_exam,
)
from login.tests.factories import UserFactory
from mail.models import Message
from mail.tests.factories import MailTemplateFactory


@pytest.mark.django_db
def test_answered_a_question():
    user = UserFactory()
    exam_version = setup_exam()
    exam_version.exam.init_exam(user)
    # get the user_exam_version
    user_exam_version = UserExamVersion.objects.get(user=user)
    # answer some questions
    for number in (3, 9, 12):
        q = Question.objects.get(exam_version=exam_version, number=number)
        user_exam_version.set_answer(q, q.answer)
    assert 3 == user_exam_version.answered_a_question()


@pytest.mark.django_db
def test_answered_a_question_not():
    user = UserFactory()
    exam_version = setup_exam()
    exam_version.exam.init_exam(user)
    # get the user_exam_version
    user_exam_version = UserExamVersion.objects.get(user=user)
    assert 0 == user_exam_version.answered_a_question()


@pytest.mark.django_db
def test_answered_a_question_retake():
    MailTemplateFactory(slug=UserExamVersion.MAIL_TEMPLATE_FAIL_INITIAL)
    user = UserFactory(is_staff=False, first_name="Pedro", last_name="Pepper")
    exam_version = setup_exam("Cake")
    exam_version.exam.init_exam(user)
    user_exam_version = UserExamVersion.objects.get(
        user=user, exam_version=exam_version
    )
    set_correct_answers_all(user_exam_version)
    set_incorrect_answers(user_exam_version, [1, 3, 5, 7, 8, 9])
    user_exam_version.mark()
    user_exam_version = exam_version.exam.user_exam_version(user)
    assert 0 == user_exam_version.answered_a_question()


@pytest.mark.django_db
def test_answers_remaining():
    user = UserFactory()
    exam_version = setup_exam()
    exam_version.exam.init_exam(user)
    # get the user_exam_version
    user_exam_version = UserExamVersion.objects.get(user=user)
    assert 20 == user_exam_version.answers_remaining()


@pytest.mark.django_db
def test_answers_remaining_none():
    user = UserFactory()
    exam_version = setup_exam()
    exam_version.exam.init_exam(user)
    # get the user_exam_version
    user_exam_version = UserExamVersion.objects.get(user=user)
    set_correct_answers_all(user_exam_version)
    assert 0 == user_exam_version.answers_remaining()


@pytest.mark.django_db
def test_answers_remaining_retake():
    MailTemplateFactory(slug=UserExamVersion.MAIL_TEMPLATE_FAIL_INITIAL)
    user = UserFactory(is_staff=False, first_name="Pedro", last_name="Pepper")
    exam_version = setup_exam("Cake")
    exam_version.exam.init_exam(user)
    user_exam_version = UserExamVersion.objects.get(
        user=user, exam_version=exam_version
    )
    set_correct_answers_all(user_exam_version)
    set_incorrect_answers(user_exam_version, [1, 3, 5, 7, 8, 9])
    user_exam_version.mark()
    user_exam_version = exam_version.exam.user_exam_version(user)
    assert 6 == user_exam_version.answers_remaining()


@pytest.mark.django_db
def test_find_course_units():
    """Find the ``CourseUnit`` for this exam.

    .. note:: The first sort order is the ``UnitExam``.

    .. note:: There may be more than one because an exam can link to
              several units and the unit can link to several courses.

    """
    unit_1 = UnitFactory(name="Software Development")
    course_unit_1 = CourseUnitFactory(unit=unit_1)
    # unit 2
    unit_2 = UnitFactory(name="Dairy Farming")
    course_unit_2 = CourseUnitFactory(unit=unit_2)
    # exam
    exam = ExamFactory(name="python")
    exam_version = ExamVersionFactory(exam=exam)
    user_exam_version = UserExamVersionFactory(
        exam_version=exam_version, user=UserFactory()
    )
    # unit 1
    UnitExamFactory(unit=unit_1, exam=exam)
    # unit 2
    UnitExamFactory(unit=unit_2, exam=exam)
    assert [
        course_unit_2,
        course_unit_1,
    ] == user_exam_version.find_course_units()


@pytest.mark.django_db
def test_first_question():
    user = UserFactory()
    exam_version = setup_exam("Cake")
    exam_version.exam.init_exam(user)
    user_exam_version = exam_version.exam.user_exam_version(user)
    assert 1 == user_exam_version.first_question().number


@pytest.mark.django_db
def test_first_question_retake():
    MailTemplateFactory(slug=UserExamVersion.MAIL_TEMPLATE_FAIL_INITIAL)
    user = UserFactory()
    exam_version = setup_exam("Cake")
    exam_version.exam.init_exam(user)
    user_exam_version = exam_version.exam.user_exam_version(user)
    set_correct_answers_all(user_exam_version)
    set_incorrect_answers(user_exam_version, [5, 7, 8, 9, 10, 11])
    user_exam_version.mark()
    user_exam_version = exam_version.exam.user_exam_version(user)
    assert 5 == user_exam_version.first_question().number


@pytest.mark.django_db
def test_is_fail():
    user = UserFactory()
    user_exam_version = UserExamVersionFactory(user=user)
    user_exam_version.set_result(15)
    assert user_exam_version.is_fail() is True


@pytest.mark.django_db
def test_is_fail_no_result():
    user = UserFactory()
    user_exam_version = UserExamVersionFactory(user=user)
    assert user_exam_version.is_fail() is None


@pytest.mark.django_db
def test_is_fail_pass():
    user = UserFactory()
    user_exam_version = UserExamVersionFactory(user=user)
    user_exam_version.set_result(16)
    assert user_exam_version.is_fail() is False


@pytest.mark.django_db
def test_is_marked():
    user = UserFactory()
    user_exam_version = UserExamVersionFactory(user=user)
    user_exam_version.set_result(16)
    assert user_exam_version.is_marked() is True


@pytest.mark.django_db
def test_is_marked_not():
    user = UserFactory()
    user_exam_version = UserExamVersionFactory(user=user)
    assert user_exam_version.is_marked() is False


@pytest.mark.django_db
def test_is_marked_override():
    user = UserFactory()
    user_exam_version = UserExamVersionFactory(user=user)
    # cannot override until the result has been set
    user_exam_version.set_result(16)
    user_exam_version.set_percent_override(16, user)
    assert user_exam_version.is_marked() is True


@pytest.mark.django_db
def test_is_pass():
    user = UserFactory()
    user_exam_version = UserExamVersionFactory(user=user)
    user_exam_version.set_result(16)
    assert user_exam_version.is_pass() is True


@pytest.mark.django_db
def test_is_pass_fail():
    user = UserFactory()
    user_exam_version = UserExamVersionFactory(user=user)
    user_exam_version.set_result(10)
    assert user_exam_version.is_pass() is False


@pytest.mark.django_db
def test_is_pass_no_result():
    user = UserFactory()
    user_exam_version = UserExamVersionFactory(user=user)
    assert user_exam_version.is_pass() is None


@pytest.mark.django_db
def test_last_question():
    user = UserFactory()
    exam_version = setup_exam("Cake")
    exam_version.exam.init_exam(user)
    user_exam_version = exam_version.exam.user_exam_version(user)
    assert 20 == user_exam_version.last_question().number


@pytest.mark.django_db
def test_last_question_retake():
    MailTemplateFactory(slug=UserExamVersion.MAIL_TEMPLATE_FAIL_INITIAL)
    user = UserFactory()
    exam_version = setup_exam("Cake")
    exam_version.exam.init_exam(user)
    user_exam_version = exam_version.exam.user_exam_version(user)
    set_correct_answers_all(user_exam_version)
    set_incorrect_answers(user_exam_version, [5, 7, 8, 9, 16, 18])
    user_exam_version.mark()
    user_exam_version = exam_version.exam.user_exam_version(user)
    assert 18 == user_exam_version.last_question().number


@pytest.mark.django_db
def test_mark_all_correct():
    MailTemplateFactory(slug=UserExamVersion.MAIL_TEMPLATE_PASS)
    user = UserFactory(is_staff=False, first_name="Paul", last_name="Pepper")
    exam_version = setup_exam("Cake")
    exam_version.exam.init_exam(user)
    user_exam_version = UserExamVersion.objects.get(
        user=user, exam_version=exam_version
    )
    set_correct_answers_all(user_exam_version)
    user_exam_version.mark()
    assert user_exam_version.is_pass() is True
    message = Message.objects.first()
    assert message is not None
    mail = message.mail_set.first()
    assert mail is not None
    assert 5 == mail.mailfield_set.count()
    assert {
        "exam_name": "Cake",
        "name": "Paul",
        "questions": " ",
        "result": "20",
        "result_as_percent": "100",
    } == {f.key: f.value for f in mail.mailfield_set.all()}


@pytest.mark.django_db
def test_mark_pass():
    MailTemplateFactory(slug=UserExamVersion.MAIL_TEMPLATE_PASS)
    user = UserFactory(is_staff=False, first_name="Peter", last_name="Pepper")
    exam_version = setup_exam("Cake")
    exam_version.exam.init_exam(user)
    user_exam_version = UserExamVersion.objects.get(
        user=user, exam_version=exam_version
    )
    set_correct_answers_all(user_exam_version)
    set_incorrect_answers(user_exam_version, [3, 9])
    user_exam_version.mark()
    assert user_exam_version.is_pass() is True
    message = Message.objects.first()
    assert message is not None
    mail = message.mail_set.first()
    assert mail is not None
    assert 5 == mail.mailfield_set.count()
    expect = (
        "You answered the following questions incorrectly:"
        "<br>3. question 3<br><strong>The correct answer is:</strong><br>option 3.1<br>"
        "<br>9. question 9<br><strong>The correct answer is:</strong><br>option 9.1<br>"
    )
    assert {
        "exam_name": "Cake",
        "name": "Peter",
        "questions": expect,
        "result": "18",
        "result_as_percent": "90",
    } == {f.key: f.value for f in mail.mailfield_set.all()}


@pytest.mark.django_db
def test_mark_pass_retake():
    MailTemplateFactory(slug=UserExamVersion.MAIL_TEMPLATE_FAIL_INITIAL)
    MailTemplateFactory(slug=UserExamVersion.MAIL_TEMPLATE_FAIL_RETAKE)
    MailTemplateFactory(slug=UserExamVersion.MAIL_TEMPLATE_PASS)
    user = UserFactory(is_staff=False, first_name="Patrick", last_name="Pepper")
    exam_version = setup_exam("Cake")
    exam_version.exam.init_exam(user)
    user_exam_version = UserExamVersion.objects.get(
        user=user, exam_version=exam_version
    )
    set_correct_answers_all(user_exam_version)
    set_incorrect_answers(user_exam_version, [1, 3, 5, 7, 8, 9])
    user_exam_version.mark()
    user_exam_version = exam_version.exam.user_exam_version(user)
    set_correct_answers(user_exam_version, [1, 3, 5, 7, 9])
    set_incorrect_answers(user_exam_version, [8])
    user_exam_version.mark()
    assert user_exam_version.is_fail() is False
    assert user_exam_version.is_pass() is True
    message = Message.objects.first()
    assert message is not None
    mail = message.mail_set.first()
    assert mail is not None
    assert 5 == mail.mailfield_set.count()
    expect = (
        "You answered the following questions incorrectly:"
        "<br>8. question 8<br><strong>The correct answer is:</strong><br>option 8.1<br>"
    )
    assert {
        "exam_name": "Cake",
        "name": "Patrick",
        "questions": expect,
        "result": "19",
        "result_as_percent": "80",
    } == {f.key: f.value for f in mail.mailfield_set.all()}


@pytest.mark.django_db
def test_mark_fail():
    MailTemplateFactory(slug=UserExamVersion.MAIL_TEMPLATE_FAIL_INITIAL)
    user = UserFactory(is_staff=False, first_name="Pete", last_name="Pepper")
    exam_version = setup_exam("Cake")
    exam_version.exam.init_exam(user)
    user_exam_version = UserExamVersion.objects.get(
        user=user, exam_version=exam_version
    )
    set_correct_answers_all(user_exam_version)
    set_incorrect_answers(user_exam_version, [3, 5, 7, 8, 9])
    user_exam_version.mark()
    assert user_exam_version.is_fail() is True
    message = Message.objects.first()
    assert message is not None
    mail = message.mail_set.first()
    assert mail is not None
    assert 5 == mail.mailfield_set.count()
    expect = (
        "You answered the following questions incorrectly:"
        "<br>3. question 3<br>5. question 5"
        "<br>7. question 7<br>8. question 8"
        "<br>9. question 9"
    )
    assert {
        "exam_name": "Cake",
        "name": "Pete",
        "questions": expect,
        "result": "15",
        "result_as_percent": "75",
    } == {f.key: f.value for f in mail.mailfield_set.all()}


@pytest.mark.django_db
def test_mark_fail_retake():
    MailTemplateFactory(slug=UserExamVersion.MAIL_TEMPLATE_FAIL_INITIAL)
    MailTemplateFactory(slug=UserExamVersion.MAIL_TEMPLATE_FAIL_RETAKE)
    user = UserFactory(is_staff=False, first_name="Pa", last_name="Pepper")
    exam_version = setup_exam("Cake")
    exam_version.exam.init_exam(user)
    user_exam_version = UserExamVersion.objects.get(
        user=user, exam_version=exam_version
    )
    set_correct_answers_all(user_exam_version)
    set_incorrect_answers(user_exam_version, [1, 3, 5, 7, 8, 9])
    user_exam_version.mark()
    user_exam_version = exam_version.exam.user_exam_version(user)
    set_correct_answers(user_exam_version, [7])
    set_incorrect_answers(user_exam_version, [1, 3, 5, 8, 9])
    user_exam_version.mark()
    assert user_exam_version.is_fail() is True
    message = Message.objects.first()
    assert message is not None
    mail = message.mail_set.first()
    assert mail is not None
    assert 5 == mail.mailfield_set.count()
    expect = (
        "You answered the following questions incorrectly:"
        "<br>1. question 1<br>3. question 3<br>5. question 5"
        "<br>8. question 8<br>9. question 9"
    )
    assert {
        "exam_name": "Cake",
        "name": "Pa",
        "questions": expect,
        "result": "15",
        "result_as_percent": "75",
    } == {f.key: f.value for f in mail.mailfield_set.all()}


@pytest.mark.django_db
def test_mark_unanswered():
    MailTemplateFactory(slug=UserExamVersion.MAIL_TEMPLATE_FAIL_INITIAL)
    user = UserFactory(is_staff=False, username="pat")
    exam = ExamFactory()
    exam_version = ExamVersionFactory(exam=exam)
    # question_1
    question_1 = QuestionFactory(exam_version=exam_version)
    answer_1 = OptionFactory(question=question_1)
    question_1.answer = answer_1
    question_1.save()
    # question_2
    question_2 = QuestionFactory(exam_version=exam_version)
    answer_2 = OptionFactory(question=question_2)
    question_2.answer = answer_2
    question_2.save()
    # user_exam_version
    user_exam_version = UserExamVersionFactory(
        user=user,
        exam_version=exam_version,
    )
    # no answer (student doesn't need to answer all the questions)
    # SelectionFactory(user_exam_version=user_exam_version, answer=answer_1)
    # answer (student answered this question)
    SelectionFactory(user_exam_version=user_exam_version, answer=answer_2)
    with pytest.raises(ExamError) as e:
        user_exam_version.mark()
    assert (
        f"User 'pat' has not answered question '{question_1.pk}' "
        f"({user_exam_version.pk})"
    ) in str(e.value)
    user_exam_version.refresh_from_db()
    assert user_exam_version.result is None


@pytest.mark.django_db
def test_next_test():
    user = UserFactory()
    u1 = UserExamVersionFactory(user=user)
    u2 = UserExamVersionFactory(user=user, exam_version=u1.exam_version)
    assert u2 == u1.next_test()


@pytest.mark.django_db
def test_next_test_before():
    user = UserFactory()
    u1 = UserExamVersionFactory(user=user)
    u2 = UserExamVersionFactory(user=user, exam_version=u1.exam_version)
    assert u2.next_test() is None


@pytest.mark.django_db
def test_next_test_not():
    user = UserFactory()
    obj = UserExamVersionFactory(user=user)
    assert obj.next_test() is None


@pytest.mark.django_db
def test_next_url():
    user = UserFactory()
    exam_version = setup_exam()
    exam_version.exam.init_exam(user)
    user_exam_version = UserExamVersion.objects.get(user=user)
    q3 = Question.objects.get(exam_version=exam_version, number=3)
    q4 = Question.objects.get(exam_version=exam_version, number=4)
    expect = reverse(
        "web.exam.user.question", args=[user_exam_version.pk, q4.pk]
    )
    assert expect == user_exam_version.next_url(q3)


@pytest.mark.django_db
def test_next_url_last_question():
    """Ask for the 'next_url' for the final question."""
    user = UserFactory()
    exam_version = setup_exam()
    exam_version.exam.init_exam(user)
    user_exam_version = UserExamVersion.objects.get(user=user)
    q20 = Question.objects.get(exam_version=exam_version, number=20)
    expect = reverse(
        "web.exam.user.version.update", args=[user_exam_version.pk]
    )
    assert expect == user_exam_version.next_url(q20)


@pytest.mark.django_db
def test_next_url_retake():
    """Retake... and question 4 was answered correctly the first time."""
    user = UserFactory()
    exam_version = setup_exam()
    exam_version.exam.init_exam(user)
    user_exam_version = UserExamVersion.objects.get(user=user)
    q3 = Question.objects.get(exam_version=exam_version, number=3)
    q4 = Question.objects.get(exam_version=exam_version, number=4)
    SelectionFactory(
        user_exam_version=user_exam_version,
        copy_of_previous=True,
        answer=q4.answer,
    )
    q5 = Question.objects.get(exam_version=exam_version, number=5)
    expect = reverse(
        "web.exam.user.question", args=[user_exam_version.pk, q5.pk]
    )
    assert expect == user_exam_version.next_url(q3)


@pytest.mark.django_db
def test_previous_test():
    user = UserFactory()
    u1 = UserExamVersionFactory(user=user)
    u2 = UserExamVersionFactory(user=user, exam_version=u1.exam_version)
    assert u1 == u2.previous_test()


@pytest.mark.django_db
def test_previous_test_after():
    user = UserFactory()
    obj = UserExamVersionFactory(user=user)
    UserExamVersionFactory(user=user, exam_version=obj.exam_version)
    assert obj.previous_test() is None


@pytest.mark.django_db
def test_previous_test_not():
    user = UserFactory()
    obj = UserExamVersionFactory(user=user)
    assert obj.previous_test() is None


@pytest.mark.django_db
def test_question_state():
    user = UserFactory()
    exam_version = setup_exam()
    exam_version.exam.init_exam(user)
    # get the user_exam_version
    user_exam_version = UserExamVersion.objects.get(user=user)
    # set the answers for question 3
    q3 = Question.objects.get(exam_version=exam_version, number=3)
    a3 = Option.objects.get(question=q3, number=2)
    user_exam_version.set_answer(q3, a3)
    # set the answers for question 12
    q12 = Question.objects.get(exam_version=exam_version, number=12)
    a12 = Option.objects.get(question=q12, number=4)
    user_exam_version.set_answer(q12, a12)
    # test
    result = user_exam_version.question_state()
    check = []
    for question, answered in result:
        check.append((question.number, answered))
    assert [
        (1, False),
        (2, False),
        (3, True),
        (4, False),
        (5, False),
        (6, False),
        (7, False),
        (8, False),
        (9, False),
        (10, False),
        (11, False),
        (12, True),
        (13, False),
        (14, False),
        (15, False),
        (16, False),
        (17, False),
        (18, False),
        (19, False),
        (20, False),
    ] == check


@pytest.mark.django_db
def test_question_state_retake():
    MailTemplateFactory(slug=UserExamVersion.MAIL_TEMPLATE_FAIL_INITIAL)
    user = UserFactory()
    exam_version = setup_exam()
    exam_version.exam.init_exam(user)
    user_exam_version = UserExamVersion.objects.get(
        user=user, exam_version=exam_version
    )
    set_correct_answers_all(user_exam_version)
    set_incorrect_answers(user_exam_version, [1, 3, 5, 7, 8, 9])
    user_exam_version.mark()
    user_exam_version = exam_version.exam.user_exam_version(user)
    # set the answers for question 3
    q3 = Question.objects.get(exam_version=exam_version, number=3)
    a3 = Option.objects.get(question=q3, number=1)
    user_exam_version.set_answer(q3, a3)
    # set the answer for question 7 (wrong or right?)
    q7 = Question.objects.get(exam_version=exam_version, number=7)
    a7 = Option.objects.get(question=q7, number=4)
    user_exam_version.set_answer(q7, a7)
    # test
    result = user_exam_version.question_state()
    check = []
    for question, answered in result:
        check.append((question.number, answered))
    assert [
        (1, False),
        (3, True),
        (5, False),
        (7, True),
        (8, False),
        (9, False),
    ] == check


@pytest.mark.django_db
def test_questions():
    exam_version = setup_exam()
    assert [
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
    ] == [obj.number for obj in exam_version.questions().order_by("number")]


@pytest.mark.django_db
def test_result_as_percent_80():
    user = UserFactory()
    obj = UserExamVersionFactory(user=user)
    obj.set_result(18)
    assert 90 == obj.result_as_percent()


@pytest.mark.django_db
def test_result_as_percent_50():
    user = UserFactory()
    obj = UserExamVersionFactory(user=user)
    obj.set_result(10)
    assert 50 == obj.result_as_percent()


@pytest.mark.django_db
def test_result_as_text():
    user_exam_version = UserExamVersionFactory(user=UserFactory())
    assert "Awaiting Completion" == user_exam_version.result_as_text()


@pytest.mark.parametrize(
    "result,expect",
    [
        (11, "55% FAIL (RETAKE)"),
        (12, "60% FAIL (RETAKE)"),
        (13, "65% FAIL (RETAKE)"),
        (14, "70% FAIL (RETAKE)"),
        (15, "75% FAIL (RETAKE)"),
    ],
)
@pytest.mark.django_db
def test_result_as_text_fail_retake(result, expect):
    user = UserFactory()
    exam_version = ExamVersionFactory()
    original = UserExamVersionFactory(user=user, exam_version=exam_version)
    original.set_result(10)
    retake = UserExamVersionFactory(user=user, exam_version=exam_version)
    retake.set_result(result)
    assert retake.previous_test() == original
    assert original.previous_test() is None
    assert expect == retake.result_as_text()


@pytest.mark.parametrize(
    "result,expect",
    [
        (16, "80% PASS"),
        (17, "85% PASS"),
        (18, "90% PASS"),
        (19, "95% PASS"),
        (20, "100% PASS"),
    ],
)
@pytest.mark.django_db
def test_result_as_text_pass(result, expect):
    user_exam_version = UserExamVersionFactory(user=UserFactory())
    user_exam_version.set_result(result)
    assert expect == user_exam_version.result_as_text()


@pytest.mark.django_db
def test_result_as_text_pass_retake():
    user = UserFactory()
    exam_version = ExamVersionFactory()
    original = UserExamVersionFactory(user=user, exam_version=exam_version)
    original.set_result(10)
    retake = UserExamVersionFactory(user=user, exam_version=exam_version)
    retake.set_result(16)
    assert retake.previous_test() == original
    assert original.previous_test() is None
    assert "80% PASS (RETAKE)" == retake.result_as_text()


@pytest.mark.django_db
def test_result_as_text_pass_retake_but_passed_already():
    user = UserFactory()
    exam_version = ExamVersionFactory()
    original = UserExamVersionFactory(user=user, exam_version=exam_version)
    original.set_result(80)
    retake = UserExamVersionFactory(user=user, exam_version=exam_version)
    assert retake.previous_test() == original
    assert original.previous_test() is None
    with pytest.raises(ExamError) as e:
        retake.result_as_text()
    assert (
        "Found a retake, but the student passed the "
        f"previous test ('user_exam_version': {retake.pk})"
    ) in str(e.value)


@pytest.mark.parametrize(
    "result,expect",
    [
        (11, "55% RETAKE"),
        (12, "60% RETAKE"),
        (13, "65% RETAKE"),
        (14, "70% RETAKE"),
        (15, "75% RETAKE"),
    ],
)
@pytest.mark.django_db
def test_result_as_text_retake(result, expect):
    user = UserFactory()
    exam_version = ExamVersionFactory()
    original = UserExamVersionFactory(user=user, exam_version=exam_version)
    original.set_result(result)
    retake = UserExamVersionFactory(user=user, exam_version=exam_version)
    assert retake.previous_test() == original
    assert original.previous_test() is None
    assert expect == retake.result_as_text()


@pytest.mark.django_db
def test_percent_override_as_percent_80():
    user = UserFactory()
    obj = UserExamVersionFactory(user=user)
    obj.set_result(1)
    obj.set_percent_override(90, user)
    assert 90 == obj.result_as_percent()


@pytest.mark.django_db
def test_result_override_as_percent_50():
    user = UserFactory()
    obj = UserExamVersionFactory(user=user)
    obj.set_result(1)
    obj.set_percent_override(50, user)
    assert 50 == obj.result_as_percent()


@pytest.mark.django_db
def test_result_as_percent_none():
    user = UserFactory()
    obj = UserExamVersionFactory(user=user, result=None)
    assert obj.result_as_percent() is None


@pytest.mark.django_db
def test_results_in_full():
    """Check the tests results.

    .. note:: We force a pass result using the ``set_percent_override`` method.

    """
    user = UserFactory()
    exam_version = setup_exam(name="Fruit", question_count=2)
    exam_version.exam.init_exam(user)
    MailTemplateFactory(slug=UserExamVersion.MAIL_TEMPLATE_FAIL_INITIAL)
    # get the user_exam_version
    user_exam_version = UserExamVersion.objects.get(user=user)
    set_correct_answers_all(user_exam_version)
    set_incorrect_answers(user_exam_version, [2])
    user_exam_version.mark()
    user_exam_version.set_percent_override(90, user)
    # test
    expect = {
        "legacy": False,
        "total_wrong": 1,
        "questions": [
            {
                "number": 1,
                "question": "question 1",
                "right": True,
                "wrong": False,
                "options": [
                    {
                        "number": 1,
                        "option": "option 1.1",
                        "correct_answer": True,
                        "user_selection": True,
                    },
                    {
                        "number": 2,
                        "option": "option 1.2",
                        "correct_answer": False,
                        "user_selection": False,
                    },
                    {
                        "number": 3,
                        "option": "option 1.3",
                        "correct_answer": False,
                        "user_selection": False,
                    },
                    {
                        "number": 4,
                        "option": "option 1.4",
                        "correct_answer": False,
                        "user_selection": False,
                    },
                ],
            },
            {
                "number": 2,
                "question": "question 2",
                "right": False,
                "wrong": True,
                "options": [
                    {
                        "number": 1,
                        "option": "option 2.1",
                        "correct_answer": True,
                        "user_selection": False,
                    },
                    {
                        "number": 2,
                        "option": "option 2.2",
                        "correct_answer": False,
                        "user_selection": False,
                    },
                    {
                        "number": 3,
                        "option": "option 2.3",
                        "correct_answer": False,
                        "user_selection": False,
                    },
                    {
                        "number": 4,
                        "option": "option 2.4",
                        "correct_answer": False,
                        "user_selection": True,
                    },
                ],
            },
        ],
    }
    result = user_exam_version.results_in_full(user)
    assert expect == result


@pytest.mark.django_db
def test_results_in_full_fail():
    user = UserFactory()
    MailTemplateFactory(slug=UserExamVersion.MAIL_TEMPLATE_FAIL_INITIAL)
    exam_version = setup_exam(name="Fruit", question_count=2)
    exam_version.exam.init_exam(user)
    # get the user_exam_version
    user_exam_version = UserExamVersion.objects.get(user=user)
    set_correct_answers_all(user_exam_version)
    set_incorrect_answers(user_exam_version, [2])
    user_exam_version.mark()
    # test
    result = user_exam_version.results_in_full(user)
    expect = {
        # "show_correct_option": False,
        "legacy": False,
        "total_wrong": 1,
        "questions": [
            {
                "number": 1,
                "question": "question 1",
                "right": True,
                "wrong": False,
                "options": [
                    {
                        "number": 1,
                        "option": "option 1.1",
                        "correct_answer": None,
                        "user_selection": True,
                    },
                    {
                        "number": 2,
                        "option": "option 1.2",
                        "correct_answer": None,
                        "user_selection": False,
                    },
                    {
                        "number": 3,
                        "option": "option 1.3",
                        "correct_answer": None,
                        "user_selection": False,
                    },
                    {
                        "number": 4,
                        "option": "option 1.4",
                        "correct_answer": None,
                        "user_selection": False,
                    },
                ],
            },
            {
                "number": 2,
                "question": "question 2",
                "right": False,
                "wrong": True,
                "options": [
                    {
                        "number": 1,
                        "option": "option 2.1",
                        "correct_answer": None,
                        "user_selection": False,
                    },
                    {
                        "number": 2,
                        "option": "option 2.2",
                        "correct_answer": None,
                        "user_selection": False,
                    },
                    {
                        "number": 3,
                        "option": "option 2.3",
                        "correct_answer": None,
                        "user_selection": False,
                    },
                    {
                        "number": 4,
                        "option": "option 2.4",
                        "correct_answer": None,
                        "user_selection": True,
                    },
                ],
            },
        ],
    }
    assert expect == result


@pytest.mark.django_db
def test_results_in_full_fail_staff():
    user = UserFactory()
    MailTemplateFactory(slug=UserExamVersion.MAIL_TEMPLATE_FAIL_INITIAL)
    exam_version = setup_exam(name="Fruit", question_count=2)
    exam_version.exam.init_exam(user)
    # get the user_exam_version
    user_exam_version = UserExamVersion.objects.get(user=user)
    set_correct_answers_all(user_exam_version)
    set_incorrect_answers(user_exam_version, [1, 2])
    user_exam_version.mark()
    # test
    result = user_exam_version.results_in_full(UserFactory(is_staff=True))
    expect = {
        "legacy": False,
        "total_wrong": 2,
        "questions": [
            {
                "number": 1,
                "question": "question 1",
                "right": False,
                "wrong": True,
                "options": [
                    {
                        "number": 1,
                        "option": "option 1.1",
                        "correct_answer": True,
                        "user_selection": False,
                    },
                    {
                        "number": 2,
                        "option": "option 1.2",
                        "correct_answer": False,
                        "user_selection": False,
                    },
                    {
                        "number": 3,
                        "option": "option 1.3",
                        "correct_answer": False,
                        "user_selection": False,
                    },
                    {
                        "number": 4,
                        "option": "option 1.4",
                        "correct_answer": False,
                        "user_selection": True,
                    },
                ],
            },
            {
                "number": 2,
                "question": "question 2",
                "right": False,
                "wrong": True,
                "options": [
                    {
                        "number": 1,
                        "option": "option 2.1",
                        "correct_answer": True,
                        "user_selection": False,
                    },
                    {
                        "number": 2,
                        "option": "option 2.2",
                        "correct_answer": False,
                        "user_selection": False,
                    },
                    {
                        "number": 3,
                        "option": "option 2.3",
                        "correct_answer": False,
                        "user_selection": False,
                    },
                    {
                        "number": 4,
                        "option": "option 2.4",
                        "correct_answer": False,
                        "user_selection": True,
                    },
                ],
            },
        ],
    }
    assert expect == result


@pytest.mark.django_db
def test_results_in_full_not_marked():
    user = UserFactory()
    exam_version = setup_exam(name="Fruit", question_count=3)
    exam_version.exam.init_exam(user)
    # get the user_exam_version
    user_exam_version = UserExamVersion.objects.get(user=user)
    set_correct_answers(user_exam_version, [1])
    set_incorrect_answers(user_exam_version, [3])
    # test
    result = user_exam_version.results_in_full(user)
    expect = {
        "legacy": False,
        "total_wrong": 0,
        "questions": [
            {
                "number": 1,
                "question": "question 1",
                "right": None,
                "wrong": None,
                "options": [
                    {
                        "number": 1,
                        "option": "option 1.1",
                        "correct_answer": None,
                        "user_selection": True,
                    },
                    {
                        "number": 2,
                        "option": "option 1.2",
                        "correct_answer": None,
                        "user_selection": False,
                    },
                    {
                        "number": 3,
                        "option": "option 1.3",
                        "correct_answer": None,
                        "user_selection": False,
                    },
                    {
                        "number": 4,
                        "option": "option 1.4",
                        "correct_answer": None,
                        "user_selection": False,
                    },
                ],
            },
            {
                "number": 2,
                "question": "question 2",
                "right": None,
                "wrong": None,
                "options": [
                    {
                        "number": 1,
                        "option": "option 2.1",
                        "correct_answer": None,
                        "user_selection": None,
                    },
                    {
                        "number": 2,
                        "option": "option 2.2",
                        "correct_answer": None,
                        "user_selection": None,
                    },
                    {
                        "number": 3,
                        "option": "option 2.3",
                        "correct_answer": None,
                        "user_selection": None,
                    },
                    {
                        "number": 4,
                        "option": "option 2.4",
                        "correct_answer": None,
                        "user_selection": None,
                    },
                ],
            },
            {
                "number": 3,
                "question": "question 3",
                "right": None,
                "wrong": None,
                "options": [
                    {
                        "number": 1,
                        "option": "option 3.1",
                        "correct_answer": None,
                        "user_selection": False,
                    },
                    {
                        "number": 2,
                        "option": "option 3.2",
                        "correct_answer": None,
                        "user_selection": False,
                    },
                    {
                        "number": 3,
                        "option": "option 3.3",
                        "correct_answer": None,
                        "user_selection": False,
                    },
                    {
                        "number": 4,
                        "option": "option 3.4",
                        "correct_answer": None,
                        "user_selection": True,
                    },
                ],
            },
        ],
    }
    assert expect == result


@pytest.mark.django_db
def test_results_in_full_retake_after_fail():
    user = UserFactory()
    MailTemplateFactory(slug=UserExamVersion.MAIL_TEMPLATE_FAIL_INITIAL)
    exam_version = setup_exam(name="Fruit", question_count=2)
    exam_version.exam.init_exam(user)
    # get the user_exam_version
    user_exam_version = UserExamVersion.objects.get(user=user)
    set_correct_answers_all(user_exam_version)
    set_incorrect_answers(user_exam_version, [2])
    user_exam_version.mark()
    # retake
    user_exam_version = exam_version.exam.user_exam_version(user)
    # test
    result = user_exam_version.results_in_full(user)
    expect = {
        "legacy": False,
        "total_wrong": 0,
        "questions": [
            {
                "number": 1,
                "question": "question 1",
                "right": True,
                "wrong": False,
                "options": [
                    {
                        "number": 1,
                        "option": "option 1.1",
                        "correct_answer": True,
                        "user_selection": True,
                    },
                    {
                        "number": 2,
                        "option": "option 1.2",
                        "correct_answer": False,
                        "user_selection": False,
                    },
                    {
                        "number": 3,
                        "option": "option 1.3",
                        "correct_answer": False,
                        "user_selection": False,
                    },
                    {
                        "number": 4,
                        "option": "option 1.4",
                        "correct_answer": False,
                        "user_selection": False,
                    },
                ],
            },
            {
                "number": 2,
                "question": "question 2",
                "right": None,
                "wrong": None,
                "options": [
                    {
                        "number": 1,
                        "option": "option 2.1",
                        "correct_answer": None,
                        "user_selection": None,
                    },
                    {
                        "number": 2,
                        "option": "option 2.2",
                        "correct_answer": None,
                        "user_selection": None,
                    },
                    {
                        "number": 3,
                        "option": "option 2.3",
                        "correct_answer": None,
                        "user_selection": None,
                    },
                    {
                        "number": 4,
                        "option": "option 2.4",
                        "correct_answer": None,
                        "user_selection": None,
                    },
                ],
            },
        ],
    }
    assert expect == result


@pytest.mark.django_db
def test_set_result_override():
    user = UserFactory()
    obj = UserExamVersionFactory(user=user)
    obj.set_result(10)
    obj.set_percent_override(75, user)
    obj.refresh_from_db()
    assert 10 == obj.result
    assert 75 == obj.percent_override
    assert user == obj.percent_override_user
    assert timezone.now().date() == obj.percent_override_date.date()


@pytest.mark.django_db
def test_set_result_override_not_marked():
    user = UserFactory()
    obj = UserExamVersionFactory(user=user)
    with pytest.raises(ExamError) as e:
        obj.set_percent_override(10, user)
    assert ("cannot override an exam result until it has been marked") in str(
        e.value
    )
