# -*- encoding: utf-8 -*-
import pytest

from exam.models import UnitExam
from exam.tests.factories import ExamFactory, UnitExamFactory, UnitFactory
from login.tests.factories import UserFactory


@pytest.mark.django_db
def test_create_unit_exam():
    exam = ExamFactory()
    unit = UnitFactory()
    x = UnitExam.objects.create_unit_exam(unit, exam)
    assert bool(x.pk) is True
    assert unit == x.unit
    assert exam == x.exam


@pytest.mark.django_db
def test_current():
    unit_exam_1 = UnitExamFactory()
    unit_exam_2 = UnitExamFactory()
    unit_exam_2.set_deleted(UserFactory())
    unit_exam_3 = UnitExamFactory()
    assert set([unit_exam_1.pk, unit_exam_3.pk]) == set(
        [x.pk for x in UnitExam.objects.current()]
    )


@pytest.mark.django_db
def test_current_for_unit():
    unit = UnitFactory()
    unit_exam_1 = UnitExamFactory(unit=unit)
    unit_exam_2 = UnitExamFactory()
    unit_exam_2.set_deleted(UserFactory())
    unit_exam_3 = UnitExamFactory()
    assert [unit_exam_1.pk] == [x.pk for x in UnitExam.objects.current(unit)]


@pytest.mark.django_db
def test_init_unit_exam():
    exam = ExamFactory()
    unit = UnitFactory()
    obj1 = UnitExam.objects.init_unit_exam(unit, exam)
    obj2 = UnitExam.objects.init_unit_exam(unit, exam)
    assert bool(obj1.pk) is True
    assert obj1.pk == obj2.pk
