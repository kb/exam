# -*- encoding: utf-8 -*-
import pytest

from exam.models import Unit
from exam.tests.factories import (
    AssessmentFactory,
    CourseFactory,
    CourseUnitFactory,
    ExamFactory,
    UnitExamFactory,
    UnitFactory,
)
from login.tests.factories import UserFactory


@pytest.mark.django_db
def test_assessments():
    unit = UnitFactory()
    AssessmentFactory(unit=unit, name="a", order=3)
    AssessmentFactory(unit=unit, name="b", order=2).set_deleted(UserFactory())
    AssessmentFactory(unit=unit, name="c", order=1)
    assert ["c", "a"] == [x.name for x in unit.assessments()]


@pytest.mark.django_db
@pytest.mark.parametrize(
    "assignment_or_assessment,assessment_count,expect",
    [
        ("assessment", 1, "assessment"),
        ("assessment", 2, "assessments"),
        ("Assessment", 1, "assessment"),
        ("AssessMENT", 1, "assessment"),
        ("assignment", 1, "assignment"),
        ("assignment", 2, "assignments"),
        ("Assignment", 1, "assignment"),
        ("AssiGNMENT", 1, "assignment"),
    ],
)
def test_get_assignment_or_assessment(
    assignment_or_assessment, assessment_count, expect
):
    unit = UnitFactory(assignment_or_assessment=assignment_or_assessment)
    assert expect == unit.get_assignment_or_assessment(assessment_count)


@pytest.mark.django_db
def test_create_unit():
    course = CourseFactory()
    x = Unit.objects.create_unit("Fruit")
    assert bool(x.pk) is True
    assert "Fruit" == x.name


@pytest.mark.django_db
def test_current():
    course = CourseFactory()
    unit_1 = UnitFactory()
    unit_2 = UnitFactory()
    unit_2.set_deleted(UserFactory())
    unit_3 = UnitFactory()
    unit_4 = UnitFactory()
    assert set([unit_1.pk, unit_3.pk, unit_4.pk]) == set(
        [x.pk for x in Unit.objects.current()]
    )


@pytest.mark.django_db
def test_current_for_course():
    course = CourseFactory()
    unit_1 = UnitFactory()
    CourseUnitFactory(course=course, unit=unit_1, order=1)
    unit_2 = UnitFactory()
    CourseUnitFactory(course=course, unit=unit_2, order=2)
    unit_2.set_deleted(UserFactory())
    unit_3 = UnitFactory()
    CourseUnitFactory(course=CourseFactory(), unit=unit_3, order=4)
    unit_4 = UnitFactory()
    CourseUnitFactory(course=course, unit=unit_4, order=4)
    assert set([unit_1.pk, unit_4.pk]) == set(
        [x.pk for x in Unit.objects.current(course)]
    )


@pytest.mark.django_db
def test_current_with_assessment(django_assert_num_queries):
    course = CourseFactory()
    unit_1 = UnitFactory()
    CourseUnitFactory(course=course, unit=unit_1, order=1)
    AssessmentFactory(unit=unit_1, name="one", order=1)
    unit_2 = UnitFactory()
    CourseUnitFactory(course=course, unit=unit_2, order=2)
    unit_2.set_deleted(UserFactory())
    unit_3 = UnitFactory()
    CourseUnitFactory(course=course, unit=unit_3, order=3)
    assessment = AssessmentFactory(unit=unit_3, name="three", order=3)
    assessment.set_deleted(UserFactory())
    unit_4 = UnitFactory()
    CourseUnitFactory(course=course, unit=unit_4, order=4)
    unit_5 = UnitFactory()
    CourseUnitFactory(course=course, unit=unit_5, order=5)
    AssessmentFactory(unit=unit_5, name="five", order=5)
    with django_assert_num_queries(1):
        result = [x.pk for x in Unit.objects.current_with_assessment(course)]
    assert set([unit_1.pk, unit_5.pk]) == set(result)


@pytest.mark.django_db
def test_is_assignment():
    unit = UnitFactory(assignment_or_assessment=Unit.ASSIGNMENT)
    assert unit.is_assignment() is True


@pytest.mark.django_db
def test_is_assignment_is_assessment():
    unit = UnitFactory(assignment_or_assessment=Unit.ASSESSMENT)
    assert unit.is_assignment() is False


@pytest.mark.django_db
def test_current_with_assessment_count(django_assert_num_queries):
    unit_1 = UnitFactory(name="unit-1")
    AssessmentFactory(unit=unit_1)
    assessment = AssessmentFactory(unit=unit_1)
    assessment.set_deleted(UserFactory())
    unit_2 = UnitFactory(name="unit-2")
    unit_2.set_deleted(UserFactory())
    unit_3 = UnitFactory(name="unit-3")
    AssessmentFactory(unit=unit_3)
    AssessmentFactory(unit=unit_3)
    unit_4 = UnitFactory(name="unit-4")
    with django_assert_num_queries(1):
        result = [
            (x.name, x.count_assessment)
            for x in Unit.objects.current_with_assessment_count().order_by(
                "name"
            )
        ]
    # from rich.pretty import pprint
    # pprint(result, expand_all=True)
    assert [("unit-1", 1), ("unit-3", 2), ("unit-4", 0)] == result


@pytest.mark.django_db
def test_prev_unit_order_1():
    course = CourseFactory()
    unit_1 = UnitFactory()
    CourseUnitFactory(course=course, unit=unit_1, order=1)
    assert course.prev_unit(unit_1) == None


@pytest.mark.django_db
def test_str():
    course = CourseFactory(name="Tree")
    unit = Unit.objects.create_unit("Fruit")
    assert "Fruit" == str(unit)


@pytest.mark.django_db
def test_unit_exams():
    unit = UnitFactory()
    UnitExamFactory(unit=unit, exam=ExamFactory(name="a"))
    UnitExamFactory(unit=unit, exam=ExamFactory(name="b")).set_deleted(
        UserFactory()
    )
    UnitExamFactory(unit=unit, exam=ExamFactory(name="c"))
    assert ["a", "c"] == [x.exam.name for x in unit.unit_exams()]
