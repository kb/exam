# -*- encoding: utf-8 -*-
import pytest

from datetime import date
from dateutil.relativedelta import relativedelta
from django.urls import reverse
from django.utils import timezone

from exam.models import (
    Exam,
    ExamError,
    ExamVersion,
    Option,
    Question,
    UserExamVersion,
)
from exam.tests.factories import (
    CourseFactory,
    CourseUnitFactory,
    ExamFactory,
    ExamVersionFactory,
    UnitExamFactory,
    UnitFactory,
    UserCourseFactory,
    UserExamVersionFactory,
)
from exam.tests.scenario import (
    create_answer_for_each_question,
    create_option_content,
    create_question_content,
    set_correct_answers_all,
    set_incorrect_answers,
    setup_exam,
)
from login.tests.factories import UserFactory
from mail.tests.factories import MailTemplateFactory


# @pytest.mark.django_db
# def test_context():
#     """Context for a user who is taking the exam for the first time."""
#     user = UserFactory()
#     exam = setup_exam()
#     # user sits the exam
#     exam.sit_exam(user)
#     set_correct_answers(user, exam, [1, 2])
#     set_incorrect_answers(user, exam, [3, 4, 5])
#     expect = dict(
#         answered_count=5,
#         can_mark=False,
#         can_retake=False,
#         is_fail=None,
#         is_retake=False,
#         question_count=20,
#         result=None,
#         sit_exam_url=reverse('exam.student.redirect', args=[exam.pk]),
#     )
#     assert expect == exam.context(user)
#
#
# @pytest.mark.django_db
# def test_context_can_retake():
#     """Context for a user who is taking the exam for the first time."""
#     user = UserFactory()
#     exam = setup_exam()
#     # user sits the exam
#     exam.sit_exam(user)
#     set_correct_answers_all(user, exam)
#     set_incorrect_answers(user, exam, [3, 4, 5, 7, 8])
#     exam.mark(user)
#     expect = dict(
#         answered_count=20,
#         can_mark=False,
#         can_retake=True,
#         is_fail=True,
#         is_retake=False,
#         question_count=20,
#         result=15,
#         sit_exam_url=reverse('exam.student.redirect', args=[exam.pk]),
#     )
#     assert expect == exam.context(user)
#
#
# @pytest.mark.django_db
# def test_context_pass():
#     """Context for a user who passed the exam first time."""
#     user = UserFactory()
#     exam = setup_exam()
#     # user sits the exam
#     exam.sit_exam(user)
#     set_correct_answers_all(user, exam)
#     set_incorrect_answers(user, exam, [3, 4])
#     exam.mark(user)
#     expect = dict(
#         answered_count=20,
#         can_mark=False,
#         can_retake=False,
#         is_fail=False,
#         is_retake=False,
#         question_count=20,
#         result=18,
#         sit_exam_url=reverse('exam.student.redirect', args=[exam.pk]),
#     )
#     assert expect == exam.context(user)
#
#
# @pytest.mark.django_db
# def test_context_pass_on_retake():
#     """Context for a user who is re-taking the exam."""
#     user = UserFactory()
#     exam = setup_exam()
#     # user sits the exam
#     exam.sit_exam(user)
#     # answer all the questions
#     set_correct_answers_all(user, exam)
#     set_incorrect_answers(user, exam, [3, 4, 5, 10, 11, 20])
#     exam.mark(user)
#     # user retakes the exam
#     exam.sit_exam(user)
#     set_correct_answers(user, exam, [3, 10, 11, 20])
#     set_incorrect_answers(user, exam, [4, 5])
#     exam.mark(user)
#     expect = dict(
#         answered_count=6,
#         can_mark=False,
#         can_retake=False,
#         is_fail=False,
#         is_retake=True,
#         question_count=6,
#         result=18,
#         sit_exam_url=reverse('exam.student.redirect', args=[exam.pk]),
#     )
#     assert expect == exam.context(user)
#
#
# @pytest.mark.django_db
# def test_context_retake():
#     """Context for a user who is re-taking the exam."""
#     user = UserFactory()
#     exam = setup_exam()
#     # user sits the exam
#     exam.sit_exam(user)
#     # answer all the questions
#     set_correct_answers_all(user, exam)
#     set_incorrect_answers(user, exam, [3, 4, 5, 10, 11, 20])
#     exam.mark(user)
#     # user retakes the exam
#     exam.sit_exam(user)
#     set_correct_answers(user, exam, [2])
#     set_incorrect_answers(user, exam, [4, 5])
#     expect = dict(
#         answered_count=3,
#         can_mark=False,
#         can_retake=False,
#         is_fail=True,
#         is_retake=True,
#         question_count=6,
#         result=14,
#         sit_exam_url=reverse('exam.student.redirect', args=[exam.pk]),
#     )
#     assert expect == exam.context(user)


@pytest.mark.django_db
def test_check_perm():
    """Check permissions (using LMS models)."""
    course = CourseFactory()
    unit = UnitFactory()
    CourseUnitFactory(course=course, unit=unit)
    exam = ExamFactory()
    UnitExamFactory(unit=unit, exam=exam)
    user = UserFactory()
    UserCourseFactory(user=user, course=course)
    assert Exam.objects.check_perm(user, exam) is True


@pytest.mark.django_db
def test_exams():
    """Exclude deleted and unpublished exams."""
    ExamVersionFactory(exam=ExamFactory(name="a"))
    b = ExamVersionFactory(exam=ExamFactory(name="b"))
    b.exam.set_deleted(UserFactory())
    ExamVersionFactory(exam=ExamFactory(name="c"))
    ExamVersionFactory(exam=ExamFactory(name="d"), date_published=None)
    ExamVersionFactory(exam=ExamFactory(name="e"))
    assert ["a", "c", "e"] == [
        obj.name for obj in Exam.objects.exams().order_by("name")
    ]


@pytest.mark.django_db
def test_init_exam():
    user = UserFactory()
    exam = ExamFactory()
    exam_version = exam.create_questions()
    create_question_content(exam_version)
    create_option_content(exam_version)
    create_answer_for_each_question(exam_version)
    exam_version.publish()
    assert 0 == UserExamVersion.objects.count()
    exam.init_exam(user)
    assert 1 == UserExamVersion.objects.count()
    exam.init_exam(user)
    assert 1 == UserExamVersion.objects.count()
    qs = UserExamVersion.objects.user_exam_versions(user, exam)
    assert 1 == qs.count()
    assert user == qs.first().user
    assert exam == qs.first().exam_version.exam


@pytest.mark.django_db
def test_factory():
    ExamFactory()


@pytest.mark.django_db
def test_get_absolute_url():
    obj = ExamFactory()
    expect = reverse("exam.user.redirect", args=[obj.pk])
    assert expect == obj.get_absolute_url()


@pytest.mark.django_db
def test_ordering():
    ExamFactory(name="a")
    ExamFactory(name="c")
    ExamFactory(name="b")
    assert ["a", "b", "c"] == [obj.name for obj in Exam.objects.all()]


@pytest.mark.django_db
def test_create_questions():
    exam = ExamFactory(name="Exam1")
    exam.create_questions()
    exam_version = ExamVersion.objects.get(exam=exam)
    questions = Question.objects.filter(exam_version=exam_version).order_by(
        "number"
    )
    assert [
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
    ] == [obj.number for obj in questions]


@pytest.mark.django_db
def test_current_version():
    """Get the current published version of the exam."""
    exam = ExamFactory()
    v1 = exam.create_questions()
    create_question_content(v1)
    create_option_content(v1)
    create_answer_for_each_question(v1)
    # test no current version
    obj = exam.current_version()
    assert obj is None
    # test current version
    v1.publish()
    obj = exam.current_version()
    assert v1 == obj
    assert date.today() == obj.date_published.date()
    # test delete exam version
    v2 = exam.edit_version()
    v2.publish()
    obj = exam.current_version()
    assert v2 == obj
    obj.set_deleted(UserFactory())
    obj = exam.current_version()
    assert v1 == obj
    # test another version
    v3 = exam.edit_version()
    v3.publish()
    obj = exam.current_version()
    assert v3 == obj
    assert date.today() == obj.date_published.date()


@pytest.mark.django_db
def test_current_version_two_published():
    v1 = setup_exam()
    exam = v1.exam
    v2 = exam.edit_version()
    v2.publish()
    assert v2 == exam.current_version()


@pytest.mark.django_db
def test_edit_questions():
    exam = ExamFactory(name="Exam1")
    exam_version = exam.create_questions()
    assert [
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
    ] == [obj.number for obj in exam_version.questions()]


@pytest.mark.django_db
def test_edit_questions_no_questions():
    exam = ExamFactory()
    exam_version = ExamVersionFactory(exam=exam, date_published=None)
    with pytest.raises(ExamError) as e:
        exam_version.questions()
    assert "has no questions" in str(e.value)


@pytest.mark.django_db
def test_edit_questions_no_version():
    exam = ExamFactory()
    with pytest.raises(ExamError) as e:
        exam.edit_version()
    assert "has no editing version" in str(e.value)


@pytest.mark.django_db
def test_can_publish_no_answer():
    """Test has a question with no answer."""
    exam = ExamFactory(name="Exam1")
    version = exam.create_questions()
    create_answer_for_each_question(version)
    create_question_content(version)
    create_option_content(version)
    # clear the answer for one of the questions
    question = Question.objects.get(exam_version=version, number=3)
    question.answer = None
    question.save()
    # test
    result, message = version.can_publish()
    assert result is False
    assert "Question 3 does not have an answer" in message


@pytest.mark.django_db
def test_can_publish_not_question():
    exam = ExamFactory(name="Exam1")
    version = exam.create_questions()
    result, message = version.can_publish()
    assert result is False
    assert "Question 1 is blank" in message


@pytest.mark.django_db
def test_can_publish_not_option():
    exam = ExamFactory(name="Exam1")
    version = exam.create_questions()
    question_row = Question.objects.get(number=1)
    question_row.question = "Question 1"
    question_row.save()
    result, message = version.can_publish()
    assert result is False
    # 11/04/2022, This test has an intermittent failure.
    # This would suggest a factory is causing an issue?
    assert "Option" in message
    assert "is blank" in message


@pytest.mark.django_db
def test_exam_versions():
    """List of exam versions, showing the number of tests in progress.

    In the test below:

    - v4 can be edited, because it is the currently active version
    - v3 cannot be edited, because no students are working on the exam
    - v2 can be edited because two students are still working on the exam

    """
    u1 = UserFactory()
    u2 = UserFactory()
    u3 = UserFactory()
    u4 = UserFactory()
    u5 = UserFactory()
    u6 = UserFactory()
    exam = ExamFactory()
    v1 = exam.create_questions()
    create_question_content(v1)
    create_option_content(v1)
    create_answer_for_each_question(v1)
    v1.publish()
    exam.init_exam(u3)
    v2 = exam.edit_version()
    v2.publish()
    exam.init_exam(u1)
    exam.init_exam(u2)
    exam.init_exam(u4)
    v3 = exam.edit_version()
    v3.publish()
    exam.init_exam(u5)
    exam.init_exam(u6)
    v4 = exam.edit_version()
    v4.publish()
    # set result on one of the exams (so 'active' is zero i.e. no edit)
    user_exam_version = UserExamVersion.objects.get(user=u6, exam_version=v3)
    user_exam_version.legacy_result = 1
    user_exam_version.save()
    exam.edit_version()
    # set result on one of the exams (so 'active' is zero i.e. no edit)
    user_exam_version = UserExamVersion.objects.get(user=u5, exam_version=v3)
    user_exam_version.result = 1
    user_exam_version.save()
    exam.edit_version()
    # set result on one of the exams (so 'active' is less than the 'total')
    user_exam_version = UserExamVersion.objects.get(user=u2, exam_version=v2)
    user_exam_version.result = 1
    user_exam_version.save()
    # test
    result = exam.exam_versions()
    # check
    assert 4 == len(result)
    assert [
        "active",
        "created",
        "date_published",
        "pk",
        "total",
    ] == sorted(result[0].keys())
    check = []
    for obj in result:
        check.append(
            {
                "pk": obj["pk"],
                "active": obj["active"],
                "total": obj["total"],
            }
        )
    assert [
        {"pk": v4.pk, "active": 0, "total": 0},
        {"pk": v3.pk, "active": 0, "total": 2},
        {"pk": v2.pk, "active": 2, "total": 3},
        {"pk": v1.pk, "active": 1, "total": 1},
    ] == check


@pytest.mark.django_db
def test_exam_versions_include_published():
    user = UserFactory()
    # dates
    today = timezone.now()
    one_day = today + relativedelta(days=-1)
    two_day = today + relativedelta(days=-2)
    # exam
    exam = ExamFactory()
    exam_version_1 = ExamVersionFactory(exam=exam, date_published=two_day)
    exam_version_2 = ExamVersionFactory(exam=exam, date_published=one_day)
    exam_version_3 = ExamVersionFactory(exam=exam, date_published=today)
    exam_version_4 = ExamVersionFactory(exam=exam, date_published=today)
    # this one is excluded, because it hasn't been published
    exam_version_5 = ExamVersionFactory(exam=exam, date_published=None)
    UserExamVersionFactory(
        user=user,
        exam_version=exam_version_1,
        legacy_result=False,
        result=None,
    )
    UserExamVersionFactory(
        user=user,
        exam_version=exam_version_3,
        legacy_result=False,
        result=None,
    )
    UserExamVersionFactory(
        user=user, exam_version=exam_version_5, legacy_result=True, result=None
    )
    assert [
        exam_version_4.pk,
        exam_version_3.pk,
        exam_version_2.pk,
        exam_version_1.pk,
    ] == [x["pk"] for x in exam.exam_versions()]


@pytest.mark.django_db
def test_publish():
    exam = ExamFactory(name="Exam1")
    version = exam.create_questions()
    create_question_content(version)
    create_option_content(version)
    create_answer_for_each_question(version)
    # get question number 3
    exam_version = ExamVersion.objects.get(exam=exam)
    question = Question.objects.get(exam_version=exam_version, number=3)
    # update option 2 for question 3
    option = Option.objects.get(question=question, number=2)
    option.option = "Hereford"
    option.save()
    # set the answer for question 3
    question.answer = option
    question.save()
    assert exam_version.date_published is None
    # test
    exam_version.publish()
    # check
    qs = ExamVersion.objects.filter(exam=exam).order_by("pk")
    assert 2 == qs.count()
    exam_version = qs[0]
    assert exam_version.date_published is not None
    exam_version = qs[1]
    assert exam_version.date_published is None
    questions = Question.objects.filter(exam_version=exam_version)
    assert [
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
    ] == [obj.number for obj in questions.order_by("number")]
    # get question number 3 for the new exam_version
    question = Question.objects.get(exam_version=exam_version, number=3)
    assert question.answer is not None
    assert "Hereford" == question.answer.option


@pytest.mark.django_db
def test_publish_again():
    exam = ExamFactory(name="Exam1")
    version = exam.create_questions()
    create_question_content(version)
    create_option_content(version)
    create_answer_for_each_question(version)
    version.publish()
    exam.edit_version().publish()
    qs = ExamVersion.objects.filter(exam=exam).order_by("pk")
    assert 3 == qs.count()
    exam_version = qs[0]
    assert exam_version.date_published is not None
    exam_version = qs[1]
    assert exam_version.date_published is not None
    exam_version = qs[2]
    assert exam_version.date_published is None


# @pytest.mark.django_db
# def test_question_state_wrong():
#     """Exam re-take, will only display wrong questions from original."""
#     user = UserFactory()
#     exam = setup_exam()
#
#
#     user_exam_version = exam.sit_exam(user)
#     set_correct_answers_all(user, exam)
#     set_incorrect_answers(user, exam, [3, 4, 5, 7, 8, 9, 12])
#     exam.mark(user)
#
#
#     exam_version = exam.current_version()
#     # user sits the exam
#     user_exam_version = UserExamVersion.objects.sit_exam(user, exam)
#     # set the answers for question 3
#     question_3 = Question.objects.get(exam_version=exam_version, number=3)
#     answer_3 = Option.objects.get(question=question_3, number=2)
#     user_exam_version.set_answer(user, question_3, answer_3)
#     # set the answers for question 12
#     question_12 = Question.objects.get(exam_version=exam_version, number=12)
#     answer_12 = Option.objects.get(question=question_12, number=4)
#     user_exam_version.set_answer(user, question_12, answer_12)
#     # test
#     result = exam.question_state(user)
#     check = []
#     for question, answered in result:
#         check.append((question.number, answered))
#     assert [
#         (3, True),
#         (4, False),
#         (5, False),
#         (7, False),
#         (8, False),
#         (9, False),
#         (12, True),
#     ] == check


@pytest.mark.django_db
def test_str():
    exam = ExamFactory(name="Exam1")
    assert "Exam1" == str(exam)


@pytest.mark.django_db
def test_user_exam_version():
    user = UserFactory()
    exam_version = setup_exam()
    exam_version.exam.init_exam(user)
    user_exam_version = UserExamVersion.objects.get(
        user=user, exam_version=exam_version
    )
    assert user_exam_version == exam_version.exam.user_exam_version(user)


@pytest.mark.django_db
def test_user_exam_version_retake():
    MailTemplateFactory(slug=UserExamVersion.MAIL_TEMPLATE_FAIL_INITIAL)
    user = UserFactory()
    exam_version = setup_exam()
    exam_version.exam.init_exam(user)
    user_exam_version = UserExamVersion.objects.get(
        user=user, exam_version=exam_version
    )
    set_correct_answers_all(user_exam_version)
    set_incorrect_answers(user_exam_version, [3, 4, 5, 7, 8, 9])
    user_exam_version.mark()
    check = UserExamVersion.objects.get(
        user=user, exam_version=exam_version, result__isnull=True
    )
    assert check == exam_version.exam.user_exam_version(user)
