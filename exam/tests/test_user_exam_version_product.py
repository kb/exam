# -*- encoding: utf-8 -*-
import pytest

from exam.models import ExamError, UserExamVersionProduct
from exam.tests.factories import (
    ExamFactory,
    ExamVersionFactory,
    ProductExamFactory,
    UserExamVersionFactory,
)
from login.tests.factories import UserFactory
from stock.tests.factories import ProductFactory


@pytest.mark.django_db
def test_link_user_exam_version_product():
    user = UserFactory()
    user_exam_version = UserExamVersionFactory(user=user)
    product_exam = ProductExamFactory(exam=user_exam_version.exam_version.exam)
    assert 0 == UserExamVersionProduct.objects.count()
    assert 1 == UserExamVersionProduct.objects.link_user_exam_version_product(
        user_exam_version, [product_exam.product]
    )
    assert 1 == UserExamVersionProduct.objects.count()


@pytest.mark.django_db
def test_link_user_exam_version_product_is_staff():
    """If a member of staff took the exam, then don't raise an exception."""
    user = UserFactory(is_staff=True)
    user_exam_version = UserExamVersionFactory(user=user)
    assert 0 == UserExamVersionProduct.objects.count()
    assert 0 == UserExamVersionProduct.objects.link_user_exam_version_product(
        user_exam_version, []
    )
    assert 0 == UserExamVersionProduct.objects.count()


@pytest.mark.django_db
def test_link_user_exam_version_product_legacy():
    """Should not link unless we are running a legacy data migration."""
    user = UserFactory()
    user_exam_version = UserExamVersionFactory(user=user)
    product_exam = ProductExamFactory(
        exam=user_exam_version.exam_version.exam, legacy=True
    )
    with pytest.raises(ExamError) as e:
        UserExamVersionProduct.objects.link_user_exam_version_product(
            user_exam_version, [product_exam.product]
        )
    assert "Cannot find any matching products" in str(e.value)


@pytest.mark.django_db
def test_link_user_exam_version_product_legacy_migration():
    """Allow link because we are running a legacy data migration."""
    user = UserFactory()
    user_exam_version = UserExamVersionFactory(user=user)
    product_exam = ProductExamFactory(
        exam=user_exam_version.exam_version.exam, legacy=True
    )
    assert 0 == UserExamVersionProduct.objects.count()
    assert 1 == UserExamVersionProduct.objects.link_user_exam_version_product(
        user_exam_version, [product_exam.product], legacy=True
    )
    assert 1 == UserExamVersionProduct.objects.count()


@pytest.mark.django_db
def test_product_results():
    e1 = ExamFactory(name="Apple")
    v1 = ExamVersionFactory(exam=e1)
    e2 = ExamFactory(name="Orange")
    v2 = ExamVersionFactory(exam=e2)
    e3 = ExamFactory(name="Grape")
    e4 = ExamFactory(name="Four")
    user = UserFactory()
    product = ProductFactory()
    # previous version of the exam
    UserExamVersionFactory(user=user, exam_version=v1)
    user_exam_version_1 = UserExamVersionFactory(user=user, exam_version=v1)
    user_exam_version_1.set_result(10)
    user_exam_version_2 = UserExamVersionFactory(user=user, exam_version=v2)
    user_exam_version_2.set_result(18)
    ProductExamFactory(product=product, order=4, exam=e4, legacy=True)
    ProductExamFactory(product=product, order=3, exam=e1)
    ProductExamFactory(product=product, order=2, exam=e3)
    ProductExamFactory(product=product, order=1, exam=e2)
    assert 1 == UserExamVersionProduct.objects.link_user_exam_version_product(
        user_exam_version_1, [product]
    )
    assert 1 == UserExamVersionProduct.objects.link_user_exam_version_product(
        user_exam_version_2, [product]
    )
    data = UserExamVersionProduct.objects.product_results(user, product)
    assert [
        {
            "taken": True,
            "pk": user_exam_version_2.pk,
            "exam_pk": e2.pk,
            "exam_name": "Orange",
            "percent": 90,
            "is_fail": False,
            "is_retake": False,
            "legacy_result": False,
            "legacy_retake": False,
        },
        {"exam_pk": e3.pk, "exam_name": "Grape", "taken": False},
        {
            "taken": True,
            "pk": user_exam_version_1.pk,
            "exam_pk": e1.pk,
            "exam_name": "Apple",
            "percent": 50,
            "is_fail": True,
            "is_retake": True,
            "legacy_result": False,
            "legacy_retake": False,
        },
    ] == data
