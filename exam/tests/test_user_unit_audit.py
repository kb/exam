# -*- encoding: utf-8 -*-
import pytest

from django.conf import settings
from django.utils import timezone

from exam.models import UserUnitAudit, UserUnitAuditManager
from exam.tests.factories import (
    UserAssessmentFactory,
    UserUnitFactory,
)

from login.tests.factories import UserFactory


@pytest.mark.django_db
def test_create_user_unit_audit():
    user_unit = UserUnitFactory()
    x = UserUnitAudit.objects.create_user_unit_audit(
        user=UserFactory(username="John"),
        user_unit=user_unit,
        result=78,
    )
    assert "John" == x.user.username
    assert user_unit == x.user_unit
    assert 78 == x.result
    assert timezone.now().date() == x.created.date()


@pytest.mark.django_db
def test_ordering():
    UserUnitAudit.objects.create_user_unit_audit(
        user=UserFactory(username="John"),
        user_unit=UserUnitFactory(),
        result=78,
    )
    UserUnitAudit.objects.create_user_unit_audit(
        user=UserFactory(username="Patrick"),
        user_unit=UserUnitFactory(),
        result=33,
    )

    assert ["Patrick", "John"] == [
        x.user.username for x in UserUnitAudit.objects.all()
    ]


@pytest.mark.django_db
def test_str():
    user_unit = UserUnitFactory()
    x = UserUnitAudit.objects.create_user_unit_audit(
        user=UserFactory(username="John"),
        user_unit=user_unit,
        result=78,
    )
    assert "John" == str(x)
