# -*- encoding: utf-8 -*-
import pytest

from django.core.files.uploadedfile import SimpleUploadedFile

from exam.forms import (
    _course_products,
    _unit_products,
    AssessmentAnswerFileForm,
    UserUnitCommentForm,
)
from exam.tests.factories import ExamSettingsFactory, UserUnitFactory
from stock.tests.factories import (
    ProductCategoryFactory,
    ProductFactory,
    ProductTypeFactory,
)


@pytest.mark.django_db
def test_course_products():
    product_type = ProductTypeFactory()
    ExamSettingsFactory(course=product_type)
    category = ProductCategoryFactory(
        product_type=product_type, slug="my-old-report"
    )
    ProductFactory(category=category, name="C")
    ProductFactory(category=category, name="B")
    ProductFactory(category=category, name="A")
    assert ["A", "B", "C"] == [x.name for x in _course_products()]


@pytest.mark.django_db
def test_unit_products():
    product_type = ProductTypeFactory()
    ExamSettingsFactory(unit=product_type)
    category = ProductCategoryFactory(
        product_type=product_type, slug="my-old-report"
    )
    ProductFactory(category=category, name="C")
    ProductFactory(category=category, name="B")
    ProductFactory(category=category, name="A")
    assert ["A", "B", "C"] == [x.name for x in _unit_products()]


@pytest.mark.django_db
def test_user_unit_form():
    form = UserUnitCommentForm(data={"assessor_comments": "I marked it."})
    form.instance = UserUnitFactory(result=77)
    assert form.is_valid() is True, form.errors


@pytest.mark.django_db
def test_user_assessment_form():
    """Test the ``AssessmentAnswerFileForm``.

    Copied from::

      lms/tests/test_lms_forms.py

    """
    form = AssessmentAnswerFileForm(
        data={},
        files={
            "answer_file": SimpleUploadedFile(
                "my-answers.doc", b"file contents"
            )
        },
    )
    assert form.is_valid() is True, form.errors


@pytest.mark.django_db
def test_user_assessment_form_invalid():
    """Test the ``AssessmentAnswerFileForm``."""
    form = AssessmentAnswerFileForm(data={})
    assert form.is_valid() is False
    assert {"answer_file": ["This field is required."]} == form.errors
