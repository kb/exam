# Generated by Django 3.2.8 on 2022-02-23 09:44

from django.conf import settings
import django.core.files.storage
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("exam", "0008_auto_20220114_1625"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="userassessment",
            options={
                "ordering": ("user_unit", "assessment", "answer_submitted"),
                "verbose_name": "UserAssessment",
                "verbose_name_plural": "UserAssessments",
            },
        ),
        migrations.RenameField(
            model_name="userassessment",
            old_name="submitted",
            new_name="answer_submitted",
        ),
        migrations.RemoveField(
            model_name="userassessment",
            name="answer",
        ),
        migrations.RemoveField(
            model_name="userassessment",
            name="assessed",
        ),
        migrations.RemoveField(
            model_name="userassessment",
            name="assessor",
        ),
        migrations.RemoveField(
            model_name="userassessment",
            name="assessor_comments",
        ),
        migrations.RemoveField(
            model_name="userassessment",
            name="original_file_name",
        ),
        migrations.AddField(
            model_name="userassessment",
            name="answer_original_file_name",
            field=models.TextField(blank=True),
        ),
        migrations.AddField(
            model_name="userassessment",
            name="can_retake_date",
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name="userassessment",
            name="can_retake_user",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="+",
                to=settings.AUTH_USER_MODEL,
            ),
        ),
        migrations.AddField(
            model_name="userassessment",
            name="retake_answer_file",
            field=models.FileField(
                blank=True,
                storage=django.core.files.storage.FileSystemStorage(
                    location="media-private"
                ),
                upload_to="exam/assessment/user",
            ),
        ),
        migrations.AddField(
            model_name="userassessment",
            name="retake_answer_original_file_name",
            field=models.TextField(blank=True),
        ),
        migrations.AddField(
            model_name="userassessment",
            name="retake_submitted",
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name="userunit",
            name="assessed",
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name="userunit",
            name="retake_assessor",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="+",
                to=settings.AUTH_USER_MODEL,
            ),
        ),
        migrations.AddField(
            model_name="userunit",
            name="retake_comments",
            field=models.TextField(blank=True),
        ),
        migrations.AddField(
            model_name="userunit",
            name="retake_requested",
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name="userunit",
            name="retake_submitted",
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name="userunit",
            name="result",
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
