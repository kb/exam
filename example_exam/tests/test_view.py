# -*- encoding: utf-8 -*-
import pytest

from datetime import date
from django.urls import reverse
from http import HTTPStatus

from base.url_utils import url_with_querystring
from exam.models import Course, Question, UserCourse, UserExamVersion, UserUnit
from exam.tests.factories import (
    AssessmentFactory,
    ClassInformationFactory,
    CourseFactory,
    CourseProductFactory,
    CourseUnitFactory,
    ExamFactory,
    ExamSettingsFactory,
    ExamVersionFactory,
    ProductExamFactory,
    QuestionFactory,
    UnitExamFactory,
    UnitFactory,
    UserCourseFactory,
    UserExamVersionFactory,
    UserUnitFactory,
)
from exam.tests.scenario import (
    set_correct_answers_all,
    set_incorrect_answers,
    setup_exam,
)
from login.tests.factories import TEST_PASSWORD, UserFactory
from mail.tests.factories import MailTemplateFactory
from stock.tests.factories import ProductCategoryFactory, ProductFactory
from .factories import ContactFactory


@pytest.mark.django_db
def test_assessment_download_pdf(client):
    user = UserFactory(is_staff=False)
    # login
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    # lms
    course = CourseFactory()
    unit = UnitFactory()
    assessment = AssessmentFactory(unit=unit, name="1a: Client’s Statement")
    CourseUnitFactory(course=course, unit=unit)
    # lms - user
    user_course = UserCourseFactory(user=user, course=course)
    UserUnitFactory(user_course=user_course, unit=unit)
    url = reverse("exam.assessment.download.pdf", args=[assessment.pk])
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    assert "application/pdf" == response.headers.get("Content-Type")
    assert (
        'attachment; filename="1a-Clients-Statement-Assessment.pdf"'
        == response.headers.get("Content-Disposition")
    )


@pytest.mark.django_db
def test_user_course_create(client):
    """Create a course for a contact (if it has no optional units).

    If the course has optional (``core=False``) units, then redirect to
    ``UserCourseCoreCreateView``.

    """
    user = UserFactory(is_staff=True)
    contact = ContactFactory(user=user)
    course_product = CourseProductFactory()
    CourseUnitFactory(course=course_product.course, unit=UnitFactory(name="a"))
    course_unit = CourseUnitFactory(
        course=course_product.course, unit=UnitFactory(name="b")
    )
    course_unit.set_deleted(UserFactory())
    CourseUnitFactory(
        course=course_product.course, unit=UnitFactory(name="c"), core=True
    )
    assert 0 == UserCourse.objects.count()
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("exam.user.course.create", args=[contact.pk])
    response = client.post(
        url,
        {
            "course_product": course_product.pk,
            "payment_details": "Three Instalments",
        },
    )
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert reverse("contact.detail", args=[contact.pk]) == response.url
    assert 1 == UserCourse.objects.count()
    user_course = UserCourse.objects.first()
    assert course_product.course == user_course.course
    assert "Three Instalments" == user_course.payment_details
    qs = UserUnit.objects.for_user_course(user_course)
    assert ["a", "c"] == [x.unit.name for x in qs]


@pytest.mark.django_db
def test_user_course_create_core(client):
    """Create a course for a contact (if it has optional units).

    This view will be used by ``UserCourseCreateView`` if the course has
    optional (``core=False``) units.

    """
    user = UserFactory(is_staff=True)
    contact = ContactFactory(user=user)
    course = CourseFactory()
    CourseUnitFactory(course=course, unit=UnitFactory(name="a"))
    course_unit_b = CourseUnitFactory(course=course, unit=UnitFactory(name="b"))
    course_unit_b.set_deleted(UserFactory())
    course_unit_c = CourseUnitFactory(
        course=course, unit=UnitFactory(name="c"), core=False
    )
    CourseUnitFactory(course=course, unit=UnitFactory(name="d"), core=True)
    course_unit_e = CourseUnitFactory(
        course=course, unit=UnitFactory(name="e"), core=False
    )
    course_unit_f = CourseUnitFactory(
        course=course, unit=UnitFactory(name="f"), core=False
    )
    assert 0 == UserCourse.objects.count()
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("exam.user.course.create.core", args=[contact.pk, course.pk])
    response = client.post(
        url,
        {
            "course_units": [
                course_unit_c.pk,
                course_unit_e.pk,
                course_unit_f.pk,
            ],
            "payment_details": "Paid by Phone",
        },
    )
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert reverse("contact.detail", args=[contact.pk]) == response.url
    assert 1 == UserCourse.objects.count()
    user_course = UserCourse.objects.first()
    assert course == user_course.course
    assert "Paid by Phone" == user_course.payment_details
    qs = UserUnit.objects.for_user_course(user_course)
    assert ["a", "c", "d", "e", "f"] == [x.unit.name for x in qs]


@pytest.mark.django_db
def test_user_course_create_core_get(client):
    """Create a course for a contact (if it has optional units).

    This view will be used by ``UserCourseCreateView`` if the course has
    optional (``core=False``) units.

    """
    user = UserFactory(is_staff=True)
    contact = ContactFactory(user=user)
    course = CourseFactory()
    CourseUnitFactory(course=course, unit=UnitFactory(name="a"))
    course_unit_b = CourseUnitFactory(course=course, unit=UnitFactory(name="b"))
    course_unit_b.set_deleted(UserFactory())
    course_unit_c = CourseUnitFactory(
        course=course, unit=UnitFactory(name="c"), core=False
    )
    CourseUnitFactory(course=course, unit=UnitFactory(name="d"), core=True)
    course_unit_e = CourseUnitFactory(
        course=course, unit=UnitFactory(name="e"), core=False
    )
    course_unit_f = CourseUnitFactory(
        course=course, unit=UnitFactory(name="f"), core=False
    )
    assert 0 == UserCourse.objects.count()
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("exam.user.course.create.core", args=[contact.pk, course.pk])
    response = client.get(
        url_with_querystring(url, payment_details="Paid by Phone")
    )
    assert HTTPStatus.OK == response.status_code
    form = response.context["form"]
    assert {"payment_details": "Paid by Phone"} == form.initial
    qs = form.fields["course_units"]
    assert ["c", "e", "f"] == [x.unit.name for x in qs.queryset]
    assert "Paid by Phone" == response.context["payment_details"]
    assert course == response.context["course"]
    assert contact == response.context["contact"]
    course_units_core = response.context["course_units_core"]
    assert ["a", "d"] == [x.unit.name for x in course_units_core]


@pytest.mark.django_db
def test_user_course_create_core_validate(client):
    """Create a course for a contact (if it has optional units).

    This view will be used by ``UserCourseCreateView`` if the course has
    optional (``core=False``) units.

    22/07/2024, Remove the check for three optional units.
    Foundation and Advanced Courses can have 0, 1, 2 or 3 optional units.
    https://www.kbsoftware.co.uk/crm/ticket/7270/

    """
    user = UserFactory(is_staff=True)
    contact = ContactFactory(user=user)
    course = CourseFactory()
    CourseUnitFactory(course=course, unit=UnitFactory(name="a"))
    course_unit = CourseUnitFactory(course=course, unit=UnitFactory(name="b"))
    course_unit.set_deleted(UserFactory())
    CourseUnitFactory(course=course, unit=UnitFactory(name="c"), core=False)
    CourseUnitFactory(course=course, unit=UnitFactory(name="d"), core=True)
    course_unit_e = CourseUnitFactory(
        course=course, unit=UnitFactory(name="e"), core=False
    )
    assert 0 == UserCourse.objects.count()
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("exam.user.course.create.core", args=[contact.pk, course.pk])
    response = client.post(
        url_with_querystring(url, payment_details="Paid by Phone"),
        {"course_units": course_unit_e.pk},
    )
    assert HTTPStatus.FOUND == response.status_code
    user_course = UserCourse.objects.get(user=user, course=course)
    assert ["a", "d", "e"] == [
        x.unit.name
        for x in UserUnit.objects.filter(user_course=user_course).order_by(
            "unit__name"
        )
    ]


@pytest.mark.django_db
def test_user_course_create_core_validate_course_unit_not_required(client):
    """Create a course for a contact (if it has optional units).

    This view will be used by ``UserCourseCreateView`` if the course has
    optional (``core=False``) units.

    """
    user = UserFactory(is_staff=True)
    contact = ContactFactory(user=user)
    course = CourseFactory()
    CourseUnitFactory(course=course, unit=UnitFactory(name="a"))
    course_unit = CourseUnitFactory(course=course, unit=UnitFactory(name="b"))
    course_unit.set_deleted(UserFactory())
    CourseUnitFactory(course=course, unit=UnitFactory(name="c"), core=False)
    CourseUnitFactory(course=course, unit=UnitFactory(name="d"), core=True)
    course_unit_e = CourseUnitFactory(
        course=course, unit=UnitFactory(name="e"), core=False
    )
    assert 0 == UserCourse.objects.count()
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("exam.user.course.create.core", args=[contact.pk, course.pk])
    response = client.post(
        url_with_querystring(url, payment_details="Paid by Phone"), {}
    )
    assert HTTPStatus.FOUND == response.status_code
    user_course = UserCourse.objects.get(user=user, course=course)
    assert ["a", "d"] == [
        x.unit.name
        for x in UserUnit.objects.filter(user_course=user_course).order_by(
            "unit__name"
        )
    ]


@pytest.mark.django_db
def test_user_course_create_get(client):
    user = UserFactory(is_staff=True)
    contact = ContactFactory(user=user)
    CourseProductFactory(product=ProductFactory(name="a"))
    course_product = CourseProductFactory(product=ProductFactory(name="b"))
    course_product.set_deleted(UserFactory())
    CourseProductFactory(product=ProductFactory(name="c"))
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("exam.user.course.create", args=[contact.pk])
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    form = response.context["form"]
    qs = form.fields["course_product"]
    assert ["a", "c"] == [x.product.name for x in qs.queryset]


@pytest.mark.django_db
def test_user_course_create_redirect_to_core(client):
    """Create a course with ``core`` units for a contact.

    Redirect to ``UserCourseCoreCreateView`` if the course has ``core``
    (optional) units.

    """
    user = UserFactory(is_staff=True)
    contact = ContactFactory(user=user)
    course_product = CourseProductFactory()
    CourseUnitFactory(course=course_product.course, unit=UnitFactory(name="a"))
    course_unit = CourseUnitFactory(
        course=course_product.course, unit=UnitFactory(name="b")
    )
    course_unit.set_deleted(UserFactory())
    CourseUnitFactory(
        course=course_product.course, unit=UnitFactory(name="c"), core=False
    )
    CourseUnitFactory(
        course=course_product.course, unit=UnitFactory(name="d"), core=True
    )
    assert 0 == UserCourse.objects.count()
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("exam.user.course.create", args=[contact.pk])
    response = client.post(
        url,
        {
            "course_product": course_product.pk,
            "payment_details": "Three Instalments",
        },
    )
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    # view should redirect - not create the course
    assert 0 == UserCourse.objects.count()
    url = reverse(
        "exam.user.course.create.core",
        args=[contact.pk, course_product.course.pk],
    )
    assert (
        url_with_querystring(url, payment_details="Three Instalments")
        == response.url
    )


@pytest.mark.django_db
def test_user_course_create_tailored(client):
    user = UserFactory(is_staff=True)
    contact = ContactFactory(user=user)
    category = ProductCategoryFactory()
    ProductFactory(name="Agriculture", category=category)
    ExamSettingsFactory(course=category.product_type)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    assert 0 == UserCourse.objects.count()
    assert 0 == Course.objects.count()
    url = reverse("exam.user.course.create.tailored", args=[contact.pk])
    response = client.post(url, data={"name": "Farming"})
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert reverse("contact.detail", args=[contact.pk]) == response.url
    assert 1 == Course.objects.count()
    assert 1 == UserCourse.objects.count()
    course = Course.objects.first()
    user_course = UserCourse.objects.first()
    assert course.is_tailored is True
    assert "Farming" == user_course.course.name
    assert contact.user == user_course.user


@pytest.mark.django_db
def test_user_course_profile_update(client):
    course = CourseFactory()
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    user_course = UserCourseFactory(
        user=UserFactory(first_name="Patrick"),
        course=course,
    )
    class_information = ClassInformationFactory(
        course=course,
        description="March 2023",
        start_date=date.today(),
    )
    url = reverse("exam.user.course.profile.update", args=[user_course.pk])
    response = client.post(
        url,
        {
            "payment_details": "Cheque",
            "selected_class": class_information.pk,
            "extension_expires": "2023-12-25",
            "performance_notes": "Perfect!",
            "additional_notes": "What else can I say?",
            "cancellation_date": "2023-11-22",
        },
    )
    assert HTTPStatus.FOUND == response.status_code
    assert (
        reverse("exam.user.course.detail", args=[user_course.pk])
        == response.url
    )
    user_course.refresh_from_db()
    assert "Cheque" == user_course.payment_details
    assert "March 2023" == user_course.selected_class.description
    assert date(2023, 12, 25) == user_course.extension_expires
    assert "Perfect!" == user_course.performance_notes
    assert "What else can I say?" == user_course.additional_notes
    assert date(2023, 11, 22) == user_course.cancellation_date


@pytest.mark.django_db
def test_user_exam_redirect_fail(client):
    """The user has failed the exam, but can do a retake."""
    # login
    user = UserFactory(is_staff=False)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    # mail
    MailTemplateFactory(slug=UserExamVersion.MAIL_TEMPLATE_FAIL_INITIAL)
    # exam
    exam_version = setup_exam()
    exam_version.exam.init_exam(user)
    user_exam_version = UserExamVersion.objects.get(
        user=user, exam_version=exam_version
    )
    set_correct_answers_all(user_exam_version)
    set_incorrect_answers(user_exam_version, [3, 4, 5, 7, 8, 9])
    user_exam_version.mark()
    # test
    url = reverse("exam.user.redirect", args=[exam_version.exam.pk])
    response = client.get(url)
    assert HTTPStatus.FOUND == response.status_code
    # check
    user_exam_version = UserExamVersion.objects.get(
        user=user, exam_version=exam_version, result__isnull=True
    )
    expect = reverse(
        "web.exam.user.version.update", args=[user_exam_version.pk]
    )
    assert expect == response.url


@pytest.mark.django_db
def test_user_exam_redirect_pass(client):
    """If the user has passed the exam, display the result."""
    # login
    user = UserFactory(is_staff=False)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    # mail
    MailTemplateFactory(slug=UserExamVersion.MAIL_TEMPLATE_PASS)
    # exam
    exam_version = setup_exam()
    exam_version.exam.init_exam(user)
    user_exam_version = UserExamVersion.objects.get(
        user=user, exam_version=exam_version
    )
    set_correct_answers_all(user_exam_version)
    user_exam_version.mark()
    # test
    url = reverse("exam.user.redirect", args=[exam_version.exam.pk])
    response = client.get(url)
    # check
    assert HTTPStatus.FOUND == response.status_code
    expect = reverse(
        "web.exam.user.version.result", args=[user_exam_version.pk]
    )
    assert expect == response.url


@pytest.mark.django_db
def test_user_exam_version_download(client):
    # login
    user = UserFactory(is_staff=False)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    # exam
    exam_version = setup_exam()
    exam_version.exam.init_exam(user)
    user_exam_version = UserExamVersion.objects.get(
        user=user, exam_version=exam_version
    )
    url = reverse("exam.user.download", args=[user_exam_version.pk])
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_user_exam_version_redirect(client):
    """Called when the user decides to take a question in the exam.

    - Redirect to the view for the first question.

    """
    published_version = setup_exam()
    # get question number 1
    question_1 = Question.objects.get(exam_version=published_version, number=1)
    # login
    user = UserFactory(is_staff=False)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    # initialise the exam
    published_version.exam.init_exam(user)
    user_exam_version = UserExamVersion.objects.get(user=user)
    # test
    url = reverse("exam.user.version.redirect", args=[user_exam_version.pk])
    response = client.get(url)
    assert HTTPStatus.FOUND == response.status_code
    expect = reverse(
        "web.exam.user.question", args=[user_exam_version.pk, question_1.pk]
    )
    assert expect == response.url


@pytest.mark.django_db
def test_user_exam_version_detail_update_view(client):
    course = CourseFactory()
    unit = UnitFactory()
    CourseUnitFactory(course=course, unit=unit)
    exam = ExamFactory()
    UnitExamFactory(unit=unit, exam=exam)
    user = UserFactory()
    UserCourseFactory(user=user, course=course)
    exam_version = ExamVersionFactory(exam=exam)
    QuestionFactory(exam_version=exam_version)
    user_exam_version = UserExamVersionFactory(
        user=user, exam_version=exam_version
    )
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("web.exam.user.version.update", args=[user_exam_version.pk])
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_user_exam_version_detail_update_view_post(client):
    MailTemplateFactory(slug=UserExamVersion.MAIL_TEMPLATE_PASS)
    course = CourseFactory()
    course_product = CourseProductFactory(course=course)
    unit = UnitFactory()
    CourseUnitFactory(course=course, unit=unit)
    user = UserFactory()
    UserCourseFactory(user=user, course=course)
    exam_version = setup_exam()
    exam_version.exam.init_exam(user)
    UnitExamFactory(unit=unit, exam=exam_version.exam)
    ProductExamFactory(product=course_product.product, exam=exam_version.exam)
    user_exam_version = UserExamVersionFactory(
        user=user, exam_version=exam_version
    )
    set_correct_answers_all(user_exam_version)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("web.exam.user.version.update", args=[user_exam_version.pk])
    response = client.post(url)
    assert HTTPStatus.FOUND == response.status_code
    assert (
        reverse("web.exam.user.version.submitted", args=[user_exam_version.pk])
        == response.url
    )
