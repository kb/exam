# -*- encoding: utf-8 -*-
from django.conf import settings
from django.core.management.base import BaseCommand

from exam.models import UserCourse, UserExamVersion, UserUnit
from mail.models import MailTemplate


class Command(BaseCommand):
    help = "Initialise 'exam' application"

    def handle(self, *args, **options):
        self.stdout.write(f"{self.help}...")
        templates = {
            UserExamVersion.MAIL_TEMPLATE_FAIL_INITIAL: "Failed initial",
            UserExamVersion.MAIL_TEMPLATE_FAIL_RETAKE: "Failed retake",
            UserExamVersion.MAIL_TEMPLATE_PASS: "Passed",
        }
        for template_name, caption in templates.items():
            MailTemplate.objects.init_mail_template(
                template_name,
                caption,
                (
                    "You can add the following variables to the template:\n"
                    "{{ exam_name }} name of the exam.\n"
                    "{{ name }} name of the student.\n"
                    "{{ questions }} list of questions.\n"
                    "{{ result }} result.\n"
                    "{{ result_as_percent }} result as a percentage.\n"
                ),
                False,
                settings.MAIL_TEMPLATE_TYPE,
                subject=caption,
                description="Here is the result of your exam.",
            )
        # "exam_user_course_result"
        MailTemplate.objects.init_mail_template(
            UserCourse.MAIL_TEMPLATE_USER_COURSE_RESULT,
            "Course result",
            (
                "You can add the following variables to the template:\n"
                "{{ name }} name of the student.\n"
            ),
            False,
            settings.MAIL_TEMPLATE_TYPE,
            subject="Course result",
            description="Here is the result of your course.",
        )
        # "exam_user_unit_result"
        MailTemplate.objects.init_mail_template(
            UserUnit.MAIL_TEMPLATE_USER_UNIT_RESULT,
            "Unit result",
            (
                "You can add the following variables to the template:\n"
                "{{ name }} name of the student.\n"
            ),
            False,
            settings.MAIL_TEMPLATE_TYPE,
            subject="Unit result",
            description="Here is the result of your unit.",
        )
        # "exam_user_unit_retake"
        MailTemplate.objects.init_mail_template(
            UserUnit.MAIL_TEMPLATE_USER_UNIT_RETAKE,
            "Unit retake comments",
            (
                "You can add the following variables to the template:\n"
                "{{ name }} name of the student.\n"
            ),
            False,
            settings.MAIL_TEMPLATE_TYPE,
            subject="Unit retake comments",
            description="Here are the retake comments for your unit.",
        )
        self.stdout.write(f"{self.help} - Complete...")
