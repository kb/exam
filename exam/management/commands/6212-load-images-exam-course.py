# -*- encoding: utf-8 -*-
import json
import pathlib

from django.contrib.auth import get_user_model
from django.core.files import File
from django.core.management.base import BaseCommand

from exam.models import Course
from gallery.models import Image, ImageCategory


class Command(BaseCommand):
    help = "Exam - course - load images #6212"

    def _save_image(self, data):
        title = data["title"]
        file_name = data["file_name"]
        user_name = data["user"]
        category = data["category"]
        try:
            image_category = ImageCategory.objects.get(slug=category)
        except ImageCategory.DoesNotExist:
            image_category = None
        user = get_user_model().objects.get(username=user_name)
        image = Image(
            title=title,
            user=user,
            category=image_category,
        )
        with open(file_name, "rb") as f:
            django_file = File(f)
            image.image.save(
                pathlib.Path(file_name).name, django_file, save=True
            )
        return image

    def handle(self, *args, **options):
        json_file_name = "6212-dump-images-exam-course.json"
        self.stdout.write(
            f"{self.help}.  Load images from '{json_file_name}'..."
        )
        with open(json_file_name, "r") as f:
            data = json.load(f)
        # pprint(data, expand_all=True)
        for course_pk, image_data in data.items():
            course = Course.objects.get(pk=course_pk)
            self.stdout.write(f"{course}")
            # banner
            image_fields = image_data.get("banner", None)
            if image_fields:
                image = self._save_image(image_fields)
                self.stdout.write(f"- banner: {image}")
                course.banner = image
                course.save()
            # banner_mobile
            image_fields = image_data.get("banner_mobile", None)
            if image_fields:
                image = self._save_image(image_fields)
                self.stdout.write(f"- banner_mobile: {image}")
                course.banner_mobile = image
                course.save()
            image_fields = image_data.get("picture", None)
            if image_fields:
                image = self._save_image(image_fields)
                self.stdout.write(f"- picture: {image}")
                course.picture = image
                course.save()
        self.stdout.write(
            f"{self.help}.  Images loaded from '{json_file_name}' - Complete"
        )
