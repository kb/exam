# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse
from http import HTTPStatus

from exam.models import Question, UserExamVersion
from exam.tests.factories import (
    CourseFactory,
    ExamSettingsFactory,
    ProductExamFactory,
    UserCourseFactory,
)
from exam.tests.scenario import set_correct_answers_all, setup_exam
from login.tests.factories import TEST_PASSWORD, UserFactory
from login.tests.fixture import perm_check
from mail.tests.factories import MailTemplateFactory
from stock.tests.factories import ProductFactory
from .factories import ContactFactory


@pytest.mark.django_db
def test_lms_dash(perm_check):
    url = reverse("lms.dash")
    perm_check.staff(url)


@pytest.mark.django_db
def test_user_course_create(perm_check):
    contact = ContactFactory(user=UserFactory())
    ExamSettingsFactory()
    url = reverse("exam.user.course.create", args=[contact.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_user_course_create_core(perm_check):
    contact = ContactFactory(user=UserFactory())
    course = CourseFactory()
    ExamSettingsFactory()
    url = reverse("exam.user.course.create.core", args=[contact.pk, course.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_user_course_create_tailored(perm_check):
    contact = ContactFactory(user=UserFactory())
    ExamSettingsFactory()
    url = reverse("exam.user.course.create.tailored", args=[contact.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_user_course_list_to_mark_team_member(client):
    user = UserFactory(is_staff=False)
    ContactFactory(user=user, team_member=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(reverse("exam.user.course.list.to.mark"))
    assert HTTPStatus.OK == response.status_code, response.url


@pytest.mark.django_db
def test_user_course_marking_list_team_member(client):
    """Testing the ``UserCourseMarkingListView``.

    .. tip:: Also see ``test_user_course_marking_list``
             in ``exam/tests/test_view_perm.py``.

    """
    user = UserFactory(is_staff=False)
    ContactFactory(user=user, team_member=True)
    UserCourseFactory()
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("exam.user.course.marking.list")
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_student_detail(client):
    """Cannot test using 'perm_check' fixture as exam is for one user."""
    # login
    user = UserFactory(is_staff=False)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    # exam
    exam_version = setup_exam()
    exam_version.exam.init_exam(user)
    user_exam_version = exam_version.exam.user_exam_version(user)
    course = ProductFactory()
    ProductExamFactory(exam=exam_version.exam, product=course)
    # test
    url = reverse("web.exam.user.version.update", args=[user_exam_version.pk])
    response = client.get(url)
    # check
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_student_detail_not_logged_in(client):
    """Cannot test using 'perm_check' fixture as exam is for one user."""
    # login
    user = UserFactory(is_staff=False)
    # exam
    exam_version = setup_exam()
    exam_version.exam.init_exam(user)
    user_exam_version = exam_version.exam.user_exam_version(user)
    # test
    url = reverse("web.exam.user.version.update", args=[user_exam_version.pk])
    response = client.get(url)
    # check
    assert HTTPStatus.FOUND == response.status_code
    assert "/login/" in response["Location"]


@pytest.mark.django_db
def test_student_question(perm_check):
    version = setup_exam()
    # find a published question
    question = Question.objects.get(
        exam_version__date_published__isnull=False,
        exam_version=version,
        number=3,
    )
    # initialise the exam
    user = UserFactory()
    version.exam.init_exam(user)
    user_exam_version = UserExamVersion.objects.get(user=user)
    # product permission
    course = ProductFactory()
    ProductExamFactory(exam=user_exam_version.exam_version.exam, product=course)
    # check
    url = reverse(
        "web.exam.user.question", args=[user_exam_version.pk, question.pk]
    )
    perm_check.auth(url)


@pytest.mark.django_db
def test_student_exam_redirect(client):
    """Cannot test using 'perm_check' fixture as exam is for one user."""
    # login
    user = UserFactory(is_staff=False)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    # mail
    MailTemplateFactory(slug=UserExamVersion.MAIL_TEMPLATE_PASS)
    # exam
    exam_version = setup_exam()
    exam_version.exam.init_exam(user)
    user_exam_version = exam_version.exam.user_exam_version(user)
    set_correct_answers_all(user_exam_version)
    user_exam_version.mark()
    # test
    url = reverse("exam.user.redirect", args=[exam_version.exam.pk])
    response = client.get(url)
    # check
    assert HTTPStatus.FOUND == response.status_code
    expect = reverse(
        "web.exam.user.version.result", args=[user_exam_version.pk]
    )
    assert expect == response["Location"]


@pytest.mark.django_db
def test_student_exam_redirect_not_logged_in(client):
    """Cannot test using 'perm_check' fixture as exam is for one user."""
    # exam
    exam_version = setup_exam()
    # test
    url = reverse("exam.user.redirect", args=[exam_version.exam.pk])
    response = client.get(url)
    # check
    assert HTTPStatus.FOUND == response.status_code
    assert "/login/" in response["Location"]


@pytest.mark.django_db
def test_student_exam_result(perm_check):
    version = setup_exam()
    # initialise the exam
    user = UserFactory()
    version.exam.init_exam(user)
    user_exam_version = UserExamVersion.objects.get(user=user)
    # product permission
    course = ProductFactory()
    ProductExamFactory(exam=user_exam_version.exam_version.exam, product=course)
    # test
    url = reverse("web.exam.user.version.result", args=[user_exam_version.pk])
    perm_check.auth(url)
