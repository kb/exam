# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from exam.models import UserAssessment, UserUnit


class Command(BaseCommand):
    help = "Student can see feedback #7006"

    def reset(self, user_unit):
        user_unit.retake_assessed = None
        user_unit.retake_comments = ""
        user_unit.assessed = None
        user_unit.comments = ""
        user_unit.result_assessed = None
        user_unit.result = None
        user_unit.save()
        count = UserAssessment.objects.filter(user_unit=user_unit).update(
            assessed=None,
            assessor=None,
            assessor_comments="",
            can_retake=False,
            has_passed=False,
            retake_assessed=None,
            retake_assessor=None,
            retake_comments="",
        )
        self.stdout.write("")
        self.stdout.write("RESET")
        self.stdout.write("RESET")
        self.stdout.write("RESET")
        self.stdout.write(f"Reset: {user_unit} and {count} assessments")
        self.stdout.write("")

    def add_arguments(self, parser):
        parser.add_argument("user-unit-pk", type=int)

    def handle(self, *args, **options):
        self.stdout.write(f"{self.help}...")
        user_unit_pk = options["user-unit-pk"]
        user_unit = UserUnit.objects.get(pk=user_unit_pk)
        # self.reset(user_unit)
        self.stdout.write(f"{user_unit}")
        self.stdout.write(
            f"  {'retake_assessed':<31} {user_unit.retake_assessed}"
        )
        self.stdout.write(
            f"  {'is_marking_finished_retake':<31} {user_unit.is_marking_finished_retake()}"
        )
        self.stdout.write("")
        self.stdout.write(f"  {'result':<31} {user_unit.result}")
        self.stdout.write(
            f"  {'result_assessed':<31} {user_unit.result_assessed}"
        )
        self.stdout.write(f"  {'assessed':<31} {user_unit.assessed}")
        self.stdout.write(
            f"  {'is_marking_finished':<31} {user_unit.is_marking_finished()}"
        )
        self.stdout.write("")
        qs = UserAssessment.objects.filter(user_unit=user_unit).order_by(
            "assessment__order"
        )
        for count, x in enumerate(qs, 1):
            self.stdout.write(f"{count}. {x.assessment}")
            self.stdout.write(f"   {'can_retake':<30} {x.can_retake}")
            self.stdout.write(f"   {'has_passed':<30} {x.has_passed}")
            self.stdout.write(f"   {'assessed':<30} {x.assessed}")
            self.stdout.write(
                f"   {'assessor_comments':<30} {x.assessor_comments}"
            )
            self.stdout.write(
                f"   {'feedback_enabled_for_retake':<30} {x.feedback_enabled_for_retake()}"
            )
            self.stdout.write("")
            self.stdout.write(f"   {'retake_assessed':<30} {x.retake_assessed}")
            self.stdout.write(
                f"   {'retake_has_passed':<30} {x.retake_has_passed}"
            )
            self.stdout.write(
                f"   {'answer_button_caption':<30} {x.answer_button_caption()}"
            )
            self.stdout.write(
                f"   {'feedback_enabled':<30} {x.feedback_enabled()}"
            )
            if count > 1:
                break
        self.stdout.write(f"{self.help} - Complete")
