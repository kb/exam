# -*- encoding: utf-8 -*-
import pytest

from datetime import date

from exam.models import ClassInformation
from .factories import CourseFactory, ClassInformationFactory


@pytest.mark.django_db
def test_ordering():
    ClassInformationFactory(
        course=CourseFactory(),
        start_date=date(2022, 3, 1),
        description="March",
    )
    ClassInformationFactory(
        course=CourseFactory(),
        start_date=date(2022, 4, 1),
        description="April",
    )
    assert ["March", "April"] == [
        x.description for x in ClassInformation.objects.all()
    ]


@pytest.mark.django_db
def test_str():
    class_information = ClassInformationFactory(
        course=CourseFactory(),
        start_date=date(2022, 12, 19),
        description="March 2023",
    )
    assert "19/12/2022" == str(class_information)
