Testing
*******

.. highlight:: python

From ``test_mark_unanswered`` in ``exam/tests/test_user_exam_version.py``::

  user = UserFactory(is_staff=False)
  exam = ExamFactory()
  exam_version = ExamVersionFactory(exam=exam)
  # question_1
  question_1 = QuestionFactory(exam_version=exam_version)
  answer_1 = OptionFactory(question=question_1)
  question_1.answer = answer_1
  question_1.save()
  # question_2
  question_2 = QuestionFactory(exam_version=exam_version)
  answer_2 = OptionFactory(question=question_2)
  question_2.answer = answer_2
  question_2.save()
  # user_exam_version
  user_exam_version = UserExamVersionFactory(
      user=user,
      exam_version=exam_version,
  )
  # no answer (student doesn't need to answer all the questions)
  # SelectionFactory(user_exam_version=user_exam_version, answer=answer_1)
  # answer (student answered this question)
  SelectionFactory(user_exam_version=user_exam_version, answer=answer_2)
  user_exam_version.mark()
