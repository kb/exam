# -*- encoding: utf-8 -*-
import datetime
import pytest

from django.contrib.auth.models import AnonymousUser
from django.utils import timezone
from freezegun import freeze_time

from exam.models import ExamError, Unit, UserCourse
from exam.tests.factories import (
    AssessmentFactory,
    CourseFactory,
    CourseUnitFactory,
    ExamFactory,
    ExamVersionFactory,
    UnitExamFactory,
    UnitFactory,
    UserAssessmentFactory,
    UserCourseFactory,
    UserExamVersionFactory,
    UserUnitFactory,
)
from login.tests.factories import UserFactory


@pytest.mark.django_db
def test_course_results(django_assert_num_queries):
    """

    11/11/2022, Update to only include ``exam`` where the user has access to
    the unit.

    """
    course = CourseFactory()
    unit_1 = UnitFactory(
        name="General Procedures", assignment_or_assessment=Unit.ASSIGNMENT
    )
    unit_2 = UnitFactory(name="Civil Litigation")
    unit_3 = UnitFactory(
        name="English Legal", assignment_or_assessment=Unit.ASSIGNMENT
    )
    unit_4 = UnitFactory(name="Wills and Probate")
    unit_5 = UnitFactory(name="Dairy Farming")
    unit_6 = UnitFactory(name="Matrimonial")
    unit_7 = UnitFactory(name="Consumer Law")
    unit_8 = UnitFactory(name="Commercial Law")
    unit_9 = UnitFactory(name="Arable Farming")
    # exam
    exam_1 = ExamFactory(name="General Procedures")
    exam_2 = ExamFactory(name="English Legal")
    exam_3 = ExamFactory(name="Milking Cows")
    # course unit
    course_unit_1 = CourseUnitFactory(course=course, unit=unit_1, order=1)
    course_unit_2 = CourseUnitFactory(
        course=course, unit=unit_2, order=2, name="Litigation"
    )
    course_unit_3 = CourseUnitFactory(course=course, unit=unit_3, order=3)
    CourseUnitFactory(course=course, unit=unit_4, order=4)
    CourseUnitFactory(course=course, unit=unit_5, order=5)
    course_unit_6 = CourseUnitFactory(course=course, unit=unit_6, order=6)
    course_unit_7 = CourseUnitFactory(course=course, unit=unit_7, order=7)
    course_unit_8 = CourseUnitFactory(course=course, unit=unit_8, order=8)
    course_unit_9 = CourseUnitFactory(course=course, unit=unit_9, order=9)
    # exam
    unit_exam_1 = UnitExamFactory(unit=unit_1, exam=exam_1)
    unit_exam_3 = UnitExamFactory(unit=unit_3, exam=exam_2)
    UnitExamFactory(unit=unit_5, exam=exam_3)
    # assessments
    AssessmentFactory(unit=unit_1)
    AssessmentFactory(unit=unit_2)
    AssessmentFactory(unit=unit_2)
    AssessmentFactory(unit=unit_3)
    AssessmentFactory(unit=unit_6)
    assessment_7 = AssessmentFactory(unit=unit_7)
    assessment_8 = AssessmentFactory(unit=unit_8)
    assessment_9 = AssessmentFactory(unit=unit_9)
    # user related
    user = UserFactory()
    user_course = UserCourseFactory(user=user, course=course)
    UserUnitFactory(
        user_course=user_course,
        unit=unit_1,
        result=100,
        assessed=timezone.now(),
    )
    UserUnitFactory(user_course=user_course, unit=unit_2)
    UserUnitFactory(user_course=user_course, unit=unit_3)
    UserUnitFactory(user_course=user_course, unit=unit_4, result=None)
    UserUnitFactory(
        user_course=user_course, unit=unit_6, submitted=timezone.now()
    )
    user_unit_7 = UserUnitFactory(
        user_course=user_course,
        unit=unit_7,
        submitted=timezone.now(),
        retake_submitted=timezone.now(),
    )
    user_unit_8 = UserUnitFactory(
        user_course=user_course,
        unit=unit_8,
        submitted=timezone.now(),
        retake_submitted=None,
    )
    user_unit_9 = UserUnitFactory(
        user_course=user_course,
        unit=unit_9,
        result=99,
        assessed=None,
    )
    UserAssessmentFactory(
        user_unit=user_unit_7, assessment=assessment_7, can_retake=True
    )
    UserAssessmentFactory(
        user_unit=user_unit_8, assessment=assessment_8, can_retake=True
    )
    UserAssessmentFactory(
        user_unit=user_unit_9, assessment=assessment_9, can_retake=False
    )
    exam_version_1 = ExamVersionFactory(exam=exam_1)
    UserExamVersionFactory(
        user=user,
        exam_version=exam_version_1,
        date_marked=timezone.now(),
        result=10,
    )
    user_exam_version_1 = UserExamVersionFactory(
        user=user,
        exam_version=exam_version_1,
        date_marked=None,
        result=None,
    )
    user_exam_version_2 = UserExamVersionFactory(
        user=user,
        exam_version=ExamVersionFactory(exam=exam_2),
        date_marked=timezone.now(),
        result=10,
    )
    user_exam_version_3 = UserExamVersionFactory(
        user=user,
        exam_version=ExamVersionFactory(exam=exam_2),
        date_marked=timezone.now(),
        result=18,
    )
    with django_assert_num_queries(65):
        course_results, summary = user_course.course_results()
    assert {
        "assessment_count": 7,
        "assessment_done": 1,
        "exam_count": 2,
        "exam_done": 1,
    } == summary
    assert [
        {
            "heading": "Achievement Test",
            "description": "General Procedures",
            "status": "50% RETAKE",
            "exam_pk": unit_exam_1.pk,
            "unit_pk": course_unit_1.pk,
            "user_exam_version_pk": user_exam_version_1.pk,
        },
        {
            "heading": "Assignment",
            "description": "General Procedures",
            "status": "100%",
            "exam_pk": None,
            "unit_pk": course_unit_1.pk,
            "user_exam_version_pk": None,
        },
        {
            "heading": "Assessments",
            "description": "Litigation",
            "status": "Awaiting Completion",
            "exam_pk": None,
            "unit_pk": course_unit_2.pk,
            "user_exam_version_pk": None,
        },
        {
            "heading": "Achievement Test",
            "description": "English Legal",
            "status": "90% PASS",
            "exam_pk": unit_exam_3.pk,
            "unit_pk": course_unit_3.pk,
            "user_exam_version_pk": user_exam_version_3.pk,
        },
        {
            "heading": "Assignment",
            "description": "English Legal",
            "status": "Awaiting Completion",
            "exam_pk": None,
            "unit_pk": course_unit_3.pk,
            "user_exam_version_pk": None,
        },
        {
            "heading": "Assessment",
            "description": "Matrimonial",
            "status": "Submitted",
            "exam_pk": None,
            "unit_pk": course_unit_6.pk,
            "user_exam_version_pk": None,
        },
        {
            "heading": "Assessment",
            "description": "Consumer Law",
            "status": "Retake Submitted",
            "exam_pk": None,
            "unit_pk": course_unit_7.pk,
            "user_exam_version_pk": None,
        },
        {
            "heading": "Assessment",
            "description": "Commercial Law",
            "status": "Awaiting Retake",
            "exam_pk": None,
            "unit_pk": course_unit_8.pk,
            "user_exam_version_pk": None,
        },
        {
            "heading": "Assessment",
            "description": "Arable Farming",
            "status": "Awaiting Completion",
            "exam_pk": None,
            "unit_pk": course_unit_9.pk,
            "user_exam_version_pk": None,
        },
        # Unit does not have an assessment, so should not be included
        # {
        #     "heading": "Assessments",
        #     "description": "Wills and Probate",
        #     "status": "Awaiting Completion",
        #     "exam_pk": None,
        #     "unit_pk": course_unit_4.pk,
        # },
    ] == course_results


@pytest.mark.django_db
def test_create_user_course():
    user = UserFactory()
    course = CourseFactory()
    obj = UserCourse.objects.create_user_course(user, course)
    assert bool(obj.pk) is True
    assert user == obj.user
    assert course == obj.course
    assert obj.assessor is None
    assert "" == obj.assessor_comments


# from django.db.utils import IntegrityError
# @pytest.mark.django_db
# def test_create_user_course_duplicate_course():
#    """A user should only be able to take a course once."""
#    user = UserFactory()
#    course = CourseFactory()
#    UserCourse.objects.create_user_course(user, course)
#    with pytest.raises(IntegrityError):
#        UserCourse.objects.create_user_course(user, course)


@pytest.mark.django_db
def test_current():
    course = CourseFactory()
    UserCourseFactory(course=course, user=UserFactory(username="u1"))
    UserCourseFactory(
        course=course, user=UserFactory(username="u2")
    ).set_deleted(UserFactory())
    UserCourseFactory(course=course, user=UserFactory(username="u3"))
    UserCourseFactory(
        course=course,
        user=UserFactory(username="u4"),
        cancellation_date=datetime.date.today(),
    )
    assert set(["u1", "u3"]) == set(
        [x.user.username for x in UserCourse.objects.current()]
    )


@pytest.mark.django_db
def test_current_include_cancelled():
    course = CourseFactory()
    UserCourseFactory(course=course, user=UserFactory(username="u1"))
    UserCourseFactory(
        course=course, user=UserFactory(username="u2")
    ).set_deleted(UserFactory())
    UserCourseFactory(course=course, user=UserFactory(username="u3"))
    UserCourseFactory(
        course=course,
        user=UserFactory(username="u4"),
        cancellation_date=datetime.date.today(),
    )
    assert set(["u1", "u3", "u4"]) == set(
        [
            x.user.username
            for x in UserCourse.objects.current(include_cancelled=True)
        ]
    )


@pytest.mark.django_db
def test_current_marking(django_assert_num_queries):
    """List of courses which are marked (or partly marked)."""
    course = CourseFactory()
    user_course_1 = UserCourseFactory(
        course=course, user=UserFactory(username="uc1")
    )
    UserUnitFactory(user_course=user_course_1, result=4)
    user_course_2 = UserCourseFactory(
        course=course, user=UserFactory(username="uc2")
    )
    user_course_2.set_deleted(UserFactory())
    UserUnitFactory(user_course=user_course_2, result=4)
    user_course_3 = UserCourseFactory(
        course=course, user=UserFactory(username="uc3")
    )
    UserUnitFactory(user_course=user_course_3, result=4)
    user_course_4 = UserCourseFactory(
        course=course,
        user=UserFactory(username="uc4"),
        cancellation_date=datetime.date.today(),
    )
    UserUnitFactory(user_course=user_course_4, result=4)
    user_course_5 = UserCourseFactory(
        course=course, user=UserFactory(username="uc5")
    )
    UserUnitFactory(user_course=user_course_5, result=None)
    with django_assert_num_queries(3):
        result = [x.user.username for x in UserCourse.objects.current_marking()]
    assert set(["uc1", "uc3"]) == set(result)


@pytest.mark.django_db
def test_current_user():
    user = UserFactory()
    UserCourseFactory(course=CourseFactory(name="c1"), user=user)
    UserCourseFactory(course=CourseFactory(name="c2"), user=user).set_deleted(
        UserFactory()
    )
    UserCourseFactory(course=CourseFactory(name="c3"), user=UserFactory())
    UserCourseFactory(course=CourseFactory(name="c4"), user=user)
    set(["u1", "u3"]) == set(
        [x.course.name for x in UserCourse.objects.current(user=user)]
    )


@pytest.mark.django_db
def test_days_after_enrol():
    with freeze_time(
        datetime.datetime(2023, 10, 1, 1, 1, 1, tzinfo=datetime.timezone.utc)
    ):
        user_course = UserCourseFactory()
    with freeze_time(
        datetime.datetime(2023, 10, 4, 1, 1, 1, tzinfo=datetime.timezone.utc)
    ):
        assert 3 == user_course.days_after_enrol


@pytest.mark.django_db
def test_enrol():
    """`assessor` is null. `assessor` defaults to blank."""
    x = UserCourse.objects.enrol(UserFactory(), CourseFactory())
    assert x.assessor is None


@pytest.mark.django_db
def test_enrol_mark():
    """`assessor` is null. `assessor` defaults to blank."""
    x = UserCourse.objects.enrol(UserFactory(), CourseFactory())
    assert x.assessor is None
    assert "" == x.assessor_comments
    user = UserFactory()
    x.mark(user, "Apples")
    assert user == x.assessor
    assert "Apples" == x.assessor_comments


@pytest.mark.django_db
def test_enrol_two_users():
    course_1 = CourseFactory()
    course_2 = CourseFactory()
    user_1 = UserFactory()
    user_2 = UserFactory()
    x1 = UserCourse.objects.enrol(user_1, course_1)
    x2 = UserCourse.objects.enrol(user_2, course_2)
    assert int(x1.pk) > 0
    assert user_1 == x1.user
    assert course_1 == x1.course
    assert int(x2.pk) > 0
    assert user_2 == x2.user
    assert course_2 == x2.course


@pytest.mark.django_db
def test_init_user_course():
    """Initialised correctly."""
    user = UserFactory()
    course = CourseFactory()
    x1 = UserCourse.objects.init_user_course(user, course)
    x2 = UserCourse.objects.init_user_course(user, course)
    assert x1.pk == x2.pk


@pytest.mark.django_db
def test_init_user_course_duplicate():
    """Initialised correctly."""
    user = UserFactory(username="pkimber")
    course = CourseFactory(name="Farming")
    UserCourseFactory(user=user, course=course)
    UserCourseFactory(user=user, course=course)
    with pytest.raises(ExamError) as e:
        UserCourse.objects.init_user_course(user, course)
    assert (
        "ExamError, Found more than one 'UserCourse' records "
        f"for 'pkimber' for the Farming course ({course.pk})"
    ) in str(e.value)


@pytest.mark.django_db
def test_is_cancelled():
    user_course = UserCourseFactory(cancellation_date=None)
    assert user_course.is_cancelled() is False


@pytest.mark.django_db
def test_is_cancelled_not():
    user_course = UserCourseFactory(
        cancellation_date=datetime.date(2013, 3, 30)
    )
    assert user_course.is_cancelled() is True


@pytest.mark.django_db
def test_is_complete():
    """Is the course complete (or cancelled)?

    .. tip:: For ``_can_send_email`` (in ``mentor/service.py``).

    """
    user_course = UserCourseFactory(
        assessed=timezone.now(), cancellation_date=None
    )
    assert user_course.is_complete() is True


@pytest.mark.django_db
def test_is_complete_cancelled():
    """Is the course complete (or cancelled)?

    .. tip:: For ``_can_send_email`` (in ``mentor/service.py``).

    """
    user_course = UserCourseFactory(
        assessed=None, cancellation_date=timezone.now()
    )
    assert user_course.is_complete() is True


@pytest.mark.django_db
def test_is_complete_not():
    """Is the course complete (or cancelled)?

    .. tip:: For ``_can_send_email`` (in ``mentor/service.py``).

    """
    user_course = UserCourseFactory(assessed=None, cancellation_date=None)
    assert user_course.is_complete() is False


@pytest.mark.django_db
def test_is_complete_and_ready_for_final_result(django_assert_num_queries):
    """Has the student completed all units and exams?"""
    user = UserFactory()
    course = CourseFactory()
    # units
    unit_1 = UnitFactory()
    unit_2 = UnitFactory()
    unit_3 = UnitFactory()
    CourseUnitFactory(course=course, unit=unit_1)
    CourseUnitFactory(course=course, unit=unit_2, core=False)
    CourseUnitFactory(course=course, unit=unit_3, core=False)
    user_course = UserCourseFactory(course=course, user=user)
    UserUnitFactory(
        user_course=user_course, unit=unit_1, assessed=timezone.now()
    )
    # the user didn't select 'unit_2'
    UserUnitFactory(
        user_course=user_course, unit=unit_3, assessed=timezone.now()
    )
    # exam
    exam = ExamFactory()
    UnitExamFactory(unit=unit_1, exam=exam)
    exam_version = ExamVersionFactory(exam=exam)
    UserExamVersionFactory(user=user, exam_version=exam_version, result=45)
    # test
    with django_assert_num_queries(4):
        assert user_course.is_complete_and_ready_for_final_result() is True


@pytest.mark.django_db
def test_is_complete_and_ready_for_final_result_not_user_exam(
    django_assert_num_queries,
):
    """Has the student completed all units and exams?

    In this test, the user has not taken the exam (``UserExamVersionFactory``).

    """
    user = UserFactory()
    course = CourseFactory()
    # unit
    unit = UnitFactory()
    CourseUnitFactory(course=course, unit=unit)
    user_course = UserCourseFactory(course=course, user=user)
    UserUnitFactory(user_course=user_course, unit=unit, assessed=timezone.now())
    # exam
    exam = ExamFactory()
    UnitExamFactory(unit=unit, exam=exam)
    # test
    with django_assert_num_queries(4):
        assert user_course.is_complete_and_ready_for_final_result() is False


@pytest.mark.django_db
def test_is_complete_and_ready_for_final_result_not_user_exam_result(
    django_assert_num_queries,
):
    """Has the student completed all units and exams?

    In this test, the user has not taken the exam (``UserExamVersionFactory``).

    """
    user = UserFactory()
    course = CourseFactory()
    # unit
    unit = UnitFactory()
    CourseUnitFactory(course=course, unit=unit)
    user_course = UserCourseFactory(course=course, user=user)
    UserUnitFactory(user_course=user_course, unit=unit, assessed=timezone.now())
    # exam
    exam = ExamFactory()
    UnitExamFactory(unit=unit, exam=exam)
    exam_version = ExamVersionFactory(exam=exam)
    UserExamVersionFactory(user=user, exam_version=exam_version, result=None)
    # test
    with django_assert_num_queries(4):
        assert user_course.is_complete_and_ready_for_final_result() is False


@pytest.mark.django_db
def test_is_complete_and_ready_for_final_result_not_user_unit(
    django_assert_num_queries,
):
    """Has the student completed all units and exams?

    In this test, the user has not taken the unit (``UserUnitFactory``).

    """
    user = UserFactory()
    course = CourseFactory()
    # unit
    unit = UnitFactory()
    CourseUnitFactory(course=course, unit=unit)
    user_course = UserCourseFactory(course=course, user=user)
    UserUnitFactory(user_course=user_course, unit=unit, assessed=None)
    # test
    with django_assert_num_queries(2):
        assert user_course.is_complete_and_ready_for_final_result() is False


@pytest.mark.django_db
def test_is_lms():
    user_course = UserCourseFactory()
    assert user_course.is_lms() is True


@pytest.mark.django_db
def test_is_user_enrolled():
    course = CourseFactory()
    user = UserFactory()
    user_course = UserCourseFactory(user=user, course=course)
    assert user_course == UserCourse.objects.is_user_enrolled(user, course)
    # check not enrolled for another course
    assert UserCourse.objects.is_user_enrolled(user, CourseFactory()) is None


@pytest.mark.django_db
def test_is_user_enrolled_cancelled():
    course = CourseFactory()
    user = UserFactory()
    today = datetime.date.today()
    UserCourseFactory(user=user, course=course, cancellation_date=today)
    user_course = UserCourseFactory(user=user, course=course)
    user_course.set_deleted(UserFactory())
    UserCourseFactory(user=user, course=course, cancellation_date=today)
    assert UserCourse.objects.is_user_enrolled(user, course) is None


@pytest.mark.django_db
def test_is_user_enrolled_deleted():
    course = CourseFactory()
    user = UserFactory()
    user_course = UserCourseFactory(user=user, course=course)
    user_course.set_deleted(UserFactory())
    assert UserCourse.objects.is_user_enrolled(user, course) is None


@pytest.mark.django_db
def test_is_user_enrolled_multi_courses():
    course = CourseFactory()
    user = UserFactory(username="pkimber")
    UserCourseFactory(user=user, course=course)
    UserCourseFactory(user=user, course=course)
    with pytest.raises(ExamError) as e:
        UserCourse.objects.is_user_enrolled(user, course)
    assert "Student 'pkimber' is enrolled on more than one course" in str(
        e.value
    )


@pytest.mark.django_db
def test_is_user_enrolled_not():
    course = CourseFactory()
    user = UserFactory()
    assert UserCourse.objects.is_user_enrolled(user, course) is None


@pytest.mark.django_db
def test_is_user_enrolled_staff():
    course = CourseFactory()
    user = UserFactory(is_staff=True)
    assert UserCourse.objects.is_user_enrolled(user, course) is None


@pytest.mark.django_db
def test_is_user_enrolled_or_staff():
    course = CourseFactory()
    user = UserFactory()
    user_course = UserCourseFactory(user=user, course=course)
    assert user_course.is_user_enrolled_or_staff(user) is True


@pytest.mark.django_db
def test_is_user_enrolled_or_staff_anonymous():
    user_course = UserCourseFactory(user=UserFactory(), course=CourseFactory())
    assert user_course.is_user_enrolled_or_staff(AnonymousUser) is False


@pytest.mark.django_db
def test_is_user_enrolled_or_staff_cancelled():
    course = CourseFactory()
    user = UserFactory()
    user_course = UserCourseFactory(
        user=user, course=course, cancellation_date=datetime.date.today()
    )
    assert user_course.is_user_enrolled_or_staff(user) is False


@pytest.mark.django_db
def test_is_user_enrolled_or_staff_deleted():
    course = CourseFactory()
    user = UserFactory()
    user_course = UserCourseFactory(user=user, course=course)
    user_course.set_deleted(UserFactory())
    assert user_course.is_user_enrolled_or_staff(user) is False


@pytest.mark.django_db
def test_is_user_enrolled_or_staff_not():
    user = UserFactory()
    course = CourseFactory()
    user_course = UserCourseFactory(user=UserFactory(), course=CourseFactory())
    assert user_course.is_user_enrolled_or_staff(UserFactory()) is False


@pytest.mark.django_db
def test_is_user_enrolled_or_staff_staff():
    user_course = UserCourseFactory(user=UserFactory(), course=CourseFactory())
    assert (
        user_course.is_user_enrolled_or_staff(UserFactory(is_staff=True))
        is True
    )


@pytest.mark.django_db
def test_str():
    course = CourseFactory(name="Apple")
    user = UserFactory(username="patrick")
    user_course = UserCourseFactory(user=user, course=course)
    assert "patrick: Apple" == str(user_course)


@pytest.mark.django_db
def test_user_units_marking():
    course = CourseFactory()
    unit_1 = UnitFactory(
        name="1. Procedures",
        assignment_or_assessment=Unit.ASSIGNMENT,
    )
    # has no 'UserUnit', so will not be included
    unit_2 = UnitFactory(name="2. Litigation")
    unit_9 = UnitFactory(name="9. Farming")
    CourseUnitFactory(
        course=course, unit=unit_1, order=1, name="11. General Procedures"
    )
    CourseUnitFactory(
        course=course, unit=unit_2, order=2, name="22. Civil Litigation"
    )

    CourseUnitFactory(
        course=course, unit=unit_9, order=9, name="99. Arable Farming"
    )
    user = UserFactory()
    user_course = UserCourseFactory(user=user, course=course)
    user_unit_1 = UserUnitFactory(
        user_course=user_course,
        unit=unit_1,
        result=100,
        assessed=timezone.now(),
        result_assessed=None,
        result_assessor=UserFactory(
            first_name="P", last_name="Kimber", username="user-1"
        ),
    )
    user_unit_9 = UserUnitFactory(
        user_course=user_course,
        unit=unit_9,
        result=99,
        result_assessed=datetime.datetime(
            2023, 10, 12, 6, 0, 0, tzinfo=datetime.timezone.utc
        ),
        assessed=None,
    )
    result = user_course.user_units_marking()
    assert [
        {
            "result": 100,
            "result_assessed": "",
            "result_assessor": "P Kimber",
            "unit_name": "1. Procedures",
            "user_unit_pk": user_unit_1.pk,
        },
        {
            "result": 99,
            "result_assessed": "12/10/2023 06:00",
            "result_assessor": "",
            "unit_name": "9. Farming",
            "user_unit_pk": user_unit_9.pk,
        },
    ] == result
