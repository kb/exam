# -*- encoding: utf-8 -*-
import pytest

from django.core.management import call_command

from exam.models import get_contact_model
from exam.tests.factories import UserExamVersionFactory
from login.tests.factories import UserFactory


@pytest.mark.django_db
def test_exam_info():
    contact_model = get_contact_model()
    contact = contact_model(user=UserFactory())
    contact.save()
    UserExamVersionFactory(user=contact.user)
    call_command("exam_info", [contact.pk])


@pytest.mark.django_db
def test_init_app():
    call_command("init_app_exam")
