# -*- encoding: utf-8 -*-
import datetime
import logging
import os
import pathlib

from datetime import date
from django.apps import apps
from django.conf import settings
from django.db import models
from django.db.models import Count, Q
from django.urls import reverse
from django.utils import timezone
from reversion import revisions as reversion

from base.model_utils import (
    private_file_store,
    TimedCreateModifyDeleteModel,
    TimeStampedModel,
)
from base.singleton import SingletonModel
from gallery.models import Image, Wizard, WizardModelMixin
from mail.service import queue_mail_template
from stock.models import Product, ProductType


logger = logging.getLogger(__name__)


def isalnum_or_dash(x):
    return x.isalnum() or x == "-"


def _tidy_name_form_file_name(name):
    result = name.replace("|", " ")
    result = " ".join(result.split())
    result = result.replace(" ", "-")
    result = "".join(filter(isalnum_or_dash, result))
    return result


def check_perm_user_exam_version(user, user_exam_version):
    """Check permissions using the LMS tables."""
    result = user.is_staff
    if not result:
        result = Exam.objects.check_perm(
            user, user_exam_version.exam_version.exam
        )
    return result


def check_perm_user_unit(user, unit):
    """Is the user enroled on a course for this unit?"""
    result = user.is_staff
    if not result:
        # list of courses using this unit
        unit_course_pks = CourseUnit.objects.courses_for_unit(unit).values_list(
            "course__pk", flat=True
        )
        # list of courses for this user
        user_course_pks = UserCourse.objects.current(user=user).values_list(
            "course__pk", flat=True
        )
        # is the user enroled on any of these courses?
        result = bool(set(unit_course_pks).intersection(user_course_pks))
    return result


def get_contact_model():
    return apps.get_model(settings.CONTACT_MODEL)


def get_course_guidance_model():
    """Course guidance.

    The course guidance is in the customers project, so the model might change.

    The model should have the following methods:

    - ``get_caption`` - description of the guidance
    - ``get_update_url`` - URL of the update view
    - ``has_guidance`` - ``True`` if guidance has been set for the course

    The model manager should have the following methods:

    - ``init_course_guidance`` - create or initialise guidance for the course

    """
    return apps.get_model(settings.LMS_COURSE_GUIDANCE_MODEL)


def is_staff_or_team_member(user):
    """Copied from 'qanda/models.py'."""
    result = user.is_staff
    if not result:
        result = is_team_member(user)
    return result


def is_team_member(user):
    """Copied from 'qanda/models.py'."""
    result = False
    if user.is_anonymous:
        pass
    else:
        contact_model = get_contact_model()
        try:
            contact = contact_model.objects.get(user=user)
            result = contact.is_team_member()
        except contact_model.DoesNotExist:
            pass
    return result


def is_team_member_not_staff(user):
    """Is this a team member (but not a member of staff).

    Copied from 'qanda/models.py'.

    """
    result = False
    if user.is_staff:
        pass
    else:
        result = is_team_member(user)
    return result


class ExamError(Exception):
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value

    def __str__(self):
        return repr("{}, {}".format(self.__class__.__name__, self.value))


class CourseManager(models.Manager):
    def create_course(self, name, is_tailored=None, order=None):
        if is_tailored is None:
            is_tailored = False
        x = self.model(
            name=name,
            is_tailored=is_tailored,
        )
        if order:
            x.order = order
        else:
            x.order = 0
        x.save()
        return x

    def current(self, user=None):
        qs = self.model.objects.exclude(deleted=True)
        if user is None:
            return qs.filter(is_tailored=False)
        else:
            course_pks = UserCourse.objects.filter(user=user).values_list(
                "course__pk", flat=True
            )
            return qs.filter(is_tailored=True).filter(pk__in=course_pks)

    def init_course(self, name, is_tailored=None, order=None):
        if is_tailored is None:
            is_tailored = False
        try:
            x = self.model.objects.get(name=name)
            if order:
                x.order = order
            x.is_tailored = is_tailored
            x.save()
        except self.model.DoesNotExist:
            x = self.create_course(name, is_tailored=is_tailored, order=order)
        return x


@reversion.register()
class Course(TimedCreateModifyDeleteModel, WizardModelMixin):
    """A training Course with many Units.

    Needs to be compatible with ``exam`` because the exams will be listed on
    the unit summary page (the exam uses ``ProductExam``).

    .. tip:: The ``WizardModelMixin`` is from the ``gallery`` app.
             This allows the user to browse for an image.

    """

    # 24/11/2022, Moved to the unit.
    # ASSIGNMENT_OR_ASSESSMENT_CHOICES = [
    #    ("assessment", "Assessment"),
    #    ("assignment", "Assignment"),
    # ]

    name = models.CharField(unique=True, max_length=100)
    title = models.CharField(max_length=100, blank=True)
    description = models.TextField(blank=True)
    # 24/11/2022, Moved to the unit.
    # assignment_or_assessment = models.CharField(
    #     choices=ASSIGNMENT_OR_ASSESSMENT_CHOICES,
    #     default="assessment",
    #     max_length=100,
    # )
    is_tailored = models.BooleanField()
    order = models.IntegerField(default=0)
    picture = models.ForeignKey(
        Image,
        related_name="+",
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )
    banner = models.ForeignKey(
        Image,
        related_name="+",
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )
    banner_mobile = models.ForeignKey(
        Image,
        related_name="+",
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )
    objects = CourseManager()

    class Meta:
        ordering = ("order", "name")
        verbose_name = "Course"
        verbose_name_plural = "Courses"

    def __str__(self):
        return str(self.name)

    def course_units(self):
        """List of course units for a course."""
        return (
            self.courseunit_set.filter(course=self)
            .exclude(deleted=True)
            .exclude(unit__deleted=True)
        )

    # 24/11/2022, Moved to the unit.
    # def get_assignment_or_assessment(self, assessment_count):
    #    """Does this course have assignments or assessments?"""
    #    result = self.assignment_or_assessment.lower()
    #    if assessment_count and assessment_count > 1:
    #        result = f"{result}s"
    #    return result

    def prev_unit(self, current_unit):
        course_unit = CourseUnit.objects.get(course=self, unit=current_unit)
        return course_unit.prev_unit()

    def units(self):
        """List of units for a course.

        .. tip:: To get the ``order``, use the ``course_units`` method.

        """
        unit_pks = self.course_units().values_list("unit__pk")
        return Unit.objects.filter(pk__in=unit_pks).exclude(deleted=True)

    # properties and methods for the image wizard
    def get_design_url(self):
        return self.get_absolute_url()

    def get_absolute_url(self):
        return reverse("exam.course.detail", args=[self.pk])

    def set_pending_edit(self):
        return True

    @property
    def wizard_fields(self):
        return [
            Wizard("picture", Wizard.IMAGE, Wizard.SINGLE),
            Wizard("banner", Wizard.IMAGE, Wizard.SINGLE),
            Wizard("banner_mobile", Wizard.IMAGE, Wizard.SINGLE),
        ]


class CourseProductManager(models.Manager):
    def create_course_product(self, course, product):
        obj = self.model(course=course, product=product)
        obj.save()
        return obj

    def current(self):
        return self.model.objects.exclude(deleted=True)

    def for_course(self, course):
        return self.current().filter(course=course)

    def get_course(self, product):
        """Find the course for a product.

        .. note:: If we can find a course, then we use the LMS system to
                  create a course for the user.

        """
        result = None
        try:
            course_product = self.model.objects.get(product=product)
            result = course_product.course
        except self.model.DoesNotExist:
            pass
        return result

    def init_course_product(self, course, product, order=None):
        try:
            x = self.model.objects.get(product=product)
            if x.is_deleted:
                x.undelete()
            if course == x.course:
                pass
            else:
                raise ExamError(
                    "Cannot link '{}' to '{}' "
                    "(it is already linked to '{}')".format(
                        course.name, x.product.name, x.course.name
                    )
                )
        except self.model.DoesNotExist:
            x = self.create_course_product(course, product)
        return x


@reversion.register()
class CourseProduct(TimedCreateModifyDeleteModel):
    """The product for a course.

    Different products can link to the same course e.g. The *Diploma* course
    can have two products, *Diploma (Online)* and *Diploma (Live)*

    TODO:

    - Probably need to remove (or update) (or wait for it to become redundant)
      the ``ProductExam`` model (inheritance)?

    - The ``ProductExam`` model links to an ``Exam`` not a ``Course``, so I
      don't think it will become redundant.... **unless** we link an ``Exam``
      to a ``ProductCourse`` (or a ``Course`` to a ``ProductExam``)?

    """

    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    product = models.OneToOneField(Product, on_delete=models.CASCADE)
    objects = CourseProductManager()

    class Meta:
        ordering = ("course__order", "product__name")
        verbose_name = "Product course"
        verbose_name_plural = "Product courses"

    def __str__(self):
        return "{}: {}".format(self.course, self.product)


class UnitManager(models.Manager):
    def create_unit(self, name):
        x = self.model(name=name)
        x.save()
        return x

    def current(self, course=None):
        if course:
            qs = course.units()
        else:
            qs = self.model.objects.exclude(deleted=True)
        return qs

    def current_with_assessment(self, course):
        """Current units with assessments.

        If a unit has no assessments, then it can be removed from the marking
        section:

        - https://www.kbsoftware.co.uk/crm/ticket/5531/
        - https://www.kbsoftware.co.uk/crm/ticket/6465/

        """
        units = self.current(course)
        units_with_assessment = (
            Assessment.objects.exclude(deleted=True)
            .filter(unit__in=units)
            .values_list("unit__pk", flat=True)
        )
        return units.filter(pk__in=units_with_assessment)

    def current_with_assessment_count(self):
        qs = self.current()
        return qs.annotate(
            count_assessment=Count(
                "assessment", filter=~Q(assessment__deleted=True)
            )
        )


@reversion.register()
class Unit(TimedCreateModifyDeleteModel, WizardModelMixin):
    """A `Course` Unit with many Assessments Users are expect to pass."""

    ASSIGNMENT = "assignment"
    ASSESSMENT = "assessment"
    ASSIGNMENT_OR_ASSESSMENT_CHOICES = [
        (ASSESSMENT, "Assessment"),
        (ASSIGNMENT, "Assignment"),
    ]

    name = models.CharField(
        max_length=100,
        help_text="Internal name of the unit - students will never see it",
    )
    description = models.TextField(blank=True)
    assignment_or_assessment = models.CharField(
        choices=ASSIGNMENT_OR_ASSESSMENT_CHOICES,
        default=ASSESSMENT,
        max_length=100,
    )
    course_material = models.FileField(
        upload_to="exam/unit/course/material",
        storage=private_file_store,
        blank=True,
    )
    picture = models.ForeignKey(
        Image,
        related_name="+",
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )
    banner = models.ForeignKey(
        Image,
        related_name="+",
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )
    banner_mobile = models.ForeignKey(
        Image,
        related_name="+",
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )
    objects = UnitManager()

    class Meta:
        ordering = ("name",)
        # unique_together = ("course", "order")
        verbose_name = "Unit"
        verbose_name_plural = "Units"

    def __str__(self):
        return "{}".format(self.name)

    def assessments(self):
        return self.assessment_set.exclude(deleted=True).order_by("order")

    def get_assignment_or_assessment(self, assessment_count=None):
        """Does this unit have assignments or assessments?"""
        result = self.assignment_or_assessment.lower()
        if assessment_count and assessment_count > 1:
            result = f"{result}s"
        return result

    def is_assignment(self):
        return self.assignment_or_assessment == self.ASSIGNMENT

    def unit_exams(self):
        return (
            self.unitexam_set.exclude(deleted=True)
            .exclude(exam__deleted=True)
            .order_by("exam__name")
        )

    # properties and methods for the image wizard
    def get_design_url(self):
        return reverse("exam.unit.detail", args=[self.pk])

    def get_absolute_url(self):
        return reverse("exam.unit.detail", args=[self.pk])

    def set_pending_edit(self):
        return True

    @property
    def wizard_fields(self):
        return [
            Wizard("picture", Wizard.IMAGE, Wizard.SINGLE),
            Wizard("banner", Wizard.IMAGE, Wizard.SINGLE),
            Wizard("banner_mobile", Wizard.IMAGE, Wizard.SINGLE),
        ]


class CourseUnitManager(models.Manager):
    def create_course_unit(self, course, unit, order):
        x = self.model(course=course, unit=unit, order=order)
        x.save()
        return x

    def init_course_unit(self, course, unit, order):
        try:
            x = self.model.objects.get(course=course, unit=unit)
            x.order = order
            x.save()
        except self.model.DoesNotExist:
            x = self.create_course_unit(course, unit, order)
        return x

    def courses_for_unit(self, unit):
        return (
            self.model.objects.filter(unit=unit)
            .exclude(course__deleted=True)
            .exclude(unit__deleted=True)
        )

    def for_course(self, course):
        """List of course units for a course."""
        return (
            self.model.objects.filter(course=course)
            .exclude(deleted=True)
            .exclude(unit__deleted=True)
        )

    def units_available(self, course):
        """List of units which can be linked to the course."""
        unit_pks = self.for_course(course).values_list("unit__pk", flat=True)
        return Unit.objects.exclude(pk__in=unit_pks).exclude(deleted=True)


@reversion.register()
class CourseUnit(TimedCreateModifyDeleteModel):
    """Link a unit to a course.

    .. note:: ``core`` units are added to the course when it is created.
              https://www.kbsoftware.co.uk/crm/ticket/6411/

    """

    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    unit = models.ForeignKey(Unit, on_delete=models.CASCADE)
    name = models.CharField(
        max_length=100,
        help_text="Name of the unit for the student",
    )
    core = models.BooleanField(
        default=True, help_text="Default unit for this course"
    )
    product = models.ForeignKey(
        Product, blank=True, null=True, on_delete=models.CASCADE
    )
    order = models.IntegerField()
    objects = CourseUnitManager()

    class Meta:
        ordering = ("course__name", "order", "unit__name")
        unique_together = ("course", "unit")
        verbose_name = "Course Unit"
        verbose_name_plural = "Course Units"

    def __str__(self):
        return "{} for {}".format(self.unit.name, self.course.name)

    def prev_unit(self):
        if self.order < 2:
            return None
        course_unit = (
            CourseUnit.objects.filter(course=self.course, order__lt=self.order)
            .exclude(deleted=True)
            .exclude(unit__deleted=True)
            .order_by("-order", "unit__name")
            .first()
        )
        if course_unit:
            return course_unit.unit
        else:
            return None


class AssessmentManager(models.Manager):
    def create_assessment(self, unit, order, name, instructions, what_to_do):
        obj = self.model(
            unit=unit,
            order=order,
            name=name,
            instructions=instructions,
            what_to_do=what_to_do,
        )
        obj.save()
        return obj

    def current(self, unit):
        return self.model.objects.exclude(deleted=True).filter(unit=unit)

    def init_assessment(self, unit, order, name, instructions, what_to_do):
        try:
            obj = self.model.objects.get(unit=unit, order=order)
            obj.name = name
            obj.instructions = instructions
            obj.what_to_do = what_to_do
            obj.save()
        except self.model.DoesNotExist:
            obj = self.create_assessment(
                unit, order, name, instructions, what_to_do
            )
        return obj


@reversion.register()
class Assessment(TimedCreateModifyDeleteModel):
    """The model of a Unit's Assessment which a User will need to pass."""

    ASSESSMENT_UPLOADED = "u"
    ASSESSMENT_WRITTEN = "w"
    ASSESSMENT_TYPES = (
        (ASSESSMENT_UPLOADED, "Uploaded"),
        (ASSESSMENT_WRITTEN, "Written"),
    )

    # Update the 'order' on deleted assessments.
    # See ``_update_order_on_deleted``in ``exam/forms.py``.
    HIGH_ORDER = 999999

    unit = models.ForeignKey(
        Unit, blank=False, null=False, on_delete=models.CASCADE
    )
    order = models.IntegerField()
    name = models.CharField(max_length=100)
    instructions = models.TextField()
    what_to_do = models.CharField(
        max_length=150,
        help_text=(
            "Specific instruction for the student "
            "e.g. Upload completed AP1 form"
        ),
    )
    assessment_type = models.CharField(
        max_length=1, choices=ASSESSMENT_TYPES, default=ASSESSMENT_UPLOADED
    )
    assessment_criteria = models.TextField(blank=True)
    # assessment_criteria = models.FileField(
    #     upload_to="exam/assessment/criteria", blank=True
    # )
    blank_form = models.FileField(
        upload_to="exam/assessment/blank",
        storage=private_file_store,
        blank=True,
    )
    example_form = models.FileField(
        upload_to="exam/assessment/example",
        storage=private_file_store,
        blank=True,
    )
    objects = AssessmentManager()

    class Meta:
        ordering = ("unit", "order", "name")
        unique_together = ("unit", "order")
        verbose_name = "Assessment"
        verbose_name_plural = "Assessments"

    def __str__(self):
        return "{} for {}".format(self.name, self.unit.name)

    def assessment_download_file_name(self):
        """File name for the assessment download.

        Also see:

        - ``blank_form_download_file_name``
        - ``example_form_download_file_name``

        https://www.kbsoftware.co.uk/crm/ticket/6961/

        """
        name = _tidy_name_form_file_name(self.name)
        assignment_or_assessment = (
            self.unit.assignment_or_assessment.capitalize()
        )
        return f"{name}-{assignment_or_assessment}.pdf"

    def blank_form_download_file_name(self):
        """File name for the blank form download.

        Also see:

        - ``assessment_download_file_name``
        - ``example_form_download_file_name``

        https://www.kbsoftware.co.uk/crm/ticket/6961/

        """
        suffix = pathlib.Path(self.blank_form.path).suffix
        name = _tidy_name_form_file_name(self.name)
        return f"{name}-Blank-Form{suffix}"

    def example_form_download_file_name(self):
        """File name for the example form download.

        Also see:

        - ``assessment_download_file_name``
        - ``blank_form_download_file_name``

        https://www.kbsoftware.co.uk/crm/ticket/6961/

        """
        suffix = pathlib.Path(self.example_form.path).suffix
        name = _tidy_name_form_file_name(self.name)
        return f"{name}-Example-Form{suffix}"

    def is_uploaded(self):
        return self.assessment_type == self.ASSESSMENT_UPLOADED

    def is_user_enrolled_or_staff(self, user):
        """Is the user a member of staff or enrolled for this assessment?"""
        result = False
        if user.is_anonymous:
            pass
        elif user.is_staff:
            result = True
        else:
            for course_unit in CourseUnit.objects.courses_for_unit(self.unit):
                if UserCourse.objects.is_user_enrolled(
                    user, course_unit.course
                ):
                    result = True
                    break
        return result

    def is_written(self):
        return self.assessment_type == self.ASSESSMENT_WRITTEN


@reversion.register()
class ClassInformation(models.Model):
    """Diploma classes - evening classes in London and diploma with webinar
    classes option.

    An diploma class is just a start date and a Product.

    .. note:: Copied from the legacy ``DiplomaClass`` model.

    """

    start_date = models.DateField(unique=True)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    description = models.TextField(blank=True)

    class Meta:
        ordering = ["course__name", "-start_date"]
        verbose_name = "Class Information"

    def __str__(self):
        start_date = self.start_date.strftime("%d/%m/%Y")
        return f"{start_date}"


class ExamManager(models.Manager):
    def check_perm(self, user, exam):
        """Can the user take this exam (using LMS models)."""
        # courses for this user
        course_pks = UserCourse.objects.filter(user=user).values_list(
            "course__pk", flat=True
        )
        # units which include this exam
        unit_pks = UnitExam.objects.filter(exam=exam).values_list(
            "unit__pk", flat=True
        )
        # check the users courses include the unit
        count = CourseUnit.objects.filter(
            course__in=course_pks, unit__in=unit_pks
        )
        return bool(count)

    def exams(self):
        """List of published exams."""
        qs = ExamVersion.objects.filter(date_published__isnull=False)
        published = [obj.exam.pk for obj in qs]
        return self.model.objects.filter(pk__in=published).exclude(deleted=True)


@reversion.register()
class Exam(TimedCreateModifyDeleteModel):
    name = models.CharField(max_length=100)
    description = models.TextField(blank=True)
    objects = ExamManager()

    class Meta:
        ordering = ["name"]
        verbose_name = "Exam"
        verbose_name_plural = "Exams"

    def __str__(self):
        return self.name

    def create_questions(self, question_count=None):
        if not question_count:
            question_count = 20
        if ExamVersion.objects.filter(exam=self).count():
            raise ExamError(
                "Exam '{}' has existing versions. "
                "Cannot create questions.".format(self.pk)
            )
        exam_version = ExamVersion(exam=self)
        exam_version.save()
        for counter in range(0, question_count):
            question = Question(number=counter + 1, exam_version=exam_version)
            question.save()
            for option_number in range(1, 5):
                option = Option(number=option_number, question=question)
                option.save()
        return exam_version

    def current_version(self):
        """The current *published* version of the exam."""
        qs = (
            ExamVersion.objects.filter(exam=self, date_published__isnull=False)
            .exclude(deleted=True)
            .order_by("date_published")
        )
        return qs.last()

    def edit_version(self):
        """The version of the exam we are editing i.e. not published."""
        try:
            return ExamVersion.objects.get(
                exam=self, date_published__isnull=True
            )
        except ExamVersion.DoesNotExist:
            raise ExamError("Exam '{}' has no editing version".format(self.pk))

    def exam_versions(self):
        """List of exam versions.

        Returns a list of dictionaries with the following keys:
        pk -- primary key of the exam version
        created -- date created
        date_published -- date published
        total -- total number of students who have worked on the exam
        active -- number of students who have started, but not finished

        """
        qs = (
            UserExamVersion.objects.filter(exam_version__exam=self)
            .filter(result__isnull=True)
            .exclude(legacy_result=True)
            .values("exam_version__pk")
            .annotate(active=Count("pk"))
        )
        active = {}
        for item in qs:
            active[item["exam_version__pk"]] = item["active"]
        qs = (
            self.examversion_set.exclude(date_published__isnull=True)
            .annotate(total=Count("userexamversion"))
            .order_by("-created")
        )
        result = []
        for item in qs:
            active_count = active.get(item.pk, 0)
            result.append(
                dict(
                    pk=item.pk,
                    created=item.created,
                    date_published=item.date_published,
                    total=item.total,
                    active=active_count,
                )
            )
        return result

    def get_absolute_url(self):
        return reverse("exam.user.redirect", args=[self.pk])

    def init_exam(self, user):
        qs = UserExamVersion.objects.filter(user=user, exam_version__exam=self)
        if not qs.count():
            exam_version = self.current_version()
            if exam_version:
                UserExamVersion.objects.create_user_exam_version(
                    user, exam_version
                )
            else:
                raise ExamError(
                    "Exam '{}' has not been published".format(self.pk)
                )

    def user_exam_version(self, user):
        return (
            UserExamVersion.objects.filter(exam_version__exam=self, user=user)
            .order_by("-created")
            .first()
        )

    def user_exam_versions(self):
        return UserExamVersion.objects.filter(exam_version__exam=self).order_by(
            "-created"
        )


class UnitExamManager(models.Manager):
    def current(self, unit=None):
        qs = self.exclude(deleted=True)
        if unit:
            qs = qs.filter(unit=unit)
        return qs

    def create_unit_exam(self, unit, exam):
        obj = self.model(unit=unit, exam=exam)
        obj.save()
        return obj

    def init_unit_exam(self, unit, exam):
        try:
            obj = self.model.objects.get(unit=unit, exam=exam)
            obj.save()
        except self.model.DoesNotExist:
            obj = self.create_unit_exam(unit, exam)
        return obj


@reversion.register()
class UnitExam(TimedCreateModifyDeleteModel):
    """The final exam for a unit."""

    unit = models.ForeignKey(Unit, on_delete=models.CASCADE)
    exam = models.ForeignKey(Exam, on_delete=models.CASCADE)
    objects = UnitExamManager()

    class Meta:
        ordering = ("unit", "exam")
        unique_together = ("unit", "exam")
        verbose_name = "UnitExam"
        verbose_name_plural = "UnitExams"

    def __str__(self):
        return "{}: {}".format(self.unit, self.exam)


class ExamSettingsManager(models.Manager):
    def settings(self):
        try:
            return self.model.objects.get()
        except self.model.DoesNotExist:
            raise ExamError("Exam settings have not been set-up in admin")


@reversion.register()
class ExamSettings(SingletonModel):
    """Exam settings.

    Mostly instructions for the user for navigating through the test system:

    - Course, the product type which links to the exams e.g. the farm water
      supply test belongs to a course type of Agriculture.
    - Fail, but can retake (``fail_but_can_retake``) e.g:
      Unfortunately you haven't passed this time but you can retake the exam
      one more time after 24 hours have elapsed.
    - Retake warning (``retake_warning``) e.g.
      This is an exam re-take and is your last chance to take the test.
    - Fail retake (``fail_retake``) e.g:
      Unfortunately you haven't passed.
      Please contact us for advice on what to do next.
    - Passed (``pass``) e.g:
      Congratulations you have passed!

    """

    course = models.ForeignKey(
        ProductType,
        blank=True,
        null=True,
        help_text="Product type for a course",
        on_delete=models.CASCADE,
        related_name="+",
    )
    unit = models.ForeignKey(
        ProductType,
        blank=True,
        null=True,
        help_text="Product type for a unit",
        on_delete=models.CASCADE,
        related_name="+",
    )
    fail_but_can_retake = models.TextField(
        help_text="e.g. You failed, but can retake the test"
    )
    retake_warning = models.TextField(help_text="e.g. This is your last chance")
    fail_retake = models.TextField(help_text="e.g. Sorry, you failed")
    passed = models.TextField(help_text="e.g. Congratulations")
    objects = ExamSettingsManager()

    class Meta:
        verbose_name = "Exam settings"

    def __str__(self):
        course = ""
        if self.course:
            course = " Course: '{}'".format(self.course.name)
        return "Exam Settings: '{}'{}".format(self.pk, course)


class ExamVersionManager(models.Manager):
    def published(self, exam):
        return (
            self.model.objects.filter(exam=exam, date_published__isnull=False)
            .order_by("created")
            .last()
        )


@reversion.register()
class ExamVersion(TimedCreateModifyDeleteModel):
    exam = models.ForeignKey(Exam, on_delete=models.CASCADE)
    date_published = models.DateTimeField(blank=True, null=True)
    objects = ExamVersionManager()

    class Meta:
        verbose_name = "Exam Version"
        verbose_name_plural = "Exam Versions"

    def __str__(self):
        return "{}. {}".format(self.pk, self.exam.name)

    def _can_publish_questions(self):
        """Can we publish the questions for this version of the exam?"""
        result = True
        message = None
        for question in self.questions():
            # check the question isn't blank (strip spaces)
            if len(question.question.strip()) == 0:
                result = False
                message = "Question {} is blank".format(question.number)
                return result, message
            # check the options for each question - not blank (strip spaces)
            qs = Option.objects.filter(question=question)
            for option_row in qs:
                if len(option_row.option.strip()) == 0:
                    result = False
                    message = "Question {} Option {} is blank".format(
                        question.number, option_row.number
                    )
                    return result, message
            # check the question has an answer identified against an option
            if not question.answer:
                result = False
                message = "Question {} does not have an " "answer".format(
                    question.number
                )
                break
        return result, message

    def can_publish(self):
        """Can we publish this version of the exam?"""
        result = True
        message = None
        if self.date_published:
            result = False
            message = (
                "Version number '{}' has already "
                "been published".format(self.pk)
            )
        else:
            result, message = self._can_publish_questions()
        return result, message

    def is_published(self):
        return bool(self.date_published)

    def publish(self):
        """Publish the exam version.

        The publish button should only be visible if ``can_publish`` returns
        ``True``.

        """
        can_publish, message = self.can_publish()
        if not can_publish:
            raise ExamError(
                "Cannot publish exam version "
                "'{}' ({})".format(self.pk, message)
            )
        self.date_published = timezone.now()
        self.save()
        # Create new version in readiness after publishing
        new_version = ExamVersion(exam=self.exam)
        new_version.save()
        for old_question in self.questions():
            # save the pk of the old answer
            old_answer_pk = old_question.answer.pk
            new_question = Question(
                question=old_question.question,
                number=old_question.number,
                exam_version=new_version,
            )
            new_question.save()
            qs2 = Option.objects.filter(question=old_question)
            for old_option in qs2:
                new_option = Option(
                    question=new_question,
                    number=old_option.number,
                    option=old_option.option,
                )
                new_option.save()
                # is this option the old answer?
                if old_option.pk == old_answer_pk:
                    new_question.answer = new_option
                    new_question.save()

    def questions(self):
        qs = Question.objects.filter(exam_version=self).order_by("number")
        if not qs.count():
            raise ExamError("Version '{}' has no questions".format(self.pk))
        return qs

    def question_count(self):
        """Get the number of questions for this version of the exam."""
        return self.questions().count()

    def user_exam_versions(self):
        return (
            self.userexamversion_set.all()
            .exclude(legacy_result=True)
            .order_by("-created")
        )


@reversion.register()
class Question(TimeStampedModel):
    exam_version = models.ForeignKey(ExamVersion, on_delete=models.CASCADE)
    number = models.IntegerField()
    question = models.TextField()
    answer = models.ForeignKey(
        "Option",
        blank=True,
        null=True,
        related_name="answer",
        on_delete=models.CASCADE,
    )

    class Meta:
        unique_together = ["exam_version", "number"]
        verbose_name = "Exam Question"
        verbose_name_plural = "Exam Questions"

    def __str__(self):
        return "{}. {}".format(self.number, self.question[:30])

    def can_edit_answer(self):
        """Answers can be edited if the exam version hasn't been published."""
        return not bool(self.exam_version.date_published)

    def options(self):
        return Option.objects.filter(question=self).order_by("number")


@reversion.register()
class Option(TimeStampedModel):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    option = models.TextField()
    number = models.IntegerField()

    class Meta:
        unique_together = ["question", "number"]
        verbose_name = "Answer Option"
        verbose_name_plural = "Answer Options"

    def __str__(self):
        return "{}. {}".format(self.number, self.option[:30])


class ProductExamManager(models.Manager):
    def check_perm(self, exam, user, products):
        result = False
        # user courses (products)
        product_pks = [obj.pk for obj in products]
        # check exam products (courses)
        products = self.model.objects.products(exam)
        # exam permissions (products)
        perm_pks = [obj.pk for obj in products]
        if len(product_pks):
            # is the user doing a course linked to the exam?
            if set(product_pks).intersection(perm_pks):
                result = True
            else:
                message = (
                    "User '{}' does not have permission to take "
                    "test '{}'".format(user.username, exam.pk)
                )
                logger.error(message)
        else:
            message = "No exam products for exam '{}'".format(exam.pk)
            logger.error(message)
        return result

    def set_deleted(self, user, product, exam):
        obj = self.model.objects.get(product=product, exam=exam)
        obj.set_deleted(user)

    def init_product_exam(self, product, exam, order=None, legacy=None):
        """List of exams for a product (course).

        Keyword arguments:
        order -- on screen display order (default None)
        legacy -- created as part of a data migration (not set-up by the user)

        """
        try:
            obj = self.model.objects.get(product=product, exam=exam)
            if obj.is_deleted:
                obj.undelete()
            # do not set to ``legacy`` if the row already exists
            if order:
                if obj.order:
                    obj.order = order
                    obj.save()
            else:
                if obj.legacy:
                    obj.legacy = False
                    obj.save()
        except self.model.DoesNotExist:
            obj = self.model(product=product, exam=exam)
            if legacy:
                obj.legacy = True
            if order:
                obj.order = order
            else:
                obj.order = 0
            obj.save()
        return obj

    def is_valid(self, exam):
        return bool(self.model.objects.filter(exam=exam).count())

    def products(self, exam):
        qs = (
            self.model.objects.filter(exam=exam)
            .exclude(deleted=True)
            .exclude(legacy=True)
            .order_by("product__name")
        )
        return [obj.product for obj in qs]


@reversion.register()
class ProductExam(TimedCreateModifyDeleteModel):
    """Link an exam to a product (course).

    An exam can be linked to more than one product (course).

    An exam is linked to one or more products (courses).  A user can only take
    the exam if they are enrolled on one of these courses.

    We have an ``order`` to allow us to display tests in the same order as they
    were on a legacy system.

    """

    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    order = models.IntegerField()
    exam = models.ForeignKey(Exam, on_delete=models.CASCADE)
    legacy = models.BooleanField(default=False)
    objects = ProductExamManager()

    def __str__(self):
        return "{}".format(self.exam.name)

    class Meta:
        ordering = ["order", "exam__name"]
        unique_together = ["product", "exam"]
        verbose_name = "Product exam"
        verbose_name_plural = "Product exams"


class UserExamVersionManager(models.Manager):
    def create_user_exam_version(self, user, exam_version):
        obj = self.model(user=user, exam_version=exam_version)
        obj.save()
        return obj

    def create_user_exam_version_retake(self, old_user_exam_version):
        """The user has failed the first text - prepare the retake records.

        We copy any correct selections to the new test.  We mark these records
        by setting ``copy_of_previous`` to ``True``.

        The student will be asked to answer any question which was not copied
        from a previous test (``copy_of_previous == True``).

        """
        new_user_exam_version = self.create_user_exam_version(
            old_user_exam_version.user, old_user_exam_version.exam_version
        )
        for question in old_user_exam_version.questions():
            old_selection = old_user_exam_version.selection(question)
            if old_selection.correct_answer():
                new_selection = Selection(
                    user_exam_version=new_user_exam_version,
                    copy_of_previous=True,
                    answer=old_selection.answer,
                )
                new_selection.save()

    def user_exam_versions(self, user, exam):
        return self.model.objects.filter(
            user=user, exam_version__exam=exam
        ).order_by("-created")


@reversion.register()
class UserExamVersion(TimeStampedModel):
    """Result of a user taking a test.

    - ``legacy_result`` is set to ``True`` for results from old/legacy systems.
    - ``percent_override`` is used to change the displayed ``result`` (ignoring
      the actual ``result``.

    """

    MAIL_TEMPLATE_FAIL_INITIAL = "exam_fail_initial"
    MAIL_TEMPLATE_FAIL_RETAKE = "exam_fail_retake"
    MAIL_TEMPLATE_PASS = "exam_pass"

    exam_version = models.ForeignKey(ExamVersion, on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    date_marked = models.DateTimeField(blank=True, null=True)
    legacy_result = models.BooleanField(default=False)
    legacy_retake = models.BooleanField(default=False)
    result = models.IntegerField(blank=True, null=True)
    percent_override = models.IntegerField(blank=True, null=True)
    percent_override_user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        blank=True,
        null=True,
        related_name="+",
        on_delete=models.CASCADE,
    )
    percent_override_date = models.DateTimeField(blank=True, null=True)
    objects = UserExamVersionManager()

    class Meta:
        verbose_name = "User Exam"
        verbose_name_plural = "User Exams"

    def __str__(self):
        return "{}. {} {} {}".format(
            self.pk,
            self.user.username,
            self.exam_version.exam.name,
            self.result,
        )

    def _check(self):
        if self.result or self.percent_override or self.is_marked():
            raise ExamError(
                "The exam result for '{}' reference '{}' was set on "
                "'{}'".format(self.user.username, self.pk, self.date_marked)
            )

    def _check_question(self, question):
        """Check this is a valid question for this exam."""
        if not self.exam_version == question.exam_version:
            raise ExamError(
                "Question '{}' is not part of "
                "exam version '{}'".format(question.pk, self.exam_version.pk)
            )

    def _questions_copied_from_previous(self):
        """List of questions which were copied from the previous test."""
        qs = Selection.objects.filter(
            user_exam_version=self, copy_of_previous=True
        )
        pks = []
        for selection in qs:
            pks.append(selection.answer.question.pk)
        return pks

    def _questions_wrong_for_mail(self):
        result = None
        is_pass = self.is_pass()
        questions = []
        results_in_full = self.results_in_full(self.user)
        for question in results_in_full["questions"]:
            if question["wrong"]:
                questions.append(
                    "{}. {}".format(question["number"], question["question"])
                )
                if is_pass:
                    for option in question["options"]:
                        if option["correct_answer"]:
                            questions.append(
                                "<strong>The correct answer is:</strong>"
                                "<br>{}<br>".format(option["option"])
                            )
        if questions:
            result = "You answered the following questions incorrectly:<br>"
            result = result + "<br>".join(questions)
        return result

    def _result(self):
        result = 0
        for question in self.exam_version.questions():
            selection = self.selection(question)
            if selection.answer == question.answer:
                # the answer is correct
                result = result + 1
        return result

    def _results_in_full_questions(self, request_user):
        result = []
        is_pass = self.date_marked and self.is_pass()
        is_staff = request_user.is_staff
        for question in self.exam_version.questions().order_by("number"):
            try:
                selection = Selection.objects.get(
                    user_exam_version=self, answer__question=question
                )
            except Selection.DoesNotExist:
                selection = None
            options = []
            # 'copy_of_previous' a correct selection was made on previous test
            copy_of_previous = selection and selection.copy_of_previous
            for option in question.options():
                correct_answer = None
                if is_pass or is_staff or copy_of_previous:
                    correct_answer = question.answer == option
                options.append(
                    {
                        "number": option.number,
                        "option": option.option,
                        "user_selection": selection
                        and selection.answer == option,
                        "correct_answer": correct_answer,
                    }
                )
            is_right = None
            is_wrong = None
            if copy_of_previous or self.is_marked():
                is_right = question.answer == selection.answer
                is_wrong = not is_right
            result.append(
                {
                    "number": question.number,
                    "question": question.question,
                    "options": options,
                    "right": is_right,
                    "wrong": is_wrong,
                }
            )
        return result

    def _send_mail(self):
        data = dict(
            exam_name=self.exam_version.exam.name,
            name=self.user.first_name.capitalize(),
            questions=self._questions_wrong_for_mail() or " ",
            result=self.result,
            result_as_percent=self.result_as_percent(),
        )
        queue_mail_template(
            self, self._template_for_email(), {self.user.email: data}
        )

    def _template_for_email(self):
        result = None
        if self.is_pass():
            result = self.MAIL_TEMPLATE_PASS
        elif self.is_fail():
            if self.previous_test():
                result = self.MAIL_TEMPLATE_FAIL_RETAKE
            else:
                result = self.MAIL_TEMPLATE_FAIL_INITIAL
        else:
            raise ExamError(
                "User exam version '{}' has not been passed or failed. "
                "Cannot send result email.".format(self.pk)
            )
        return result

    def answered_a_question(self):
        return (
            Selection.objects.filter(user_exam_version=self)
            .exclude(copy_of_previous=True)
            .count()
        )

    def answers_remaining(self):
        questions = self.questions()
        number_of_answers = self.answered_a_question()
        number_answered_previously = Selection.objects.filter(
            user_exam_version=self, copy_of_previous=True
        ).count()
        number_of_questions = questions.count() - number_answered_previously
        return number_of_questions - number_of_answers

    def find_course_units(self):
        """Find the ``CourseUnit`` for this exam.

        .. note:: There may be more than one because an exam can link to
                  several units and the unit can link to several courses.

        """
        result = []
        exam = self.exam_version.exam
        unit_exam_qs = (
            UnitExam.objects.current().filter(exam=exam).order_by("-pk")
        )
        for unit_exam in unit_exam_qs:
            course_unit_qs = CourseUnit.objects.courses_for_unit(
                unit_exam.unit
            ).order_by("-pk")
            for course_unit in course_unit_qs:
                result.append(course_unit)
        return result

    def first_question(self):
        return self.next_question()

    def last_question(self):
        question_pks = self._questions_copied_from_previous()
        qs = self.questions()
        qs = qs.exclude(pk__in=question_pks)
        return qs.last()

    def get_answer(self, question):
        """Get the user's current answer for this question."""
        result = None
        self._check_question(question)
        try:
            obj = Selection.objects.get(
                user_exam_version=self, answer__question=question
            )
            result = obj.answer
        except Selection.DoesNotExist:
            pass
        return result

    def is_fail(self):
        """Did the user fail the exam?

        .. note:: Will return ``None`` if the result hasn't been set.

        """
        result = None
        percent = self.result_as_percent()
        if percent is None:
            pass
        else:
            result = not self.is_pass()
        return result

    def is_marked(self):
        result = False
        if self.percent_override_date or self.date_marked:
            result = True
        return result

    def is_pass(self):
        """
        This method is in the UserExamVersion class of exam.models

        Did the user pass the exam?

        .. note:: Will return ``None`` if the result hasn't been set.

        """
        result = None
        percent = self.result_as_percent()
        if percent is None:
            pass
        else:
            result = bool(percent >= 80)
        return result

    def mark(self):
        """Mark the exam.

        - Create a new user exam version if the user fails the exam and they
          haven't done a retake before.

        """
        self._check()
        self.set_result(self._result())
        if self.is_fail() and not self.previous_test():
            UserExamVersion.objects.create_user_exam_version_retake(self)
        self._send_mail()

    def next_question(self, question=None):
        """Get the next question.

        Keyword arguments:
        question -- if None, get the first question

        """
        qs = self.questions()
        if question:
            qs = qs.filter(number__gt=question.number)
        question_pks = self._questions_copied_from_previous()
        qs = qs.exclude(pk__in=question_pks)
        return qs.first()

    def next_url(self, question):
        next_question = self.next_question(question)
        if next_question:
            url = reverse(
                "web.exam.user.question", args=[self.pk, next_question.pk]
            )
        else:
            url = reverse("web.exam.user.version.update", args=[self.pk])
        return url

    def next_test(self):
        """Was this exam failed and the user is working on a retake?"""
        qs = UserExamVersion.objects.filter(
            user=self.user, exam_version=self.exam_version, pk__gt=self.pk
        ).order_by("created")
        return qs.last()

    def previous_test(self):
        """Is this an exam retake?"""
        qs = UserExamVersion.objects.filter(
            user=self.user, exam_version=self.exam_version, pk__lt=self.pk
        ).order_by("created")
        return qs.last()

    def questions(self):
        return self.exam_version.questions().order_by("number")

    def result_as_percent(self):
        """This is in UserExamVersion

        Convert the result into a percentage.

        If this is a retake, then the maximum result is 80% (for a retake we
        won't display the actual number of questions they got wrong).

        .. note:: Will return ``None`` if the result hasn't been set.

        """
        result = None
        if self.percent_override_date:
            result = self.percent_override
        elif self.date_marked:
            result = int(self.result / 20 * 100) if self.result else 0
            if self.previous_test() and result > 80:
                result = 80
        return result

    def result_as_text(self):
        """The result as text (see email below)::

        20/12/2022, email from Emma::

          The wording should be as follows:

          AWAITING COMPLETION (before a test has been submitted)
          80% (and above) PASS
          75% (and below) RETAKE
          80% (and above on their second attempt, with a max mark of 80% shown) PASS (RETAKE)
          75% (and below on their second attempt) FAIL

        .. tip:: There are 20 questions in the test (``exam``), so it is not
                 possible to get a result between 76 and 79!

        """
        result = []
        previous_test = self.previous_test()
        if self.is_marked():
            result.append(f"{self.result_as_percent()}%")
            if self.is_fail():
                result.append("FAIL")
            if self.is_pass():
                result.append("PASS")
            if previous_test:
                result.append("(RETAKE)")
        else:
            if previous_test and previous_test.is_marked():
                result.append(f"{previous_test.result_as_percent()}%")
                if previous_test.is_fail():
                    result.append("RETAKE")
                else:
                    raise ExamError(
                        "Found a retake, but the student passed the "
                        f"previous test ('user_exam_version': {self.pk})"
                    )
            else:
                result = ["Awaiting Completion"]
        return " ".join(result)

    def results_in_full(self, request_user):
        """Test results for a user.

        A student can always see:

        - their selection

        If the student has passed the test, they can see:

        - correct answer
        - their selection
        - is the selection right or wrong

        If the student has failed the test, they can see:

        - their selection
        - is the selection right or wrong

        If the student is doing a retake, they can see:

        - their correct answers from the previous test

        A member of staff can always see:

        - correct answer
        - user selection
        - is the selection right or wrong

        .. note:: Old notes

        .. note:: if the test has not been marked or has been failed, then the
                  ``correct_answer`` and ``user_answer`` will be set to
                  ``None``.

        """
        count = 0
        if self.legacy_result:
            questions = []
        else:
            questions = self._results_in_full_questions(request_user)
            for question in questions:
                if question["wrong"]:
                    count = count + 1
        return {
            "legacy": self.legacy_result,
            "questions": questions,
            "total_wrong": count,
        }

    def question_state(self):
        result = []
        for question in self.questions():
            try:
                selection = Selection.objects.get(
                    user_exam_version=self, answer__question=question
                )
                if not selection.copy_of_previous:
                    result.append((question, True))
            except Selection.DoesNotExist:
                result.append((question, False))
        return result

    def questions_wrong(self):
        pks = []
        for question in self.questions():
            selection = self.selection(question)
            if selection.correct_answer():
                pass
            else:
                # the answer was wrong, so add to the list of questions
                pks.append(question.pk)
        return (
            self.exam_version.questions().filter(pk__in=pks).order_by("number")
        )

    def ready_to_mark(self):
        result = not self.result
        for question, answer in self.question_state():
            if not answer:
                result = False
                break
        return result

    def selection(self, question):
        try:
            return Selection.objects.get(
                user_exam_version=self, answer__question=question
            )
        except Selection.DoesNotExist:
            raise ExamError(
                "User '{}' has not answered question "
                "'{}' ({})".format(self.user.username, question.pk, self.pk)
            )

    def set_answer(self, question, answer):
        self._check()
        self._check_question(question)
        if not answer.question == question:
            raise ExamError(
                "Answer '{}' is not an option for "
                "question '{}'".format(answer.pk, question.pk)
            )
        try:
            obj = Selection.objects.get(
                user_exam_version=self, answer__question=question
            )
            obj.answer = answer
            obj.save()
        except Selection.DoesNotExist:
            obj = Selection(user_exam_version=self, answer=answer)
            obj.save()
        return obj

    def set_result(self, result):
        self._check()
        self.result = result
        self.date_marked = timezone.now()
        self.save()

    def set_percent_override(
        self, percent, staff_user, legacy_result=None, legacy_retake=None
    ):
        if self.date_marked or self.legacy_result or legacy_result:
            self.percent_override = percent
            self.percent_override_date = timezone.now()
            self.percent_override_user = staff_user
            if legacy_result is not None:
                self.legacy_result = True
            if self.legacy_result and legacy_retake is not None:
                self.legacy_retake = legacy_retake
            self.save()
        else:
            raise ExamError(
                "You cannot override an exam result until it has been marked "
                "(or is a legacy result): '{}'".format(self.pk)
            )


@reversion.register()
class Selection(TimeStampedModel):
    """The answer selected by the user.

    To find the questions to ask the student, exclude any where
    ``copy_of_previous`` is set to ``True``.

    """

    user_exam_version = models.ForeignKey(
        UserExamVersion, on_delete=models.CASCADE
    )
    copy_of_previous = models.BooleanField(
        default=False, help_text="Correct selection made on previous test"
    )
    answer = models.ForeignKey(Option, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "User selection (answer)"
        verbose_name_plural = "User selections (answers)"

    def __str__(self):
        return "{}. {}".format(self.pk, self.answer.pk)

    def correct_answer(self):
        return self.answer and self.answer == self.answer.question.answer


class UserCourseManager(models.Manager):
    def create_user_course(self, user, course):
        obj = self.model(user=user, course=course)
        obj.save()
        return obj

    def current(self, user=None, include_cancelled=None):
        if user is None:
            qs = self.model.objects.all()
        else:
            qs = self.model.objects.filter(user=user)
        if include_cancelled:
            pass
        else:
            qs = qs.exclude(cancellation_date__isnull=False)
        return qs.exclude(deleted=True)

    def current_marking(self):
        """List of courses which are marked (or partly marked)."""
        user_course_pks = (
            UserUnit.objects.current()
            .filter(result__gte=0)
            .values_list("user_course__pk", flat=True)
        )
        return self.current().filter(pk__in=user_course_pks)

    def enrol(self, user, course):
        """For now, shadows `init_usercourse`."""
        return self.init_user_course(user, course)

    def init_user_course(self, user, course):
        try:
            obj = self.model.objects.get(user=user, course=course)
        except self.model.DoesNotExist:
            obj = self.create_user_course(user, course)
        except self.model.MultipleObjectsReturned:
            raise ExamError(
                "Found more than one 'UserCourse' records for "
                f"'{user.username}' for the {course.name} course "
                f"({course.pk})"
            )
        return obj

    def is_user_enrolled(self, user, course):
        result = None
        qs = (
            self.model.objects.filter(cancellation_date__isnull=True)
            .exclude(deleted=True)
            .filter(user=user, course=course)
        )
        count = qs.count()
        if count == 1:
            user_course = qs.first()
            if user_course.is_user_enrolled_or_staff(user):
                result = user_course
        elif count > 1:
            raise ExamError(
                f"Student '{user.username}' is "
                "enrolled on more than one course"
            )
        else:
            # Student not enrolled
            pass
        return result

    def pass_year_product_category_lms(self, year):
        """Report - courses passed in a year."""
        start_date = date(year, 1, 1)
        end_date = date(year, 12, 31)
        return self.current().filter(
            assessed__gte=start_date,
            assessed__lte=end_date,
            result__gte=1,
        )


@reversion.register()
class UserCourse(TimedCreateModifyDeleteModel):
    """Represents a user's access to a course."""

    MAIL_TEMPLATE_USER_COURSE_RESULT = "exam_user_course_result"

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name="+", on_delete=models.CASCADE
    )
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    assessor = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="+",
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )
    assessed = models.DateTimeField(blank=True, null=True)
    assessor_comments = models.TextField(blank=True)
    result = models.IntegerField(blank=True, null=True)
    # fields copied from the old ``MemberCourse`` model
    #
    # 16/12/2022, emailed Emma ref the 'enrol_date'.  Do we need it?
    # *The enrol date will always be the date that the application is*
    # *approved. It's fine for this to be a read only date*.
    # enrol_date = models.DateField()
    #
    payment_details = models.CharField(max_length=50, blank=True)
    # for diploma and single subject courses #485
    extension_expires = models.DateField(blank=True, null=True)
    cancellation_date = models.DateField(blank=True, null=True)
    # for diploma evening classes
    selected_class = models.ForeignKey(
        ClassInformation,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )
    performance_notes = models.TextField(blank=True)
    additional_notes = models.TextField(blank=True)
    # end of (fields copied from the old ``MemberCourse`` model)
    objects = UserCourseManager()

    class Meta:
        ordering = ("user__username", "course__order")
        verbose_name = "User Course"
        verbose_name_plural = "User Courses"

    def __str__(self):
        return "{}: {}".format(self.user.username, self.course.name)

    def course_results(self):
        """Course results (units and tests) for the student.

        .. tip:: This class is ``UserCourse``.

        Note:

        - Only units with an assessment will get a status / result:
          https://www.kbsoftware.co.uk/crm/ticket/6465/
        - The database will allow more than one assessment per unit, but this
          code counts the number of units with at least one assessment
          (``len(units_pks_with_assessment)``).
        - ``UserUnitManager``, ``with_assessment`` is similar.
        - The designs for the page are here:
          https://drive.google.com/file/d/1iRmlWfjpBdQYKlkIilH7zoW-AvtbECDD/

        To allow us to iterate through a single data structure on the results page,
        we return a ``list`` of ``dict`` with the following fields:

        - ``unit_pk``
        - ``exam_pk``
        - ``heading``
        - ``description``
        - ``status``

        1. To get the list of units for a course::

          CourseUnit.objects.for_course(course)

        2. How do I get the result for each one?

        - The ``UserUnit`` has the ``result`` (when it has been 'assessed')

        3. How do I get the tests (exams)?

        - From the ``UnitExam``.

        """
        result = []
        assessment_count = assessment_done = exam_count = exam_done = 0
        # see 'with_assessment'
        units_pks_with_assessment = Unit.objects.current_with_assessment(
            course=self.course
        ).values_list("pk", flat=True)
        course_units = (
            CourseUnit.objects.for_course(self.course)
            .order_by("order")
            .prefetch_related("unit")
        )
        for course_unit in course_units:
            user_unit = None
            try:
                user_unit = UserUnit.objects.get(
                    user_course=self, unit=course_unit.unit
                )
            except UserUnit.DoesNotExist:
                pass
            if user_unit:
                for unit_exam in UnitExam.objects.current(
                    unit=course_unit.unit
                ):
                    exam_count = exam_count + 1
                    status = "Awaiting Completion"
                    user_exam_version = (
                        UserExamVersion.objects.user_exam_versions(
                            self.user, unit_exam.exam
                        )
                        .order_by("-created")
                        .first()
                    )
                    if user_exam_version:
                        status = user_exam_version.result_as_text()
                        if user_exam_version.is_marked():
                            exam_done = exam_done + 1
                    result.append(
                        {
                            "unit_pk": course_unit.pk,
                            "exam_pk": unit_exam.pk,
                            "heading": "Achievement Test",
                            "description": unit_exam.exam.name,
                            "status": status,
                            "user_exam_version_pk": (
                                user_exam_version.pk
                                if user_exam_version
                                else None
                            ),
                        }
                    )
                # assessment / assignment
                if course_unit.unit.pk in units_pks_with_assessment:
                    assessment_count = assessment_count + 1
                    status = user_unit.result_status()
                    if user_unit.assessed and user_unit.result:
                        assessment_done = assessment_done + 1
                    assignment_or_assessment = (
                        user_unit.unit.get_assignment_or_assessment(
                            user_unit.unit.assessments().count()
                        ).title()
                    )
                    result.append(
                        {
                            "unit_pk": course_unit.pk,
                            "exam_pk": None,
                            "heading": assignment_or_assessment,
                            "description": course_unit.name
                            or course_unit.unit.name,
                            "status": status,
                            "user_exam_version_pk": None,
                        }
                    )
        return result, {
            "assessment_count": assessment_count,
            "assessment_done": assessment_done,
            "exam_count": exam_count,
            "exam_done": exam_done,
        }

    @property
    def days_after_enrol(self):
        return (date.today() - self.created.date()).days

    def is_cancelled(self):
        return bool(self.cancellation_date)

    def is_complete(self):
        """Is the course complete?

        .. tip:: For ``_can_send_email`` (in ``mentor/service.py``).

        """
        return bool(self.assessed) or self.is_cancelled()

    def is_complete_and_ready_for_final_result(self):
        """Can the assessor give the final result?

        i.e. are all assessments and exams complete?

        Logic based on the ``current_to_mark`` method (see the manager).

        .. warning:: I am not checking to see if the student has completed the
                     unit / exam.  This can be added later if required.

        .. tip:: This method is in the ``UserCourse`` class.

        """
        result = False
        unit_pks = UserUnit.objects.current(self).values_list(
            "unit__pk", flat=True
        )
        units_pks_assessed = (
            UserUnit.objects.current(self)
            .filter(assessed__isnull=False)
            .values_list("unit__pk", flat=True)
        )
        if set(unit_pks) == set(units_pks_assessed):
            exam_pks = (
                UnitExam.objects.current()
                .filter(unit__pk__in=unit_pks)
                .values_list("exam__pk", flat=True)
            )
            # find the exam IDs taken by the user
            exam_pks_completed = UserExamVersion.objects.filter(
                user=self.user,
                exam_version__exam__pk__in=exam_pks,
                result__isnull=False,
            ).values_list("exam_version__exam__pk", flat=True)
            result = set(exam_pks) == set(exam_pks_completed)
        return result

    def is_lms(self):
        """Is this an LMS course i.e. not a legacy course?

        Used by ``process_course_mailing`` in a third party project.

        """
        return True

    def is_user_enrolled_or_staff(self, user):
        """Check the user is a member of staff, or enrolled on this course."""
        result = False
        if self.is_deleted:
            pass
        else:
            if user.is_anonymous:
                pass
            elif user.is_staff:
                result = True
            else:
                if self.is_cancelled():
                    pass
                elif user == self.user:
                    result = True
        return result

    def mark(self, assessor, assessor_comments):
        """Add/Update a User's Course performance."""
        self.assessor = assessor
        self.assessor_comments = assessor_comments
        self.save()
        return self

    def single_subject(self):
        """If this is a single subject course, then return the user unit."""
        result = None
        qs = UserUnit.objects.current(self)
        if qs.count() == 1:
            result = qs.first()
        return result

    def user_units_marking(self):
        """List of courses / units which have been marked.

        So the *team can take a look at previous feedback on
        assessments/assignments*.

        Called in the ``exam/user_course_marking_list.html`` template
        for ``UserCourseMarkingListView``.

        .. tip:: This class is ``UserCourse``.

        """
        result = []
        course_units = (
            CourseUnit.objects.for_course(self.course)
            .order_by("order")
            .prefetch_related("unit")
        ).order_by("order")
        for course_unit in course_units:
            user_unit = None
            try:
                user_unit = UserUnit.objects.get(
                    user_course=self, unit=course_unit.unit, deleted=False
                )
            except UserUnit.DoesNotExist:
                pass
            if user_unit and user_unit.result:
                result_assessed = result_assessor = ""
                if user_unit.result_assessed:
                    result_assessed = user_unit.result_assessed.strftime(
                        "%d/%m/%Y %H:%M"
                    )
                if user_unit.result_assessor:
                    result_assessor = user_unit.result_assessor.get_full_name()
                # 'unit_name' = 'exam/userunit_detail.html' / 'current_to_mark'
                unit_name = course_unit.unit.name
                result.append(
                    dict(
                        result=user_unit.result,
                        result_assessed=result_assessed,
                        result_assessor=result_assessor,
                        unit_name=unit_name,
                        user_unit_pk=user_unit.pk,
                    )
                )

        # result = UserUnit.objects.for_user_course(self) #, unit_filter=None):
        # print(result.count())
        return result


class UserExamVersionProductManager(models.Manager):
    def link_user_exam_version_product(
        self, user_exam_version, products_for_user, legacy=None
    ):
        """Link the ``user_exam_version`` to the products for the user.

        Keyword arguments:
        legacy -- created as part of a data migration

        If the ``ProductExam`` row was created as part of a data migration,

        """
        count = 0
        exam = user_exam_version.exam_version.exam
        for product in products_for_user:
            try:
                # do we have a match?
                product_exam = ProductExam.objects.get(
                    product=product, exam=exam
                )
                if product_exam.legacy and not legacy:
                    # don't create the link (unless running a data migration)
                    pass
                else:
                    # if so, create the link
                    user_exam_version_product = self.model(
                        user_exam_version=user_exam_version, product=product
                    )
                    user_exam_version_product.save()
                    count = count + 1
            except ProductExam.DoesNotExist:
                pass
        if not count:
            if user_exam_version.user.is_staff:
                # No need to link exams to products for a member of staff
                # https://www.kbsoftware.co.uk/crm/ticket/3601/
                #
                # Note: This exemption might also need to be extended to a team
                # member (see 'contact.team_member' in one of our projects).
                pass
            else:
                raise ExamError(
                    "Cannot find any matching products for user exam version "
                    "'{}' '{}'".format(user_exam_version.pk, products_for_user)
                )
        return count

    def product_results(self, user, product):
        result = []
        product_exams = ProductExam.objects.filter(product=product).order_by(
            "order", "product__name"
        )
        for product_exam in product_exams:
            item = {
                "exam_pk": product_exam.exam.pk,
                "exam_name": product_exam.exam.name,
            }
            obj = (
                self.model.objects.filter(
                    user_exam_version__user=user,
                    user_exam_version__exam_version__exam=product_exam.exam,
                    product=product_exam.product,
                )
                .order_by("-user_exam_version__created")
                .first()
            )
            if obj:
                item["pk"] = obj.user_exam_version.pk
                item["percent"] = obj.user_exam_version.result_as_percent()
                item["is_fail"] = obj.user_exam_version.is_fail()
                item["legacy_result"] = obj.user_exam_version.legacy_result
                item["is_retake"] = bool(obj.user_exam_version.previous_test())
                legacy_retake = False
                if obj.user_exam_version.legacy_result:
                    if obj.user_exam_version.legacy_retake:
                        legacy_retake = True
                item["legacy_retake"] = legacy_retake
            item["taken"] = True if obj else False
            # only include legacy exams if the student actually took the test
            if obj or not product_exam.legacy:
                result.append(item)
        return result


@reversion.register()
class UserExamVersionProduct(models.Model):
    user_exam_version = models.ForeignKey(
        UserExamVersion, on_delete=models.CASCADE
    )
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    objects = UserExamVersionProductManager()

    def __str__(self):
        return "{} {}".format(
            self.product.name, self.user_exam_version.exam_version.exam.name
        )

    class Meta:
        unique_together = ["user_exam_version", "product"]
        verbose_name = "User exam version - product"
        verbose_name_plural = "User exam version - products"


class UserUnitManager(models.Manager):
    def _create_user_unit(self, user_course, unit):
        """Create a unit for the user / course.

        .. warning:: ``UserUnit`` records are created on enrolment, so this
                      method should not be called any other time.

        """
        try:
            CourseUnit.objects.get(course=user_course.course, unit=unit)
        except CourseUnit.DoesNotExist:
            raise ExamError(
                "Cannot 'create_user_unit'.  The '{}' unit does not "
                "belong to the '{}' course".format(
                    unit.name, user_course.course.name
                )
            )
        x = self.model(user_course=user_course, unit=unit)
        x.save()
        return x

    def _init_user_unit(self, user_course, unit):
        """Initialise a unit for the user / course.

        .. warning:: ``UserUnit`` records are created on enrolment, so this
                      method should not be called any other time.


        To create a ``UserUnit`` using ``factories``::

          user = UserFactory()
          course = CourseFactory()
          user_course = UserCourseFactory(user=user, course=course)

          unit = UnitFactory()
          course_unit = CourseUnitFactory(course=course, unit=unit, order=4)

          user_unit = UserUnitFactory(user_course=user_course, unit=unit)

        """
        try:
            x = self.model.objects.get(user_course=user_course, unit=unit)
            if x.is_deleted:
                x.undelete()
        except self.model.DoesNotExist:
            x = self._create_user_unit(user_course, unit)
        return x

    def current(self, user_course=None):
        qs = self.model.objects.exclude(deleted=True)
        if user_course:
            qs = qs.filter(user_course=user_course)
        return qs

    def current_to_mark(self):
        """List of units which are ready to mark for a user.

        .. tip:: This method is in the ``UserUnitManager``.

        https://www.kbsoftware.co.uk/crm/ticket/6805/

        """
        user_unit_data = []
        user_course_pks = UserCourse.objects.current().values_list(
            "pk", flat=True
        )
        user_unit_qs = (
            (
                self.current()
                .filter(user_course__pk__in=user_course_pks)
                .filter(
                    Q(
                        Q(submitted__isnull=False)  # datetime
                        & Q(assessed__isnull=True)  # None
                        & Q(retake_assessed__isnull=True)  # None
                    )
                    | Q(
                        Q(retake_submitted__isnull=False)  # datetime
                        & Q(assessed__isnull=True)  # None
                    )
                )
            )
            .prefetch_related("unit")
            .prefetch_related("user_course")
            .prefetch_related("user_course__user")
        )
        for user_unit in user_unit_qs:
            # 'unit_name' = 'exam/userunit_detail.html' / 'user_units_marking'
            unit_name = ""
            user_course = user_unit.user_course
            try:
                course_unit = CourseUnit.objects.get(
                    course=user_course.course, unit=user_unit.unit
                )
                unit_name = course_unit.name
            except CourseUnit.DoesNotExist:
                logger.error(
                    "Cannot find 'CourseUnit' for 'UserUnit' "
                    f"{user_unit.pk}: '{user_course.course.name}' "
                    f"({user_course.course.pk}), unit "
                    f"'{user_unit.unit.name}' ({user_unit.unit.pk})"
                )
            # calling 'is_retake' on the 'UserUnit' adds several SQL queries,
            # so keep it simple for now...
            retake_submitted = user_unit.retake_submitted
            is_retake = bool(retake_submitted)
            user_unit_data.append(
                {
                    "course_name": user_course.course.name,
                    "full_name": user_course.user.get_full_name(),
                    "is_retake": is_retake,
                    "retake_submitted": retake_submitted,
                    "submitted": user_unit.submitted,
                    "unit_name": unit_name,
                    "user_course_pk": user_course.pk,
                    "user_name": user_course.user.username,
                    "user_pk": user_course.user.pk,
                    "user_unit_pk": user_unit.pk,
                }
            )
        return user_unit_data

    def for_user_course(self, user_course, unit_filter=None):
        """User units for a course.

        .. tip:: This class is the ``UserUnitManager``.

        .. tip:: See ``with_assessment`` (very similar)

        .. tip:: The ``unit_filter`` allows the developer to filter units.

        .. tip:: ``UserCourse``, ``course_results`` does something similar.

        - The ``order`` is from the ``CourseUnit``
          (which can be different for each course).

        """
        result = []
        qs = CourseUnit.objects.for_course(user_course.course)
        if unit_filter:
            qs = qs.filter(unit__in=unit_filter)
        units = qs.order_by("order").values_list("unit", flat=True)
        for unit in units:
            try:
                user_unit = UserUnit.objects.get(
                    user_course=user_course, unit=unit
                )
                result.append(user_unit)
            except UserUnit.DoesNotExist:
                pass
        return result

    def can_user_start(self, user_course, unit):
        """Returns whether this unit can be started by this student.

        Allow the students to work on the modules in any order
        https://www.kbsoftware.co.uk/crm/ticket/3893/

        """
        result = False
        try:
            course_unit = CourseUnit.objects.get(
                course=user_course.course, unit=unit
            )
            result = True
        except CourseUnit.DoesNotExist:
            raise ExamError(
                "'CourseUnit', '{}' is not linked to '{}'".format(
                    unit.name, user_course.course.name
                )
            )
        #    # Student can take if no previous unit.
        #    if course_unit.order < 2:
        #        return True
        #    prev_unit = user_course.course.prev_unit(unit)
        #    try:
        #        prev_user_unit = self.model.objects.get(
        #            user_course=user_course, unit=prev_unit
        #        )
        #    except self.model.DoesNotExist:
        #        # Student has not started previous unit.
        #        return False
        #    # Student can take if 'prev_user_unit' has been submitted
        #    return bool(prev_user_unit.submitted)
        return result

    def user_unit_from_course_unit(self, user, course_unit):
        """Find a user assessment from a 'user' and 'course_unit'.

        - Check the user is enrolled.

        .. note:: ``UserUnit`` records are created on enrolment.

        """
        user_course = UserCourse.objects.is_user_enrolled(
            user, course_unit.course
        )
        if not user_course:
            raise ExamError(
                f"'{user.username}' (ID {user.pk}) is not enrolled on the "
                f"'{course_unit.course.name}' course "
                f"(ID {course_unit.course.pk}) "
                f"(unit: '{course_unit.unit.name}')"
            )
        user_unit = self.user_unit_if_exists(user_course, course_unit.unit)
        if not user_unit:
            raise ExamError(
                f"'{user.username}' (ID {user.pk}) is not enrolled on the "
                f"'{course_unit.unit.name}' unit "
                f"(ID {course_unit.unit.pk}) "
                f"(course: '{course_unit.course.name}')"
            )
        return user_unit

    def user_unit_if_exists(self, user_course, unit):
        """Finds the user unit (if the user is enrolled)."""
        try:
            return self.model.objects.get(user_course=user_course, unit=unit)
        except self.model.DoesNotExist:
            return None

    def with_assessment(self, user_course):
        """Units for a course (where the unit has an assessment).

        .. tip:: This class is the ``UserUnitManager``.

        .. tip:: See ``for_user_course`` (very similar)

        """
        units_with_assessment = Unit.objects.current_with_assessment(
            course=user_course.course
        )
        return self.for_user_course(user_course, units_with_assessment)


@reversion.register()
class UserUnit(TimedCreateModifyDeleteModel):
    """Status and container for the ``UserAssessment`` list for this unit.

    .. note:: Created when the user enrols on the course.
              Some units are ``core`` and others are selected.

    """

    MAIL_TEMPLATE_USER_UNIT_RESULT = "exam_user_unit_result"
    MAIL_TEMPLATE_USER_UNIT_RETAKE = "exam_user_unit_retake"

    user_course = models.ForeignKey(UserCourse, on_delete=models.CASCADE)
    unit = models.ForeignKey(Unit, on_delete=models.CASCADE)
    # student - submit assessment (original)
    submitted = models.DateTimeField(blank=True, null=True)
    # student - submit retake
    retake_submitted = models.DateTimeField(blank=True, null=True)
    # retake
    retake_comments = models.TextField(blank=True)
    retake_assessor = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="+",
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )
    retake_assessed = models.DateTimeField(blank=True, null=True)
    # assessor - result
    result_assessor = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="+",
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )
    result_assessed = models.DateTimeField(blank=True, null=True)
    result = models.IntegerField(blank=True, null=True)
    # assessor - assessment (original)
    assessor = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="+",
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )
    assessed = models.DateTimeField(blank=True, null=True)
    assessor_comments = models.TextField(blank=True)
    objects = UserUnitManager()

    class Meta:
        ordering = ("user_course__user", "unit__name")
        unique_together = ("user_course", "unit")
        verbose_name = "Course"
        verbose_name_plural = "Courses"

    def __str__(self):
        return "{}: {} (for {})".format(
            self.user_course.user.username,
            self.unit.name,
            self.user_course.course.name,
        )

    def _is_complete_assessments(self):
        """Get the primary keys of the assessments and user assessments.

        - ``assessment_pks``, the assessments for the unit
        - ``pks_initial``, assessments which have been submitted by the user
        - ``pks_retake``, retakes which have been submitted by the user

        Used by:

        - ``assessment_completed_count``
        - ``is_complete_assessments``

        """
        assessment_pks = set([x.pk for x in self.unit.assessments()])
        pks_initial = [
            x.assessment.pk
            for x in self.userassessment_set.exclude(deleted=True)
            .filter(assessment__pk__in=assessment_pks)
            .filter(can_retake=False)
            .filter(answer_submitted__isnull=False)
        ]
        pks_retake = [
            x.assessment.pk
            for x in self.userassessment_set.exclude(deleted=True)
            .filter(assessment__pk__in=assessment_pks)
            .filter(can_retake=True)
            .filter(retake_submitted__isnull=False)
        ]
        return assessment_pks, pks_initial, pks_retake

    def _is_complete_exams(self):
        result = True
        for unit_exam in UnitExam.objects.filter(unit=self.unit):
            user_exam_version = UserExamVersion.objects.user_exam_versions(
                self.user_course.user, unit_exam.exam
            ).first()
            if user_exam_version and user_exam_version.is_pass():
                pass
            else:
                result = False
                break
        return result

    def assessment_completed_count(self):
        """Number of completed assessments."""
        (
            assessment_pks,
            pks_initial,
            pks_retake,
        ) = self._is_complete_assessments()
        return len(set(pks_initial + pks_retake))

    def assessment_count(self):
        """Total number of assessments for this unit."""
        return self.unit.assessments().count()

    def assessments(self):
        """Return a dictionary containing all assessments for a user's unit.

        10/01/2025, Exclude deleted
        https://www.kbsoftware.co.uk/crm/ticket/7502/

        """
        result = []
        qs = self.unit.assessments()
        for assessment in qs:
            result.append(
                {
                    "assessment": assessment,
                    "user_assessments": [
                        x
                        for x in self.userassessment_set.filter(
                            assessment=assessment
                        )
                        .exclude(deleted=True)
                        .order_by("created")
                    ],
                }
            )
        return result

    def assessments_exist(self):
        return self.unit.assessments().exists()

    def is_complete(self):
        """Has the student completed the module (unit)?

        1. Has the student submitted all assessments?
        2. Has the student passed all the tests (exams)?

        .. tip:: For ``_can_send_email`` (in ``mentor/service.py``).

        """
        result = self.is_complete_assessments()
        if result:
            result = self._is_complete_exams()
        return result

    def is_complete_assessments(self):
        (
            assessment_pks,
            pks_initial,
            pks_retake,
        ) = self._is_complete_assessments()
        return bool(assessment_pks == set(pks_initial + pks_retake))

    def is_current_user_assessment(self):
        """Is this a legacy course?

        We need to identify legacy ``UserAssessment`` records.

        1. The course was created before 26/01/2024
        2. The unit has a result (assessed).

        .. tip:: This method is in the ``UserUnit`` class.

        https://www.kbsoftware.co.uk/crm/ticket/7017/

        """
        is_legacy = bool(
            self.user_course.created
            < datetime.datetime(
                2024, 1, 26, 1, 1, 1, tzinfo=datetime.timezone.utc
            )
            and self.assessed
            and self.result
        )
        return not is_legacy

    def is_marking_finished(self):
        """Has the assessor finished marking the unit?

        email from Megan, 01/02/2024

          Adding the comments, or requesting a retake is the point when
          marking is finished and ready for the Students to see their
          feedback etc.

        For notes, see ``docs-kb/source/user-feedback.rst``.

        .. tip:: This method is in the ``UserUnit`` class.

        """
        return bool(self.assessed and self.result)

    def is_marking_finished_retake(self):
        """Has the assessor requested a retake?

        .. tip:: See comments in ``is_marking_finished``.

        .. tip:: This method is in the ``UserUnit`` class.

        """
        return bool(self.retake_assessed and self.is_retake())

    def is_retake(self):
        """Has the student been asked to retake any of the assessments?

        .. tip:: This method is in the ``UserUnit`` class.

        """
        assessment_pks = set([x.pk for x in self.unit.assessments()])
        return (
            self.userassessment_set.exclude(deleted=True)
            .filter(assessment__pk__in=assessment_pks)
            .filter(can_retake=True)
            .exists()
        )

    def is_submitted(self):
        """Has the user has submitted the unit?

        - The user submitted the unit and hasn't been asked to retake anything.
        - The user was asked to retake and submitted the original and retake.

        """
        result = bool(self.submitted)
        if result:
            if self.retake_submitted:
                result = True
            elif self.is_retake():
                result = False
        return result

    def is_pass(self):
        """This class is UserUnit

        Did the user pass the assessment/assignment?

        .. note:: Will return ``None`` if the result hasn't been set.

        """
        result = None
        percent = self.result_as_percent()
        if percent is None:
            pass
        else:
            result = bool(percent >= 80)
        return result

    def is_fail(self):
        """This class is UserUnit

        Did the user fail the assignment/assessment?

        .. note:: Will return ``None`` if the result hasn't been set.

        """
        result = None
        percent = self.result_as_percent()
        if percent is None:
            pass
        else:
            result = not self.is_pass()
        return result

    def result_as_percent(self):
        """Convert the result into a percentage.

        This method is in UserUnit.

        If this is a retake, then the maximum result is 80% (for a retake we
        won't display the actual number of questions they got wrong).

        .. note:: Will return ``None`` if the result hasn't been set.

        """
        result = None
        if self.percent_override_date:
            result = self.percent_override
        elif self.date_marked:
            result = int(self.result / 20 * 100) if self.result else 0
            if self.previous_test() and result > 80:
                result = 80
        return result

    def result_status(self):
        """For use with the ``UserCourse``, ``course_results`` method.

        .. tip:: This method is in the ``UserUnit`` class.

        """
        status = "Awaiting Completion"
        if self.assessed and self.result:
            status = f"{self.result}%"
        else:
            is_retake = self.is_retake()
            if is_retake:
                if self.retake_submitted:
                    status = "Retake Submitted"
                else:
                    status = "Awaiting Retake"
            elif self.submitted:
                status = "Submitted"
        return status

    def submit(self):
        """Submit a user's assessments for review.

        .. tip:: To find the ``UserUnit``, the developer could use
                 ``user_unit_from_course_unit``. It checks to make sure the
                 user is enrolled on the course and unit.

        """
        # has the unit already been submitted?
        is_retake = False
        submitted = self.submitted
        if submitted:
            is_retake = self.is_retake()
            if is_retake:
                submitted = self.retake_submitted
        if submitted:
            raise ExamError(
                "'{}' has already been submitted for '{}' ({})".format(
                    self.unit.name,
                    self.user_course.user.username,
                    self.pk,
                )
            )
        if not self.is_complete_assessments():
            raise ExamError(
                (
                    "Cannot submit '{}' until all the assessments "
                    "for '{}' have been submitted ({})"
                ).format(
                    self.unit.name,
                    self.user_course.user.username,
                    self.pk,
                )
            )
        if is_retake:
            self.retake_submitted = timezone.now()
        else:
            self.submitted = timezone.now()
        self.save()

    def prev_user_unit(self):
        if self.unit.order < 2:
            return None
        older_units = UserUnit.objects.filter(
            user_course=self.user_course, unit__order__lt=self.unit.order
        ).order_by("-unit__order")
        return older_units.first()
        """Returns the biggest number (in reverse order of number)."""

    def can_mark(self):
        """Can the unit be marked?

        1. Unit must be submitted.
        2. Unit must not be already marked.

        .. tip:: This method is part of the ``UserUnit`` class.

        """
        result = False
        if self.assessed:
            pass
        else:
            if self.is_retake() and self.retake_submitted:
                result = True
            elif self.submitted:
                result = True
        return result

    def can_submit(self):
        """Can the user submit the unit for the first time.

        - Unit must not yet be submitted.
        - All assessments must be complete.

        """
        result = False
        if self.is_complete_assessments():
            if self.submitted:
                pass
            else:
                result = True
        return result

    def can_submit_retake(self):
        """Can the user submit a unit retake.
        Unit retake must not yet be submitted.
        All assessments must be complete.
        """
        result = False
        if self.is_complete_assessments():
            if self.retake_submitted:
                pass
            else:
                result = True
        return result


class UserAssessmentManager(models.Manager):
    def create_user_assessment(self, user_unit, assessment):
        obj = self.model(user_unit=user_unit, assessment=assessment)
        obj.save()
        return obj

    def init_user_assessment(self, user_unit, assessment):
        try:
            x = self.model.objects.get(
                user_unit=user_unit, assessment=assessment
            )
        except self.model.DoesNotExist:
            x = self.create_user_assessment(user_unit, assessment)
        return x

    def sat_user_assessment(self, user_course, assessment):
        """Finds a previous sitting, if available.

        .. note:: The original design created a new ``user_assessment`` record
                  for a retake.  We are now using the ``retake_answer_file``
                  instead.

        20/02/2025, Ignore deleted records
        (not sure why this would happen, but it did)

        - https://www.kbsoftware.co.uk/crm/ticket/7502/
        - https://www.kbsoftware.co.uk/crm/ticket/7587/

        """
        user_unit = UserUnit.objects.user_unit_if_exists(
            user_course, assessment.unit
        )
        if user_unit:
            try:
                return self.model.objects.get(
                    user_unit=user_unit, assessment=assessment, deleted=False
                )
                # can_retake=False,
            except self.model.DoesNotExist:
                return False
            except self.model.MultipleObjectsReturned:
                raise ExamError(
                    "Found two 'UserAssessment' records for "
                    f"'UserUnit' {user_unit.pk}, 'Assessment' {assessment.pk}"
                )
        else:
            return False

    def sit_assessment(self, user_unit, assessment, answer_file):
        """Captures a user's assessment for review."""
        user_assessment = None
        if UserUnit.objects.can_user_start(
            user_unit.user_course, assessment.unit
        ):
            # initialise the user_assessment
            user_assessment = self.init_user_assessment(user_unit, assessment)
            if answer_file:
                if user_assessment.can_retake:
                    user_assessment.retake_answer_file = answer_file
                    user_assessment.retake_submitted = timezone.now()
                    user_assessment.retake_answer_original_file_name = (
                        os.path.basename(answer_file.name)
                    )
                else:
                    user_assessment.answer_file = answer_file
                    user_assessment.answer_submitted = timezone.now()
                    user_assessment.answer_original_file_name = (
                        os.path.basename(answer_file.name)
                    )
                user_assessment.save()
        return user_assessment


@reversion.register()
class UserAssessment(TimedCreateModifyDeleteModel):
    """Completed assessment for a student."""

    user_unit = models.ForeignKey(UserUnit, on_delete=models.CASCADE)
    assessment = models.ForeignKey(Assessment, on_delete=models.CASCADE)
    # answer
    answer_file = models.FileField(
        upload_to="exam/assessment/user",
        storage=private_file_store,
        blank=True,
    )
    answer_original_file_name = models.TextField(blank=True)
    answer_submitted = models.DateTimeField(blank=True, null=True)
    # set by assessor
    assessor = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="+",
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )
    assessed = models.DateTimeField(blank=True, null=True)
    assessor_comments = models.TextField(blank=True)
    error_count = models.IntegerField(
        blank=True, null=True, help_text="Error count (for assessments)"
    )
    grade = models.IntegerField(
        blank=True, null=True, help_text="Grade (for assignments)"
    )
    # can retake (set by the assessor)
    can_retake = models.BooleanField(default=False)
    can_retake_date = models.DateTimeField(blank=True, null=True)
    can_retake_user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="+",
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )
    # has_passed (used by assessor, tick to indicate a pass)
    # only used on ASSESSMENTS, not ASSIGNMENTS
    has_passed = models.BooleanField(default=False)
    # retake (uploaded by student)
    retake_answer_file = models.FileField(
        upload_to="exam/assessment/user",
        storage=private_file_store,
        blank=True,
    )
    retake_answer_original_file_name = models.TextField(blank=True)
    retake_submitted = models.DateTimeField(blank=True, null=True)
    # set by assessor
    retake_assessor = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="+",
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )
    retake_assessed = models.DateTimeField(blank=True, null=True)
    retake_comments = models.TextField(blank=True)
    retake_error_count = models.IntegerField(
        blank=True, null=True, help_text="Retake error count (for assessments)"
    )
    retake_grade = models.IntegerField(
        blank=True, null=True, help_text="Retake grade (for assignments)"
    )
    # retake_has_passed (used by assessor, tick to indicate retake is passed)
    # only used on ASSESSMENTS, not ASSIGNMENTS
    retake_has_passed = models.BooleanField(default=False)
    objects = UserAssessmentManager()

    class Meta:
        ordering = ("user_unit", "assessment", "answer_submitted")
        verbose_name = "UserAssessment"
        verbose_name_plural = "UserAssessments"

    def __str__(self):
        result = "{}: {} (for '{}')".format(
            self.assessment.name,
            self.user_unit.unit.name,
            self.user_unit.user_course.user.username,
        )
        if self.can_retake:
            result = "{} (Retake)".format(result)
        return result

    def answer_button_caption(self):
        """Caption for the answer button.

        .. note:: This method is in the ``UserAssessment`` class.

        For documentation, see ``docs-kb/source/user-feedback.rst``.

        """
        result = ""
        is_assignment = self.user_unit.unit.is_assignment()
        is_marking_finished = self.user_unit.is_marking_finished()
        is_marking_finished_retake = self.user_unit.is_marking_finished_retake()
        if is_marking_finished_retake and self.can_retake:
            if self.retake_submitted:
                if is_assignment:
                    if is_marking_finished and self.retake_grade:
                        if self.retake_grade >= 80:
                            result = "PASS"
                        else:
                            result = "FAIL"
                    elif self.user_unit.retake_submitted:
                        result = "Retake Submitted"
                    else:
                        result = "Retake Uploaded"
                else:
                    is_current = self.user_unit.is_current_user_assessment()
                    if (
                        is_current
                        and is_marking_finished
                        and self.retake_has_passed
                    ):
                        result = "PASS"
                    elif (
                        is_current
                        and is_marking_finished
                        and self.user_unit.assessed
                    ):
                        result = "FAIL"
                    elif self.user_unit.retake_submitted:
                        result = "Retake Submitted"
                    else:
                        result = "Retake Uploaded"
            else:
                result = "Retake"
        else:
            if self.answer_submitted:
                is_finished = is_marking_finished or is_marking_finished_retake
                if is_assignment:
                    if is_finished and self.grade:
                        if self.grade >= 80:
                            result = "PASS"
                        else:
                            result = "FAIL"
                    elif self.user_unit.submitted:
                        result = "Submitted"
                    else:
                        result = "Uploaded"
                else:
                    is_current = self.user_unit.is_current_user_assessment()
                    if is_current and is_finished and self.has_passed:
                        result = "PASS"
                    elif is_current and is_finished and self.user_unit.assessed:
                        result = "FAIL"
                    elif self.user_unit.submitted:
                        result = "Submitted"
                    else:
                        result = "Uploaded"
            else:
                result = "Start"
        return result

    def answer_button_enabled(self):
        result = False
        if self.can_retake:
            if not self.user_unit.retake_submitted:
                result = True
        else:
            if not self.user_unit.submitted:
                result = True
        return result

    def answer_file_name(self):
        """The filename to use for downloading the answer file."""
        result = None
        if self.answer_original_file_name:
            result = self.answer_original_file_name
        else:
            result = os.path.basename(self.answer_file.name)
        return result

    def can_mark(self):
        """Can the assessment be marked?

        If the assessment has been submitted and not assessed, yes.
        Otherwise, no.

        """
        result = False
        if self.answer_submitted:
            if bool(self.assessed):
                pass
            else:
                result = True
        return result

    def can_mark_retake(self):
        """Can the assessment retake be marked.

        If the retake has been submitted and not assessed, then yes.
        Otherwise, no.

        """
        result = False
        if self.retake_submitted:
            if bool(self.retake_assessed):
                pass
            else:
                result = True
        return result

    def can_user_start(self):
        # can_user_start is True if answer_submitted is False
        return not bool(self.answer_submitted)

    def can_user_start_retake(self):
        """Can the student retake the assessment.
        unit retake no submitted, is retake is true
        """
        result = False
        if self.is_retake():
            if bool(self.user_unit.retake_submitted):
                pass
            else:
                result = True
        return result

    def feedback_enabled(self):
        """Can the student see the feedback on the assessment page?

        .. note:: This method is in the ``UserAssessment`` class.

        .. note:: ``is_marking_finished`` checks to see if a result and
                  comment have been entered for the unit.

        For documentation, see ``docs-kb/source/user-feedback.rst``.

        """
        result = False
        is_assignment = self.user_unit.unit.is_assignment()
        is_marking_finished = self.user_unit.is_marking_finished()
        if is_marking_finished:
            if self.user_unit.is_retake():
                is_comments = bool(
                    self.retake_comments and self.retake_comments.strip()
                )
            else:
                is_comments = bool(
                    self.assessor_comments and self.assessor_comments.strip()
                )
            if is_comments:
                # marking is finished.  Did they pass?
                if is_assignment:
                    result = bool(self.grade or self.retake_grade)
                else:
                    result = bool(self.has_passed or self.retake_has_passed)
                if not result:
                    # did they fail?
                    result = bool(self.can_retake and self.retake_assessed)
        return result

    def feedback_enabled_for_retake(self):
        """Can the student see the feedback on the assessment page?

        .. note:: ``is_marking_finished_retake`` checks to see if retake
                  comments have been entered for the unit.

        For documentation, see ``docs-kb/source/user-feedback.rst``.

        """
        result = False
        is_assignment = self.user_unit.unit.is_assignment()
        is_marking_finished = self.user_unit.is_marking_finished_retake()
        if is_marking_finished:
            is_comments = bool(
                self.assessor_comments and self.assessor_comments.strip()
            )
            if is_comments:
                # marking is finished.  Did they pass?
                if is_assignment:
                    result = bool(self.grade)
                else:
                    result = bool(self.has_passed)
                if not result:
                    # did they fail?
                    result = bool(self.assessed)
        return result

    def is_retake(self):
        """

        .. tip:: This method is in the ``UserAssessment`` class.

        """
        result = False
        if self.user_unit.assessed and self.can_retake:
            result = True
        return result

    def retake_answer_file_name(self):
        """The filename to use for downloading the (retake) answer file."""
        result = None
        if self.retake_answer_original_file_name:
            result = self.retake_answer_original_file_name
        else:
            result = os.path.basename(self.retake_answer_file.name)
        return result


class UserUnitAuditManager(models.Manager):
    def create_user_unit_audit(self, user_unit, user, result):
        user_unit_audit = self.model(
            user_unit=user_unit,
            user=user,
            result=result,
        )
        user_unit_audit.save()
        return user_unit_audit


class UserUnitAudit(TimeStampedModel):
    """
    Added audit trail for UserUnit and UserAssessment, to allow
    updating of unit result and assessment comments.

    Requested as an open-ended feature.
    https://www.kbsoftware.co.uk/crm/ticket/6790/

    """

    user_unit = models.ForeignKey(UserUnit, on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    result = models.IntegerField(blank=True, null=True)
    objects = UserUnitAuditManager()

    class Meta:
        ordering = ("-created",)
        verbose_name = "User unit audit"
        verbose_name_plural = "User unit audit"

    def __str__(self):
        # Do I need to return more than this?
        return f"{self.user.username}"


class UserAssessmentAuditManager(models.Manager):
    def create_user_assessment_audit(
        self, user_assessment, user, can_retake, error_count, assessor_comments
    ):
        user_assessment_audit = self.model(
            user_assessment=user_assessment,
            user=user,
            can_retake=can_retake,
            error_count=error_count,
            assessor_comments=assessor_comments,
        )
        user_assessment_audit.save()
        return user_assessment_audit


class UserAssessmentAudit(TimeStampedModel):
    """
    Added audit trail for UserUnit and UserAssessment, to allow
    updating of unit result and assessment comments.

    Requested as an open-ended feature.
    https://www.kbsoftware.co.uk/crm/ticket/6790/

    """

    user_assessment = models.ForeignKey(
        UserAssessment, on_delete=models.CASCADE
    )
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    can_retake = models.BooleanField(default=False)
    error_count = models.IntegerField(
        blank=True, null=True, help_text="Error count (for assessments)"
    )
    assessor_comments = models.TextField(blank=True)
    result = models.IntegerField(blank=True, null=True)
    objects = UserAssessmentAuditManager()

    class Meta:
        ordering = ("-created",)
        verbose_name = "User assessment audit"
        verbose_name_plural = "User assessment audit"

    def __str__(self):
        # Do I need to return more than this?
        return f"{self.user.username}"
