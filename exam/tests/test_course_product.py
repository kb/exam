# -*- encoding: utf-8 -*-
import pytest

from django.db.utils import IntegrityError

from exam.models import CourseProduct, ExamError
from exam.tests.factories import CourseFactory, CourseProductFactory
from login.tests.factories import UserFactory
from stock.tests.factories import ProductFactory


@pytest.mark.django_db
def test_create_course_product():
    course = CourseFactory()
    product = ProductFactory()
    obj = CourseProduct.objects.create_course_product(course, product)
    assert bool(obj.pk) is True
    assert course == obj.course
    assert product == obj.product


@pytest.mark.django_db
def test_create_course_product_duplicate():
    product = ProductFactory()
    CourseProduct.objects.create_course_product(CourseFactory(), product)
    with pytest.raises(IntegrityError):
        CourseProduct.objects.create_course_product(CourseFactory(), product)


@pytest.mark.django_db
def test_create_course_product_duplicate_course():
    """Check that two different products can link to the same course."""
    course = CourseFactory()
    CourseProduct.objects.create_course_product(course, ProductFactory())
    CourseProduct.objects.create_course_product(course, ProductFactory())


@pytest.mark.django_db
def test_current():
    CourseProductFactory(product=ProductFactory(slug="a"))
    product_b = CourseProductFactory(product=ProductFactory(slug="b"))
    product_b.set_deleted(UserFactory())
    CourseProductFactory(product=ProductFactory(slug="c"))
    assert ["a", "c"] == [
        x.product.slug
        for x in CourseProduct.objects.current().order_by("product__slug")
    ]


@pytest.mark.django_db
def test_for_course():
    course = CourseFactory()
    CourseProductFactory(
        course=CourseFactory(), product=ProductFactory(slug="a")
    )
    product_b = CourseProductFactory(
        course=course, product=ProductFactory(slug="b")
    )
    product_b.set_deleted(UserFactory())
    CourseProductFactory(course=course, product=ProductFactory(slug="c"))
    assert ["c"] == [
        x.product.slug
        for x in CourseProduct.objects.for_course(course).order_by(
            "product__slug"
        )
    ]


@pytest.mark.django_db
def test_get_course():
    course = CourseFactory()
    product = ProductFactory()
    CourseProductFactory(course=course, product=product)
    assert course == CourseProduct.objects.get_course(product)


@pytest.mark.django_db
def test_get_course_does_not_exist():
    CourseProductFactory()
    product = ProductFactory(slug="apple")
    result = CourseProduct.objects.get_course(product)
    assert result is None


@pytest.mark.django_db
def test_init_course_product():
    course = CourseFactory()
    product = ProductFactory()
    obj1 = CourseProduct.objects.init_course_product(course, product)
    obj2 = CourseProduct.objects.init_course_product(course, product)
    assert bool(obj1.pk) is True
    assert obj1.pk == obj2.pk


@pytest.mark.django_db
def test_init_course_product_undelete():
    course = CourseFactory()
    product = ProductFactory()
    x = CourseProduct.objects.init_course_product(course, product)
    x.set_deleted(UserFactory())
    assert x.is_deleted is True
    x = CourseProduct.objects.init_course_product(course, product)
    x.refresh_from_db()
    assert x.is_deleted is False


@pytest.mark.django_db
def test_init_course_product_course_changed():
    product = ProductFactory(name="Apple")
    CourseProduct.objects.init_course_product(
        CourseFactory(name="Fruit"), product
    )
    with pytest.raises(ExamError) as e:
        CourseProduct.objects.init_course_product(
            CourseFactory(name="Vegetable"), product
        )
    assert (
        "Cannot link 'Vegetable' to 'Apple' (it is already linked to 'Fruit')"
    ) in str(e.value)
