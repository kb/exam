# -*- encoding: utf-8 -*-
from decimal import Decimal
from django.contrib.staticfiles import finders
from django.utils import timezone
from reportlab import platypus
from reportlab.lib.pagesizes import A4
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.units import mm
from reportlab.pdfgen import canvas


class MyReport:
    """Basic report class.

    .. note:: This is a copy of the ``MyReport`` class in the ``report`` app.
              We can't use the ``report`` app in
              ``institutelegalsecretaries_com`` because it already has a
              ``report`` app which does nearly the same thing.  Need to remove
              ``report`` from ``institutelegalsecretaries_com`` and use the
              ``report`` app instead.

    """

    def __init__(self):
        # Use the sample style sheet.
        style_sheet = getSampleStyleSheet()
        self.body = style_sheet["BodyText"]
        self.head_1 = style_sheet["Heading1"]
        self.head_2 = style_sheet["Heading2"]
        self.GRID_LINE_WIDTH = 0.25

    def _bold(self, text):
        return self._para("<b>{}</b>".format(text))

    def _head(self, text):
        return platypus.Paragraph(text, self.head_2)

    def _image(self, file_name, **kwargs):
        return platypus.Image(file_name, **kwargs)

    def _italic(self, text):
        return self._para("<i>{}</i>".format(text))

    def _para(self, text):
        return platypus.Paragraph(text, self.body)

    def _round(self, value):
        return value.quantize(Decimal(".01"))


class NumberedCanvas(canvas.Canvas):
    """
    Copied from ActiveState, Improved ReportLab recipe for page x of y
    http://code.activestate.com/recipes/576832/
    """

    def __init__(self, *args, **kwargs):
        canvas.Canvas.__init__(self, *args, **kwargs)
        self._saved_page_states = []

    def showPage(self):
        self._saved_page_states.append(dict(self.__dict__))
        self._startPage()

    def save(self):
        """add page info to each page (page x of y)"""
        num_pages = len(self._saved_page_states)
        for state in self._saved_page_states:
            self.__dict__.update(state)
            self.draw_page_number(num_pages)
            canvas.Canvas.showPage(self)
        canvas.Canvas.save(self)

    def draw_page_number(self, page_count):
        self.setFont("Helvetica", 7)
        self.drawRightString(
            200 * mm, 20 * mm, "Page %d of %d" % (self._pageNumber, page_count)
        )


class ReportAssessment(MyReport):
    def report(self, assessment, user, response):
        """PDF report showing assessment.

        Choose other characters from:
        http://graphemica.com/blocks/dingbats

        """
        # Create the document template
        doc = platypus.SimpleDocTemplate(
            response,
            title=f"Assessment - {assessment.name}",
            pagesize=A4,
        )
        # Container for the 'Flowable' objects
        elements = []
        # logo
        file_name = finders.find("img/project/logo.png")
        if file_name:
            logo = self._image(file_name, width=426, height=71)
            logo.hAlign = "LEFT"
            elements.append(logo)

        elements.append(
            self._head(f"{assessment.unit.name}, {assessment.name}")
        )
        elements.append(self._head("What To Do:"))
        elements.append(
            self._para(assessment.what_to_do.replace("\r\n", "<br />"))
        )
        elements.append(self._head("Assessment Criteria:"))
        elements.append(
            self._para(assessment.assessment_criteria.replace("\r\n", "<br />"))
        )
        elements.append(platypus.Spacer(1, 12))
        elements.append(self._head("Instructions"))
        elements.append(
            self._para(assessment.instructions.replace("\r\n", "<br />"))
        )
        doc.build(elements, canvasmaker=NumberedCanvas)


class ReportUserExamVersion(MyReport):
    def report(self, user_exam_version, user, response):
        """PDF report showing exam results for a user.

        Choose other characters from:
        http://graphemica.com/blocks/dingbats

        """
        # Create the document template
        doc = platypus.SimpleDocTemplate(
            response,
            title="Test - {}".format(user_exam_version.exam_version.exam.name),
            pagesize=A4,
        )
        # Container for the 'Flowable' objects
        elements = []
        # logo
        file_name = finders.find("img/project/logo.png")
        if file_name:
            logo = self._image(file_name, width=426, height=71)
            logo.hAlign = "LEFT"
            elements.append(logo)

        elements.append(
            self._head("{}".format(user_exam_version.exam_version.exam.name))
        )
        elements.append(
            self._para(
                "Candidate: {}".format(user_exam_version.user.get_full_name())
            )
        )
        if user_exam_version.date_marked:
            s = user_exam_version.date_marked.strftime("%d/%m/%Y %H:%M")
            elements.append(self._para("Marked on: {}".format(s)))
            elements.append(
                self._para(
                    "Result: {}%".format(user_exam_version.result_as_percent())
                )
            )
        elements.append(platypus.Spacer(1, 12))
        elements.append(
            self._bold(
                "The following information applies as you "
                "progress through the test answering questions "
                "and after you submit it for marking."
            )
        )
        elements.append(
            self._para(
                "1) A cross indicates a wrong answer<br />"
                "2) A tick indicates a correct answer<br />"
                "3) Italics indicate the answer you selected<br />"
                "4) Underlining indicates the correct answer (this is only shown "
                "once you have passed the test)"
            )
        )
        elements.append(platypus.Spacer(1, 12))
        count = 0
        first_page = True
        results_in_full = user_exam_version.results_in_full(user)
        questions = results_in_full["questions"]
        total = len(questions)
        for question in questions:
            count = count + 1
            elements.append(
                self._para(
                    "<b>Question {}: {}</b>".format(
                        question["number"], question["question"]
                    )
                )
            )
            for option in question["options"]:
                # underline if this is the correct answer
                underline = underline_end = ""
                if option["correct_answer"]:
                    underline = "<u>"
                    underline_end = "</u>"
                # tick or cross if right or wrong
                tick_cross = ""
                if option["user_selection"]:
                    if question["right"]:
                        tick_cross = "\u2714"
                    elif question["wrong"]:
                        tick_cross = "\u2718"
                text = "{}{} {}{} {}".format(
                    underline,
                    "-",
                    # option["number"],
                    option["option"],
                    underline_end,
                    tick_cross,
                )
                # append the element
                if option["user_selection"]:
                    elements.append(self._italic(text))
                else:
                    elements.append(self._para(text))
            # was that the last question?
            if question["number"] >= total:
                elements.append(platypus.Spacer(1, 12))
                s = timezone.now().strftime("%d/%m/%Y %H:%M")
                elements.append(
                    self._para(
                        "<b>Printed {} by {}</b>".format(
                            s, user.get_full_name()
                        )
                    )
                )
            elif first_page and count >= 4:
                elements.append(platypus.PageBreak())
                count = 0
                first_page = False
            elif count >= 6:
                elements.append(platypus.PageBreak())
                count = 0
            elements.append(platypus.Spacer(1, 12))
        doc.build(elements, canvasmaker=NumberedCanvas)
