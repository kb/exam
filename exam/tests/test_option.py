# -*- encoding: utf-8 -*-
import pytest
from exam.tests.factories import OptionFactory


@pytest.mark.django_db
def test_factory():
    OptionFactory()


@pytest.mark.django_db
def test_str():
    option = OptionFactory(number=4, option="Option 4")
    assert "4. Option 4" == str(option)


@pytest.mark.django_db
def test_str_long():
    option = OptionFactory(
        number=5, option="Vine let people share 6 second long video clips"
    )
    assert "5. Vine let people share 6 second" == str(option)
