# -*- encoding: utf-8 -*-
import json

from django.core.management.base import BaseCommand

# from rich.pretty import pprint


class Command(BaseCommand):
    help = "Set images in dump file to 'null' #6212"

    def handle(self, *args, **options):
        json_file_names = ["exam-course.json", "exam-unit.json"]
        for file_name in json_file_names:
            with open(file_name, "r") as f:
                data = json.load(f)
            for row in data:
                fields = row["fields"]
                fields["banner"] = None
                fields["banner_mobile"] = None
                fields["picture"] = None
                # pprint(row, expand_all=True)
            # pprint(data, expand_all=True)
            with open(file_name, "w") as f:
                json.dump(data, f, indent=2)
