# -*- encoding: utf-8 -*-
from django.conf import settings
from django.core.management.base import BaseCommand
from rich import print as rprint


from exam.models import UserCourse


class Command(BaseCommand):
    help = "'UserCourse' - check 'is_complete_and_ready_for_final_result' #6620"

    def add_arguments(self, parser):
        parser.add_argument("user_course_pk", type=int)

    def handle(self, *args, **options):
        self.stdout.write(f"{self.help}...")
        user_course_pk = options["user_course_pk"]
        user_course = UserCourse.objects.get(pk=user_course_pk)
        rprint(f"[yellow]Found 'UserCourse' for '{user_course.user.username}'")
        rprint(f"  Created {user_course.created}")
        rprint(
            (
                "[green]  'is_complete_and_ready_for_final_result': "
                f"{user_course.is_complete_and_ready_for_final_result()}"
            )
        )
        self.stdout.write(f"{self.help} - Complete...")
