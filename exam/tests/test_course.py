# -*- encoding: utf-8 -*-
import pytest

from exam.models import Course
from exam.tests.factories import CourseFactory, CourseUnitFactory, UnitFactory
from login.tests.factories import UserFactory


@pytest.mark.django_db
def test_course_units():
    course = CourseFactory()
    unit_1 = UnitFactory(name="u1")
    CourseUnitFactory(course=course, unit=unit_1, order=1)
    unit_2 = UnitFactory(name="u2")
    CourseUnitFactory(course=course, unit=unit_2, order=2)
    unit_2.set_deleted(UserFactory())
    unit_3 = UnitFactory(name="u3")
    CourseUnitFactory(course=CourseFactory(), unit=unit_3, order=3)
    unit_4 = UnitFactory(name="u4")
    CourseUnitFactory(course=course, unit=unit_4, order=4).set_deleted(
        UserFactory()
    )
    unit_5 = UnitFactory(name="u5")
    course_unit_5 = CourseUnitFactory(course=course, unit=unit_5, order=5)
    course_unit_5.set_deleted(UserFactory())
    unit_6 = UnitFactory(name="u6")
    CourseUnitFactory(course=course, unit=unit_6, order=6)
    assert ["u1", "u6"] == [
        x.unit.name for x in course.course_units().order_by("order")
    ]


@pytest.mark.django_db
def test_create_course():
    x = Course.objects.create_course("Farming")
    assert bool(x.pk) is True
    assert 0 == x.order
    assert "Farming" == x.name
    assert x.is_tailored is False


# @pytest.mark.django_db
# @pytest.mark.parametrize(
#     "assignment_or_assessment,assessment_count,expect",
#     [
#         ("assessment", 1, "assessment"),
#         ("assessment", 2, "assessments"),
#         ("Assessment", 1, "assessment"),
#         ("AssessMENT", 1, "assessment"),
#         ("assignment", 1, "assignment"),
#         ("assignment", 2, "assignments"),
#         ("Assignment", 1, "assignment"),
#         ("AssiGNMENT", 1, "assignment"),
#     ],
# )
# def test_get_assignment_or_assessment(
#     assignment_or_assessment, assessment_count, expect
# ):
#     course = CourseFactory(assignment_or_assessment=assignment_or_assessment)
#     assert expect == course.get_assignment_or_assessment(assessment_count)


@pytest.mark.django_db
def test_init_course():
    course_1 = Course.objects.init_course("Farming")
    course_1.refresh_from_db()
    assert "Farming" == course_1.name
    assert course_1.is_tailored is False
    assert 0 == course_1.order
    course_2 = Course.objects.init_course("Farming", is_tailored=True, order=2)
    course_2.refresh_from_db()
    assert course_1.pk == course_2.pk
    assert "Farming" == course_1.name
    assert 2 == course_2.order
    assert course_2.is_tailored is True


@pytest.mark.django_db
def test_prev_unit():
    course = CourseFactory()
    # 1
    unit_1 = UnitFactory(name="unit_1")
    CourseUnitFactory(course=course, unit=unit_1, order=1)
    # 2
    unit_2 = UnitFactory(name="unit_2")
    CourseUnitFactory(course=course, unit=unit_2, order=2)
    # 3. delete the unit
    unit_3 = UnitFactory(name="unit_3")
    unit_3.set_deleted(UserFactory())
    CourseUnitFactory(course=course, unit=unit_3, order=3)
    # 4
    unit_4 = UnitFactory(name="unit_4")
    CourseUnitFactory(course=course, unit=unit_4, order=4)
    # 5. delete the course unit
    unit_5 = UnitFactory(name="unit_5")
    course_unit_5 = CourseUnitFactory(course=course, unit=unit_5, order=5)
    course_unit_5.set_deleted(UserFactory())
    # 6
    unit_6 = UnitFactory(name="unit_6")
    CourseUnitFactory(course=course, unit=unit_6, order=6)
    # test
    assert course.prev_unit(unit_1) is None
    assert unit_1 == course.prev_unit(unit_2)
    assert unit_2 == course.prev_unit(unit_4)


@pytest.mark.django_db
def test_units():
    course = CourseFactory()
    unit_1 = UnitFactory(name="u1")
    CourseUnitFactory(course=course, unit=unit_1, order=1)
    unit_2 = UnitFactory(name="u2")
    CourseUnitFactory(course=course, unit=unit_2, order=2)
    unit_2.set_deleted(UserFactory())
    unit_3 = UnitFactory(name="u3")
    CourseUnitFactory(course=CourseFactory(), unit=unit_3, order=3)
    unit_4 = UnitFactory(name="u4")
    CourseUnitFactory(course=course, unit=unit_4, order=4).set_deleted(
        UserFactory()
    )
    unit_5 = UnitFactory(name="u5")
    CourseUnitFactory(course=course, unit=unit_5, order=5)
    assert ["u1", "u5"] == [x.name for x in course.units().order_by("name")]
