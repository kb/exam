# -*- encoding: utf-8 -*-
from braces.views import LoginRequiredMixin, StaffuserRequiredMixin
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.urls import reverse
from django.views.generic import (
    DetailView,
    RedirectView,
    TemplateView,
    UpdateView,
)

from base.view_utils import BaseMixin
from exam.models import check_perm_user_exam_version, ProductExam
from exam.views import (
    UserExamQuestionMixin,
    UserExamVersionResultMixin,
    UserExamVersionUpdateMixin,
)
from stock.models import Product
from .models import Contact


def _check_perm_user_exam_version(user, user_exam_version):
    """Does the user have permission to access the exam?

    .. note:: This is a test function (it reads all products)!

    """
    result = check_perm_user_exam_version(user, user_exam_version)
    if not result:
        # Check using the old (pre-LMS) product permissions
        result = ProductExam.objects.check_perm(
            user_exam_version.exam_version.exam,
            user,
            [product for product in Product.objects.all()],
        )
    return result


class ContactDetailView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, DetailView
):
    model = Contact


class LmsDashView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    BaseMixin,
    TemplateView,
):
    """LMS is designed to *sit above* the 'exam' and 'qanda' apps."""

    template_name = "example/lms-dash.html"


class HomeView(TemplateView):
    template_name = "example/home.html"


class SettingsView(TemplateView):
    template_name = "example/settings.html"


class UserExamVersionUpdateView(
    LoginRequiredMixin, UserExamVersionUpdateMixin, BaseMixin, UpdateView
):
    template_name = "example/student_exam_update.html"

    def products_for_user(self):
        user_exam_version = self.object
        qs = ProductExam.objects.filter(
            exam=user_exam_version.exam_version.exam
        )
        return [x.product for x in qs]

    def check_perm_user_exam_version(self):
        return _check_perm_user_exam_version(self.request.user, self.object)


class UserExamQuestionView(
    LoginRequiredMixin, UserExamQuestionMixin, BaseMixin, UpdateView
):
    template_name = "example/student_question_form.html"

    def check_perm_user_exam_version(self, user_exam_version):
        return _check_perm_user_exam_version(
            self.request.user, user_exam_version
        )


class UserExamVersionResultView(
    LoginRequiredMixin, UserExamVersionResultMixin, BaseMixin, DetailView
):
    template_name = "example/student_exam_result.html"

    def check_perm_user_exam_version(self):
        return _check_perm_user_exam_version(self.request.user, self.object)


class UserContactRedirectView(RedirectView):
    permanent = False

    def get_redirect_url(self, *args, **kwargs):
        pk = kwargs["pk"]
        user = get_user_model().objects.get(pk=pk)
        messages.info(
            self.request,
            (
                "A live project would redirect to the contact page "
                "for the '{}' user.".format(user.username)
            ),
        )
        return reverse("project.settings")
