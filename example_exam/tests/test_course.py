# -*- encoding: utf-8 -*-
import pytest

from exam.tests.factories import CourseFactory, UserCourseFactory
from exam.models import Course
from login.tests.factories import UserFactory
from .factories import ContactFactory


@pytest.mark.django_db
def test_current():
    CourseFactory(name="a")
    course = CourseFactory(name="b")
    course.set_deleted(UserFactory())
    CourseFactory(name="c")
    assert ["a", "c"] == [x.name for x in Course.objects.current()]


@pytest.mark.django_db
def test_current_exclude_tailored():
    CourseFactory(name="a")
    course = CourseFactory(name="b", is_tailored=True)
    UserCourseFactory(user=UserFactory(), course=course)
    CourseFactory(name="c")
    assert ["a", "c"] == [x.name for x in Course.objects.current()]


@pytest.mark.django_db
def test_current_for_contact():
    CourseFactory(name="a")
    course = CourseFactory(name="b", is_tailored=True)
    user = UserFactory()
    UserCourseFactory(user=user, course=course)
    CourseFactory(name="c")
    course = CourseFactory(name="d", is_tailored=True)
    UserCourseFactory(user=user, course=course)
    course.set_deleted(UserFactory())
    assert ["b"] == [x.name for x in Course.objects.current(user)]
