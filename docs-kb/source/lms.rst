LMS
***

.. highlight:: python

- WIP_
- `Ticket 3119`_

Test System
===========

From `Ticket 3119`_

Restore the data::

  # migrate
  django-admin.py migrate --no-input
  # create the cms pages
  django-admin.py init_app_lms
  # create the "Course" for each product
  django-admin.py 3119-create-courses-for-products
  # run the server
  django-admin.py runserver 0.0.0.0:8000

Application

- Make sure you are *not* logged in...
- Browse to http://localhost:8000/training/application/
- Fill in the application form (I am setting ``username`` == ``kimber``)
- Pay for the course

Log into the site as a member of staff

- *Approve* the application

Log into the site as the *Application* user

- Browse to http://localhost:8000/membership/my/
- Find the *MY COURSE MATERIAL* section
- Click on the course material which does **not** have the pretty picture.

WIP
===

We have a
``ProductExam`` (``product`` / ``exam``)
and a
``ProductCourse`` (``product`` / ``course``)

Perhaps we should have::

  Course
    ProductCourse
      Exam (exam links to ProductCourse)

- Move the views over to ``exam`` one at a time when I understand what they do.
  Update them to our standards as well...

To Do
-----

- I think the ``can_user_start`` method for the ``UserUnit`` class should
  check to see if the ``UserUnit`` has been submitted (or not).

  - ``is_complete`` for a ``UserUnit`` checks to make sure the number of
    assessments for the ``Unit`` is equal to the number of ``assessments``
    initialised for the ``UserUnit``.
  - This will not be enough because it needs to check the exam as well.
  - It also needs to make sure the exam has been taken and the assessment
    uploaded.
  - Also a ``UserUnit`` should not be unlocked until it has been submitted.
  - The first project will always have an ``Exam`` or an ``Assessment`` for each
    unit, but we can still require the student to submit the unit.

- The first couple of units have no assessments, so what are they for?
- A ``UserUnit`` is initialised in two places:

  1. When an assessment is created (in ``create_user_assessment``).
  2. In ``submit`` in ``UserUnitManager`` - but I can't see when this method is
     called?

- The ``StudentUnitDetailView`` will list all the ``assessments`` (if there are
  any, but does it list exams?  No, I don't think it does.  I can't see anything
  being done with ``UnitExam``.

- Rename ``UserAssessment``, ``userunit`` to ``user_unit``
- What should ``create_member_courses`` do for products which don't have a
  course (or should this never happen)?
- Add the *Mark* link to the list of courses for the contact
  e.g. ``/member/contact/13592/``
- I like the filtering, but I don't think users will know what to do.

  Replace the filters with a search form (with drop-downs) on
  ``/exam/user/course/``.

  02/12/2018, I am not sure we need to do this.  We no longer have the
  ``product`` filter and the user filter can be replaced with a link to the list
  of courses for the student.

- Why is ``UserUnit``, ``result`` a ``CharField``?
- When setting the ``result`` on a ``UserUnit``
  e.g. ``/lms/marking/userunit/32/``
  If you clear the ``assessor_comments``, then the ``assessor`` is set to
  ``None``.  Why?  When should the ``assessor`` be set?
  ``UserCourseMarkUserUnitView`` doesn't seem to do anything with the
  ``assessor``?
- There should be access permissions on marking
  e.g. ``/exam/user/unit/29/update/``.
  If the course has been marked, then the assessor shouldn't be able to change
  the mark.

Done
----

- Rename the ``userunit`` field in ``UserAssessment`` to ``user_unit``.
- Move ``Course`` to the ``exam`` app.
- Move ``order`` from ``ProductExam`` to ``Course``.
- Move ``ProductCourse`` to the ``exam`` app.
- Should the ``UserCourse`` model be called ``UserCourseProduct``
- ref ``UserCourseProduct``, can we remove ``course_product`` and replace with
  ``course``?
- There is a disconnect between users and courses:

  1. A ``User`` takes a ``Unit``
  2. The ``Unit`` is linked to a ``Course``
  3. The ``User`` takes a ``UserCourseProduct``.
  4. To find the ``UserCourseProduct`` for a ``User`` from the ``Unit``, we have
     to lookup the ``UserCourseProduct`` (see ``get_user_course_product`` in
     ``UserUnitUpdateView``.

  I think I need to make it so that a ``User`` can only take a ``Course`` once
  i.e. add a unique index to ``UserCourseProduct`` on ``User`` + ``Course``.

  A member will purchase products using ``EnrolProduct``.
  Exams are linked to products using ``UserExamVersionProduct``.

  For the LMS, I think a user should enrol on a course, so we should change
  ``UserCourseProduct`` to ``UserCourse``
  (change the ``course_product`` field to ``course``).

  Now I am worried... does an assessor need to know if they are doing distance
  learning or in the class (both will be the same course)?

28/11/2018
==========

Trying to move all LMS models to exam

The following are linked to ILSPA specific stuff, so don't move them:

1. ``model_maker.py``
2. ``lms_cases.py``
3. ``scenario.py``
4. ``test_lms_models``


.. _`Ticket 3119`: https://www.kbsoftware.co.uk/crm/ticket/3119/
.. _WIP: https://docs.google.com/document/d/1yV-2Ew9h41pzfWFETmjb4jZ-y-8V44JiXP4aUukDI6g/edit?usp=sharing
